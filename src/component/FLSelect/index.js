import React, { useEffect, useState } from "react";
import Popover from "react-popover";
import FLInput from "../FlInput/index";
const ListedInput = (props) => {
    const [openList, setOpenList] = useState(false);
    const [value, setValue] = useState({label:null, value: props.value});
    const [listData, setListData] = useState(props.data);

    const [searchVL, setSearchVL] = useState("");

    const [readonly, setReadonly] = useState(false);
    // useEffect(() =>{
    //     console.log('props.value', props.value);
    //     if(props.value === null){
    //         setValue({label: null, value: null});
    //     }
    // },[props.value]);
    useEffect(() => {
        setListData(props.data);

        if(props.data && props.value === null){
            setValue({label:null, value: null})
        }
        else{
            let result = props.data ?  props.data.filter(obj => {
                return obj.value === props.value
            }) : {};
            if(result[0]){
                setValue(result[0])
            }
        }
    }, [props.data, props.value]);

    const removeAc = (str) => {
        var AccentsMap = [
            "aàảãáạăằẳẵắặâầẩẫấậ",
            "AÀẢÃÁẠĂẰẲẴẮẶÂẦẨẪẤẬ",
            "dđ", "DĐ",
            "eèẻẽéẹêềểễếệ",
            "EÈẺẼÉẸÊỀỂỄẾỆ",
            "iìỉĩíị",
            "IÌỈĨÍỊ",
            "oòỏõóọôồổỗốộơờởỡớợ",
            "OÒỎÕÓỌÔỒỔỖỐỘƠỜỞỠỚỢ",
            "uùủũúụưừửữứự",
            "UÙỦŨÚỤƯỪỬỮỨỰ",
            "yỳỷỹýỵ",
            "YỲỶỸÝỴ"
        ];
        for (var i=0; i<AccentsMap.length; i++) {
            var re = new RegExp('[' + AccentsMap[i].substr(1) + ']', 'g');
            var char = AccentsMap[i][0];
            str = str.replace(re, char);
        }
        return str;
    }

    const filterFunction = (name, value) =>{
        const l1 = removeAc(name.toString()).toLowerCase()
        const l2 = removeAc(value).toLowerCase()
        return (l1.includes(l2))
    }

    const onsearchChange = (e)=>{
        setSearchVL(e.target.value);
        var filtered = props.data.filter(obx => {
            const vl = obx.label
            return filterFunction(vl, e.target.value)
        });
        setListData(filtered);
    }

    const blurInput = () => {
        setReadonly(false);
    };
    const focusInput = () => {
        setOpenList(true);
        setReadonly(true);
    };

    const popoverProps = {
        isOpen: openList,
        place: props.positionSelect ? props.positionSelect :  "below",
        preferPlace: props.positionSelect ? props.positionSelect :  "below",
        onOuterAction: () => setOpenList(false),
        className: 'flselect-cus',
        body: [<div>
            <div className="popsheader">
                {props.label}
                {props.search && (
                    <div className="search-select-sdk">
                        <input type="text" placeholder="Nhập để tìm kiếm" value={searchVL} onChange={onsearchChange} />
                        <i className="fas fa-search"></i>
                    </div>
                )}
            </div>
            <div className="list-item" style={{overflowY: 'auto', maxHeight: 300}}>
                {listData ?  listData.map((item, index)=>{
                    return <div key={index} className={(item.value == value.value)?"sx-item active":"sx-item"} onClick={(e)=>{
                        if(props.changeEvent(item.value)){
                        }else{
                            setValue(item)
                            setOpenList(false)
                        }
                    }}>{item.label}</div>
                }) : []}
            </div>
        </div>],
    };

    return (
        <Popover {...popoverProps}>
            <FLInput
                {...props}
                // onKeyDown={}
                disable={props.disable}
                readonly={readonly}
                loading={props.loading}
                label={props.label}
                hideborder={props.hideborder}
                onFocus={(e) => setOpenList(true)}
                value={value.label}
                required={props.required}
                dropdown={true}
            />
        </Popover>
    );
};
export default ListedInput;
