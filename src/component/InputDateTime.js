import React, {useState} from "react";
import DatePicker, {registerLocale} from "react-datepicker";
// import "react-datepicker/dist/react-datepicker.css";
import vi from "date-fns/locale/vi";
import {Form} from "react-bootstrap";
registerLocale("vi", vi);

const InputDateTime = (props) => {
    return (
        <React.Fragment>
            <div id="cus-input-label">
                <Form.Group>
                    {props.label && (
                        <Form.Label style={{width: '100%'}}>{props.label}
                            {props.required ? <span style={{color: "#DA2128"}}>*</span> : ""}
                        </Form.Label>
                    )}
                    <DatePicker
                        {...props}
                        className="cus-input-date form-control"
                        selected={props.selected}
                        onChange={props.onChange}
                        popperPlacement="bottom"
                        dateFormat="dd/MM/yyyy"
                        locale="vi"
                        popperModifiers={{
                            flip: {
                                behavior: ['bottom']
                            },
                            preventOverflow: {
                                enabled: false
                            },
                            hide: {
                                enabled: false
                            }
                        }}
                    />
                    <div className="icon"><i className="fa fa-calendar-alt" aria-hidden="true"></i></div>
                    {props.error ? <label style={{color: '#DA2128', marginBottom: 0}}>{props.error}</label> : null}
                </Form.Group>
            </div>
        </React.Fragment>
    );
};

export default InputDateTime;
