import React, {
  useState,
  useEffect,
  useCallback,
  forwardRef,
  createRef,
  useImperativeHandle,
} from "react";
import Dropzone from "react-dropzone";
import { Button, Form } from "react-bootstrap";
import { withPreviews, clearPreviews } from "./preview";
import SlideImage from "./slide-image";
import moment from "moment";
import {showDialogDis} from "../../service/Utils";

const Main = forwardRef((props, ref) => {
  const dropzoneRef = createRef();
  const [files, setFiles] = useState(props?.dataFile ? props?.dataFile : []);


  useEffect(() =>{
    if(props?.dataFile.length > 0){
      setFiles(props?.dataFile);
    }
  },[props?.dataFile])

  useEffect(() => {
    props?.setFileUpload(props?.idx, files);
  }, [files]);

  useImperativeHandle(ref, () => ({
    openDropZone() {
      dropzoneRef.current.open();
    },
  }));
  const withPreviews = (dropHandler) => (accepted, rejected) => {
    const acceptedWithPreview = accepted.map((file, index) => {
      const fileType = file?.name?.split(".").pop();
      const newFileName = props?.idx
        ? props?.idx + "_" + moment().valueOf() + "." + fileType
        : file?.name;
      return {
        ...file,
        preview: URL.createObjectURL(file),
        type: file.type ? file.type.slice(-3) : "jpg",
        name: newFileName,
        rawFile: file,
      };
    });
    if(rejected && rejected.length > 0 && props?.rejectedMessage) {
      showDialogDis(props.rejectedMessage);
    }
    dropHandler(acceptedWithPreview, rejected);
  };

  const removeFiles = (arr, vl) => {
    let rs = arr.filter((item) => item.preview !== vl);
    setFiles(rs);
  };

  // const clearPreviews = (files) => files.forEach((file) => URL.revokeObjectURL(file.preview)); // Xoa all file

  const handleDrop = useCallback((accepted) =>
    setFiles((files) => [...files, ...accepted])
  );



  return (
    <React.Fragment>
      <Dropzone
        onDrop={withPreviews(handleDrop)}
        ref={dropzoneRef}
        accept={props.typeUpload || ""}
      >
        {({ getRootProps, getInputProps }) => (
          <section>
            <div {...getRootProps()}>
              <input {...getInputProps()} />
            </div>
          </section>
        )}
      </Dropzone>
      {/*<Dropzone onDrop={withPreviews(handleDrop)}>Chon Anh</Dropzone>*/}
      {files.length > 0 ? (
        <SlideImage listImg={files} removeFiles={removeFiles} isReadOnly={props?.isReadOnly}/>
      ) : (
        <Dropzone
          onDrop={withPreviews(handleDrop)}
          accept={props.typeUpload || ""}
        >
          {({ getRootProps, getInputProps }) => (
            <section>
              <div {...getRootProps()}>
                <input {...getInputProps()} />
                <div className="btn-item-download">
                  <i className="fas fa-cloud-upload-alt" />
                  <p>{props.labelDropZone || "Ấn vào đây để chọn file"}</p>
                </div>
              </div>
            </section>
          )}
        </Dropzone>
      )}
    </React.Fragment>
  );
});
export default Main;
