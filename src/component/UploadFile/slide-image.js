import React, { useEffect, useState } from "react";
import Carousel from "react-multi-carousel";
import { Tooltip, OverlayTrigger } from "react-bootstrap";
// import {openBase64Pdf} from '../service/Utils'
// import "react-multi-carousel/lib/style.module.css";

const CarouselImage = (props) => {
  const [listData, setListdata] = useState(props.listImg);
  const [isReadOnly, setIsReadOnly] = useState(props.isReadOnly || false);

  const [isPreview, setIsPreview] = useState(false);
  const [imgActive, setImgActive] = useState(null);

  useEffect(() => {
    setListdata(props.listImg);
    if (props?.listImg && props?.listImg.length > 0) {
      if (imgActive > 0) {
        setImgActive(imgActive - 1);
      }
    } else {
      setIsPreview(false);
    }
  }, [props.listImg]);

  const responsive = {
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3,
      slidesToSlide: 3,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2,
      slidesToSlide: 2,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
      slidesToSlide: 1,
    },
  };

  const showImage = (e, type_file, file, i) => {
    e.preventDefault();
    if (type_file === "pdf") {
      window.open(file.preview, "_blank");
    } else if (!isReadOnly) {
      setImgActive(i);
      setIsPreview(!isPreview);
    }
  };

  const showImgActive = (type_file, preview, i) => {
    // if (type_file === "pdf") {
    //   window.open(preview, "_blank");
    // } else {
    //   setImgActive(i);
    // }
    setImgActive(i);
  };

  // const removeItemActive = (arr, vl) => {
  //     let rs = props.deleteItem(arr, vl);
  //     setImgActive(0);
  //     setListImg([...rs])
  //     switch (props.checkListImg) {
  //         case '0':
  //             props.setListImgGYC(rs);
  //             break;
  //         case '1':
  //             props.setListImgCMND(rs);
  //             break;
  //         case '2':
  //             props.setListImgOther(rs)
  //             break;
  //         default:
  //             break;
  //     }
  // }

  const showImgChoose = (file) => {
    let fileImg = getThumailFile(file.name || file.FILE_NAME) || file.preview;
    return (
      <div className="wrap_tiem_choice" style={{ textAlign: 'center' }}>
        <img className="img-fluid" src={fileImg} alt="" />{" "}
        {file.type === "pdf" && (
          <label
            className="hover-text-pdf-cs"
            onClick={() => openFilePdf(file.preview)}
          >
            Xem File PDF
          </label>
        )}
        <label style={{ color: 'white' }}>{file?.path}</label>
      </div>
    );
  };

  const openFilePdf = (file) => {
    window.open(file, "_blank");
  };

  const removeItemFile = (listData, itemFile) => {
    props?.removeFiles(listData, itemFile);
  };
  const renderTooltip = (props) => (
    <Tooltip id="button-tooltip" {...props}>
      {props}
    </Tooltip>
  );

  const getThumailFile = (fileName) => {
    if (!fileName) return null;
    
    if (fileName?.includes('.pdf')) {
      return `${process.env.REACT_APP_DM3}/img/pdf.png`;
    }
    if (fileName?.includes('.doc')) {
      return `${process.env.REACT_APP_DM3}/img/doc.png`;
    }
    if (fileName?.includes('.xls') || fileName.includes('.csv')) {
      return `${process.env.REACT_APP_DM3}/img/xls.png`;
    }
    return null;
  }
  return (
    <React.Fragment>
      <Carousel
        prefixCls="cus-tom-carousel"
        showDots={true}
        responsive={responsive}
        deviceType={"desktop"}
        itemClass="image-item"
      >
        {listData &&
          listData.map((file, i) => {
            const itemFile = getThumailFile(file.name || file.FILE_NAME) || file.preview;
            return (
              <OverlayTrigger
                placement="bottom"
                delay={{ show: 250, hide: 400 }}
                overlay={renderTooltip(file.path || file.FILE_NAME)}
              >
                <div key={i} className="item-image-file">
                  <div onClick={(e) => showImage(e, file.type, file, i)}>
                    <img
                      key={file.name}
                      src={itemFile}
                      style={{ width: getThumailFile(file.name || file.FILE_NAME) ? "50%" : "100%", height: "100%", borderRadius: 6, display: 'block', margin: '15% auto auto auto' }}
                      alt="img"
                    />
                  </div>
                  <label style={{ width: "100%", whiteSpace: "nowrap", overflow: 'hidden', textOverflow: 'ellipsis' }}>{file.path ||file.FILE_NAME}</label>
                  {file.type === "pdf" && (
                    <label
                      className="hover-text-pdf"
                      onClick={() => openFilePdf(file.preview)}
                    >
                      Xem File PDF
                    </label>
                  )}
                  {props.type != "preview" && !isReadOnly && (
                    <div
                      className="remove-item-img"
                      onClick={() => removeItemFile(listData, file.preview)}
                    >
                      <i className="fas fa-times" />
                    </div>
                  )}
                </div>
              </OverlayTrigger>
            );
          })}
      </Carousel>
      {isPreview && (
        <div className="preview-image">
          <div className="item-choose">
            {showImgChoose(listData ? listData[imgActive] : null)}
            {/*<img className="img-fluid" src={listImg ? listImg[imgActive] : null} alt="img upload" />*/}
            {props.type != "preview" && (
              <div
                className="btn-remove-img-preview"
                onClick={() =>
                  removeItemFile(listData, listData[imgActive].preview)
                }
              >
                <i className="far fa-trash-alt" />
                <label>Xóa file này</label>
              </div>
            )}
          </div>
          <div className="list-preview-img">
            {listData &&
              listData.map((file, i) => {
                let typeFile = getThumailFile(file.name || file.FILE_NAME) || file.preview;
                return (
                  <div
                    key={i}
                    className={
                      imgActive === i ? "item-preview-active" : "item-preview"
                    }
                  >
                    <div
                      onClick={() => showImgActive(file?.type, file.preview, i)}
                    >
                      <img
                        className="img-fluid"
                        src={typeFile}
                        alt={typeFile}
                      />
                    </div>

                    {/* {imgActive !== i ? (
                      <>
                      
                        <div
                          className="remove-item-img"
                          onClick={(e) => handleRemoveItem(e, listImg, vl)}
                        >
                          <i className="fas fa-times" />
                        </div>
                      </>
                    ) : null} */}
                  </div>
                );
              })}
          </div>
          <div className="btn-close-preview-img" onClick={showImage}>
            <i className="fas fa-times"></i>
            <label>Thoát</label>
          </div>
        </div>
      )}
    </React.Fragment>
  );
};
export default CarouselImage;
