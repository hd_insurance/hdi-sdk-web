import React, {useEffect, useState} from "react";
import Popover from "react-popover";
import PopSelect from "./pop-address.js";
// import FLInput from "./index.jsx";
import InputBase from "./InputBase";

const AddressInput = (props) => {
    const [openAddress, setOpenAddress] = useState(false);
    const [addressValue, setAddressValue] = useState(props.value ? props.value.label : '');


    useEffect(() => {
        setAddressValue(props.value ? props.value.label : '')
    }, [props.value]);

    const locationFormSelect = (vl) => {
        setAddressValue(vl.label);
        props.changeEvent(vl)
        setTimeout(() => {
            setOpenAddress(false);
        }, 200);
    };

    const popoverPropsAddress = {
        isOpen: openAddress,
        place: "column",
        preferPlace: "column",
        className: 'custom-popover-address',
        onOuterAction: () => setOpenAddress(false),
        body: [<PopSelect addressSelect={locationFormSelect} />],
    };

    return (
        <Popover {...popoverPropsAddress}>
            <InputBase
                {...props}
                loading={props.loading}
                label={props.label}
                onFocus={(e) => setOpenAddress(true)}
                readonly={true}
                value={addressValue}
                required={true}
                icon="far fa-map-marker-alt"
            />
        </Popover>
    );
};
export default AddressInput;
