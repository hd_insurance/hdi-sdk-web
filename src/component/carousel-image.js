import React, {useEffect, useState} from "react";
import Carousel from "react-multi-carousel";
import imgPdf from '../img_pdf.png';
import {openBase64Pdf} from '../service/Utils'
// import "react-multi-carousel/lib/style.module.css";

const CarouselImage = (props) => {
    const [listImg, setListImg] = useState(props.dataValue);
    const [showPreview, setShowPreview] = useState(false);
    const [imgActive, setImgActive] = useState(null);

    useEffect(() => {
        setListImg(props.dataValue);
    }, [props.dataValue]);

    const responsive = {
        desktop: {
            breakpoint: {max: 3000, min: 1024},
            items: 3,
            slidesToSlide: 3
        },
        tablet: {
            breakpoint: {max: 1024, min: 464},
            items: 2,
            slidesToSlide: 2
        },
        mobile: {
            breakpoint: {max: 464, min: 0},
            items: 1,
            slidesToSlide: 1
        }
    };
    const showImage = (e, type_file, vl, i) => {
        e.preventDefault();
        if (type_file === 'pdf') {
            let rs = vl ? vl.split(',')[1] : null;
            openBase64Pdf(rs);
        } else {
            setImgActive(i);
            setShowPreview(!showPreview);
        }
    }

    const showImgActive = (type_file, vl, i) => {
        if (type_file === 'pdf') {
            let rs = vl ? vl.split(',')[1] : null;
            openBase64Pdf(rs);
        } else {
            setImgActive(i);
        }
    }
    const removeItemActive = (arr, vl) => {
        let rs = props.deleteItem(arr, vl);
        setImgActive(0);
        setListImg([...rs])
        switch (props.checkListImg) {
            case '0':
                props.setListImgGYC(rs);
                break;
            case '1':
                props.setListImgCMND(rs);
                break;
            case '2':
                props.setListImgOther(rs)
                break;
            default:
                break;
        }
    }

    const handleRemoveItem = (e, arr, vl) => {
        e.preventDefault();
        removeItemActive(arr, vl);
    }

    const showImgChoose = (img) => {
        const type_img = img ? img.split(';')[0].split('/')[1] : '';
        return (
            <div>
                {type_img === 'pdf' ? (
                    <img className="img-fluid" src={imgPdf} alt="pdf"/>
                ) : (
                    <img className="img-fluid" src={img} alt="img upload"/>
                )}
            </div>
        )
    }

    return (
        <React.Fragment>
            <Carousel
                prefixCls="cus-tom-carousel"
                showDots={true}
                responsive={responsive}
                deviceType={"desktop"}
                itemClass="image-item"
            >
                {listImg && listImg.map((vl, i) => {
                    const type_file = vl ? vl.split(';')[0].split('/')[1] : '';
                    return (
                        <div key={i} className="item-image-file">
                            <div onClick={(e) => showImage(e, type_file, vl, i)}>
                                {type_file === 'pdf' ? (
                                    <img style={{width: "100%", height: "100%", borderRadius: 6}} src={imgPdf}
                                         alt="pdf"/>
                                ) : (
                                    <img style={{width: "100%", height: "100%", borderRadius: 6}} src={vl} alt="img"/>
                                )}
                            </div>

                            <div className="remove-item-img"
                                 onClick={(e) => handleRemoveItem(e, listImg, vl)}>
                                <i className="fas fa-times"></i>
                            </div>
                        </div>
                    );
                })}
            </Carousel>
            {showPreview && (
                <div className="preview-image">
                    <div className="item-choose">
                        {
                            showImgChoose(listImg ? listImg[imgActive] : null)
                        }
                        {/*<img className="img-fluid" src={listImg ? listImg[imgActive] : null} alt="img upload" />*/}
                        <div className="btn-remove-img-preview"
                             onClick={() => removeItemActive(listImg, listImg[imgActive])}>
                            <i className="far fa-trash-alt"></i>
                            <label>Xóa ảnh này</label>
                        </div>
                    </div>
                    <div className="list-preview-img">
                        {listImg && listImg.map((vl, i) => {
                            const type_file = vl ? vl.split(';')[0].split('/')[1] : '';
                            return (
                                <div key={i} className={imgActive === i ? "item-preview-active" : "item-preview"}>

                                    <div onClick={() => showImgActive(type_file, vl, i)}>
                                        {type_file === 'pdf' ? (
                                            <img className="img-fluid" src={imgPdf} alt="pdf"/>
                                        ) : (
                                            <img className="img-fluid" src={vl} alt="img upload"/>
                                        )}
                                    </div>

                                    {imgActive !== i ? (
                                        <>
                                            {/*<div className="backdrop-item-img"></div>*/}
                                            <div className="remove-item-img"
                                                 onClick={(e) => handleRemoveItem(e, listImg, vl)}>
                                                <i className="fas fa-times"></i>
                                            </div>
                                        </>
                                    ) : null}

                                </div>
                            );
                        })}
                    </div>
                    <div className="btn-close-preview-img" onClick={showImage}>
                        <i className="fas fa-times"></i>
                        <label>Thoát</label>
                    </div>
                </div>)}
        </React.Fragment>
    )
}
export default CarouselImage;
