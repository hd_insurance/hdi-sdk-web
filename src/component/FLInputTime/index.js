import React, { useEffect, useState } from "react";
// import FLInput from "../FlInput/index";
import DatePicker, {registerLocale} from "react-datepicker";
import "../FlInput/style.css";
import {formatTimeByDay} from "../../service/Utils";
// import "react-datepicker/dist/react-datepicker.css";
import vi from "date-fns/locale/vi";
import {Form} from "react-bootstrap";
import moment from "moment";
import FlInput from "../FlInput";
registerLocale("vi", vi);


const Main = (props) => {
    const [startDate, setStartDate] = useState("");
    // const [eff, setEff] = useState(props.eff);
    const [isActive, setIsActive] = useState(props.value!=null);

    useEffect(() =>{ // phaafn nay check lai sau
       let rs = formatTimeByDay(props.value, props.eff);
       if(rs){
           setStartDate(rs);
       }
    },[props.value, props.eff])

    const onChangeTimeInput = (date) =>{
        setStartDate(date);
        props.changeEvent(moment(date).format("HH:mm"));
    }

    // const handleDateChangeRaw = (e) => {
    //     e.preventDefault();
    // }

    const CustomInput = ({onChange, value, onClick,label }) => {
        if (value !== '') {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
        return (
            <div id="float-label" className={props.disable ? "ipt-disable" : ""}>
                <Form.Control
                    type="text"
                    value={value ? moment(value).format("HH:mm") : ''}
                    // onFocus={onClick}
                    onChange={onChange}
                    disabled={props.disable}
                    // disabled={true}
                />

                <div className="icon" onClick={props.disable ? null : onClick} style={{cursor: 'pointer'}}><i className="fas fa-clock" /></div>
                {props.dropdown ? <div className="dropdown">
                    <i className={`fa fa-caret-down`} aria-hidden="true"></i>
                </div> : null}
                <label
                    className={(isActive || props.value != null) ? "Active" : ""}
                    htmlFor="inp">
                    {label || ""} {props.required ? <span style={{color: "#DA2128"}}>*</span> : ""}
                </label>
            </div>
        )
    };
    return (
        <DatePicker
            value={startDate}
            // selected={startDate}
            onChange={onChangeTimeInput}
            // onChangeRaw={handleDateChangeRaw}
            customInput={<CustomInput {...props} />}
            // popperPlacement="bottom"
            showTimeSelect
            showTimeSelectOnly
            timeIntervals={15}
            timeCaption={props.label}
            dateFormat="HH:mm"
        />
    );
}
export default Main;
