// import React, { useEffect, useState } from "react";
// import FLInput from "../FlInput/index";
// import DatePicker, {registerLocale} from "react-datepicker";
// import "../FlInput/style.css";
//
// // import "react-datepicker/dist/react-datepicker.css";
// import vi from "date-fns/locale/vi";
// import {Form} from "react-bootstrap";
// registerLocale("vi", vi);
//
//
// const Main = (props) => {
//     const [startDate, setStartDate] = useState(null);
//     const [isActive, setIsActive] = useState(props.value!=null);
//
//     const CustomInput = ({onChange, value, onClick,label }) => {
//         if (value !== '') {
//             setIsActive(true);
//         } else {
//             setIsActive(false);
//         }
//         return (
//         <div id="float-label" className={props.disable ? "ipt-disable" : ""}>
//             <Form.Control
//                 type="text"
//                 value={value ? value : null}
//                 onFocus={onClick}
//                 onChange={onChange}
//             />
//
//             <div className="icon"><i className="fa fa-calendar-alt" aria-hidden="true" /></div>
//             <label
//                 className={(isActive || props.value != null) ? "Active" : ""}
//                 htmlFor="inp">
//                 {label || ""} {props.required ? <span style={{color: "#DA2128"}}>*</span> : ""}
//             </label>
//         </div>
//         )
//     };
//     return (
//         <DatePicker
//             value={startDate}
//             selected={startDate}
//             onChange={date => setStartDate(date)}
//             customInput={<CustomInput label={props.label} />}
//             // popperPlacement="bottom"
//             dateFormat="dd/MM/yyyy"
//             locale="vi"
//         />
//     );
// }
// export default Main;

import React, {useEffect, useState} from "react";
import Popover from "react-popover";
import PopSelectDate from "./datetimepicker";
import {checkFormatDate} from "../service/Utils";
import InputBase from "./InputBase";
const Main = (props) => {
    const [openDateTime, setOpenDateTime] = useState(false);
    const [dateValue, setDateValue] = useState(props.value);


    const dateSelect = (value) => {
        if(!checkFormatDate(dateValue)) return;

        if(value) {
            setDateValue(`${value.day<10?`0${value.day}`:value.day}/${value.month<10?`0${value.month}`:value.month}/${value.year}`);
        }
        setTimeout(() => {
            props.changeEvent(`${value.day<10?`0${value.day}`:value.day}/${value.month<10?`0${value.month}`:value.month}/${value.year}`)
            setOpenDateTime(false);
        }, 200);
    };

    const popoverPropsDate = {
        isOpen: openDateTime,
        place: "below",
        className: 'custom-popover-date',
        preferPlace: "right",
        onOuterAction: () => setOpenDateTime(false),
        body: [<PopSelectDate
            selectedDay={props.selectedDay}
            minimumDate={props.minimumDate}
            maximumDate={props.maximumDate}
            dateSelect={dateSelect}
        />],
    };

    const onKeyPress = (e) =>{
        let input = e.target;
        if(e.charCode < 47 || e.charCode > 57) {
            e.preventDefault();
        }
        var len = input.value.length;

        if(len !== 1 || len !== 3) {
            if(e.charCode == 47) {
                e.preventDefault();
            }
        }
        if(len === 2) {
            input.value += '/';
        }
        if(len === 5) {
            input.value += '/';
        }
    }

    const openDate = () =>{
        if(checkFormatDate(props.value)){
            setOpenDateTime(true);
        }else{
            setOpenDateTime(false);
        }
    };

    return (
        <Popover {...popoverPropsDate}>
            <InputBase
                {...props}
                loading={props.loading}
                readOnly={true}
                label={props.label}
                onFocus={(e) => setOpenDateTime(false)}
                onKeyPress={onKeyPress}
                value={dateValue}
                date={true}
                icon="fa-calendar-alt"
                onClickIc ={openDate}
            />
        </Popover>
    );
};
export default Main;
