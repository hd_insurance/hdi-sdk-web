import React, {useEffect, useState} from "react";
import Popover from "react-popover";
import PopSelectDate from "./datetimepicker";
import {checkFormatDate} from "../service/Utils";

import InputBase from "./InputBase";
import moment from "moment";
import cogoToast from "cogo-toast";
const Main = (props) => {
    const [openDateTime, setOpenDateTime] = useState(false);
    const [dateValue, setDateValue] = useState(props.value);

    useEffect(() =>{
        // if(checkFormatDate(props.value)){
            setDateValue(props.value);
        // }
    },[props.value])

    const dateSelect = (value) => {
        // if(!checkFormatDate(dateValue)) return;
        if(value) {
            setDateValue(`${value.day<10?`0${value.day}`:value.day}/${value.month<10?`0${value.month}`:value.month}/${value.year}`);
        }
        setTimeout(() => {

            props.changeEvent(`${value.day<10?`0${value.day}`:value.day}/${value.month<10?`0${value.month}`:value.month}/${value.year}`)
            setOpenDateTime(false);
        }, 200);
    };

    const popoverPropsDate = {
        isOpen: openDateTime,
        place: "below",
        className: 'custom-popover-date',
        preferPlace: "right",
        onOuterAction: () => setOpenDateTime(false),
        body: [<PopSelectDate
            selectedDay={props.selectedDay}
            minimumDate={props.minimumDate}
            maximumDate={props.maximumDate}
            dateSelect={dateSelect}
        />],
    };

    const onKeyPress = (e) =>{
        let input = e.target;
        if(e.charCode < 47 || e.charCode > 57) {
            e.preventDefault();
        }
        var len = input.value.length;

        if(len !== 1 || len !== 3) {
            if(e.charCode == 47) {
                e.preventDefault();
            }
        }
        if(len === 2) {
            input.value += '/';
        }
        if(len === 5) {
            input.value += '/';
        }
    }

    const openDate = () =>{
        if(checkFormatDate(dateValue)){
            setOpenDateTime(true);
        }else if(dateValue === null){
            setOpenDateTime(true);
        }else{
            cogoToast.error('Vui lòng nhập đúng định dạng ngày DD/MM/YYYY')
            setOpenDateTime(false);
        }
    };

    return (
        <Popover {...popoverPropsDate}>
            <InputBase
                {...props}
                loading={props.loading}
                // readOnly={true}
                label={props.label}
                onFocus={(e) => setOpenDateTime(false)}
                onKeyPress={onKeyPress}
                value={dateValue}
                date={true}
                icon="fa-calendar-alt"
                onClickIc ={openDate}
            />
        </Popover>
    );
};
export default Main;
