import React, {
    useEffect,
    useState,
    createRef,
    forwardRef,
    useImperativeHandle,
} from "react";
import styles from "../style.module.css";
import Popover from "react-popover";
import FLMultiSelect  from "./mutilSelect";
import FLInput from "../FlInput";

const Main = forwardRef((props, ref) => {
    const [openRelation, setShowRelation] = useState(false);
    // const [component_obj, setComponentObject] = useState({ render: 0 });
    const [isDisable, setDisable] = useState(props.isDisable || false);
    const [readonly, setReadonly] = useState(false);

    const [relation, setRelation] = useState(
        props.value || {
            title: "",
            value: "",
        }
    );
    const [listData, setListData] = useState(props.data || []);

    const relationSelect = (value) => {
        setRelation(value);
        setShowRelation(false);
        if (props.changeEvent) {
            props.changeEvent(value);
        }
    };

    useEffect(() =>{
        setListData(props.data);
    },[props.data])

    // useEffect(() => {
    //     if (props.component_obj.define.list) {
    //         setListData(props.component_obj.define.list);
    //         // console.log(props.component_obj.define.list)
    //     }
    //     props.component_obj.openRelation = (isOpen) => {
    //         setShowRelation(isOpen)
    //     }
    //     setComponentObject(props.component_obj);
    // }, [props.component_obj]);

    const openPop = () => {
        if (isDisable) return 1;
        setShowRelation(true);
    };

    const popoverPropsRelation = {
        isOpen: openRelation,
        place: "below",
        preferPlace: "right",
        className: 'flselect-cus',
        onOuterAction: () => setShowRelation(false),
        body: [
            <FLMultiSelect
                title={"Chọn mối quan hệ"}
                relationList={listData}
                selected={relationSelect}
            />,
        ],
    };

    useImperativeHandle(ref, () => ({}));
    return (
        <div>

            {/*<Popover {...popoverPropsRelation}>*/}
            {/*    <a className={styles.relation} onClick={(e) => openPop()}>*/}
            {/*        {relation.title*/}
            {/*            ? `${`Mối quan hệ`}: ` + relation.title*/}
            {/*            : "Chọn mối quan hệ "}*/}
            {/*        <i className={`${styles.fas} ${styles.fa_sort_down}`}></i>*/}
            {/*    </a>*/}
            {/*</Popover>*/}

            <Popover {...popoverPropsRelation}>
                <FLInput
                    {...props}
                    disable={props.disable}
                    readonly={readonly}
                    loading={props.loading}
                    label={props.label}
                    hideborder={props.hideborder}
                    onFocus={(e) => openPop()}
                    value={relation.title}
                    required={props.required}
                    dropdown={true}
                />
            </Popover>
        </div>
    );
});

export default Main;
