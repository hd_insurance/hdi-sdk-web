import React, {
  forwardRef,
  useEffect,
  useImperativeHandle,
  useState,
} from "react";
import { Table } from "react-bootstrap";

import { numberFormatCommas } from "../../service/Utils";

const Main = (props) => {
  const [listInsur, setListInsur] = useState(
    props?.list_insur ? props?.list_insur : []
  );
  const [dataDefine, setDatadefine] = useState(
    props?.dataDefine ? props?.dataDefine : []
  );

  useEffect(() => {
    setListInsur(props?.list_insur);
  }, [props?.list_insur]);

  useEffect(() => {
    setDatadefine(props?.dataDefine);
  }, [props?.dataDefine]);
  const handleViewCer = (idFile = "") => {
    if (props?.handleViewCer) {
      props.handleViewCer(idFile);
    }
  };
  return (
    <Table striped bordered hover responsive size="sm" className="mt-15">
      <thead>
        <tr style={{ fontSize: 14 }}>
          <th>STT</th>
          {props?.preview ? <th>Thao tác</th> : null}
          <th>Họ và tên</th>
          <th>Mối quan hệ</th>
          <th>Số CMND/CCCD</th>
          <th>Ngày sinh</th>
          <th>Gói </th>
          <th>Phí BH</th>
          {props.view_cer && <th className="text-center">Giấy chứng nhận</th>}
        </tr>
      </thead>
      <tbody>
        {listInsur.length > 0 ? (
          listInsur.map((vl, i) => {
            let pkgInfo = [],
              labelDate = "";
            if (dataDefine) {
              pkgInfo = dataDefine[2]?.filter((e) => e.value === vl.PACK_CODE);
              labelDate =
                "(" + vl.EFFECTIVE_DATE + " - " + vl.EXPIRATION_DATE + ")";
            }
            return (
              <tr key={i}>
                <td>{i + 1}</td>
                {props?.preview && (
                  <td className="icon-table" style={{ textAlign: "center" }}>
                    <i
                      className="fas fa-edit"
                      onClick={() => props?.showModal("EDIT", i)}
                    />
                    <i
                      className="fas fa-trash-alt"
                      onClick={() => props?.removeItemInsur(i)}
                    />
                  </td>
                )}
                <td>{vl.NAME}</td>
                <td>
                  {vl?.RELATIONSHIP === "BAN_THAN"
                    ? "Bản thân"
                    : props?.getLabelRelationShip(vl?.RELATIONSHIP, vl?.GENDER)}
                </td>
                <td>{vl.IDCARD}</td>
                <td>{vl.DOB}</td>
                <td>
                  <div>{`${pkgInfo[0]?.label} ${labelDate}`}</div>
                </td>
                <td>
                  {numberFormatCommas(vl?.TOTAL_AMOUNT.toString()) + " VNĐ"}
                </td>
                {props.view_cer && (
                  <td className="text-center">
                    <span
                      className="view_cer"
                      onClick={() => handleViewCer(vl.FILE[0]?.FILE_ID)}
                    >
                      Xem GCN
                    </span>
                  </td>
                )}
              </tr>
            );
          })
        ) : (
          <tr>
            <td colSpan="8" style={{ textAlign: "center" }}>
              Chưa có người được bảo hiểm
            </td>
          </tr>
        )}
      </tbody>
    </Table>
  );
};

export default Main;
