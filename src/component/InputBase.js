
import React, { useEffect, useState, useRef } from "react";
import { Form, OverlayTrigger, Tooltip, Overlay, Button} from "react-bootstrap";
import {checkFormatDate} from "../service/Utils";

const InputBase = (props) =>{
    const [value, setValue] = useState(props.value);
    const [err, setErr] = useState(null);
    //
    useEffect(() => {
        setValue(props.value)
    }, [props.value]);
    function handleTextChange(e) {
        if(props.readOnly){
            return e.preventDefault();
        }
        if(props.date){
            if(checkFormatDate(e.target.value)){
                setErr(null);
            }else{
                setErr('Lỗi định dạng ngày DD/MM/YYYY');
            }
        }
        var text= e.target.value;
        if(props.changeEvent){
            props.changeEvent(text);
        }
        setValue(text);
    }

    return(
        <React.Fragment>
            <div id="cus-input-label">
                <Form.Group style={{position: 'relative'}}>
                    {props.label && (
                        <Form.Label>{props.label}
                            {props.required ? <span style={{color: "#DA2128"}}>*</span> : ""}
                        </Form.Label>
                    )}
                    <Form.Control
                        {...props}
                        type={props.type}
                        placeholder={props.placeholder}
                        disabled={props.disable}
                        value={value || ""}
                        onFocus={props.onFocus}
                        onBlur={props.onBlur}
                        required={props.required}
                        onChange={(e) => handleTextChange(e)}
                    />
                    {props.icon ? <div className="icon" onClick={props.onClickIc}>
                        <i className={`fa ${props.icon}`} aria-hidden="true"></i>
                    </div> : null}
                    {props.error || err ? <label style={{color: '#DA2128', marginBottom: 0}}>{props.error || err}</label> : null}
                </Form.Group>
            </div>
        </React.Fragment>
    )
}
export default InputBase;
