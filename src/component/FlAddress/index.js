import React, { useEffect, useState } from "react";
import Popover from "react-popover";
import PopSelect from "../pop-address";
// import FLInput from "./index.jsx";
import FLInput from "../FlInput/index";


const Main = (props) => {
    const [openAddre, setOpenAddre] = useState(false);
    const [addressValue, setAddressValue] = useState(props?.value?.label);

    useEffect(() => {
        if(props.value && props.value.label){
            setAddressValue(props.value.label);
        } else {
            setAddressValue('')
        }
    }, [props.value])

    const locationFormSelect = (vl) => {
        setAddressValue(vl.label);
        props.changeEvent(vl)
        setTimeout(() => {
            setOpenAddre(false);
        }, 200);
    };

    const popoverPropsAddress = {
        isOpen: openAddre,
        place: props.positionSelect ? props.positionSelect : "below",
        preferPlace: props.positionSelect ? props.positionSelect : "below",
        className: 'custom-popover-address',
        onOuterAction: () => setOpenAddre(false),
        body: [<PopSelect addressSelect={locationFormSelect} />],
    };

    return (
        <Popover {...popoverPropsAddress} >
            <FLInput
                {...props}
                loading={props.loading}
                disable={props.disable}
                label={props.label}
                onFocus={(e) => setOpenAddre(true)}
                readOnly={true}
                value={addressValue}
                required={props.required}
                icon={(!props.required && addressValue) ? 'fa fa-times-circle' : 'far fa-map-marker-alt'}
                // dropdown={true}
                clear={(!props.required && addressValue) ? true : false}
            />
        </Popover>
    );
};
export default Main;
