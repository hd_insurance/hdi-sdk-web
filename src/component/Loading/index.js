import React from "react";
import './style.css';

const LoadingForm = (props) => {
    return(
        <div className="fm-loader" style={props.style}>
            <div className="loader-sdk">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>

    )
}
export default LoadingForm;
