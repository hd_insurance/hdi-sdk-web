
import React, {useRef, useState} from "react";
import {Form, Overlay, Tooltip} from "react-bootstrap";
import Select from "react-select";


const SelectBase = (props) =>{
    const [show, setShow] = useState(false);
    const target = useRef(null);

    const styleSelect = {
        control: base => ({
            ...base,
            height: 38,
            background: '#FFFFFF',
        })
    };
    return(
        <div id="cus-input-label">
            <Form.Group>
                <Form.Label>{props.label}
                    {props.required?<span style={{color:"#DA2128"}}>*</span>:""}
                    {props.tooltip? (
                        <>
                        <span className="show-tooltip-select" ref={target} onClick={() => setShow(!show)}>
                            <i className="fas fa-info-circle" />
                        </span>
                        <Overlay
                            rootClose={true}
                            onHide={() => setShow(false)}
                            target={target.current} show={show} placement="bottom">
                            {(props) => (
                                <Tooltip id="overlay-example" {...props}>
                                    <p>
                                        <p className="text-center">
                                            <strong>MÔ TẢ PHẠM VI BẢO HIỂM</strong>
                                        </p>
                                        <p style={{ marginBottom: "10px" }}>
                                            1. Phạm vi bảo hiểm A (Bảo hiểm này cung cấp độc lập):
                                            NĐBH bị tai nạn là nguyên nhân trực tiếp khiến cho
                                            NĐBH bị tử vong hoặc bị thương tật toàn bộ vĩnh viễn
                                            từ 80% trở lên;
                                        </p>
                                        <p>
                                            2. Phạm vi bảo hiểm B (Bảo hiểm này chỉ cung cấp khi
                                            NĐBH tham gia phạm vi bảo hiểm A): NĐBH bị ốm đau,
                                            bệnh tật, thai sản là nguyên nhân trực tiếp khiến cho
                                            NĐBH bị tử vong hoặc bị thương tật toàn bộ vĩnh viễn
                                            từ 80% trở lên;
                                        </p>
                                        <p>
                                            3. Phạm vi bảo hiểm C (Bảo hiểm này chỉ cung cấp khi
                                            NĐBH tham gia phạm vi bảo hiểm A): Người được bảo hiểm
                                            bị Tòa án có thẩm quyền tuyên bố mất tích theo quy
                                            định pháp luật Việt Nam
                                        </p>
                                    </p>

                                </Tooltip>
                            )}
                        </Overlay>
                        </>
                    ) : ''}
                </Form.Label>
                <Select
                    {...props}
                    isDisabled={props.disable}
                    classNamePrefix='cus-select'
                    // defaultValue={defaultVl}
                    value={props.value}
                    onChange={props.onChange}
                    options={props.options}
                    styles={styleSelect}
                />
            </Form.Group>
        </div>
    )
}

export default SelectBase;
