import React, { useEffect, useState } from "react";
import "./style.css";
import { Form } from "react-bootstrap";
import { checkFormatDate } from "../../service/Utils";

import Skeleton from "react-loading-skeleton";

const Main = (props) => {
  const [value, setValue] = useState(props.value);
  const [isActive, setIsActive] = useState(
    props.value != null && props.value != ""
  );
  const [err, setErr] = useState(null);

  useEffect(() => {
    // if(props.date){
    //   if (props.value !== '') {
    //     setIsActive(true);
    //   } else {
    //     setIsActive(false);
    //   }
    // }
    // console.log('"props.value', props.value);
    if (props.value !== null && props.value !== undefined&& props.value != "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
    if (props.dropdown && props.value !== null && props.value !== undefined) {
      if (props.value !== "") {
        setIsActive(true);
      } else {
        setIsActive(false);
      }
    }
    setValue(props.value);
  }, [props.value]);

  function handleTextChange(e) {
    if (props.readOnly) {
      return e.preventDefault();
    }
    if (props.date) {
      if (checkFormatDate(e.target.value)) {
        setErr(null);
      } else {
        setErr("Lỗi định dạng ngày DD/MM/YYYY");
      }
    }

    let text = e.target.value;
    if (props.changeEvent) {
      props.changeEvent(text);
    }
    setValue(text);
    if (text !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }
  function onClear(){
    props.changeEvent(null)
  }
  return (
    <React.Fragment>
      {/*{props.loading && <Skeleton width={'100%'} height={'53px'} />}*/}
      <div id="float-label" className={props.disable ? "ipt-disable" : ""}>
        <Form.Control
          type={props.type ? props.type : "text"}
          onKeyDown={props.onKeyDown}
          disabled={props.disable}
          value={value || ""}
          onFocus={props.onFocus}
          onKeyPress={props.onKeyPress}
          className={props.hideborder ? "no-border" : "ah"}
          required={props.required}
          readOnly={props.readonly}
          onChange={(e) => handleTextChange(e)}
          pattern={props.pattern}
          style={{cursor: props.disable? 'not-allowed' : ''}}
          maxLength={props?.maxLength}
        />
        {props.icon ? (
          <div
            className="icon"
            style={{ cursor: "pointer" }}
            onClick={props.disable ? null : props.onClickIc}
          >
            <i className={`fa ${props.icon}`} aria-hidden="true"></i>
          </div>
        ) : null}
        {props.dropdown ? (
          <div className="dropdown">
            <i className={`fa fa-caret-down`} aria-hidden="true"></i> 
          </div>
        ) : null}
        {props.clear ? (
          <div className="icon" style={{ cursor: "pointer" }}
          onClick={() => onClear()}
          >
            <i className={`fa fa-times-circle`} aria-hidden="true"></i>
          </div>
        ) : null}
        <label className={isActive ? "Active" : ""} htmlFor="inp">
          {props.label || ""}{" "}
          {props.required ? <span style={{ color: "#DA2128" }}> *</span> : ""}
        </label>
      </div>
      {props.error || err ? (
        <label style={{ color: "#DA2128", marginBottom: 0 }}>
          {props.error || err}
        </label>
      ) : null}
    </React.Fragment>
  );
};

export default Main;
