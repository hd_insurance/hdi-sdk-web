
import React, { useEffect, useState } from "react";
import './style.css'

const Main = (props) => {
  const [value, setValue] = useState(props.value);
  const [isActive, setIsActive] = useState(props.value!=null);

  React.useEffect(() => {
    setValue(props.value);
  }, [props.value]);
  function handleTextChange(text) {

    if(props.changeEvent){
      props.changeEvent(text)
    }
    setValue(text);
    if (text !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }
  return (
      <React.Fragment>
        <div id="float-label" className={props.disable ? "ipt-disable" : ""}>
      <textarea
          {...props}
          type="text"
          className="fl-text-area"
          rows="4"
          disabled={props.disable}
          value={value || ""}
          onFocus={props.onFocus}
          onBlur={props.onBlur}
          className={props.hideborder ? "no-border" : ""}
          onChange={(e) => handleTextChange(e.target.value)}
      />
          {props.icon ? <div className="icon">
            <i className={`fa ${props.icon}`} aria-hidden="true"></i>
          </div> : null}
          {props.dropdown ? <div className="dropdown">
            <i className={`fa fa-caret-down`} aria-hidden="true"></i>
          </div> : null}
          <label
              className={(isActive || props.value != null) ? "Active" : ""}
              htmlFor="inp">
            {props.label || ""} {props.required ? <span style={{color: "#DA2128"}}>*</span> : ""}
          </label>
        </div>
        {props.error ? <label style={{color: '#DA2128', marginBottom: 0}}>{props.error}</label> : null}
      </React.Fragment>

  );
};

export default Main;
