

export const config = {
    bill_info:{
        no_bill: 'N', // Khong Lay
        personal:'P', // Lay VAT cho ca nhan
        enterprise: 'E' // Lay VAT cho Doanh NGhiep
    },
    beneficiary:{
        insured: 'I',
        other: 'O',
        enterprise: 'E',
    }
}