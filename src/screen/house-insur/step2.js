
import React, { useRef, createRef, forwardRef, useEffect, useImperativeHandle, useState } from "react";
import { Form, Table, Col, Row, Modal, Button } from "react-bootstrap";
import Popover from "react-popover";

import moment from "moment";
import FLInput from "../../component/FlInput";
import FLSelect from "../../component/FLSelect/index";
import FLAddress from "../../component/FlAddress/index";
import RelationSelect from "../../component/FLMutilSelect/index";
import {
    checkFormatDate,
    selectDate,
    convertRelation,
    numberFormatCommas,
    getListYearHouseDoneYears, 
    getListYear,
    useWindowDimensions
} from "../../service/Utils";

import FLInputDate from "../../component/FLInputDate";
import cogoToast from "cogo-toast";
import NextWorkNew from "../../service/Network1";
import InfoBuyer from "../component/InfoBuyer";
import { config } from "./config-house";
import InfoCompany from "../component/InfoCompany";
import LoadingForm from "../../component/Loading";
import DataBeneficiary from "../component/DataBeneficiary";
import { caculDateLeapYear } from "../../service/Utils";
import { Tooltip, OverlayTrigger } from "react-bootstrap";

const apiNew = new NextWorkNew();

const Main = forwardRef((props, ref) => {
    const formRef = createRef();
    const infoBuyerRef = createRef();
    const otherBeneficiary = createRef();
    const refPopoverXemChiTiet = useRef();
    const { height, width } = useWindowDimensions();

    const typingTimerRef = useRef(null);

    const [infoBuyer, setInfoBuyer] = useState({})

    const [validateFormSt2, setValidateFormSt2] = useState(false);

    const [packageCode, setPackageCode] = useState(props?.infoHouse?.HOUSE_PRODUCT?.PACK_CODE || 'GOI_HDI');
    const [packageFormat, setPackageFormat] = useState(null);


    const [openBenefit, setOpenBenefit] = useState(false);
    const [benefit, setBenefit] = useState({
        label: "",
        link: "",
        description: [],
    });
    const [loadingFee, setLoadingFee] = useState(false);

    // const [detailBenefit, setDetailBenefit] = useState(false);

    // const [gender, setGender] = useState( null);

    const [address, setAddress] = useState(props?.infoHouse?.HOUSE_INFO?.ADDRESS || null);
    const [addressCode, setAddressCode] = useState({
        label: props?.infoHouse?.HOUSE_INFO?.ADDRESS_FORM || null,
        prov: props?.infoHouse?.HOUSE_INFO?.PROV || null,
        dist: props?.infoHouse?.HOUSE_INFO?.DIST || null,
        ward: props?.infoHouse?.HOUSE_INFO?.WARDS || null,
    });
    const [eff, setEff] = useState(props?.infoHouse?.HOUSE_PRODUCT?.EFF || moment().add(1, "days").format("DD/MM/YYYY"));
    const [exp, setExp] = useState(props?.infoHouse?.HOUSE_PRODUCT?.EXP || null);
    const [feePack, setFeePack] = useState(props?.infoHouse?.HOUSE_PRODUCT?.INSUR_TOTAL_AMOUNT || 0);

    // const [typeBuy, setTypeBuy] = useState('VALUE');
    const [typeBeneficiary, setTypeBeneficiary] = useState('I');
    const [typeHouse, setTypeHouse] = useState(props?.infoHouse?.HOUSE_INFO?.HOUSE_TYPE || null);
    const [areaHouse, setAreaHouse] = useState(props?.infoHouse?.HOUSE_INFO?.HOUSE_AREA || null);
    const [yearHouse, setYearHouse] = useState(props?.infoHouse?.HOUSE_INFO?.YEAR_COMPLETE || null);
    const [floorHouse, setFloorHouse] = useState(props?.infoHouse?.HOUSE_INFO?.NUM_FLOOR || null);
    const [valueHouse, setValueHouse] = useState(numberFormatCommas(props?.infoHouse?.HOUSE_PRODUCT?.INSUR_HOUSE_VALUE || "")); // so tien BH phan xa dung
    const [assetsHouse, setAssetsHouse] = useState(numberFormatCommas(props?.infoHouse?.HOUSE_PRODUCT?.INSUR_HOUSE_ASSETS || null)); // so tien BH tai san ben trong

    const [valueHouseRef, setValueHouseRef] = useState(numberFormatCommas(props?.infoHouse?.HOUSE_PRODUCT?.HOUSE_VALUE_REF || null)); // gia tri ngoi nha
    const [assetsHouseRef, setAssetsHouseRef] = useState(numberFormatCommas(props?.infoHouse?.HOUSE_PRODUCT?.ASSET_VALUE_REF || null)); // gia tri tai san ben trong

    const [insurDuration, setInsurDuration] = useState(props?.infoHouse?.HOUSE_PRODUCT?.INSUR_DURATION || 12);

    const [valueDebuctible, setValueDebuctible] = useState(props?.infoHouse?.HOUSE_PRODUCT?.INSUR_DEDUCTIBLE || null); // muc mien thuong

    const [feehouse, setFeeHouse] = useState({
        total_amount: 0,
        value_house: 0,
        assets_house: 0,
    });

    const [errorForm, setErrorForm] = useState({
        valueHouseError: ''
    })
    const [dataRight, setDataRight] = useState([]);

    const maximumDate = () => {
        const date = new Date();
        return {
            year: date.getFullYear(),
            month: date.getMonth() + 1,
            day: date.getDate() + 1,
        };
    };
    const minimumDate = () => {
        const date = new Date();
        return {
            year: date.getFullYear(),
            month: date.getMonth() + 1,
            day: date.getDate(),
        };
    };

    useEffect(() => {
        if (props?.dataDefine[2]) {
            let listPack = props?.dataDefine[2];
            setPackageFormat(props?.dataDefine[2]);
            let defaultPkg = listPack[0]?.value;
            setPackageCode(defaultPkg);
            let pkg = formatBenefit(listPack[0]);
            if (pkg) {
                setBenefit(pkg);
            }
        }

        if (props?.dataDefine[1] && props?.dataDefine[1].length > 0) {
            setValueDebuctible(props?.dataDefine[1][0]?.value);
        }
    }, [props?.dataDefine]);

    useEffect(() => {
        if (props?.sdk_params.org_code == 'HDBANK_VN' && props?.sdk_params.partner == 'LO_HDB') {
            let _t = props?.dataDefine ? props?.dataDefine[4]?.filter(p => p.value == 'O') : [] // chu nha
            setDataRight(_t);
        } else {
            setDataRight(props?.dataDefine ? props?.dataDefine[4] : [])
        }
    }, [props?.sdk_params]);

    useEffect(() => {
        if (props?.buyer?.RELATIONSHIP) {
            setInfoBuyer(props?.buyer)
        } else {
            setInfoBuyer({
                ...infoBuyer,
                ADDRESS: props?.infoHouse?.ADDRESS,
                ADDRESS_FORM: props?.infoHouse?.ADDRESS_FORM,
                DIST: props?.infoHouse?.DIST,
                DOB: props?.infoHouse?.DOB,
                EMAIL: props?.infoHouse?.EMAIL,
                FAX: props?.infoHouse?.FAX,
                GENDER: props?.infoHouse?.GENDER,
                IDCARD: props?.infoHouse?.IDCARD,
                LAND_USE_RIGHTS: props?.infoHouse?.LAND_USE_RIGHTS,
                NAME: props?.infoHouse?.NAME,
                NATIONALITY: props?.infoHouse?.NATIONALITY,
                PHONE: props?.infoHouse?.PHONE,
                PROV: props?.infoHouse?.PROV,
                WARDS: props?.infoHouse?.WARDS,
                RELATIONSHIP: props?.infoHouse?.RELATIONSHIP !== 'KHAC' ? true : false,
                TAXCODE: props?.infoHouse?.TAXCODE,
            })
        }
    }, []);

    useEffect(() => {
        // console.log('infoHouse', props?.infoHouse);

        if (props?.infoHouse?.BENEFICIARY) {
            let data = props?.infoHouse?.BENEFICIARY[0]
            if (data?.TYPE === 'CN' && data?.RELATIONSHIP === 'KHAC') {
                setTypeBeneficiary(config.beneficiary.other)
            } else if (data?.TYPE === 'CN' && data?.RELATIONSHIP === 'BAN_THAN') {
                setTypeBeneficiary(config.beneficiary.insured)
            } else {
                setTypeBeneficiary(config.beneficiary.enterprise)
            }
        }

        if (props?.sdk_params?.org_code == 'HDBANK_VN') { // HD bank default chon doan nghiep, disabled con lai
            setTypeBeneficiary(config.beneficiary.enterprise)
        }
        if (props?.infoHouse?.HOUSE_INFO?.ADDRESS_FORM) {
            setAddressCode(
                {
                    label: props?.infoHouse?.HOUSE_INFO?.ADDRESS_FORM,
                    prov: props?.infoHouse?.HOUSE_INFO?.PROV,
                    dist: props?.infoHouse?.HOUSE_INFO?.DIST,
                    ward: props?.infoHouse?.HOUSE_INFO?.WARDS,
                }
            )
        }
    }, [props?.infoHouse]);

    useEffect(() => {
        if (checkFormatDate(eff)) {
            let rs1 = moment(eff, "DD/MM/YYYY").add(insurDuration, 'months').format("DD/MM/YYYY");
            // let rs = moment(rs1, "DD/MM/YYYY").add(1, 'years').format("DD/MM/YYYY");
            setExp(rs1);
        }
    }, [eff, insurDuration]);

    useEffect(() => {
        if (yearHouse && valueHouse && eff && packageCode) {
            if (checkFormatDate(eff)) {
                let yearEff = moment(eff, "DD/MM/YYYY").year();
                let yearExp = moment(exp, "DD/MM/YYYY").year();

                let effDate = moment(eff, 'DD/MM/YYYY');
                let _exp = moment(eff, "DD/MM/YYYY").add(insurDuration, 'months').format("DD/MM/YYYY");
                let expDate = moment(_exp, 'DD/MM/YYYY');

                let arrFee = [];
                arrFee = handleArrDynamicFee(
                    yearHouse,
                    yearEff,
                    effDate,
                    expDate,
                    valueHouse,
                    assetsHouse,
                    packageCode,
                );
                handleCalcFee(arrFee);
            }
        }
    }, [yearHouse, valueHouse, eff, packageCode, assetsHouse, insurDuration])

    const onChangePackage = (data) => {
        setPackageCode(data);
        let iPack = packageFormat.findIndex((e) => e.value === data);
        let pkg = formatBenefit(props.dataDefine[2][iPack]);
        if (pkg) {
            setBenefit(pkg);
        }
    }

    const checkEffDate = (effValue) => {
        // validate khi typing
        const minDate = moment();
        const effDate = moment(effValue, "DD/MM/YYYY");
        if (!checkFormatDate(effValue)) {
            cogoToast.error('Lỗi định dạng ngày DD/MM/YYYY')
            return false;
        }
        if (minDate.diff(effDate, 'days') > 0) {
            cogoToast.error('Ngày hiệu lực không nhỏ hơn ngày hiện tại')
            return false;
        }
        return true;
    };

    useImperativeHandle(ref, () => ({
        handleSubmit() {
            try {
                const form = formRef.current;
                const dataOther = otherBeneficiary?.current?.handleInfoBuyer();

                let insuredP = infoBuyerRef?.current?.handleInfoBuyer();
                if (props?.buyer?.RELATIONSHIP) { // Nguoi mua BH là người được BH
                    insuredP = props?.buyer
                }
                if (form.checkValidity() === false) {
                    setValidateFormSt2(true);
                    return false;
                }
                if (props?.sdk_params?.org_code == 'HDBANK_VN' && props?.sdk_params?.partner == 'LO_HDB' && errorForm.valueHouseError) {
                    cogoToast.error(errorForm.valueHouseError);
                    return false;
                }
                if (insuredP) {
                    if (!checkEffDate(eff)) {
                        return false;
                    }
                    if (insuredP.DOB && !checkFormatDate(insuredP.DOB)) {
                        cogoToast.error('Lỗi định dạng ngày DD/MM/YYYY')
                        return false;
                    }

                    let yearEff = moment(eff, "DD/MM/YYYY").year();
                    let year_of_use = handleYearOfUse(yearHouse, yearEff)


                    let dataBeneficiary = null;
                    if (typeBeneficiary === config?.beneficiary.insured) {
                        dataBeneficiary = {
                            ORG_CODE: "",
                            RELATIONSHIP: "BAN_THAN",
                            TYPE: 'CN',
                            NAME: insuredP?.NAME,
                            DOB: insuredP?.DOB,
                            GENDER: insuredP?.GENDER,
                            ADDRESS: insuredP?.ADDRESS,
                            IDCARD: insuredP?.IDCARD,
                            EMAIL: insuredP?.EMAIL,
                            PHONE: insuredP?.PHONE,
                            PROV: insuredP?.PROV,
                            DIST: insuredP?.DIST,
                            WARDS: insuredP?.WARDS,
                            ADDRESS_FORM: insuredP?.ADDRESS_FORM,
                            IDCARD_D: "",
                            IDCARD_P: "",
                            FAX: "",
                            TAXCODE: null
                        }
                    } else {
                        dataBeneficiary = dataOther
                    }
                    const house_info = {
                        HOUSE_TYPE: typeHouse,
                        HOUSE_AREA: areaHouse, // dien tich
                        YEAR_COMPLETE: yearHouse, // nam hoan thanh xat dung
                        NUM_FLOOR: typeHouse !== 'AP' ? floorHouse : null, // so tang
                        PROV: addressCode?.prov || '',
                        DIST: addressCode?.dist || '',
                        WARDS: addressCode?.ward || '',
                        ADDRESS: address || '',
                        ADDRESS_FORM: addressCode?.label || '',
                    };

                    const house_product = {
                        PRODUCT_CODE: props?.sdk_params?.product_code,
                        PACK_CODE: packageCode,
                        HOUSE_USE_CODE: year_of_use,
                        EFF: eff,
                        TIME_EFF: "",
                        EXP: exp,
                        TIME_EXP: "",
                        INSUR_TOTAL_AMOUNT: 0, // tong gia tri phi nha
                        INSUR_HOUSE_VALUE: valueHouse?.split(",").join(""), // gia tri BH ngoi nha
                        INSUR_HOUSE_ASSETS: assetsHouse?.split(",").join(""), // gia tri BH tai san ben trong
                        HOUSE_VALUE_REF: valueHouseRef?.split(",").join(""), // gia tri ngoi nha
                        ASSET_VALUE_REF: assetsHouseRef?.split(",").join(""), // gia tri tai san
                        INSUR_DEDUCTIBLE: valueDebuctible,
                        INSUR_DURATION: insurDuration,
                        FEES_DATA: feehouse?.total_amount,
                    };
                    return {
                        ...props?.infoHouse,
                        CUS_ID: "",
                        TYPE: "CN",
                        NATIONALITY: "",
                        NAME: insuredP?.NAME,
                        DOB: insuredP?.DOB,
                        GENDER: insuredP?.GENDER,
                        PROV: insuredP?.PROV,
                        DIST: insuredP?.DIST,
                        WARDS: insuredP?.WARDS,
                        ADDRESS: insuredP?.ADDRESS,
                        ADDRESS_FORM: insuredP?.ADDRESS_FORM,
                        IDCARD: insuredP?.IDCARD,
                        EMAIL: insuredP?.EMAIL,
                        PHONE: insuredP?.PHONE,
                        FAX: "",
                        TAXCODE: insuredP?.TAXCODE,
                        RELATIONSHIP: props?.buyer?.RELATIONSHIP ? "BAN_THAN" : "KHAC",
                        CONTRACT_CODE: props?.infoHouse?.CONTRACT_CODE,
                        DETAIL_CODE: props?.infoHouse?.DETAIL_CODE,
                        LAND_USE_RIGHTS: insuredP?.LAND_USE_RIGHTS,
                        HOUSE_INFO: house_info,
                        HOUSE_PRODUCT: house_product,
                        BENEFICIARY: [dataBeneficiary],
                    }
                }
            } catch (error) {
                console.log('error', error)
            }
        },
    }));



    const actionopenBenefit = () => {
        if (packageCode) {
            setOpenBenefit(true);
        } else {
            cogoToast.error("Vui lòng chọn gói bảo hiểm.");
        }
    };

    const popoverBenefit = {
        isOpen: openBenefit,
        place: "below",
        preferPlace: "right",
        className: "flselect-cus",
        onOuterAction: () => setOpenBenefit(false),
        body: [
            <div className={'list_benefit_relative'}>
                <div className='h'>{benefit ? benefit?.description[0]?.PACK_NAME : null}</div>
                <div className='list_xxx'>
                    {benefit.description?.map((item, index) => {
                        // console.log(item);
                        return (
                            <div key={index}>
                                {item.BENEFITS.map((ben, id2) => {
                                    return <div className='ite'>{ben.NAME}</div>
                                })}
                            </div>
                        );
                    })}
                    <Button
                        className='view_detail_benefit'
                        onClick={(e) => window.open(benefit.link, "_blank")}
                    >
                        Xem chi tiết
                    </Button>
                </div>
            </div>,
        ],
    };

    const calcFee = async (dynamicFees) => {
        try {
            const data = {
                channel: props?.sdk_params?.channel,
                userName: props?.sdk_params?.user_name,
                ORG_CODE: props?.sdk_params?.org_code,
                PRODUCT_INFO: [
                    {
                        INX: "0",
                        CATEGORY: props?.sdk_params?.category,
                        PRODUCT_CODE: props?.sdk_params?.product_code,
                        PACK_CODE: packageCode,
                        DISCOUNT: "0",
                        DISCOUNT_UNIT: "",
                        DYNAMIC_FEES: dynamicFees,
                    },
                ],
            };
            setLoadingFee(true);
            const response = await apiNew.post("/api/ntn/calc/fee", data);
            setTimeout(() => {
                if (response?.Data) {
                    setLoadingFee(false);
                    let dataFee = response?.Data;
                    setFeeHouse({
                        total_amount: dataFee?.TOTAL_AMOUNT,
                        value_house: dataFee?.PRODUCT_DETAIL[0]?.OUT_DETAIL[1].TOTAL_AMOUNT,
                        assets_house: dataFee?.PRODUCT_DETAIL[0]?.OUT_DETAIL[0].TOTAL_AMOUNT
                    });
                    setFeePack(dataFee?.TOTAL_AMOUNT);
                }
            }, 1000);
        } catch (err) {
            console.log(err);
        }
    };

    const formatBenefit = (pkinfo) => {
        return {
            label: pkinfo?.PACK_NAME,
            link: `${pkinfo?.URL_BEN}`,
            description: JSON.parse(pkinfo?.DESCRIPTION),
        };
    };

    // const onChangeTypeBuy = (e) =>{
    //     setTypeBuy(e.target.value)
    // }

    const onChangeBeneficiary = (e) => {
        setTypeBeneficiary(e.target.value)
    }

    const handleCalcFee = async (arrDynamic) => {
        if (typingTimerRef) {
            clearTimeout(typingTimerRef.current);
        }
        typingTimerRef.current = setTimeout(() => {
            calcFee(arrDynamic);
        }, 800);
    };

    const handleArrDynamicFee = (
        yearDone, // năm hoàn thành xây dựng
        yearAffect, // năm hiệu lực
        effDate,
        expDate,
        moneyHouse,
        assetsHouse,
        packCode
    ) => {
        let yearOfUse = handleYearOfUse(yearDone, yearAffect);
        // let totalDay = caculDateLeapYear(effDate, expDate);
        return [
            {
                KEY_CODE: "NHA_GOITG",
                VAL_CODE: `${packCode}@${yearOfUse}`,
                TYPE_CODE: "CACHE",
            },
            {
                KEY_CODE: "NHA_HV1",
                VAL_CODE: moneyHouse?.split(",").join(""),
                TYPE_CODE: "CONSTANT",
            },
            {
                KEY_CODE: "NHA_FV1",
                VAL_CODE: assetsHouse?.split(",").join(""),
                TYPE_CODE: "CONSTANT",
            },
            {
                KEY_CODE: "NHA_TGBH",
                // VAL_CODE: totalDay + "",
                VAL_CODE: insurDuration + "",
                TYPE_CODE: "CONSTANT",
            },
        ];
    };

    const handleYearOfUse = (yearDone, yearAffect) => {
        let result = "";
        if (!yearDone) return result;
        let hieuYear = parseFloat(yearAffect * 1 - yearDone * 1);
        if (props?.dataDefine[7]) {
            props?.dataDefine[7].forEach((item) => {
                if (
                    hieuYear <= parseFloat(item.MAX_VALUE) &&
                    hieuYear >= parseFloat(item.MIN_VALUE)
                ) {
                    result = item.value;
                }
            });
        }
        return result;
    }

    const isDisBeneficiary = (value) => {
        let _res = false;
        if (props?.sdk_params?.org_code == 'HDBANK_VN' && props?.sdk_params?.partner == 'LO_HDB' && value != config.beneficiary.enterprise) {
            _res = true;
        }
        return _res;
    }
    const checkSetValueHouse = (value) => {
        if (props?.sdk_params?.org_code != 'HDBANK_VN' || props?.sdk_params?.partner != 'LO_HDB') {
            setValueHouse(value);
            return;
        }
        // https://hdinsurance.atlassian.net/browse/BPI-76 rieng cho HDBankVN
        if (value) {
            let _v = value.replaceAll(',', '');
            _v = parseFloat(_v);
            if (_v <= 5000000000) {
                setValueHouse(numberFormatCommas(value?.toString()));
                setErrorForm({
                    valueHouseError: ''
                });
            } else {
                setValueHouse(numberFormatCommas(value?.toString()));
                setErrorForm({
                    valueHouseError: 'Số tiền BH tối đa 5,000,000,000'
                })
            }
        }
    }
    const renderTooltipInfo = props => (
        <Tooltip {...props}>Xem quyền lợi</Tooltip>
    );
    let btnXemChiTietTooltip = () => {
        if (width > 1331) {
            return <div
                className="btn-download-document view_ql"
                style={{ marginLeft: 0, fontSize: 12 }}
                onClick={(e) => {
                    actionopenBenefit();
                }}
            >
                <section style={{ 'display': 'inherit', 'whiteSpace': 'nowrap', 'textOverflow': 'ellipsis', 'overflow': 'hidden' }}>
                    <i className="fas fa-shield-alt" />{" "}
                    Xem quyền lợi
                </section>
            </div>
        } else {
            return <OverlayTrigger placement="right" overlay={renderTooltipInfo}>
                <div
                    className="btn-download-document view_ql"
                    style={{ marginLeft: 0, fontSize: 12 }}
                    onClick={(e) => {
                        actionopenBenefit();
                    }}
                >
                    <section style={{ 'display': 'inherit', 'whiteSpace': 'nowrap', 'textOverflow': 'ellipsis', 'overflow': 'hidden' }}>
                        <i className="fas fa-shield-alt" />{" "}
                        Xem quyền lợi
                    </section>
                </div>
            </OverlayTrigger>
        }
    }

    return (
        <Form
            id="create-course-form"
            ref={formRef}
            noValidate
            validated={validateFormSt2}
        >
            {loadingFee && <LoadingForm />}
            <div className="form-content">
                <label className="col-form-label">
                    Thông tin ngôi nhà , gói bảo hiểm
                </label>
            </div>
            <Row className={'form-group'}>
                <Col md={3}>
                    <FLSelect
                        disable={(props?.sdk_params?.org_code === 'HDBANK_VN' && props?.sdk_params?.partner === 'LO_HDB') ? true : false}
                        label="Loại nhà"
                        value={typeHouse}
                        changeEvent={setTypeHouse}
                        dropdown={true}
                        required={true}
                        data={props?.dataDefine[0]}
                    />
                </Col>
                <Col md={3}>
                    <FLInput
                        label='Diện tích sử dụng (m2)'
                        value={areaHouse}
                        changeEvent={setAreaHouse}
                        required={true}
                    />
                </Col>
                <Col md={3}>
                    <FLSelect
                        disable={false}
                        label="Năm hoàn thành XD"
                        value={yearHouse}
                        changeEvent={setYearHouse}
                        dropdown={true}
                        required={true}
                        data={getListYearHouseDoneYears(15)}
                    />
                </Col>
                {typeHouse !== 'AP' && (
                    <Col md={3}>
                        <FLInput
                            label='Số tầng'
                            value={floorHouse}
                            changeEvent={setFloorHouse}
                            required={true}
                        />
                    </Col>
                )}
            </Row>

            <Row className={'form-group'}>

                <Col md={3}>
                    <FLInput
                        label='Giá trị xây dựng căn nhà'
                        value={numberFormatCommas(valueHouseRef?.toString())}
                        changeEvent={setValueHouseRef}
                        required={true}
                    />
                </Col>
                <Col md={3}>
                    <FLInput
                        label='Số tiền BH phần xây dựng'
                        value={numberFormatCommas(valueHouse?.toString())}
                        changeEvent={(event) => { checkSetValueHouse(event) }}
                        required={true}
                        error={errorForm?.valueHouseError}
                    />
                </Col>

                <Col md={3}>
                    <FLInput
                        label='Giá trị tài sản bên trong'
                        value={assetsHouseRef ? numberFormatCommas(assetsHouseRef?.toString()) : null}
                        changeEvent={setAssetsHouseRef}
                        required={false}
                        disable={true}
                    />
                </Col>
                <Col md={3}>
                    <FLInput
                        label='Số tiền BH tài sản bên trong'
                        value={assetsHouse ? numberFormatCommas(assetsHouse?.toString()) : null}
                        changeEvent={setAssetsHouse}
                        required={false}
                        disable={true}
                    />
                </Col>
            </Row>
            <Row className={'form-group'}>

                <Col md={3}>
                    <FLSelect
                        disable={false}
                        label="Mức miễn thường"
                        value={valueDebuctible}
                        changeEvent={setValueDebuctible}
                        dropdown={true}
                        required={true}
                        data={props?.dataDefine[1]}
                    />
                </Col>
                <Col md={4}>
                    <FLInput
                        label='Số nhà, ngõ / ngách, đường'
                        value={address}
                        changeEvent={setAddress}
                        required={true}
                    />
                </Col>
                <Col md={5}>
                    <FLAddress
                        disable={false}
                        label={'Tỉnh/TP - Quận/huyện - Phường/ xã'}
                        value={addressCode}
                        changeEvent={setAddressCode}
                        required={(props?.sdk_params?.org_code === 'HDBANK_VN' && props?.sdk_params?.partner === 'LO_HDB') ? false : true}
                    />
                </Col>
            </Row>
            <Row className={'form-group'}>
                <Col md={3}>
                    <div className='group_ipnv'>
                        <div style={{ width: '50%' }}>
                            <FLSelect
                                // disable={registeredChecked}
                                hideborder={true}
                                label="Gói bảo hiểm"
                                value={packageCode}
                                changeEvent={onChangePackage}
                                dropdown={true}
                                required={true}
                                data={packageFormat ? packageFormat : []}
                            />
                        </div>
                        <div className="ver-line" />
                        <div className='kkjs' ref={refPopoverXemChiTiet}>
                            <Popover {...popoverBenefit}>
                                {btnXemChiTietTooltip()}
                            </Popover>
                        </div>
                    </div>
                </Col>
                <Col md={3}>
                    <FLInput
                        label='Thời hạn bảo hiểm (tháng)'
                        value={insurDuration}
                        changeEvent={setInsurDuration}
                        required={true}
                        type={'number'}
                    />
                </Col>
                <Col md={3}>
                    <FLInputDate
                        disable={false}
                        value={eff}
                        changeEvent={setEff}
                        selectedDay={selectDate(eff)}
                        minimumDate={minimumDate()}
                        // minimumDate={utils().getToday()}
                        label={"Ngày hiệu lực"}
                        required={true}
                    />
                </Col>
                <Col md={3}>
                    <FLInputDate
                        disable={true}
                        value={exp}
                        changeEvent={setExp}
                        selectedDay={selectDate(exp)}
                        label={"Ngày kết thúc"}
                        required={true}
                    />
                </Col>

            </Row>
            <Row>
                <Col md={3}>
                    <FLInput
                        label='Phí'
                        value={numberFormatCommas(feePack?.toString()) + ' VNĐ'}
                        changeEvent={setFeePack}
                        required={false}
                        disable={true}
                    />
                </Col>
            </Row>
            {!props?.buyer?.RELATIONSHIP && (
                <>
                    <div className="form-content">
                        <label className="col-form-label">
                            Thông tin người được bảo hiểm
                        </label>
                    </div>
                    <InfoBuyer
                        ref={infoBuyerRef}
                        dataGender={props?.dataDefine ? props?.dataDefine[3] : []}
                        dataRight={dataRight || props?.dataDefine[4]}
                        isPLus={true}
                        vat={infoBuyer?.TYPE_VAT}
                        buyer={infoBuyer}
                        isHdbankLO={(props?.sdk_params?.org_code === 'HDBANK_VN' && props?.sdk_params?.partner === 'LO_HDB') ? true : false}
                    />
                </>
            )}

            <div className="form-content">
                <label className="col-form-label">
                    Thông tin người thụ hưởng
                </label>
            </div>

            <Row className='form-choose-type'>
                <Col md={2}>
                    <p>Chọn người thụ hưởng</p>
                </Col>
                <Col md={10} className='rd-group'>
                    {props?.dataDefine[5] ? (
                        props?.dataDefine[5].map((vl, i) => {
                            return (
                                <label key={i} className="form-check" style={{ color: typeBeneficiary === vl.value ? '#329945' : '#777777', cursor: isDisBeneficiary(vl.value) ? 'not-allowed' : '', }}>
                                    <input className="form-check-input" type="radio" value={vl.value}
                                        onChange={onChangeBeneficiary}
                                        checked={typeBeneficiary === vl.value}
                                        disabled={isDisBeneficiary(vl.value)}  // HDBANK_VN thi chi hien doanh nghiep
                                    />
                                    {vl.label}
                                </label>
                            )
                        })
                    ) : null}
                </Col>
            </Row>
            {typeBeneficiary !== config.beneficiary.insured && (
                <DataBeneficiary
                    ref={otherBeneficiary}
                    dataGender={props?.dataDefine ? props?.dataDefine[3] : []}
                    type={typeBeneficiary}
                    data={props?.infoHouse?.BENEFICIARY ? props?.infoHouse?.BENEFICIARY[0] : null}
                    isHdbankLO={(props?.sdk_params?.org_code === 'HDBANK_VN' && props?.sdk_params?.partner === 'LO_HDB') ? true : false}
                />
            )}
        </Form>
    )
});

export default (Main);
