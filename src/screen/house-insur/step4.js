import React, {
    useImperativeHandle,
    createRef,
    forwardRef,
    useEffect,
    useState,
} from "react";
import { Form, Col, Row } from "react-bootstrap";
import NextWorkNew from "../../service/Network1";
import { numberFormatCommas } from "../../service/Utils";
import FLInput from "../../component/FlInput";

const Main = forwardRef((props, ref) => {
    const formRef = createRef();
    const [voucherCode, setVoucherCode] = useState(null);
    const [totalAmount, setTotalAmount] = useState(
        props?.totalAmount || 0
    );
    const [discount, setDiscount] = useState(0);


    useEffect(() =>{
        setTotalAmount(props?.totalAmount)
    },[props?.totalAmount])

    useImperativeHandle(ref, () => ({
        handleSubmit() {},
        setTotalAmount(amount) {
            setTotalAmount(amount);
        },
    }));



    return (
        <Form
            ref={formRef}
            noValidate
            // validated={validateForm}
        >
            <Row className="form-group">
                <Col md={6}>
                    <div className="info-payment">
                        <div style={{ padding: 12 }}>
                            <div className="item-info-payment">
                                <label>Phí bảo hiểm:</label>
                                <label>
                                    {numberFormatCommas(totalAmount?.toString()) + " VNĐ"}
                                </label>
                            </div>
                            <div className="item-info-payment">
                                <label>Giảm phí:</label>
                                <label>
                                    {numberFormatCommas(discount?.toString()) + " VNĐ"}
                                </label>
                            </div>
                        </div>
                        <div className="total-info-payment">
                            <label>Tổng phí thanh toán:</label>
                            <label>
                                {numberFormatCommas(totalAmount?.toString()) + " VNĐ"}
                            </label>
                        </div>
                    </div>
                </Col>
                <Col md={6}>
                    <Row>
                        <div className="col-9">
                            <FLInput
                                label="Mã voucher"
                                value={voucherCode}
                                changeEvent={setVoucherCode}
                            />
                        </div>
                        <div className="col-3">
                            <div className="btn-apply-voucher">Áp dụng</div>
                        </div>
                    </Row>
                </Col>
            </Row>
        </Form>
    );
});

export default Main;
