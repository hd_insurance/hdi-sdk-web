import React, {
  createRef,
  forwardRef,
  useEffect,
  useImperativeHandle,
  useState,
} from "react";
import { Col, Form, Row } from "react-bootstrap";
import NextWorkNew from "../../service/Network1";
import Upload from "../../component/UploadFile/index";
import cogoToast from "cogo-toast";
import { Tooltip, OverlayTrigger } from "react-bootstrap";

const apiNew = new NextWorkNew();

const Main = forwardRef((props, ref) => {
  const formRef = createRef();

  const [validateForm, setValidateForm] = useState(false);

  const [listGyc, setListGyc] = useState(props?.list_file?.list_gyc || []);
  const [listKhac, setListKhac] = useState(props?.list_file?.list_khac || []);

  const listUpload = [
    {
      title: "Giấy yêu cầu bảo hiểm",
      required: false,
      type: "GYC",
      uploadRef: createRef(),
    },
    {
      title: "Các file khác",
      required: false,
      type: "KHAC",
      uploadRef: createRef(),
    },
  ];
  useImperativeHandle(ref, () => ({
    handleSubmit() {
      const form = formRef.current;
      // if (listGyc.length == 0) {
      //   cogoToast.error("Vui lòng thêm file giấy yêu cầu bảo hiểm.");
      //   return false;
      // }
      if (form.checkValidity() === false) {
        setValidateForm(true);
        return false;
      }

      return {
        list_gyc: listGyc,
        list_khac: listKhac,
      };
    },
  }));

  const setFileUpload = (idx, files) => {
    switch (idx) {
      case "GYC":
        setListGyc(files);
        break;
      case "KHAC":
        setListKhac(files);
        break;
      default:
        break;
    }
  };
  const isHDbLoFirefox = () => {
    const userBrower = window.navigator.userAgent;
    if(props?.sdk_params.org_code == 'HDBANK_VN' && props?.sdk_params.partner == 'LO_HDB' && userBrower && userBrower.toLowerCase().includes('firefox')) {
      return true;
    }
    return false;
  }
  const openDropZone = (uploadRef) => {
    uploadRef?.current?.openDropZone();
  };
  const renderTooltipInfo = props => (
    <Tooltip {...props}>Bắt buộc đính kèm GYC nếu chọn "Thanh toán" ở bước 4</Tooltip>
  );
  let iconInfor = <OverlayTrigger placement="right" overlay={renderTooltipInfo}>
    <span className="icon" style={{ cursor: 'pointer', color: 'green' }}>
      <i className="fas fa-info-circle" />
    </span>
  </OverlayTrigger>
  return (
    <Form ref={formRef} noValidate validated={validateForm}>
      <div className="header-content-step-download">
        <label>Tải giấy yêu cầu bảo hiểm:</label>
        <div
          className="btn-download-document"
          onClick={() => props.getCertificate()}
        >
          Tải về
        </div>
      </div>
      <Row className="form-group">
        {listUpload &&
          listUpload.map((vl, i) => {
            let dataFile = [];
            switch (vl?.type) {
              case "GYC":
                dataFile = listGyc;
                break;
              case "KHAC":
                dataFile = listKhac;
                break;
              default:
                break;
            }
            return (
              <Col md={4} key={i}>
                <Form.Group className="form-group-download">
                  <Form.Label>
                    {vl.title}
                    &nbsp;
                    {(dataFile?.length && dataFile?.length > 3) ? `(${dataFile.length} files)` : null}
                    {vl.required ? (
                      <span style={{ color: "#DA2128" }}>*</span>
                    ) : null}
                    &nbsp;
                    {vl.type == 'GYC' && iconInfor}
                  </Form.Label>


                  {dataFile.length > 0 && (
                    <Form.Label
                      className="add-img"
                      onClick={() => openDropZone(vl?.uploadRef)}
                    >
                      Thêm File
                    </Form.Label>
                  )}
                  <Upload
                    idx={vl?.type}
                    setFileUpload={setFileUpload}
                    dataFile={dataFile}
                    ref={vl?.uploadRef}
                    typeUpload={isHDbLoFirefox() ? 'image/*' : "image/*, application/pdf, .doc,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document, .csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"}
                    labelDropZone="Chọn hoặc kéo file vào đây"
                    rejectedMessage="Vui lòng chọn lại file có định dạng ảnh (JPG, PNG...)"
                  />
                </Form.Group>
              </Col>
            );
          })}
      </Row>
    </Form>
  );
});

export default Main;
