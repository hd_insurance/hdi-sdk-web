import React, { createRef, forwardRef, useEffect, useImperativeHandle, useState } from "react";
import { Row, Col, Form, FormCheck } from "react-bootstrap";

import InfoBuyer from "../component/InfoBuyer";
import FLSelect from "../../component/FLSelect";
import FLInput from "../../component/FlInput";
import { config } from "./config-house";
import InfoCompany from "../component/InfoCompany";
import { checkFormatDate, isEmptyObj } from "../../service/Utils";
import moment from "moment";
import cogoToast from "cogo-toast";

const Main = forwardRef((props, ref) => {
    const formRef = createRef();
    const infoBuyerRef = createRef();
    const infoCompanyRef = createRef();
    const [validateForm, setValidateForm] = useState(false);
    const [errMes, setErrMes] = useState({});

    const [isInsured, setIsInsured] = useState();

    const [typeBillInfo, setTypeBillInfo] = useState(props?.infoBuyer?.TYPE_VAT || config?.bill_info?.no_bill);
    const [codeSeller, setCodeSeller] = useState(props?.infoBuyer?.SELLER?.SELLER_CODE || '');
    const [nameSeller, setNameSeller] = useState(props?.infoBuyer?.SELLER?.SELLER_NAME || '');
    const [mailSeller, setMailSeller] = useState(props?.infoBuyer?.SELLER?.SELLER_EMAIL || '');
    const [dataRight, setDataRight] = useState([]);

    useImperativeHandle(ref, () => ({
        handleSubmit() {
            const form = formRef.current;
            const data = infoBuyerRef?.current?.handleInfoBuyer();
            if (form.checkValidity() === false) {
                setValidateForm(true);
                return false;
            }
            if (data) {
                if (data.DOB && !checkFormatDate(data.DOB)) {
                    cogoToast.error('Lỗi định dạng ngày DD/MM/YYYY')
                    return false;
                }
                return {
                    CUS_ID: "",
                    TYPE: "CN",
                    TYPE_VAT: typeBillInfo,
                    NATIONALITY: "",
                    NAME: data.NAME,
                    DOB: data.DOB,
                    GENDER: data.GENDER,
                    PROV: data.PROV,
                    DIST: data.DIST,
                    WARDS: data.WARDS,
                    ADDRESS: data.ADDRESS,
                    ADDRESS_FORM: data.ADDRESS_FORM,
                    IDCARD: data.IDCARD,
                    EMAIL: data.EMAIL,
                    PHONE: data.PHONE,
                    FAX: "",
                    TAXCODE: "",
                    SELLER: {
                        SELLER_CODE: props?.sdk_params?.org_code === 'HDI' ? props?.sdk_params?.user_name : codeSeller,
                        ORG_CODE: props?.sdk_params?.org_code,
                        SELLER_NAME: props?.sdk_params?.org_code === 'HDI' ? props?.sdk_params?.user_name : nameSeller,
                        SELLER_EMAIL: mailSeller,
                        SELLER_PHONE: "",
                        SELLER_GENDER: "",
                        ORG_TRAFFIC: "",
                        TRAFFIC_LINK: "",
                        ENVIROMENT: "WEB"
                    },
                    BILL_INFO: typeBillInfo === config?.bill_info?.no_bill ? {} : {
                        TYPE: 'CN',
                        NAME: data.NAME,
                        DOB: data.NAME,
                        GENDER: data.NAME,
                        PROV: data.PROV,
                        DIST: data.DIST,
                        WARDS: data.WARDS,
                        ADDRESS: data.ADDRESS,
                        IDCARD: data.NAME,
                        EMAIL: data.EMAIL,
                        PHONE: data.PHONE,
                        FAX: "",
                        TAXCODE: "",
                    },
                    RELATIONSHIP: isInsured,
                    LAND_USE_RIGHTS: data.LAND_USE_RIGHTS, // QUyen so huu can nha
                }
            }
        },
    }));

    const onChangeInsured = () => {
        setIsInsured(!isInsured)
    }

    const onChangeBillInfo = (e) => {
        setTypeBillInfo(e.target.value);
    };

    useEffect(() => {
        if (props?.sdk_params.org_code == 'HDBANK_VN' && props?.sdk_params.partner == 'LO_HDB') {
            let _t = props?.dataDefine ? props?.dataDefine[4]?.filter(p => p.value == 'O') : [] // chu nha
            setDataRight(_t);
        } else {
            setDataRight(props?.dataDefine ? props?.dataDefine[4] : [])
        }
    }, [props?.sdk_params]);

    useEffect(() => {
        if(props?.infoBuyer?.RELATIONSHIP !== undefined && props?.infoBuyer?.RELATIONSHIP !== null){
            setIsInsured(props?.infoBuyer?.RELATIONSHIP)
        } else {
            setIsInsured(true)
        }
    }, [props?.infoBuyer]);

    const isHdBankLO = () => {
        let _result = false;
        if (props?.sdk_params.action == 'EDIT' && props?.sdk_params.org_code == 'HDBANK_VN' && props?.sdk_params.partner == 'LO_HDB') {
            _result = true;
        }
        return _result;
    }
    return (
        <Form
            ref={formRef}
            noValidate
            validated={validateForm}
        >
            <div className="row form-group">
                <FormCheck custom type="switch">
                    <FormCheck.Input checked={isInsured} />
                    <FormCheck.Label onClick={onChangeInsured}>
                        Người mua bảo hiểm là người được bảo hiểm
                    </FormCheck.Label>
                </FormCheck>
            </div>

            <InfoBuyer
                ref={infoBuyerRef}
                dataGender={props?.dataDefine ? props?.dataDefine[3] : []}
                dataRight={dataRight || props?.dataDefine[4]}
                isPLus={isInsured}
                vat={typeBillInfo}
                buyer={props?.infoBuyer}
                errorsMess={errMes}
                isHdbankLO={isHdBankLO()}
            />

            <div className="row form-choose-type">
                <p className="col-12 col-md-2 col-xl-2 col-sm-12">Có lấy hóa đơn VAT không?</p>
                <div className="col-12 col-md-10 col-xl-10 col-sm-12 rd-group">
                    {props?.dataDefine[10] ? (
                        props?.dataDefine[10].map((vl, i) => {
                            return (
                                <label key={i} className="form-check" style={{ color: typeBillInfo === vl.value ? '#329945' : '#777777' }}>
                                    <input className="form-check-input" type="radio" value={vl.value}
                                        onChange={onChangeBillInfo}
                                        checked={typeBillInfo === vl.value} />
                                    {vl.label}
                                </label>
                            )
                        })
                    ) : null}
                </div>
            </div>

            {/*{typeBillInfo === "E" && <InfoCompany ref={infoCompanyRef} />}*/}
            {props?.sdk_params?.org_code !== 'HDI' && (
                <>
                    <div className="form-content">
                        <label className="col-form-label">
                            Thông tin người cấp đơn
                        </label>
                    </div>
                    <Row className='form-group'>
                        <Col md={4}>
                            <FLInput
                                label="Mã cán bộ cấp đơn"
                                value={codeSeller}
                                changeEvent={setCodeSeller}
                                required={true}
                                disable={isHdBankLO()}
                            />
                        </Col>
                        <Col md={4}>
                            <FLInput
                                label='Tên cán bộ cấp đơn '
                                value={nameSeller}
                                changeEvent={setNameSeller}
                                required={true}
                                disable={isHdBankLO()}
                            />
                        </Col>
                        <Col md={4}>
                            <FLInput
                                label='Mail cán bộ cấp đơn'
                                value={mailSeller}
                                changeEvent={setMailSeller}
                                required={false}
                                disable={isHdBankLO()}
                            />
                        </Col>
                    </Row>
                </>
            )}
        </Form>
    )
});

export default (Main);
