import React, { useEffect, useState, useRef, createRef } from "react";

import Step1 from "./step1";
import Step2 from "./step2";
import Step3 from "./stepUploadFile";
import Step4 from "./step4";
import NextWorkNew from "../../service/Network1";
import { getLabelAddress, ModalCK, numberFormatCommas, numberFormatDot, showDialogDis } from "../../service/Utils";
import LoadingForm from "../../component/Loading";
import DataTable from "../../component/DataTable";
import { config } from "./config-house";
import { Form, FormCheck } from "react-bootstrap";
import FLSelect from "../../component/FLSelect";
import cogoToast from "cogo-toast";
import { Col, Row } from "react-bootstrap";
import Upload from "../../component/UploadFile/index";
import { Tooltip, OverlayTrigger } from "react-bootstrap";
import axios from "axios";
import moment from "moment";

const apiNew = new NextWorkNew();


const Main = (props) => {
    const F1Ref = createRef();
    const F2Ref = createRef();
    const F3Ref = createRef();
    const F4Ref = createRef();
    const formRef = createRef();

    const [validateForm, setValidateForm] = useState(false);

    const [isPayment, setIsPayment] = useState(false);
    const [paymentType, setPaymentType] = useState("");
    const [showModalCK, setShowModalCK] = useState(false);
    const [valueSMS, setValueSMS] = useState("");


    const [step, setStep] = useState(1);
    const [editStep, setEditStep] = useState(1);

    const [checkEdit, setCheckEdit] = useState(false);

    const [loading, setLoading] = useState(false);

    const [flagUrl, setFlagUrl] = useState("0");

    const [infoBuyer, setInfoBuyer] = useState({});

    const [infoHouse, setInfoHouse] = useState([]);

    const [dataDefine, setDataDefine] = useState([]);

    const [payStatus, setPayStatus] = useState("");

    const [totalAmount, setTotalAmount] = useState(0);

    const [listFile, setListFile] = useState({});
    const [listFileUploaded, setListFileUploaded] = useState([]);

    const [userEditPermission, setUserEditPermission] = useState(true);

    const config_define = {
        PRODUCT: props?.sdk_params?.product_code,
        CHANNEL: "SDK_HOUSE_SELL",
        CATEGORY: props?.sdk_params?.category,
        ACTION: props.sdk_params.action === 'ADD' ? "BH_M" : "BH_S",
        USERNAME: "Seller_TN",
        ORG_CODE: "HDI",
    };

    useEffect(() => {
        getdefine();
    }, []);

    const getdefine = async () => {
        const data = {
            org_code: props?.sdk_params?.org_code || 'HDI',
            channel: props?.sdk_params?.channel || null,
            product_code: props?.sdk_params?.product_code || null,
        }
        const response = await apiNew.post("/api/get-define", data);
        setDataDefine(response);
    }

    useEffect(() => {
        if (props.sdk_params) {
            if (props.sdk_params.action === 'ADD') {
                setCheckEdit(true);
                setInfoBuyer({});
                setInfoHouse({});
                setListFile({});
                setPayStatus("");
                setPaymentType("");
                setStep(0);
                setEditStep(0);
            } else if (props.sdk_params.action === 'EDIT') {
                setFlagUrl("0")
                setStep(3);
                setEditStep(3);
                getDetailHouse();
                // if(props?.sdk_params?.user_permission){
                //     let parseJs = JSON.parse(props?.sdk_params?.user_permission);
                //     console.log('parseJs', parseJs);
                // }
            }

            if (props?.sdk_params?.user_permission?.CREATE_ORDER == 0 || props?.sdk_params?.user_permission?.PER_VIEW == 0) {
                setUserEditPermission(false);
            } else {
                setUserEditPermission(true);
            }
        }

    }, [props.sdk_params]);

    useEffect(() => {
        setTotalAmount(infoHouse?.HOUSE_PRODUCT?.FEES_DATA);
    }, [infoHouse])

    const submitStep1 = (e) => {
        return F1Ref.current.handleSubmit();
    };
    const submitStep2 = () => {
        return F2Ref.current.handleSubmit();
    }
    const submitStep3 = () => {
        return F3Ref.current.handleSubmit();
    }
    const submitStep4 = () => {
        return F4Ref.current.handleSubmit();
    }

    const onNextStep = async (e) => {
        try {
            e.preventDefault();
            switch (step) {
                case 0:
                    if (submitStep1()) {
                        setInfoBuyer(submitStep1());
                        // console.log('data Step1 =======>', submitStep1());
                        setStep(1);
                        setEditStep(1);
                    }
                    break;
                case 1:
                    // console.log('data Step2 =======>', submitStep2());
                    if (submitStep2()) {
                        setInfoHouse(submitStep2());
                        setStep(2);
                        setEditStep(2);
                    }
                    break;
                case 2:
                    // console.log('data Step3 =======>', submitStep3());
                    const _lstFile = submitStep3();
                    if (_lstFile) {
                        /**
                         * [{
                            "FILE_TYPE": "GYC",
                            "FILE_NAME": "GYC_1672738386206.pdf",
                            "FILE_ID": "5fcdf976b5a7d3d5d0788fc0b3f9aaa4.pdf",
                            "IS_DEL": "0",
                            "FILE_FORMAT": "pdf"
                        }]
                         */
                        const resultFiles = await uploadfile(_lstFile);
                        setListFileUploaded(resultFiles);
                        setListFile(_lstFile);
                        setStep(3);
                        setEditStep(3);
                    }
                    break;
                case 3:
                    const form = formRef.current;
                    if (form.checkValidity() === false) {
                        setValidateForm(true);
                        return false;
                    }
                    const _listGyc = listFileUploaded?.filter(p => p.FILE_TYPE === 'GYC');
                    if (isPayment && (!_listGyc || _listGyc.length === 0)) {
                        cogoToast.error("Vui lòng thêm file giấy yêu cầu bảo hiểm ở bước 3");
                        return false;
                    }
                    await createOrder();
                    break;
                default:
                    break;
            }
        } catch (error) {
            setLoading(true);
            console.log(error);
        }
    }

    const handleEditStep = (vl) => {
        setStep(vl);
        setEditStep(vl);
    };
    const appendFile = (listFile) => {
        return new Promise((resolve, reject) => {
            let formData = new FormData();
            listFile.forEach((file) => {
                formData.append("files", file.rawFile, file.name);
            });
            resolve(formData);
        });
    };
    const uploadfile = async (lstFile) => {
        try {
            setLoading(true);
            const url = `${process.env.REACT_APP_DM1}/upload/`;
            const config = {
                headers: {
                    ParentCode: "HDI_UPLOAD",
                    UserName: "HDI_UPLOAD",
                    Secret: "HDI_UPLOAD_198282911FASE1239212",
                    environment: "LIVE",
                    DeviceEnvironment: "WEB",
                    ActionCode: "UPLOAD_SIGN",
		            'Access-Control-Allow-Origin': '*',
                },
            };
            const files = [
                ...lstFile.list_gyc,
                ...lstFile.list_khac,
            ];
            const filesUploaded = files
                .filter((f) => !f.rawFile)
                .map((e) => {
                    // map instead of delete property
                    const file_extension = e.FILE_NAME.split(".").pop();
                    return {
                        FILE_TYPE: e.FILE_TYPE,
                        FILE_NAME: e.FILE_NAME,
                        FILE_ID: e.FILE_ID,
                        IS_DEL: "0",
                        FILE_FORMAT: file_extension,
                    };
                });
            // console.log(filesUploaded);
            const filesUpload = files.filter((f) => f.rawFile);

            const data = await appendFile(filesUpload);
            try {
                const rs = await axios.post(url, data, config);
                // console.log('axios ', rs);
                const result = (rs?.data?.data || []).map((file) => {
                    const file_extension = file?.file_org_name?.split(".").pop();
                    const type = file?.file_org_name?.split("_")[0];
                    return {
                        FILE_TYPE: type,
                        FILE_NAME: file.file_org_name,
                        FILE_ID: file.file_key,
                        IS_DEL: "0",
                        FILE_FORMAT: file_extension,
                    };
                });
                setLoading(false);
                return [...filesUploaded, ...result];
            } catch (error) {
                setLoading(true);
                console.log(error);
                return [];
            }
        } catch (error) {
            setLoading(true);
            console.log(error);
        }
    };

    const createOrder = async () => {
        setLoading(true);
        const SELLER = infoBuyer?.SELLER;
        const PAY_INFO = {
            PAYMENT_TYPE: isPayment ? paymentType : "",
        };
        const BUYER = {
            TYPE: infoBuyer?.TYPE,
            NAME: infoBuyer?.NAME,
            DOB: infoBuyer?.DOB,
            GENDER: infoBuyer?.GENDER,
            PROV: infoBuyer?.PROV,
            DIST: infoBuyer?.DIST,
            WARDS: infoBuyer?.WARDS,
            ADDRESS: infoBuyer?.ADDRESS,
            IDCARD: infoBuyer?.IDCARD,
            EMAIL: infoBuyer?.EMAIL,
            PHONE: infoBuyer?.PHONE,
            FAX: "",
            TAXCODE: "",
            TYPE_VAT: infoBuyer?.TYPE_VAT,
        };
        const BILL_INFO = infoBuyer?.BILL_INFO;

        let HOUSE_INSUR = [{ ...infoHouse }];
        HOUSE_INSUR[0].FILE = listFileUploaded.length > 0 ? listFileUploaded : null;
        // console.log('resultFiles', resultFiles);

        const data = {
            SELLER: SELLER,
            BUYER: BUYER,
            HOUSE_INSUR: HOUSE_INSUR,
            PAY_INFO: PAY_INFO,
            // FILE: resultFiles.length > 0 ? resultFiles : null,
            BILL_INFO: BILL_INFO,
            URL: isPayment
                ? "/OpenApi/v1/mask/insur/create_pay"
                : "/OpenApi/v1/mask/insur/create",
        };
        try {
            const rs = await apiNew.post(
                `/api/createOrder/${config_define.ACTION}/${config_define.PRODUCT}/${config_define.CHANNEL}/${config_define.CATEGORY}/${config_define.USERNAME}`,
                data
            );
            if (rs.Success) {
                if (!isPayment) {
                    if (props.sdk_params.action === "ADD") {
                        showDialogDis("Lưu đơn thành công");
                    }
                    if (props.sdk_params.action === "EDIT") {
                        showDialogDis("Sửa đơn thành công");
                    }
                }
                if (rs.Data.url_redirect && rs.Data.url_redirect?.toString()?.trim() !== '') {
                    if (paymentType === "CK" && rs?.Data?.[0]?.[0]) {
                        setShowModalCK(true);
                        setValueSMS(rs.Data[0][0].CONTENT_PAY);
                    }
                    window.open(
                        `${rs.Data.url_redirect}&callback=${window.location.origin}`,
                        "_self"
                    );
                }
                if (paymentType === "TM" || paymentType === "TH") {
                    showDialogDis("Thanh toán thành công");
                }
                props?.sdk_params?.onClose((data) => {
                    return data;
                });
                return rs?.Data;
            } else {
                setLoading(false);
                showDialogDis(rs.ErrorMessage);
            }
        } catch (error) {
            console.log("err create order", error);
            return false;
        }
    };
    const checkRelationship = (mqh) => {
        if (mqh === 'BAN_THAN') {
            return true;
        }
        if (!mqh && props?.sdk_params.action == 'EDIT' && props?.sdk_params.org_code == 'HDBANK_VN' && props?.sdk_params.partner == 'LO_HDB') {
            return true;
        }
        return false;
    }
    const getDetailHouse = async () => {
        setLoading(true);
        try {
            const data = {
                channel: props.sdk_params.channel,
                username: props.sdk_params?.user_name,
                language: "VN",
                category: props.sdk_params?.category,
                product_code: props.sdk_params?.product_code,
                contract_code: props.sdk_params?.contract_code,
                detail_code: props.sdk_params?.detail_code,
                org_code: props.sdk_params?.org_code,
            };
            const response = await apiNew.post("/api/get-detail", data);
            if (response.Success) {
                setLoading(false);
                if (response.Data) {
                    let rs = response.Data;
                    let mqh = rs?.HOUSE_INSUR[0]?.RELATIONSHIP;
                    let infoHouse = rs?.HOUSE_INSUR[0];
                    if(props?.sdk_params?.org_code === 'HDBANK_VN') {
                        rs.SELLER.ORG_CODE = props?.sdk_params?.org_code;
                    }
                    setInfoBuyer({
                        ...infoBuyer,
                        TYPE: "CN",
                        TYPE_VAT: rs?.BUYER?.TYPE_VAT,
                        NATIONALITY: "",
                        NAME: rs?.BUYER?.NAME,
                        DOB: rs?.BUYER?.DOB,
                        GENDER: rs?.BUYER?.GENDER,
                        PROV: rs?.BUYER?.PROV,
                        DIST: rs?.BUYER?.DIST,
                        WARDS: rs?.BUYER?.WARDS,
                        ADDRESS: rs?.BUYER?.ADDRESS,
                        ADDRESS_FORM: rs?.BUYER?.ADDRESS_FORM,
                        IDCARD: rs?.BUYER?.IDCARD,
                        EMAIL: rs?.BUYER?.EMAIL,
                        PHONE: rs?.BUYER?.PHONE,
                        FAX: "",
                        TAXCODE: "",
                        SELLER: rs?.SELLER,
                        BILL_INFO: rs?.BILL_INFO,
                        RELATIONSHIP: checkRelationship(mqh),
                        LAND_USE_RIGHTS: rs?.HOUSE_INSUR[0]?.LAND_USE_RIGHTS
                    });
                    setTotalAmount(infoHouse?.HOUSE_PRODUCT?.FEES_DATA);
                    if (!infoHouse?.HOUSE_PRODUCT?.FEES_DATA || infoHouse?.HOUSE_PRODUCT?.FEES_DATA == 0) {
                        handleEditStep(0);
                    }
                    setInfoHouse(infoHouse);
                    filterFile(infoHouse?.FILE);
                    setPayStatus(rs?.STATUS);
                    if (rs?.STATUS === 'WAIT_PAY') {
                        setCheckEdit(true);
                    } else {
                        setCheckEdit(false);
                    }

                    if (infoHouse?.HOUSE_PRODUCT?.EFF && infoHouse?.HOUSE_PRODUCT?.EXP) {
                        let effDate = moment(infoHouse?.HOUSE_PRODUCT?.EFF, 'DD/MM/YYYY');
                        let expDate = moment(infoHouse?.HOUSE_PRODUCT?.EXP, 'DD/MM/YYYY');
                        let _months = effDate.diff(expDate, "months", true)
                        infoHouse.HOUSE_PRODUCT.INSUR_DURATION = Math.abs(_months);
                    }

                }
                // console.log(response.Data);
            }
        } catch (err) {
            console.log("Loi get detail", err);
        }
    };
    const filterFile = (files) => {
        let list_gyc = [];
        let list_khac = [];
        if (!files || files.length == 0) {
            setListFile({
                list_gyc: list_gyc,
                list_khac: list_khac,
            });
            return;
        };
        files.forEach((f) => {
            const type =
                f.FILE_NAME.split(".").pop() == "pdf"
                    ? "pdf"
                    : f.FILE_NAME.split(".").pop();
            const file = {
                ...f,
                preview: apiNew.domainViewFile + f.FILE_ID,
                type: type,
            };
            if (file.FILE_TYPE == "GYC") {
                list_gyc.push(file);
            }
            if (file.FILE_TYPE == "KHAC") {
                list_khac.push(file);
            }
        });
        setListFile({
            list_gyc: list_gyc,
            list_khac: list_khac,
        });
        setListFileUploaded(files);
    };
    const formatBenefit = (pkinfo) => {
        if (!pkinfo) return {};
        return {
            label: pkinfo.label,
            link: `${pkinfo.URL_BEN}`,
            description: JSON.parse(pkinfo.DESCRIPTION),
        };
    };

    function getInputCertificate(org) {
        // BUYER_NAME;BUYER_ADDRESS;INSUR_NAME;CONTACT_ADDRESS;PURPOSE;INSUR_ASSET;INSUR_ADDRESS;TIME_EFF;EFF;TIME_EXP;EXP;INSUR_HOUSE_VALUE;HOUSE_VALUE_REF;BENEFICIARY_NAME;BENEFICIARY_ADDRESS;LOCAL;DATE;MONTH;YEAR;A;A1
        let _BUYER_ADDRESS = infoBuyer.ADDRESS;
        if (infoBuyer.ADDRESS_FORM) {
            _BUYER_ADDRESS += ', ' + getLabelAddress(infoBuyer.ADDRESS_FORM);
        }
        let _CONTACT_ADDRESS = infoHouse.ADDRESS;
        if (infoHouse.ADDRESS_FORM) {
            _CONTACT_ADDRESS += ', ' + getLabelAddress(infoHouse.ADDRESS_FORM);
        }
        let _INSUR_ADDRESS = infoHouse.HOUSE_INFO.ADDRESS;
        if (infoHouse.HOUSE_INFO.ADDRESS_FORM) {
            _INSUR_ADDRESS += ', ' + getLabelAddress(infoHouse.HOUSE_INFO.ADDRESS_FORM);
        }
        let _result = {
            PRODUCT_CODE: config_define.PRODUCT,
            TEMP_CODE: `${config_define.PRODUCT}_VIEW`,
            PACK_CODE: null, // Goi bao hiem de null
            DATA_TYPE: "JSON",
            JSON_DATA: {
                BUYER_NAME: infoBuyer.NAME,
                BUYER_ADDRESS: _BUYER_ADDRESS,
                INSUR_NAME: infoHouse.NAME,
                CONTACT_ADDRESS: _CONTACT_ADDRESS,
                PURPOSE: "Dùng để ở",
                INSUR_ASSET: infoHouse.HOUSE_INFO.ADDRESS,
                INSUR_ADDRESS: _INSUR_ADDRESS,
                TIME_EFF: infoHouse.HOUSE_PRODUCT.TIME_EFF,
                EFF: infoHouse.HOUSE_PRODUCT.EFF,
                TIME_EXP: infoHouse.HOUSE_PRODUCT.TIME_EXP,
                EXP: infoHouse.HOUSE_PRODUCT.EXP,
                HOUSE_VALUE_REF: numberFormatDot(infoHouse.HOUSE_PRODUCT.HOUSE_VALUE_REF),
                INSUR_HOUSE_VALUE: numberFormatDot(infoHouse.HOUSE_PRODUCT.INSUR_HOUSE_VALUE),
                BENEFICIARY_NAME: infoHouse?.BENEFICIARY.length > 0 ? (props?.sdk_params?.org_code == 'HDBANK_VN' ? `NGÂN HÀNG TMCP PHÁT TRIỂN TP. HỒ CHÍ MINH (HDBANK) - ${infoHouse.BENEFICIARY[0].NAME}` : infoHouse.BENEFICIARY[0].NAME) : '',
                BENEFICIARY_ADDRESS: infoHouse?.BENEFICIARY.length > 0 ? infoHouse?.BENEFICIARY[0].ADDRESS : '',
                A: false, // Mặc định để False
                A1: false,
                LOCAL: "........", // Để dấu........
                DATE: "........", // Để dấu........
                MONTH: "........", // Để dấu........
                YEAR: "........", // Để dấu........: 
            }
        }
        // switch (org) {
        //     case 'HDBANK_VN':
        //         // BUYER_NAME;BUYER_ADDRESS;INSUR_NAME;CONTACT_ADDRESS;PURPOSE;INSUR_ASSET;INSUR_ADDRESS;TIME_EFF;EFF;TIME_EXP;EXP;INSUR_HOUSE_VALUE;INSUR_MONEY;HDBANK_NAME;HDBANK_ADDRESS;LOCAL;DATE;MONTH;YEAR;A;A1
        //         _result = {
        //             PRODUCT_CODE: config_define.PRODUCT,
        //             TEMP_CODE: `HDBANK_VN_${config_define.PRODUCT}_VIEW`,
        //             PACK_CODE: null, // Goi bao hiem de null
        //             DATA_TYPE: "JSON",
        //             JSON_DATA: {
        //                 BUYER_NAME: infoBuyer.NAME,
        //                 BUYER_ADDRESS: getLabelAddress(
        //                     infoBuyer.ADDRESS_FORM,
        //                     infoBuyer.ADDRESS
        //                 ),
        //                 INSUR_NAME: infoHouse.NAME,
        //                 CONTACT_ADDRESS: getLabelAddress(
        //                     infoHouse.ADDRESS_FORM,
        //                     infoHouse.ADDRESS
        //                 ),
        //                 PURPOSE: "Dùng để ở",
        //                 INSUR_ASSET: infoHouse.HOUSE_PRODUCT.INSUR_HOUSE_ASSETS, // nguoi mua buoc 1
        //                 INSUR_ADDRESS: getLabelAddress(infoHouse.HOUSE_INFO.ADDRESS_FORM, infoHouse.HOUSE_INFO.ADDRESS), // nguoi mua buoc 1
        //                 TIME_EFF: infoHouse.HOUSE_PRODUCT.TIME_EFF,
        //                 EFF: infoHouse.HOUSE_PRODUCT.EFF,
        //                 TIME_EXP: infoHouse.HOUSE_PRODUCT.TIME_EXP,
        //                 EXP: infoHouse.HOUSE_PRODUCT.EXP,
        //                 INSUR_HOUSE_VALUE: infoHouse.HOUSE_PRODUCT.INSUR_HOUSE_VALUE,
        //                 INSUR_MONEY: infoHouse.HOUSE_PRODUCT.FEES_DATA,
        //                 HDBANK_NAME: '', // todo
        //                 HDBANK_ADDRESS: '', //todo
        //                 A: false, // Mặc định để False
        //                 A1: false,
        //                 LOCAL: "........", // Để dấu........
        //                 DATE: "........", // Để dấu........
        //                 MONTH: "........", // Để dấu........
        //                 YEAR: "........", // Để dấu........: 
        //             }
        //         }
        //         break;
        //     default:
        //         // NAME;DOB;IDCARD;IDCARD_DATE;IDCARD_PLACE;ADDRESS;PHONE;EMAIL;A;A1;B;B1;C;C1;LOCAL;DATE;MONTH;YEAR;[DS_DK]
        //         _result = {
        //             PRODUCT_CODE: config_define.PRODUCT,
        //             TEMP_CODE: `${config_define.PRODUCT}_VIEW`, // PRODUCT_CODE + "_VIEW"
        //             PACK_CODE: null, // Goi bao hiem de null
        //             DATA_TYPE: "JSON",
        //             JSON_DATA: {
        //                 NAME: infoBuyer.NAME, // nguoi mua buoc 1
        //                 DOB: infoBuyer.DOB, // nguoi mua buoc 1
        //                 IDCARD: infoBuyer.IDCARD, // nguoi mua buoc 1
        //                 IDCARD_DATE: "",
        //                 IDCARD_PLACE: "",
        //                 ADDRESS: getLabelAddress(
        //                     infoBuyer.ADDRESS_FORM,
        //                     infoBuyer.ADDRESS
        //                 ), // nguoi mua buoc 1
        //                 PHONE: infoBuyer.PHONE, // nguoi mua buoc 1
        //                 EMAIL: infoBuyer.EMAIL, // nguoi mua buoc 1
        //                 A: false, // Mặc định để False
        //                 A1: false,
        //                 B: false,
        //                 B1: false,
        //                 C: false,
        //                 C1: false,
        //                 LOCAL: "........", // Để dấu........
        //                 DATE: "........", // Để dấu........
        //                 MONTH: "........", // Để dấu........
        //                 YEAR: "........", // Để dấu........
        //                 DS_DK: (infoHouse || []).map((e, i) => {
        //                     const pkgInfo = dataDefine[2]?.filter(
        //                         (p) => p.value === e.PACK_CODE
        //                     );
        //                     const formatedPack = formatBenefit(pkgInfo[0]);
        //                     return {
        //                         STT: i,
        //                         NAME: e.NAME,
        //                         DOB: e.DOB,
        //                         IDCARD: e.IDCARD,
        //                         ADDRESS: getLabelAddress(e.ADDRESS_FORM, e.ADDRESS),
        //                         EFFECTIVE_DATE: `${e.EFFECTIVE_DATE} - ${e.EXPIRATION_DATE}`,
        //                         BENEFITS: formatedPack?.description[0]?.REGION,
        //                         PACK_NAME: formatedPack?.label,
        //                     };
        //                 }),
        //             },
        //         };
        //         break;
        // };

        return _result
    }

    const getCertificate = async () => {
        // console.log('infoBuyer', infoBuyer)
        // console.log('infoHouse', infoHouse)
        setLoading(true);
        try {
            const data = getInputCertificate(props?.sdk_params?.org_code);
            const res = await apiNew.post("/api/view-certificate", data);
            if (res.Success) {
                let link = document.createElement("a");
                link.setAttribute(
                    "href",
                    "data:application/pdf;base64," + res.Data.FILE_BASE64
                );
                link.setAttribute("download", res.Data.ID_FILE);
                link.click();
                setLoading(false);
                // const pdfWindow = window.open("");
                // pdfWindow?.document?.write(
                //   "<iframe frameborder='0' style='border:0; top:0px; left:0px; bottom:0px; right:0px; position: absolute;' width='100%' height='100%' src='data:application/pdf;base64, " +
                //     encodeURI(data) +
                //     "'></iframe>"
                // );
                // console.log(res);
            }
        } catch (error) {
            setLoading(false);
            cogoToast.error("Có lỗi xảy ra");
            console.log(error);
        }
    };

    const handlePayment = () => {
        if (formRef?.current) {
            formRef?.current?.scrollIntoView({
                // top: formRef?.current.offsetTop,
                left: 0,
                behavior: "smooth",
            });
        }
    };

    const previewGCN = () => {
        let rs1 = infoHouse?.FILE[0]?.FILE_ID;
        if (rs1) {
            window.open(apiNew.domainViewFile + rs1, "_blank");
        } else {
            const mess = 'Chưa có Giấy Chứng Nhận!'
            showDialogDis(mess);
        }
    };

    const summaryStep1 = () => {
        let labelAddress = infoBuyer?.ADDRESS;
        if (infoBuyer?.ADDRESS_FORM) {
            labelAddress += ', ' + getLabelAddress(infoBuyer?.ADDRESS_FORM);
        }
        return (
            <div className="unactive-step-1 typing-value-form">
                <div className="row">
                    <div className="col-4">
                        <label className="col-form-label">{infoBuyer?.GENDER === "M" ? "Ông" : "Bà"}:</label>
                        <label className="col-form-label">{infoBuyer?.NAME}</label>
                    </div>
                    <div className="col-4">
                        <label className="col-form-label">Số điện thoại:</label>
                        <label className="col-form-label">{infoBuyer?.PHONE}</label>
                    </div>
                    <div className="col-4">
                        <label className="col-form-label">Email:</label>
                        <label className="col-form-label">{infoBuyer?.EMAIL}</label>
                    </div>
                    <div className="col-4">
                        <label className="col-form-label">CMND/CCCD/Hộ chiếu:</label>
                        <label className="col-form-label">{infoBuyer?.IDCARD}</label>
                    </div>
                    <div className="col-4">
                        <label className="col-form-label">Ngày sinh:</label>
                        <label className="col-form-label">{infoBuyer?.DOB}</label>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12">
                        <label className="col-form-label">Địa chỉ:</label>
                        <label className="col-form-label">{labelAddress}</label>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12">
                        <label className="col-form-label">Lấy hoá đơn VAT:</label>
                        <label className="col-form-label">{infoBuyer?.TYPE_VAT === 'N' ? 'Không' : 'Có'}</label>
                    </div>
                </div>
                <div className="row">
                    <div className="col-4">
                        <label className="col-form-label">Mã cán bộ cấp đơn:</label>
                        <label className="col-form-label">{infoBuyer?.SELLER?.SELLER_CODE}</label>
                    </div>
                    <div className="col-4">
                        <label className="col-form-label">Tên cán bộ cấp đơn:</label>
                        <label className="col-form-label">{infoBuyer?.SELLER?.SELLER_NAME}</label>
                    </div>
                    <div className="col-4">
                        <label className="col-form-label">Mail cán bộ cấp đơn:</label>
                        <label className="col-form-label">{infoBuyer?.SELLER?.SELLER_EMAIL}</label>
                    </div>
                </div>
            </div>
        )
    }

    const getLabelForm = (dataDefine, value) => {
        if (dataDefine) {
            return dataDefine.find((vl) => {
                return vl.value === value;
            })
        }
    }

    const summaryStep2 = () => {
        let HOUSE_INFO = {};
        let HOUSE_PRODUCT = {};
        let BENEFICIARY = [];

        if (infoHouse) {
            HOUSE_INFO = infoHouse.HOUSE_INFO;
            HOUSE_PRODUCT = infoHouse.HOUSE_PRODUCT;
            BENEFICIARY = infoHouse.BENEFICIARY;
        }

        let lbTypeHouse = getLabelForm(dataDefine[0], HOUSE_INFO?.HOUSE_TYPE);
        let lbDebuctible = getLabelForm(dataDefine[1], HOUSE_PRODUCT?.INSUR_DEDUCTIBLE);
        let lbPack = getLabelForm(dataDefine[2], HOUSE_PRODUCT?.PACK_CODE);
        let lbQuyenSH = getLabelForm(dataDefine[4], infoHouse?.LAND_USE_RIGHTS);

        let lbBeneficiary = '', typeBenefi = null;
        let data = BENEFICIARY ? BENEFICIARY[0] : [];
        if (data?.TYPE === 'CN' && data?.RELATIONSHIP === 'KHAC') {
            lbBeneficiary = 'Người khác';
            typeBenefi = 'O';
        } else if (data?.TYPE === 'CN' && data?.RELATIONSHIP === 'BAN_THAN') {
            lbBeneficiary = 'Người được bảo hiểm';
            typeBenefi = 'I';

        } else {
            lbBeneficiary = 'Doanh nghiệp';
            typeBenefi = 'E';
        }
        let labelAdd1 = infoHouse?.ADDRESS || '', labelAdd2 = data?.ADDRESS || '';
        if (infoHouse?.ADDRESS_FORM) {
            labelAdd1 += ', ' + getLabelAddress(infoHouse?.ADDRESS_FORM);
        }
        if (data?.ADDRESS_FORM) {
            labelAdd2 += ', ' + getLabelAddress(data?.ADDRESS_FORM);
        }
        let _diaChiNgoiNha = HOUSE_INFO?.ADDRESS || '';
        if (HOUSE_INFO?.ADDRESS_FORM) {
            _diaChiNgoiNha += ', ' + getLabelAddress(HOUSE_INFO?.ADDRESS_FORM);
        }

        return (
            <div className="unactive-step-2 typing-value-form">
                <label>Thông tin ngôi nhà, gói bảo hiểm</label>
                <div className="row">
                    <div className="col-4">
                        <label className="col-form-label">Loại nhà:</label>
                        <label className="col-form-label">{lbTypeHouse?.label}</label>
                    </div>
                    <div className="col-4">
                        <label className="col-form-label">Diện tích:</label>
                        <label className="col-form-label">{(HOUSE_INFO?.HOUSE_AREA || '') + ' m2'}</label>
                    </div>
                    <div className="col-4">
                        <label className="col-form-label">Năm hoàn thành XD:</label>
                        <label className="col-form-label">Năm {HOUSE_INFO?.YEAR_COMPLETE}</label>
                    </div>
                    <div className="col-4">
                        <label className="col-form-label">Số tầng:</label>
                        <label className="col-form-label">{HOUSE_INFO?.NUM_FLOOR}</label>
                    </div>
                    <div className="col-4">
                        <label className="col-form-label">Số tiền BH phần xây dựng:</label>
                        <label className="col-form-label">{numberFormatCommas(HOUSE_PRODUCT?.INSUR_HOUSE_VALUE?.toString())} VNĐ</label>
                    </div>
                    <div className="col-4">
                        <label className="col-form-label">Số tiền BH tài sản bên trong:</label>
                        <label className="col-form-label">{numberFormatCommas(HOUSE_PRODUCT?.INSUR_HOUSE_ASSETS?.toString())} VNĐ</label>
                    </div>
                    <div className="col-4">
                        <label className="col-form-label">Mức miễn thường:</label>
                        <label className="col-form-label">{lbDebuctible?.label}</label>
                    </div>
                    <div className="col-4">
                        <label className="col-form-label">Giá trị xây dựng căn nhà:</label>
                        <label className="col-form-label">{numberFormatCommas(HOUSE_PRODUCT?.HOUSE_VALUE_REF?.toString())} VNĐ</label>
                    </div>
                    <div className="col-4">
                        <label className="col-form-label">Giá trị tài sản bên trong:</label>
                        <label className="col-form-label">{numberFormatCommas(HOUSE_PRODUCT?.ASSET_VALUE_REF?.toString())} VNĐ</label>
                    </div>
                </div>
                <div className="row">
                    <div className="col-8">
                        <label className="col-form-label">Địa chỉ:</label>
                        <label className="col-form-label">{_diaChiNgoiNha}</label>
                    </div>
                </div>
                <div className="row">
                    <div className="col-4">
                        <label className="col-form-label">Gói BH:</label>
                        <label className="col-form-label">{lbPack?.label}</label>
                    </div>
                    <div className="col-4">
                        <label className="col-form-label">Thời hạn bảo hiểm:</label>
                        <label className="col-form-label">{(HOUSE_PRODUCT?.EFF || '') + ' - ' + (HOUSE_PRODUCT?.EXP || '')}</label>
                    </div>
                    <div className="col-4">
                        <label className="col-form-label">Phí BH:</label>
                        <label className="col-form-label">{numberFormatCommas(HOUSE_PRODUCT?.FEES_DATA?.toString())} VNĐ</label>
                    </div>
                </div>
                <label>Thông tin người được bảo hiểm</label>
                <div className="row">
                    <div className="col-4">
                        <label className="col-form-label">{infoHouse?.GENDER === "M" ? "Ông" : "Bà"}:</label>
                        <label className="col-form-label">{infoHouse?.NAME}</label>
                    </div>
                    <div className="col-4">
                        <label className="col-form-label">Số điện thoại:</label>
                        <label className="col-form-label">{infoHouse?.PHONE}</label>
                    </div>
                    <div className="col-4">
                        <label className="col-form-label">Email:</label>
                        <label className="col-form-label">{infoHouse?.EMAIL}</label>
                    </div>
                    <div className="col-4">
                        <label className="col-form-label">CMND/CCCD/Hộ chiếu:</label>
                        <label className="col-form-label">{infoHouse?.IDCARD}</label>
                    </div>
                    <div className="col-4">
                        <label className="col-form-label">Ngày sinh:</label>
                        <label className="col-form-label">{infoHouse?.DOB}</label>
                    </div>
                    <div className="col-4">
                        <label className="col-form-label">Quyền với căn nhà:</label>
                        <label className="col-form-label">{lbQuyenSH?.label}</label>
                    </div>
                </div>
                <div className="row">
                    <div className="col-8">
                        <label className="col-form-label">Địa chỉ:</label>
                        <label className="col-form-label">{labelAdd1}</label>
                    </div>
                </div>
                <label>Thông tin người thụ hưởng</label>
                <div className="row">
                    <div className="col-4">
                        <label className="col-form-label">Người thụ hưởng:</label>
                        <label className="col-form-label">{lbBeneficiary}</label>
                    </div>
                    {typeBenefi !== 'E' && (
                        <div className="col-4">
                            <label className="col-form-label">{data?.GENDER === "M" ? "Ông" : "Bà"}:</label>
                            <label className="col-form-label">{data?.NAME}</label>
                        </div>
                    )}
                    {typeBenefi === 'E' && (
                        <div className="col-4">
                            <label className="col-form-label">Tên doanh nghiệp:</label>
                            <label className="col-form-label">{data?.NAME}</label>
                        </div>
                    )}
                    {typeBenefi === 'E' && (
                        <div className="col-4">
                            <label className="col-form-label">Mã số thuế:</label>
                            <label className="col-form-label">{data?.TAXCODE}</label>
                        </div>
                    )}

                    <div className="col-4">
                        <label className="col-form-label">Số điện thoại:</label>
                        <label className="col-form-label">{data?.PHONE}</label>
                    </div>
                    <div className="col-4">
                        <label className="col-form-label">Email:</label>
                        <label className="col-form-label">{data?.EMAIL}</label>
                    </div>
                    {typeBenefi !== 'E' && (
                        <div className="col-4">
                            <label className="col-form-label">CMND/CCCD/Hộ chiếu:</label>
                            <label className="col-form-label">{data?.IDCARD}</label>
                        </div>
                    )}
                </div>
                <div className="row">
                    {typeBenefi !== 'E' ? (
                        <div className="col-8">
                            <label className="col-form-label">Địa chỉ:</label>
                            <label className="col-form-label">{labelAdd2}</label>
                        </div>
                    ) : (
                        <div className="col-8">
                            <label className="col-form-label">Địa chỉ:</label>
                            <label className="col-form-label">{data?.ADDRESS}</label>
                        </div>
                    )}
                </div>
            </div>
        )
    }

    const summaryStep3 = () => {
        if (!listFile.list_gyc) {
            return <div></div>
        }
        const _listUpload = [
            {
                title: "Giấy yêu cầu bảo hiểm",
                required: false,
                type: "GYC",
                isReadOnly: true,
            },
            {
                title: "Các file khác",
                required: false,
                type: "KHAC",
                isReadOnly: true,
            },
        ];
        return <Row className="form-group">
            {_listUpload &&
                _listUpload.map((vl, i) => {
                    let dataFile = [];
                    switch (vl?.type) {
                        case "GYC":
                            dataFile = listFile.list_gyc;
                            break;
                        case "KHAC":
                            dataFile = listFile.list_khac;
                            break;
                        default:
                            break;
                    }
                    return (
                        <Col md={4} key={i}>
                            <Form.Group className="form-group-download">
                                <Form.Label>
                                    {vl.title}
                                    &nbsp;
                                    {dataFile?.length ? (dataFile.length === 1 ? '(1 file)' : `(${dataFile.length} files)`) : `(0 file)`}
                                </Form.Label>
                                {dataFile?.length > 0 && (
                                    <Upload
                                        idx={vl?.type}
                                        setFileUpload={() => { }}
                                        isReadOnly={vl?.isReadOnly}
                                        dataFile={dataFile}
                                    />
                                )}
                            </Form.Group>
                        </Col>
                    );
                })}
        </Row>
    }
    const renderTooltipInfo = props => (
        <Tooltip {...props}>Bắt buộc đính kèm GYC nếu chọn "Thanh toán"</Tooltip>
    );
    let iconInfor = <OverlayTrigger placement="right" overlay={renderTooltipInfo}>
        <span className="icon" style={{ cursor: 'pointer', color: 'green' }}>
            <i className="fas fa-info-circle" />
        </span>
    </OverlayTrigger>

    return (
        <div className="form-car">
            {loading && <LoadingForm />}

            <ModalCK
                showModalCK={showModalCK}
                setShowModalCK={setShowModalCK}
                valueSMS={valueSMS}
            />

            <div className="d-flex">
                {payStatus === 'WAIT_PAY' && (
                    <div className="btn-next-step btn-payment-tt" onClick={handlePayment}>
                        <i className="fas fa-cash-register" />
                        Thanh toán
                    </div>
                )}
                {payStatus === 'PAID' && (
                    <div className="btn-next-step btn-preview-gcn" onClick={previewGCN}>
                        <i className="fa fa-print" aria-hidden="true" />
                        Xem và in GCN
                    </div>
                )}
            </div>
            <div className="form-create-ord">
                <div className="step-vertical-timeline">
                    <div className="header-step-timeline">
                        <label className="title-step-timeline">Thông tin người mua</label>
                        {editStep !== 0 && checkEdit && (
                            <label className="edit-step-1" onClick={() => handleEditStep(0)}>Sửa</label>
                        )}
                    </div>
                    <div className="content-step-timeline">
                        <div className="active-step-1">
                            {step === 0 && editStep === 0 && (
                                <>
                                    <Step1
                                        ref={F1Ref}
                                        dataDefine={dataDefine}
                                        sdk_params={props?.sdk_params}
                                        infoBuyer={infoBuyer}
                                    />
                                    <button className="btn-next-step" onClick={e => onNextStep(e)} style={{ margin: 0 }}>
                                        Tiếp tục
                                    </button>
                                </>
                            )}
                            {editStep !== 0 && (
                                summaryStep1()
                            )}
                        </div>
                    </div>
                    <div className="number-step-timeline">1</div>
                </div>
                <div className="step-vertical-timeline">
                    <div className="header-step-timeline">
                        <label className="title-step-timeline">
                            Thông tin nhà, gói bảo hiểm, người được bảo hiểm, thụ hưởng
                        </label>
                        {(step === 2 || step === 3 || step === 4) && editStep !== 1 && checkEdit && (
                            <label className="edit-step-1" onClick={() => handleEditStep(1)}>Sửa</label>
                        )}
                    </div>
                    <div className="content-step-timeline">
                        {step === 1 && editStep === 1 && (
                            <>
                                <Step2
                                    ref={F2Ref}
                                    dataDefine={dataDefine}
                                    sdk_params={props?.sdk_params}
                                    buyer={infoBuyer}
                                    infoHouse={infoHouse}
                                />
                                <button
                                    className="btn-next-step"
                                    onClick={e => onNextStep(e)}
                                    style={{ margin: 0 }}
                                // disabled={disableBtn}
                                >
                                    Tiếp tục
                                </button>
                            </>
                        )}
                        {(step === 2 || step === 3 || step === 4) && editStep !== 1 && (
                            summaryStep2()
                        )}

                    </div>
                    <div className="number-step-timeline">2</div>
                </div>

                <div className="step-vertical-timeline">
                    <div className="header-step-timeline">
                        <label className="title-step-timeline">Upload File</label>
                        {step === 3 && editStep !== 2 && checkEdit && (
                            <label className="edit-step-2" onClick={() => handleEditStep(2)}>Sửa</label>
                        )}
                    </div>

                    <div className="content-step-timeline" style={{ maxWidth: "90vw" }}>
                        {step === 2 && editStep === 2 && (
                            <>
                                <Step3
                                    ref={F3Ref}
                                    dataDefine={dataDefine}
                                    sdk_params={props?.sdk_params}
                                    list_file={listFile}
                                    getCertificate={() => getCertificate()}
                                />
                                <button
                                    className="btn-next-step"
                                    onClick={e => onNextStep(e)}
                                    style={{ margin: 0 }}
                                // disabled={disableBtn}
                                >
                                    Tiếp tục
                                </button>
                            </>
                        )}
                        {(step !== 2 && editStep !== 2) && (
                            summaryStep3()
                        )}

                    </div>
                    <div className="number-step-timeline">3</div>
                </div>

                <div className="step-vertical-timeline">
                    <div className="header-step-timeline">
                        <label className="title-step-timeline">Thanh toán</label>
                        {step === 4 && editStep !== 2 && checkEdit && (
                            <label className="edit-step-2" onClick={() => handleEditStep(3)}>Sửa</label>
                        )}
                    </div>
                    {step === 3 && editStep === 3 && (
                        <div className="content-step-timeline">
                            <Step4
                                onNextStep={e => onNextStep(e)}
                                dataDefine={dataDefine}
                                // totalAmount={totalAmount}
                                sdk_params={props.sdk_params}
                                totalAmount={totalAmount}
                            />
                            {payStatus !== 'PAID' && (
                                <Form ref={formRef} noValidate validated={validateForm}>
                                    <div className="row form-group">
                                        <FormCheck custom type="switch">
                                            <FormCheck.Input
                                                checked={isPayment}
                                                onChange={() => setIsPayment(!isPayment)}
                                            />
                                            <FormCheck.Label onClick={() => { setIsPayment(!isPayment) }}>
                                                Thanh toán luôn
                                            </FormCheck.Label>
                                        </FormCheck>
                                        &nbsp;
                                        {isPayment && iconInfor}
                                    </div>

                                    {isPayment && (
                                        <div className="row form-group">
                                            <div className="col-3">
                                                <FLSelect
                                                    disable={false}
                                                    label="Hình thức thanh toán"
                                                    value={paymentType}
                                                    changeEvent={setPaymentType}
                                                    dropdown={true}
                                                    required={true}
                                                    positionSelect={"column"}
                                                    data={dataDefine && Array.isArray(dataDefine?.[11]) ? dataDefine?.[11] : []}
                                                />
                                            </div>
                                        </div>
                                    )}
                                    <button
                                        className="btn-next-step"
                                        onClick={e => onNextStep(e)}
                                        type="button"
                                        style={{ margin: 0, cursor: userEditPermission ? '' : 'not-allowed' }}
                                        disabled={!userEditPermission}
                                    >
                                        {isPayment ? "Lưu và thanh toán" : "Lưu"}
                                    </button>

                                    {!userEditPermission && <p style={{ color: '#DA2128' }}>Không được phân quyền</p>}

                                </Form>
                            )}
                        </div>
                    )}
                    {step === 4 && editStep !== 2 && (
                        <label>sum3</label>
                    )}
                    <div className="number-step-timeline">4</div>
                </div>
            </div>
        </div>
    )
}
export default Main;

// CONTRACT_CODE: "E163FFA50EC93D9EE0530100007F5090"
// DETAIL_CODE: "E163FFA50ECA3D9EE0530100007F5090"
// FILE_FORMAT: null
// FILE_ID: "578781905680923320ef165c45e1259d.pdf"
// FILE_NAME: "GYC_1655190972873.pdf"
// FILE_TYPE: "GYC"
// IS_DEL: null
// preview: "https://dev-hyperservices.hdinsurance.com.vn/f/578781905680923320ef165c45e1259d.pdf"
// type: "pdf"


// name: "GYC_1655195315684.pdf"
// path: "84a64eb6c48e43de8f6cfb2ee4041e56.pdf"
// preview: "blob:http://localhost:3000/a7b6655d-9c83-4f5b-bfe4-f6b2ca573615"
// rawFile: File {path: '84a64eb6c48e43de8f6cfb2ee4041e56.pdf', name: '84a64eb6c48e43de8f6cfb2ee4041e56.pdf', lastModified: 1653981754920, lastModifiedDate: Tue May 31 2022 14:22:34 GMT+0700 (Indochina Time), webkitRelativePath: '', …}
// type: "pdf"