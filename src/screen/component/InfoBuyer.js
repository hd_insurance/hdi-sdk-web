import React, {
  forwardRef,
  useEffect,
  useImperativeHandle,
  useState,
} from "react";
import { Col, Row } from "react-bootstrap";
import FLInput from "../../component/FlInput";
import FLSelect from "../../component/FLSelect/index";
import FLAddress from "../../component/FlAddress/index";
import { checkFormatDate, selectDate } from "../../service/Utils";

import FLInputDate from "../../component/FLInputDate";
import moment from "moment";

const Main = forwardRef((props, ref) => {
  const [gender, setGender] = useState(props?.buyer?.GENDER || "");
  const [name, setName] = useState(props?.buyer?.NAME || "");
  const [phone, setPhone] = useState(props?.buyer?.PHONE || "");
  const [email, setEmail] = useState(props?.buyer?.EMAIL || "");
  const [idCard, setIdCard] = useState(props?.buyer?.IDCARD || "");
  const [dob, setDob] = useState(props?.buyer?.DOB || "");
  const [address, setAddress] = useState(props?.buyer?.ADDRESS || "");
  const [landUseRight, setLandUseRight] = useState(
    props?.buyer?.LAND_USE_RIGHTS || null
  );
  const [addressCode, setAddressCode] = useState({
    label: props?.buyer?.ADDRESS_FORM || "",
    prov: props?.buyer?.PROV || "",
    dist: props?.buyer?.DIST || "",
    ward: props?.buyer?.WARDS || "",
  });

  const [errorsMess, setErrorsMess] = useState({});

  useEffect(() => {
    if (props.errorsMess) {
      setErrorsMess(props.errorsMess);
    }
  }, [props.errorsMess]);

  useEffect(() => {
    setGender(props?.buyer?.GENDER);
    setName(props?.buyer?.NAME);
    setPhone(props?.buyer?.PHONE);
    setEmail(props?.buyer?.EMAIL);
    setIdCard(props?.buyer?.IDCARD);
    setDob(props?.buyer?.DOB);
    setAddress(props?.buyer?.ADDRESS);
    setLandUseRight(props?.buyer?.LAND_USE_RIGHTS);
    setAddressCode({
      label: props?.buyer?.ADDRESS_FORM,
      prov: props?.buyer?.PROV,
      dist: props?.buyer?.DIST,
      ward: props?.buyer?.WARDS,
    })
  }, [props?.buyer]);

  useEffect(() => {
    if (!landUseRight && props.dataRight && props?.dataRight.length > 0) {
      setLandUseRight(props.dataRight[0].value)
    }
  }, [props.dataRight]);

  const maximumDate = () => {
    let now = moment();
    return {
      year: now.year() - 18,
      month: now.month() + 1,
      day: now.date(),
    };
  };
  const defaultDate = () => {
    let now = moment();
    return {
      year: now.year() - 18,
      month: now.month() + 1,
      day: now.date(),
    };
  };

  useImperativeHandle(ref, () => ({

    handleInfoBuyer() {
      if (!checkDate(dob)) {
        return false;
      }
      return {
        CUS_ID: "",
        TYPE: "CN",
        NATIONALITY: "",
        NAME: name,
        DOB: dob,
        GENDER: gender,
        PROV: addressCode?.prov || '',
        DIST: addressCode?.dist || '',
        WARDS: addressCode?.ward || '',
        ADDRESS: address || '',
        ADDRESS_FORM: addressCode?.label || '',
        IDCARD: idCard,
        EMAIL: email,
        PHONE: phone,
        FAX: "",
        TAXCODE: "",
        LAND_USE_RIGHTS: props?.isPLus ? landUseRight : "", // QUyen so huu can nha
      };
    },
  }));

  const checkDate = (dob) => {
    if (!dob) return true;
    if (!checkFormatDate(dob)) {
      setErrorsMess((preMess) => {
        return { ...preMess, dob: "Lỗi định dạng ngày DD/MM/YYYY" };
      });
      return false;
    }
    setErrorsMess((preMess) => {
      return { ...preMess, dob: "" };
    });
    return true;
  };


  return (
    <>
      <Row className={"form-group"}>
        <Col md={4}>
          <FLSelect
            disable={false}
            label="Danh xưng"
            value={gender}
            changeEvent={setGender}
            dropdown={true}
            required={true}
            // data={[{value: 'M', label:'Ong(Mr.)'}]}
            data={props?.dataGender}
            error={errorsMess.gender}
          />
        </Col>
        <Col md={4}>
          <FLInput
            label="Họ và tên"
            value={name}
            changeEvent={setName}
            required={true}
            error={errorsMess.name}
          />
        </Col>
        <Col md={4}>
          <FLInput
            label="Số điện thoại"
            value={phone}
            changeEvent={setPhone}
            required={true}
            error={errorsMess.phone}
            pattern={"[0-9]{10,11}"}
          />
        </Col>
      </Row>
      <Row className={"form-group"}>
        <Col md={props?.isPLus ? 3 : 4}>
          <FLInput
            label="Email"
            value={email}
            changeEvent={setEmail}
            required={props.vat == "P" || props.vat == "E" ? true : false}
            error={errorsMess.email}
            pattern={"[a-zA-Z0-9._%+-]{3,}@[a-z0-9.-]+\\.[a-z]{2,}$"}
          />
        </Col>
        <Col md={props?.isPLus ? 3 : 4}>
          <FLInput
            label="CMND/ CCCD/ Hộ chiếu"
            value={idCard}
            changeEvent={setIdCard}
            required={props.vat == "P" || props?.isPLus ? true : false}
            error={errorsMess.idCard}
            pattern={"[a-zA-Z0-9]{7,13}"}
          />
        </Col>
        <Col md={props?.isPLus ? 3 : 4}>
          <FLInputDate
            disable={false}
            value={dob}
            changeEvent={setDob}
            selectedDay={defaultDate()}
            label={"Ngày sinh"}
            required={false}
            error={errorsMess.dob}
            maximumDate={maximumDate()}
          />
        </Col>
        {props?.isPLus && (
          <Col md={props?.isPLus ? 3 : 4} className="form-group">
            <FLSelect
              disable={false}
              label="Quyền với căn nhà"
              value={landUseRight}
              changeEvent={setLandUseRight}
              dropdown={true}
              required={true}
              // data={[{value: 'M', label:'Ong(Mr.)'}]}
              data={props?.dataRight}
            />
          </Col>
        )}
      </Row>
      <Row className={"form-group"}>
        <Col md={6}>
          <FLInput
            label="Số nhà, ngõ / ngách, đường"
            value={address}
            changeEvent={setAddress}
            // required={false}
            error={errorsMess.address}
            required={props.vat == "P" || props?.isPLus ? true : false}
          />
        </Col>
        <Col md={6}>
          <FLAddress
            disable={false}
            label={"Tỉnh/TP - Quận/huyện - Phường/ xã"}
            value={addressCode}
            changeEvent={setAddressCode}
            error={errorsMess.addressCode}
            required={props.isHdbankLO ? false : ((props.vat == "P" || props?.isPLus) ? true : false)}
          />
        </Col>
      </Row>
    </>
  );
});

export default Main;
