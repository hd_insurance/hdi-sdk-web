import React, {
  forwardRef,
  useEffect,
  useImperativeHandle,
  useState,
} from "react";
import { Col, Row } from "react-bootstrap";
import FLInput from "../../component/FlInput";
import FLAddress from "../../component/FlAddress/index";

const Main = forwardRef((props, ref) => {
  const [mst, setMst] = useState(props?.company?.TAXCODE || "");
  const [name, setName] = useState(props?.company?.NAME || "");
  const [email, setEmail] = useState(props?.company?.EMAIL || "");
  const [address, setAddress] = useState(props?.company?.ADDRESS || "");
  const [addressCode, setAddressCode] = useState({
    label: props?.company?.ADDRESS_FORM || "",
    prov: props?.company?.PROV || "",
    dist: props?.company?.DIST || "",
    ward: props?.company?.WARDS || "",
  });
  useEffect(() => {
    setMst(props?.company?.TAXCODE);
    setName(props?.company?.NAME);
    setEmail(props?.company?.EMAIL);
    setAddress(props?.company?.ADDRESS);
    setAddressCode({
      label: props?.company?.ADDRESS_FORM || "",
      prov: props?.company?.PROV || "",
      dist: props?.company?.DIST || "",
      ward: props?.company?.WARDS || "",
    });
  }, [props?.company]);
  useImperativeHandle(ref, () => ({
    handleInfoEnterprise() {
      return {
        // MST: mst,
        NAME: name,
        PROV: addressCode?.prov,
        DIST: addressCode?.dist,
        WARDS: addressCode?.ward,
        ADDRESS: address,
        ADDRESS_LABEL: addressCode?.label,
        EMAIL: email,
        FAX: "",
        TAXCODE: mst,
      };
    },
  }));
  return (
    <>
      <Row className={"form-group"}>
        <Col md={4}>
          <FLInput
            label="Mã số thuế công ty"
            value={mst}
            changeEvent={setMst}
            required={true}
          />
        </Col>
        <Col md={4}>
          <FLInput
            label="Tên công ty"
            value={name}
            changeEvent={setName}
            required={true}
          />
        </Col>
        <Col md={4}>
          <FLInput
            label="Email nhận hoá đơn"
            value={email}
            changeEvent={setEmail}
            required={true}
          />
        </Col>
      </Row>
      <Row className={"form-group"}>
        <Col md={12}>
          <FLInput
            label="Địa chỉ của doanh nghiệp"
            value={address}
            changeEvent={setAddress}
            required={true}
          />
        </Col>
        {/* <Col md={6}>
          <FLAddress
            disable={false}
            label={"Tỉnh/TP - Quận/huyện - Phường/ xã"}
            value={addressCode}
            changeEvent={setAddressCode}
            required={true}
          />
        </Col> */}
      </Row>
    </>
  );
});

export default Main;
