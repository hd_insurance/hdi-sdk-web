import React, { forwardRef, useEffect, useImperativeHandle, useState } from "react";
import { Col, Row } from "react-bootstrap";
import FLInput from "../../component/FlInput";
import FLSelect from "../../component/FLSelect/index";
import FLAddress from "../../component/FlAddress/index";


const Main = forwardRef((props, ref) => {
    const [mst, setMst] = useState(props?.data?.TAXCODE || null);
    const [gender, setGender] = useState(props?.data?.GENDER || null);
    const [name, setName] = useState(props?.data?.NAME || null);
    const [phone, setPhone] = useState(props?.data?.PHONE || null);
    const [email, setEmail] = useState(props?.data?.EMAIL || null);
    const [idCard, setIdCard] = useState(props?.data?.IDCARD || null);
    const [address, setAddress] = useState(props?.data?.ADDRESS || null);
    const [addressCode, setAddressCode] = useState({
        label:
            props?.data?.ADDRESS_FORM ||
            null,
        prov: props?.data?.PROV || null,
        dist: props?.data?.DIST || null,
        ward: props?.data?.WARDS || null,
    });
    useImperativeHandle(ref, () => ({
        handleInfoBuyer() {
            return {
                ORG_CODE: "",
                RELATIONSHIP: props?.type === 'I' ? "BAN_THAN" : "KHAC",
                TYPE: props?.type === 'E' ? 'CQ' : 'CN',
                NAME: name,
                DOB: "",
                GENDER: props?.type === 'E' ? "" : gender,
                ADDRESS: address,
                IDCARD: props?.type === 'E' ? "" : idCard,
                EMAIL: email,
                PHONE: phone,
                PROV: props?.type === 'E' ? "" : addressCode?.prov,
                DIST: props?.type === 'E' ? "" : addressCode?.dist,
                WARDS: props?.type === 'E' ? "" : addressCode?.ward,
                ADDRESS_FORM: props?.type === 'E' ? "" : addressCode?.label,
                IDCARD_D: "",
                IDCARD_P: "",
                FAX: "",
                TAXCODE: props?.type === 'E' ? mst : "",
            };
        },
    }));


    return (
        <React.Fragment>
            {props?.type === 'O' ? (
                <>
                    <Row className={'form-group'}>
                        <Col md={3}>
                            <FLSelect
                                disable={false}
                                label="Danh xưng"
                                value={gender}
                                changeEvent={setGender}
                                dropdown={true}
                                required={true}
                                data={props?.dataGender}
                            />
                        </Col>

                        <Col md={3}>
                            <FLInput
                                label='Họ và tên'
                                value={name}
                                changeEvent={setName}
                                required={true}
                            />
                        </Col>

                        <Col md={3}>
                            <FLInput
                                label='Số điện thoại'
                                value={phone}
                                changeEvent={setPhone}
                                required={true}
                            />
                        </Col>
                        <Col md={3}>
                            <FLInput
                                label='Email'
                                value={email}
                                changeEvent={setEmail}
                                required={false}
                            />
                        </Col>
                    </Row>
                    <Row className={'form-group'}>

                        <Col md={3}>
                            <FLInput
                                label='CMND/ CCCD/ Hộ chiếu'
                                value={idCard}
                                changeEvent={setIdCard}
                                required={true}
                            />
                        </Col>

                        <Col md={9}>
                            <div className='group_ipnv'>
                                <div style={{ width: '50%' }}>
                                    <FLInput
                                        label='Số nhà, ngõ / ngách, đường'
                                        value={address}
                                        changeEvent={setAddress}
                                        hideborder={true}
                                        required={false}
                                    />
                                </div>
                                <div className="ver-line" />
                                <div className='kkjs'>
                                    <FLAddress
                                        disable={false}
                                        hideborder={true}
                                        label={'Tỉnh/TP - Quận/huyện - Phường/ xã'}
                                        value={addressCode}
                                        changeEvent={setAddressCode}
                                        positionSelect={"column"}
                                        required={false}
                                    />
                                </div>
                            </div>
                        </Col>

                    </Row>
                </>
            ) : (
                <>
                    <Row className={'form-group'}>
                        <Col md={4}>
                            <FLInput
                                label='Mã số thuế'
                                value={mst}
                                changeEvent={setMst}
                                required={true}
                            />
                        </Col>
                        <Col md={4}>
                            <FLInput
                                label='Tên tổ chức'
                                value={name}
                                changeEvent={setName}
                                required={true}
                            />
                        </Col>
                        <Col md={4}>
                            <FLInput
                                label='Số điện thoại'
                                value={phone}
                                changeEvent={setPhone}
                                required={false}
                            />
                        </Col>
                    </Row>
                    <Row className={'form-group'}>
                        <Col md={4}>
                            <FLInput
                                label='Email'
                                value={email}
                                changeEvent={setEmail}
                                required={props.isHdbankLO ? false : true}
                            />
                        </Col>
                        <Col md={8}>
                            <FLInput
                                label='Địa chỉ doanh nghiệp'
                                value={address}
                                changeEvent={setAddress}
                                required={props.isHdbankLO ? true : false}
                            />
                        </Col>
                    </Row>
                </>
            )}

        </React.Fragment>
    )
});

export default (Main);
