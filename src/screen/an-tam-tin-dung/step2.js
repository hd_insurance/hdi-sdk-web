import InputBase from "../../component/InputBase";
import SelectBase from "../../component/SelectBase";
import InputDate from "../../component/InputDate";
import React, {useEffect, useState} from "react";
import {
    calculateDate, compareDate,
    numberFormatCommas,
    selectDate,
    setOptionSelect,
    isEmpty, checkFormatDate,
    maximumDate
} from "../../service/Utils";
import FLAddress from "../../component/InputAddress";
import {utils} from "react-modern-calendar-datepicker";
import moment from "moment";

import InputDateTime from "../../component/InputDateTime";

const Main = (props) => {
    // const itemLo = props.loanInsured ? props.loanInsured : {};

    const [itemLo, setItemLo] = useState(props.loanInsured);

    const [checkVLSt2, setCheckVlSt2] =  useState(props.checkEditSt2);

    const curloDate = moment(new Date()).format("DD/MM/YYYY");

    const [listDefine, setListDefine] = useState([]);
    const [contractId, setContractId] = useState(itemLo.lo_contract);
    const [durationUnit, setDurationUnit] = useState(itemLo.duration_unnit);
    const [beneficiaryUnit, setBeneficiaryUnit] = useState(itemLo.beneficiary_unit);
    const [loanTerm, setLoanTerm] = useState(itemLo.duration);
    const [loanMoney, setLoanMoney] = useState(itemLo.lo_total);
    const [loanDate, setLoanDate] = useState(itemLo.lo_date || curloDate);
    const [gender, setGender]= useState(itemLo.gender);
    const [name, setName]= useState(itemLo.name);
    const [phone, setPhone]= useState(itemLo.phone);
    const [mail, setMail] = useState(itemLo.email);
    const [idCard, setIdCard] =  useState(itemLo.idcard);
    const [idCardDate, setIdCardDate] = useState(itemLo.idcard_d ? itemLo.idcard_d : null);
    const [idCardPlace, setIdcardPlace] = useState(itemLo.idcard_p);
    const [dob, setDobValue] = useState(itemLo.dob ? itemLo.dob : curloDate);
    const [address, setAddress] =  useState(itemLo.address);
    const [addressCode, setAddressCodeValue] = useState(itemLo.provide ? itemLo.provide : {});
    const [sellerName, setSellerName] = useState(itemLo.seller_name);
    const [sellerCode, setSellerCode] = useState(itemLo.seller_code);
    const [sellerEmail, setSellerMail] = useState(itemLo.seller_email);
    const [disburCode, setDisburCode] =  useState(itemLo.disbursement ? itemLo.disbursement.DISBUR_CODE : '');
    const [disburNum, setDisburNum] =  useState(itemLo.disbursement ? itemLo.disbursement.DISBUR_NUM : '');
    const [disburAmount, setDisburAmount] =  useState(itemLo.disbursement ? itemLo.disbursement.DISBUR_AMOUNT : 0);
    const [error, setError] = useState({});

    // const selectedDay = {
    //     year: date.getFullYear(),
    //     month: date.getMonth() + 1,
    //     day: date.getDate(),
    // }
    //
    // const selectLoDate = () => {
    //     const rs1 = moment(loanDate, 'DD/MM/YYYY').format('YYYY-MM-DD');
    //     return { year: moment(rs1).year(),
    //         month: moment(rs1).month() + 1,
    //         day: moment(rs1).date()}
    // }
    //

    useEffect(() =>{
        setCheckVlSt2(props.checkEditSt2)
    },[props.checkEditSt2])

    useEffect(() =>{
        if(props.paramSdk && props.paramSdk.action === 'EDIT'){
            setItemLo(props.loanInsured)
            if(props.loanInsured){
                setContractId(props.loanInsured.lo_contract);
                setLoanTerm(props.loanInsured.duration);
                setLoanMoney(numberFormatCommas(props.loanInsured.lo_total));
                setLoanDate(props.loanInsured.lo_date);
                setGender(props.loanInsured.gender);
                setName(props.loanInsured.name);
                setPhone(props.loanInsured.phone);
                setMail(props.loanInsured.email);
                setIdCard(props.loanInsured.idcard);
                setIdCardDate(props.loanInsured.idcard_d);
                setIdcardPlace(props.loanInsured.idcard_p);
                setDobValue(props.loanInsured.dob);
                setAddress(props.loanInsured.address);
                setBeneficiaryUnit(props.loanInsured.beneficiary_unit);
                setSellerCode(props.loanInsured.seller_code);
                setSellerName(props.loanInsured.seller_name);
                setSellerMail(props.loanInsured.seller_email);

                setDisburAmount(props.loanInsured.disbursement ? numberFormatCommas(props.loanInsured.disbursement.DISBUR_AMOUNT): 0)

                setAddressCodeValue(props.loanInsured.provide);
                setItemLo(props.loanInsured);
            }
        }else if(props.paramSdk && props.paramSdk.action === 'ADD' && checkVLSt2){
                setContractId('');
                // setLoanTerm(props.loanInsured.duration);
                setLoanMoney('');
                setLoanDate(curloDate);
                setLoanTerm(null);
                // setGender(props.loanInsured.gender);
                setDisburCode(null);
                setDisburCode(null);
                setDisburAmount(null);
                setName(null);
                setPhone(null);
                setMail(null);
                setIdCard(null);
                setIdCardDate(null);
                setIdcardPlace(null);
                setDobValue(curloDate)
                // setDobValue({});
                setAddress(null);
                setAddressCodeValue({});
                // setBeneficiaryUnit(props.loanInsured.beneficiary_unit);
                setSellerCode('');
                setSellerName('');
                setSellerMail('');
        }
    },[props.paramSdk,props.loanInsured,props.checkEditSt2])


    useEffect(() =>{
        setListDefine(props.dataDefine);
        if(props.dataDefine){
            setListDefine(props.dataDefine);
            if(isEmpty(durationUnit)){
                if(props.dataDefine[3] && !itemLo.duration_unnit){
                    if(props.dataDefine[3][0]){
                        const rs = [props.dataDefine[3][0]]
                        if(rs.length > 0){
                            setDurationUnit({value: rs[0].TYPE_CODE, label: rs[0].TYPE_NAME})
                        }
                        // setSelectDurationPay(setOptionSelect(rs))
                    }
                }
            }
            if(isEmpty(gender)){
                if(props.dataDefine[4]){
                    if(props.dataDefine[4][0]){
                        const rs = [props.dataDefine[4][0]]
                        if(rs.length > 0){
                            setGender({value: rs[0].TYPE_CODE, label: rs[0].TYPE_NAME})
                        }
                    }
                }
            }
            if(isEmpty(beneficiaryUnit)){
                if(props.dataDefine[6] && !itemLo.beneficiary_unit){
                    const org_code = props.paramSdk?.org_code;
                    const rs = props.dataDefine[6].find(e => e.TYPE_CODE === org_code);
                    // console.log('rs', rs);
                    setBeneficiaryUnit({value: rs.TYPE_CODE, label: rs.TYPE_NAME})
                    // if(props.dataDefine[6][0]){
                    //     const rs = [props.dataDefine[6][0]];
                    //     if(rs.length > 0){
                    //         setBeneficiaryUnit({value: rs[0].TYPE_CODE, label: rs[0].TYPE_NAME})
                    //     }
                    // }
                }
            }
            setListDefine(props.dataDefine);
        }
    },[props.dataDefine]);

    const handleChangeSelect = (value, type) =>{
        switch (type){
            case 'DU':
                setDurationUnit(value);
                break;
            case 'DX':
                setGender(value);
                break;
            // case 'BU':
            //     setBeneficiaryUnit(value);
            //     break;
            case 'DVTH':
                setBeneficiaryUnit(value);
                break;
            default:
                break;
        }
    }

    const handleNextStep2 =() => {
        if(validateForm()){
            props.setLoanInSured({
                // lo_contract: contractId ? contractId : '',
                lo_contract: contractId ? contractId : '',
                duration_unnit: durationUnit ? durationUnit : '',
                duration : loanTerm ? loanTerm : 0,
                lo_total: loanMoney ? loanMoney: '',
                lo_date: loanDate ? loanDate : '',
                gender: gender ? gender :  '',
                name: name ? name :  '',
                phone: phone ? phone : '',
                email: mail ? mail : '',
                idcard: idCard ? idCard : '',
                idcard_d: idCardDate ? idCardDate : '',
                idcard_p: idCardPlace ? idCardPlace : '',
                dob: dob ? dob : '',
                address: address ? address : '',
                provide:addressCode ? addressCode : '',
                district: '',
                wards: '',
                beneficiary_unit: beneficiaryUnit ? beneficiaryUnit : '',
                seller_name: sellerName ? sellerName : '',
                seller_code: sellerCode ? sellerCode : '',
                seller_email: sellerEmail ? sellerEmail : '',
                disbursement: {
                    "BRANCH_CODE": '',
                    "DISBUR_CODE": disburCode ? disburCode : '',
                    "DISBUR_NUM": disburNum ? disburNum : '',
                    "DISBUR_DATE": '',
                    "DISBUR_AMOUNT": disburAmount ? disburAmount.split(',').join("") : 0,
                    "INSUR_TOTAL": props.itemPack ? props.itemPack.insurTotalAmout : 0,
                },
            });
            props.onNextStep();
        }
    }

    const onChangeLoanMoney = (value) =>{
        setLoanMoney(numberFormatCommas(value));
    }

    const onChangeDisburAmount = (value) =>{
        setDisburAmount(numberFormatCommas(value))
    }

    const onChangeLoanDate = (date) => {
        if (!date) return;
        if(checkFormatDate(date)){
            setLoanDate(date);
        }
    }

    const onChangeCardDate = (date) => {
        if (!date) return;
        if(checkFormatDate){
            setIdCardDate(date);
        }
    }

    const onChangeDobValue = (date) => {
        if (!date) return;
        if(checkFormatDate(date)){
            setDobValue(date);
        }
    }

    const validateForm = () =>{
        let errors ={};
        let formIsValid = true;

        // if(!contractId){
        //     formIsValid = false;
        //     errors['contractId'] = 'Vui lòng nhập số hợp đồng vay';
        // }
        // if(!loanTerm){
        //     formIsValid = false;
        //     errors['loanTerm'] = 'Vui lòng nhập thời hạn vay';
        // }
        //
        // if(!loanMoney){
        //     formIsValid = false;
        //     errors['loanMoney'] = 'Vui lòng nhập số tiền vay';
        // }

        // if(loanMoney !== '' ||  loanMoney !== undefined){
        //     let totalInsur = parseInt(props.itemPack ?  props.itemPack.insurTotalAmout : 0);
        //     let lo_money = loanMoney ?  parseInt(loanMoney.split(',').join("")) : null;
        //     if(lo_money < totalInsur){
        //         formIsValid = false;
        //         errors['loanMoney'] = 'Số tiền vay không được nhỏ hơn số tiền bảo hiểm';
        //     }
        // }

        // if(props.typeRadio === 'DISBUR'){
        //
        //     if(!disburCode){
        //         formIsValid = false;
        //         errors['disburCode'] = 'Vui lòng nhập mã khế ước nhận nợ';
        //     }
        //
        //     if(!disburAmount){
        //         formIsValid = false;
        //         errors['disburAmount'] = 'Vui lòng nhập số tiền giải ngân';
        //     }
        //
        //     if(disburAmount !== '' ||  disburAmount !== undefined){
        //         let totalInsur = parseInt(props.itemPack ?  props.itemPack.insurTotalAmout : 0);
        //         let disburAm = disburAmount ?  parseInt(disburAmount.split(',').join("")) : null;
        //         if(disburAm < totalInsur){
        //             formIsValid = false;
        //             errors['disburAmount'] = 'Số tiền giải ngân không được nhỏ hơn số tiền bảo hiểm';
        //         }
        //     }
        // }
        if(checkFormatDate(loanDate)){
            let rs = compareDate(moment(new Date()).format("DD/MM/YYYY"), loanDate);
            if(rs){
                formIsValid = false;
                errors['loanDate'] = 'Ngày kí hợp đồng vay không được lớn hơn ngày hiện tại';
            }
        }

        if(!name){
            formIsValid = false;
            errors['name'] = 'Vui lòng nhập tên người được bảo hiểm';
        }
        if(!phone){
            formIsValid = false;
            errors['phone'] = 'Vui lòng nhập số điện thoại ';
        }
        if(phone !== '' || phone !== undefined){
            let pattern = new RegExp(/^(\d{10}|\d{11})$/i)
            if(!pattern.test(phone)){
                formIsValid = false;
                errors['phone'] = 'Số điện thoại chỉ được từ 10-11 số';
            }
        }

        if(mail !== '' || mail !== undefined){
            let pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            if (!pattern.test(mail)) {
                formIsValid = false;
                errors["mail"] = "Vui lòng nhập đúng định dạng Email";
            }
        }

        // if(sellerEmail !== '' && sellerEmail !== undefined){
        //     let pattern = new RegExp(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g);
        //     console.log('pattern.test(sellerEmail)', pattern.test(sellerEmail));
        //     if (!pattern.test(sellerEmail)) {
        //         formIsValid = false;
        //         errors["sellerEmail"] = "Vui lòng nhập đúng định dạng Email12";
        //     }
        // }

        if(!mail){
            formIsValid = false;
            errors['mail'] = 'Vui lòng nhập email ';
        }

        if(!idCard){
            formIsValid = false;
            errors['idCard'] = 'Vui lòng nhập CMND hoặc CCCD';
        }
        if(idCard !== '' || idCard !== undefined){
            let lengthID  = idCard ? idCard : '';
            if(lengthID.length < 7 || lengthID.length > 13){
                formIsValid = false;
                errors['idCard'] = 'Số CMND/CCCD chỉ được từ 7 đến 13 kí tự';
            }
        }

        if(!idCardDate){
            formIsValid = false;
            errors['idCardDate'] = 'Vui lòng nhập ngày cấp ';
        }
        if(compareDate(curloDate, idCardDate)){
            formIsValid = false;
            errors['idCardDate'] = 'Ngày cấp phải nhỏ hơn ngày hiện tại';
        }

        if(!idCardPlace){
            formIsValid = false;
            errors['idCardPlace'] = 'Vui lòng nhập nơi cấp ';
        }
        if(!dob){
            formIsValid = false;
            errors['dob'] = 'Vui lòng nhập ngày sinh';
        }

        if(dob){
            let vs  = calculateDate(dob, moment(new Date()).format("DD/MM/YYYY"))
            let yearDiff = vs.year_diff;
            if(parseInt(yearDiff) < 18 ){
                formIsValid = false;
                errors['dob'] = 'Tuổi người được bảo hiểm phải từ 18 tuổi';
            }
        }

        if(!address){
            formIsValid = false;
            errors['address'] = 'Vui lòng nhập số nhà, ngõ/ ngách, đường';
        }
        if(isEmpty(addressCode)){
            formIsValid = false;
            errors['addressCode'] = 'Vui lòng chọn Tỉnh/TP - Quận/huyện - Phường/ xã';
        }
        if(!sellerCode){
            formIsValid = false;
            errors['sellerCode'] = 'Vui lòng nhập mã cán bộ ngân hàng ';
        }
        if(!sellerName){
            formIsValid = false;
            errors['sellerName'] = 'Vui lòng nhập tên cán bộ ngân hàng';
        }
        setError(errors);
        return formIsValid;
    }

    return(
        <div className="active-step-2 form-content">
            <label className="col-form-label">Thông tin vay</label>
            <div className="row">
                {props.paramSdk.action === 'EDIT' && (
                    <div className="col-3">
                        <InputBase
                            disable={props?.paramSdk?.channel === 'LO_HDB' ? true : false}
                            value={contractId}
                            changeEvent={setContractId}
                            label={"Số hợp đồng vay"}
                            required={false}
                            // error={error.contractId}
                        />
                    </div>
                )}
                {props.paramSdk.action === 'EDIT' && (
                    <div className="col-3">
                        <InputDate
                            disable={props?.paramSdk?.channel === 'LO_HDB' ? true : false}
                            value={loanDate}
                            changeEvent={onChangeLoanDate}
                            selectedDay={selectDate(loanDate)}
                            maximumDate={maximumDate()}
                            label={"Ngày ký hợp đồng vay"}
                            required={false}
                            error={error.loanDate}
                        />
                    </div>
                )}
                {(props.typeRadio === 'DISBUR' || (props.paramSdk.action === 'EDIT' && props.typeRadio === 'TOTAL') ? (
                        <div className="col-3">
                            <InputBase
                                disable={props?.paramSdk?.channel === 'LO_HDB' ? true : false}
                                // require={true}
                                value={disburNum}
                                changeEvent={setDisburNum}
                                label={"Số khế ước nhận nợ"}
                            />
                        </div>
                ) : null)}
                <div className="col-3">
                    <SelectBase
                        required={false}
                        disable={props?.paramSdk?.channel === 'LO_HDB' ? true : false}
                        label={"Loại thời hạn vay"}
                        placeholder="Chọn loại thời hạn vay"
                        value={durationUnit}
                        options={setOptionSelect(listDefine.length > 0 ? listDefine[3] : [])}
                        onChange={value => handleChangeSelect(value,'DU')}
                    />
                </div>
                <div className="col-3">
                    <InputBase
                        disable={props?.paramSdk?.channel === 'LO_HDB' ? true : false}
                        value={loanTerm}
                        changeEvent={setLoanTerm}
                        label={"Thời hạn vay"}
                        // required={true}
                        // error={error.loanTerm}
                    />
                </div>
                <div className="col-3">
                    <InputBase
                        disable={props?.paramSdk?.channel === 'LO_HDB' ? true : false}
                        value={loanMoney}
                        changeEvent={onChangeLoanMoney}
                        label={"Số tiền vay"}
                        // required={true}
                        // error={error.loanMoney}
                    />
                </div>
                {(props.typeRadio === 'DISBUR' || (props.paramSdk.action === 'EDIT' && props.typeRadio === 'TOTAL') ? (
                    <>
                        <div className="col-3">
                            <InputBase
                                disable={props?.paramSdk?.channel === 'LO_HDB' ? true : false}
                                value={disburAmount}
                                changeEvent={onChangeDisburAmount}
                                label={"Số tiền giải ngân"}
                                // error={error.disburAmount}
                                // required={true}
                            />
                        </div>
                        <div className="col-3">
                            <InputBase
                                disable={props?.paramSdk?.channel === 'LO_HDB' ? true : false}
                                value={disburCode}
                                changeEvent={setDisburCode}
                                label={"Mã khế ước nhận nợ"}
                                // error={error.disburCode}
                                // required={true}
                            />
                        </div>

                    </>
                ) : null)}
            </div>
            <label className="col-form-label">Người được bảo hiểm</label>
            <div className="row">
                <div className="col-3">
                    <SelectBase
                        required={true}
                        label={"Danh xưng"}
                        placeholder="Chọn danh xưng"
                        value={gender}
                        options={setOptionSelect(listDefine.length > 0 ? listDefine[4] : [])}
                        onChange={value => handleChangeSelect(value,'DX')}
                    />
                </div>
                <div className="col-3">
                    <InputBase
                        disable={false}
                        value={name}
                        changeEvent={setName}
                        label={"Tên"}
                        required={true}
                        error={error.name}
                    />
                </div>
                <div className="col-3">
                    <InputBase
                        disable={false}
                        value={phone}
                        changeEvent={setPhone}
                        label={"Số điện thoại "}
                        required={true}
                        error={error.phone}
                    />
                </div>
                <div className="col-3">
                    <InputBase
                        disable={false}
                        value={mail}
                        changeEvent={setMail}
                        label={"Email"}
                        placeholder={"Email NĐBH hoặc CB ngân hàng"}
                        required={true}
                        error={error.mail}
                    />
                </div>
            </div>
            <div className="row">
                <div className="col-3">
                    <InputBase
                        disable={false}
                        value={idCard}
                        changeEvent={setIdCard}
                        label={"Số CMND/CCCD"}
                        required={true}
                        error={error.idCard}
                    />
                </div>
                <div className="col-3">
                    {/*<InputDate*/}
                    {/*    disable={false}*/}
                    {/*    value={idCardDate}*/}
                    {/*    changeEvent={setIdCardDate}*/}
                    {/*    selectedDay={selectedDay}*/}
                    {/*    maximumDate={maximumDate()}*/}
                    {/*    label={"Ngày cấp"}*/}
                    {/*    required={true}*/}
                    {/*    error={error.idCardDate}*/}
                    {/*/>*/}
                    <InputDate
                        disable={false}
                        label={"Ngày cấp"}
                        value={idCardDate}
                        changeEvent={onChangeCardDate}
                        selectedDay={selectDate(idCardDate)}
                        maximumDate={maximumDate()}
                        required={true}
                        error={error.idCardDate}
                    />
                </div>
                <div className="col-3">
                    <InputBase
                        disable={false}
                        required={true}
                        value={idCardPlace}
                        changeEvent={setIdcardPlace}
                        label={"Nơi cấp"}
                        error={error.idCardPlace}
                    />
                </div>
                <div className="col-3">
                    {/*<InputDate*/}
                    {/*    disable={false}*/}
                    {/*    value={dob}*/}
                    {/*    changeEvent={setDobValue}*/}
                    {/*    selectedDay={selectedDay}*/}
                    {/*    maximumDate={maximumDate()}*/}
                    {/*    label={"Ngày sinh"}*/}
                    {/*    required={true}*/}
                    {/*    error={error.dob}*/}
                    {/*/>*/}

                    <InputDate
                        disable={false}
                        label={"Ngày sinh"}
                        value={dob}
                        changeEvent={onChangeDobValue}
                        selectedDay={selectDate(dob)}
                        maximumDate={maximumDate()}
                        required={true}
                        error={error.dob}
                    />
                </div>
            </div>
            <div className="row">
                <div className="col-6">
                    <InputBase
                        disable={false}
                        value={address}
                        changeEvent={setAddress}
                        label={"Số nhà, ngõ/ ngách, đường"}
                        required={true}
                        error={error.address}
                    />
                </div>
                <div className="col-6">
                    <FLAddress
                        disable={false}
                        changeEvent={setAddressCodeValue}
                        value={addressCode}
                        label={'Tỉnh/TP - Quận/huyện - Phường/ xã'}
                        required={true}
                        error={error.addressCode}
                    />
                </div>
            </div>

            <label className="col-form-label">Thụ hưởng, cán bộ ngân hàng</label>
            <div className="row">
                <div className="col-3">
                    <SelectBase
                        disable={true}
                        required={true}
                        label={"Đơn vị thụ hưởng "}
                        value={beneficiaryUnit}
                        options={setOptionSelect(listDefine.length > 0 ? listDefine[6] : [])}
                        onChange={value => handleChangeSelect(value,'DVTH')}
                    />
                </div>
                <div className="col-3">
                    <InputBase
                        // disable={props?.sdk_params?.channel === 'LO_HDB' ? true : false}
                        disable={props?.paramSdk?.channel === 'LO_HDB' ? true : false}
                        value={sellerCode}
                        changeEvent={setSellerCode}
                        label={"Mã cán bộ ngân hàng"}
                        required={true}
                        error={error.sellerCode}
                    />
                </div>
                <div className="col-3">
                    <InputBase
                        disable={props?.paramSdk?.channel === 'LO_HDB' ? true : false}
                        value={sellerName}
                        changeEvent={setSellerName}
                        label={"Tên cán bộ ngân hàng"}
                        required={true}
                        error={error.sellerName}
                    />
                </div>
                <div className="col-3">
                    <InputBase
                        disable={ props?.paramSdk?.channel === 'LO_HDB' ? true : false}
                        value={sellerEmail}
                        changeEvent={setSellerMail}
                        label={"Mail cán bộ ngân hàng"}
                        error={error.sellerEmail}
                    />
                </div>

            </div>
            <div className="btn-next-step" onClick={handleNextStep2}>
                Tiếp tục
            </div>
        </div>
    )
}
export default Main;
