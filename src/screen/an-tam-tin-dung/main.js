import React, {createRef, useEffect, useState} from "react";
import {Form, Modal, Spinner, FormCheck, Col} from "react-bootstrap";
import moment from 'moment';
import ContentStep1 from "./step1";
import ContentStep2 from './step2'
import CarouselImage from "../../component/carousel-image";
import Network from "../../service/Network";
import {
    showDialogDis,
    numberFormatDot,
    maximumDate,
    selectDate,
    checkFormatDate, compareDate, numberFormatCommas
} from "../../service/Utils";
import axios from 'axios';
import SelectBase from "../../component/SelectBase";
import InputBase from "../../component/InputBase";
import InputDate from "../../component/InputDate";
import LoadingForm from "../../component/Loading";
import {config} from "../car-insur/concec";
import UploadImg from "./UploadImg";
import NextWorkNew from "../../service/Network1";

const api = new Network();
const apiNew = new NextWorkNew();

/// <summary> - Checkbox
/// TOTAL: Bảo hiểm trên dư nợ gốc -- > Cấp đơn theo hạn mức
/// DECREASE: Bảo hiểm trên dư nợ giảm dần --> 1 TH của NGân hàng mua tặng 100% Khách hàng(Nếu nhỏ hơn 100% thì là TOTAL)
/// DISBUR: Bảo hiểm trên từng khế ước nhận nợ --> Cấp đơn theo khế ước nhận nợ
/// </summary>

// const formData = new FormData();

const listPmMethod = [
    // {value: 'TM', label: 'Tiền mặt'},
    {value: 'CK', label: 'Chuyển khoản'},
    {value: 'CTT', label: 'Qua mã QR'},
];

const listColFile = [
    {
        title: "Giấy yêu cầu BH",
        required: true,
    },
    {
        title: "CMND/ CCCD/ Hộ chiếu",
        required: false,
    },
    {
        title: "Các file khác",
        required: false,
    },
];

const Main = (props) => {
    const F3Ref = createRef();

    const [paramSdk, setParamSdk] = useState(props.sdk_params);

    const [itemPack, setItemPack]= useState({});
    const [loanInsured, setLoanInSured]= useState({});
    const [step, setStep] = useState("2");
    const [editStep, setEditStep] = useState("2");
    const [isCheckEdit, setIsCheckEdit] = useState(false);
    const [checkEditSt1, setCheckEditSt1] = useState(true);
    const [checkEditSt2, setCheckEditSt2] = useState(true);
    // const [summary, setSumary] = useState("2");

    const [vlRadio , setVlRadio] = useState('TOTAL');

    const [isCheck, setCheck] = useState(null);
    const [listImgGYC, setListImgGYC] = useState([]);
    const [listImgCMND, setListImgCMND] = useState([]);
    const [listImgOther, setListImgOther] = useState([])
    const [listDefine, setListDefine] = useState([]);

    const [loadingDown, setLoadingDown] = useState(false);
    const [loadingSave, setLoadingSave] =  useState(false);
    const [loadingSavePay, setLoadingSavePay] = useState(false);

    // const[pack, setPack] =  useState({A: true, B: false, C: false});

    const [togglePayment, setTogglePayment] = useState(false);

    const [toggleSMS, setToggleSMS] = useState(false);

    const [toggleEmail, setToggleEmail] = useState(false);

    const [paymentMethod, setPaymentMethod] = useState({value: 'CK', label: 'Chuyển khoản'});

    const [showModalBSHD, setShowModalBSHD] = useState(false);

    const [showModalCK, setShowModalCK] = useState(false);
    const [valueSMS, setValueSMS] = useState('')

    const [loContract, setLoContract] = useState(null);

    const [loDate, setLoDate] = useState(null);

    const [err, setErrLodate] = useState('');
    const [errLoConTract, setErrLoConTract] = useState('');

    const [loCode, setLoCode] = useState('');

    const [listFile, setListFile] = useState({});
    const [loadingDefine, setLoadingDefine]= useState(false);

    const onChangeTypeRadio = (e) =>{
        setVlRadio(e.target.value);
    }

    useEffect(() => {
        if(loanInsured){
            setLoDate(loanInsured.lo_date)
        }
    },[loanInsured])

    useEffect(() => {
        getDefine();
    }, []);


    useEffect(() =>{
        setParamSdk(props.sdk_params);
        if(props.sdk_params){
            setTogglePayment(false);
            setToggleEmail(false);
            setToggleSMS(false)
            if(props.sdk_params.action === 'EDIT'){
                setStep("0");
                setEditStep("0");
                getDataDetail(props.sdk_params.contract_code, props.sdk_params.detail_code);
            }else{
                setListFile({});
                setIsCheckEdit(true);
                setStep("0");
                setEditStep("0");
                setItemPack({});
                setLoanInSured({});
                setLoCode('');
                setVlRadio('TOTAL');
                setLoDate('');
                setLoContract('');
                setListImgCMND([]);
                setListImgGYC([]);
                setListImgOther([]);
            }
            setParamSdk(props.sdk_params)
        }
    },[props.sdk_params]);


    useEffect(() =>{
        if(vlRadio === 'DECREASE'){
            showDialogDis("Hình thức này đang được xét duyệt. Vui lòng chọn hình thức khác.");
        }
    },[vlRadio]);

    const getDefine = async () => {
        setLoadingDefine(true);
        const paramDefine = {
            "Action": {
                "ParentCode": "HDI_WEB",
                "UserName": "HDI_WEB",
                "Secret": "B72088C7067CJF9FE0551F6E21B40329",
                "ActionCode": "APISZ0XSJ1"
            },
            "Data": {
                "channel": "SDK_LO",
                "username": props.sdk_params?.user_name,
                "org_code": "HDBANK_VN",
                "product_code": "ATTD",
                "language": ""
            },
        }
        try {
            await api.post(`/OpenApi/Post`, paramDefine).then(data =>{
                if(data.Success){
                    setListDefine(data.Data);
                    setLoadingDefine(false);
                }else{
                    showDialogDis(data.ErrorMessage);
                    setLoadingDefine(false);
                }
            });
        } catch (e) {
            console.log(e);
        }
    };

    const filterFileDetail = (files = []) => {
        let list_gyc = [];
        let list_cmnd = [];
        let list_khac = [];
        // console.log('REACT_APP_VIEW_FILE',process.env.REACT_APP_VIEW_FILE)
        files.forEach((f) => {
            const type =
                f.FILE_NAME.split(".").pop() == "pdf"
                    ? "pdf"
                    : f.FILE_NAME.split(".").pop();
            const file = {
                ...f,
                preview: process.env.REACT_APP_VIEW_FILE + f.FILE_ID,
                type: type,
            };
            if (file.FILE_TYPE == "GYC") {
                list_gyc.push(file);
            }
            if (file.FILE_TYPE == "CMND") {
                list_cmnd.push(file);
            }
            if (file.FILE_TYPE == "KHAC") {
                list_khac.push(file);
            }
        });
        setListFile({
            list_gyc: list_gyc,
            list_cmnd: list_cmnd,
            list_khac: list_khac,
        });
    };

    const getDataDetail = async  (contract_code, detail_code) =>{
        const params = {
            "Action": {
                "ParentCode": "HDI_WEB",
                "UserName": "HDI_WEB",
                "Secret": "B72088C7067CJF9FE0551F6E21B40329",
                "ActionCode": "API4CQ0Z1U"
            },
            "Data": {
                "channel": "WEB_B2B",
                "username": props.sdk_params?.user_name,
                "language": "VN",
                "category": "CN.04",
                "product_code": "ATTD",
                "contract_code":contract_code,
                "detail_code":detail_code,
                "org_code": props.sdk_params?.org_code
            },
        }
        try {
            let rs = await api.post(`/OpenApi/Post`, params);
            if(rs.Success){
                if(props.sdk_params.action === 'EDIT'){
                    if(rs.Data.length > 0){
                        // if(rs.Data[0].length > 0 && rs.Data[3].length > 0){
                        if(rs.Data[0].length > 0){
                            let rs1 = rs.Data[0][0];
                            let ras = {}
                            if(rs1.STATUS === config.ord_status_paid){
                                setIsCheckEdit(false);
                                setStep("2");
                                setEditStep("2");
                            }else{
                                setIsCheckEdit(true);
                            }
                            if(rs1.PACK_CODE === 'A'){
                                // setPack({...pack, A: true});
                                ras = {A: true, B: false, C:false}
                            }else if(rs1.PACK_CODE === 'ABC'){
                                // setPack({A: true, B: true, C:true})
                                ras = {A: true, B: true, C:true}
                            }else if(rs1.PACK_CODE === 'AB'){
                                // setPack({A: true, B: true, C: false});
                                ras = {A: true, B: true, C:false}
                            }else if(rs1.PACK_CODE === 'AC'){
                                // setPack({A: true, B: false, C: true});
                                ras = {A: true, B: false, C:true}
                            }
                            setLoCode(rs1.LO_CODE);
                            setVlRadio(rs1.LO_TYPE);

                            setItemPack({
                                effectiveDate: rs1.EFFECTIVE_DATE,
                                expirationDate: rs1.EXPIRATION_DATE,
                                insurTotalAmout: rs1.INSUR_TOTAL,
                                selectDurationPay: {value: rs1.DURATION_PAYMENT, label: rs1.DURATION_NAME},
                                selectRegion: {value: rs1.REGION, label: rs1.REGION_NAME},
                                selectInsurRange: {value: rs1.PACK_CODE, label: rs1.PACK_NAME},
                                // selectDiscount: selectDiscount,
                                packFess: '',
                                discount:'',
                                totalDiscount: '',
                                totalAmout:rs1.TOTAL_AMOUNT,
                                period: rs.Data[3],
                                packageInsur: ras,
                            });
                        }
                        if(rs.Data[1].length > 0){
                            let rs2 = rs.Data[1][0], provide = {};
                            if(rs2.ADDRESS_FORM && rs2.PROVINCE && rs2.DISTRICT && rs2.WARDS){
                                provide = {label: rs2.ADDRESS_FORM, prov: rs2.PROVINCE, dist: rs2.DISTRICT, ward: rs2.WARDS}
                            }
                            if (props?.sdk_params?.org_code === 'HDBANK_VN') {
                                rs2.TELLER_CODE = props?.sdk_params?.org_code;
                            }
                            setLoanInSured({
                                // lo_contract: contractId ? contractId : '',
                                lo_contract: rs2.LO_NO,
                                contract_code: rs2.CONTRACT_CODE,
                                duration_unnit: {label: rs2.UNIT_NAME, value: rs2.UNIT},
                                duration : rs2.DURATION,
                                lo_total: rs2.LO_TOTAL_AMOUNT ? numberFormatCommas(rs2.LO_TOTAL_AMOUNT) : 0,
                                // // lo_date: loanDate ? loanDate : '',
                                lo_date: rs2.BORROW_DATE_SIGN,
                                gender: {value: rs2.GENDER, label: rs2.GENDER_NAME},
                                name: rs2.NAME,
                                phone: rs2.PHONE,
                                email: rs2.EMAIL,
                                idcard: rs2.IDCARD,
                                idcard_d: rs2.IDCARD_D,
                                idcard_p: rs2.IDCARD_P,
                                dob: rs2.DOB,
                                address: rs2.ADDRESS,
                                provide: provide,
                                // district: '',
                                // wards: '',
                                beneficiary_unit: {value: rs2.BEN_ORG_CODE, label: rs2.BEN_NAME},
                                seller_name: rs2.TELLER_NAME,
                                seller_code: rs2.TELLER_CODE,
                                seller_email: rs2.TELLER_EMAIL,
                                disbursement: {
                                    "BRANCH_CODE": '',
                                    "DISBUR_CODE": rs2.DISBUR_CODE,
                                    "DISBUR_NUM": rs2.DISBUR_NUM,
                                    "DISBUR_DATE": '',
                                    "DISBUR_AMOUNT": rs2.DISBUR_AMOUNT,
                                    "INSUR_TOTAL": itemPack ? itemPack.insurTotalAmout : 0,
                                },
                            });
                        }

                        if(rs.Data[2].length > 0){
                            filterFileDetail(rs.Data[2]);
                            // getListImgeDetail(rs.Data[2]);
                        }
                    }
                }
            }else{
                showDialogDis(rs.ErrorMessage);
            }
        } catch (e) {
            console.log(e);
        }
    };

    // useEffect(() =>{
    //     console.log('listFile', listFile);
    //     if(listFile && listFile.length > 0){
    //         let arrGYC = [], arrCMND = [], arrKhac = [];
    //         let rs  =  listFile.filter(item => item.type === 'GYC');
    //         let rs1  =  listFile.filter(item => item.type === 'CMND');
    //         let rs2  =  listFile.filter(item => item.type === 'KHAC');
    //
    //         rs.map((vl,i)=>{
    //             arrGYC.push(vl.file)
    //         });
    //         setListImgGYC(arrGYC);
    //
    //         rs1.map((vl,i)=>{
    //             arrCMND.push(vl.file)
    //         });
    //         setListImgCMND(arrCMND);
    //
    //         rs2.map((vl,i)=>{arrKhac.push(vl.file)})
    //         setListImgOther(arrKhac);
    //     }
    // },[listFile])

    // const getListImgeDetail = (listImg) =>{
    //         const arrImg = [];
    //         listImg.map((vl, i) =>{
    //             if(vl.FILE_FORMAT === 'pdf'){
    //                 getBase64pdf(url_preview+vl.FILE_ID, (data) =>{
    //                     arrImg.push({type: vl.FILE_TYPE, file: data});
    //                 });
    //             }else{
    //                getBase64Image(url_preview+vl.FILE_ID, (data)=>{
    //                    arrImg.push({type: vl.FILE_TYPE, file: data});
    //                });
    //             }
    //         })
    //     setTimeout(() =>{
    //         setListFile(arrImg);
    //     }, 300)
    // };

    const appendFile = (listFile) => {
        return new Promise((resolve, reject) => {
            let formData = new FormData();
            listFile.forEach((file) => {
                formData.append("files", file.rawFile, file.name);
            });
            resolve(formData);
        });
    };

    const uploadfile = async (file) => {
        const url = `${process.env.REACT_APP_DM1}/upload/`;
        // const url = `http://10.28.3.78:8000/upload/`;
        const config = {
            headers: {
                ParentCode: "HDI_UPLOAD",
                UserName: "HDI_UPLOAD",
                Secret: "HDI_UPLOAD_198282911FASE1239212",
                environment: "LIVE",
                DeviceEnvironment: "WEB",
                ActionCode: "UPLOAD_SIGN",
            },
        };
        // const upListFile = JSON.parse(JSON.stringify(listFile));
        const files = [
            ...file?.list_gyc,
            ...file?.list_cmnd,
            ...file?.list_khac,
        ];

        const filesUploaded = files
            .filter((f) => !f.rawFile)
            .map((e) => {
                // map instead of delete property
                const file_extension = e.FILE_NAME.split(".").pop();
                return {
                    FILE_TYPE: e.FILE_TYPE,
                    FILE_NAME: e.FILE_NAME,
                    FILE_ID: e.FILE_ID,
                    IS_DEL: "0",
                    FILE_EXTENSION: file_extension,
                };
            });
        // console.log(filesUploaded);
        const filesUpload = files.filter((f) => f.rawFile);

        const data = await appendFile(filesUpload);
        try {
            const rs = await axios.post(url, data, config);
            console.log('rs', rs);
              if(rs?.data?.success){
                  const result = (rs?.data?.data || []).map((file) => {
                      const file_extension = file?.file_org_name?.split(".").pop();
                      const type = file?.file_org_name?.split("_")[0];
                      return {
                          FILE_TYPE: type,
                          FILE_NAME: file.file_org_name,
                          FILE_ID: file.file_key,
                          IS_DEL: "0",
                          FILE_EXTENSION: file_extension,
                      };
                  });
                  // console.log(listFile)
                  return [...filesUploaded, ...result];
              }else{
                  showDialogDis(rs?.data?.error_message)
              }
        } catch (e) {
            setLoadingSave(false);
            setLoadingSavePay(false);
            console.log('e', e);
            showDialogDis(e.data?.error_message)
            console.log(e);
            return [];
        }
    };

    const handleSaveAndPayment = async(type) =>{
        let listFile = F3Ref.current.handleSubmit();
        if(listFile){
            if(type === 'SAVE'){
                setLoadingSave(true);
            }else if(type === 'PAY'){
                setLoadingSavePay(true);
            }
            const resultFiles = await uploadfile(listFile);
            if(resultFiles?.length > 0){
                createOrderATTD(resultFiles, type)
            }else{
                setLoadingSave(false);
                setLoadingSavePay(false);
                showDialogDis('Lỗi tải file dữ liệu');
            }
        }
        // if(listImgGYC.length === 0 || listImgCMND.length === 0){
        //     setLoadingSave(false);
        //     return showDialogDis('Vui lòng tải tài liệu');
        // }
        //
        // fileUpload().then((data) =>{
        //     if(data.data.success){
        //         const listImg = data.data.data;
        //         let files = [];
        //         let type_gyc = listImg.filter(item => item.file_org_name.includes('GYC_'));
        //         type_gyc.map((item) => {
        //             let obj = objectFiles(item.file_org_name, item.file_key, 'GYC');
        //             files.push(obj);
        //         });
        //         let type_cmnd = listImg.filter(item => item.file_org_name.includes('CMND_'));
        //         type_cmnd.map((item) => {
        //             let obj = objectFiles(item.file_org_name, item.file_key, 'CMND');
        //             files.push(obj);
        //         });
        //         let type_other = listImg.filter(item => item.file_org_name.includes('KHAC_'));
        //         type_other.map((item) => {
        //             let obj = objectFiles(item.file_org_name, item.file_key, 'KHAC');
        //             files.push(obj);
        //         });
        //         if(files.length > 0){
        //             createOrderATTD(files, type);
        //         }
        //     }else{
        //         showDialogDis('Lỗi tải file dữ liệu');
        //         setLoadingSave(false);
        //     }
        // })
    };

    const createOrderATTD = async (list_file, type) => {
        let url_api='' ;
        if(type === 'SAVE'){
            url_api = '/OpenApi/orders/create';
        }else if(type === 'PAY'){
            url_api = '/OpenApi/orders/create_pay';
        }
        const params = {
            "Action": {
                "ParentCode": "HDI_WEB",
                "UserName": "HDI_WEB",
                "Secret": "B72088C7067CJF9FE0551F6E21B40329",
                "ActionCode": "API0OVG3B1"
            },
            "Data": {
                "Channel": "HDBANK_VN",
                "UserName": props.sdk_params?.user_name,
                "COMMON": {
                    "SELLER": {
                        "SELLER_CODE": loanInsured.seller_code ? loanInsured.seller_code :'',
                        "SELLER_NAME": loanInsured.seller_name ? loanInsured.seller_name : '',
                        "SELLER_EMAIL": loanInsured.seller_email ? loanInsured.seller_email :'',
                        "SELLER_PHONE": "",
                        "SELLER_GENDER": "",
                        "STRUCT_CODE": "",
                        "ORG_CODE":  props?.sdk_params?.org_code,
                        "BRANCH_CODE": null,
                        "ORG_TRAFFIC": null,
                        "TRAFFIC_LINK": null,
                        "ENVIROMENT": "SDK_HD_BANK"
                    },
                    "BUYER": {
                        "CUS_ID": null,
                        "TYPE": "CN",
                        "NAME": loanInsured.name,
                        "DOB": loanInsured.dob,
                        "GENDER":loanInsured.gender ?  loanInsured.gender.value : '',
                        "PROV": loanInsured.provide.prov,
                        "DIST": loanInsured.provide.dist,
                        "WARDS": loanInsured.provide.ward,
                        "ADDRESS": loanInsured.address,
                        "IDCARD": loanInsured.idcard,
                        "IDCARD_D": loanInsured.idcard_d,
                        "IDCARD_P": loanInsured.idcard_p,
                        "EMAIL": loanInsured.email,
                        "PHONE": loanInsured.phone,
                        "FAX": null,
                        "TAXCODE": null,
                    },
                    "RECEIVER": null,
                    "ORDER": {
                        "FIELD": "BH",
                        "ORDER_CODE": null,
                        "TYPE": paramSdk.action === 'ADD' ?  "BH_M" : "BH_S", //BH_S la sua?
                        "TITLE": "Đơn bảo hiểm ATTD",
                        "SUMMARY": "Đơn bảo hiểm ATTD",
                        // "AMOUNT": itemPack.packFess,
                        "AMOUNT": itemPack.totalAmout,
                        "DISCOUNT": itemPack.discount ? itemPack.discount : 0,
                        "DISCOUNT_UNIT": itemPack.selectInsurRange.discount_unit,
                        "VAT": 0,
                        "TOTAL_AMOUNT": itemPack.totalAmout,
                        "CURRENCY": "VND",
                        "GIF_CODE": "",
                        "STATUS": "",
                        "PAY_METHOD": "ON_HDI_CHANNEL"
                    },
                    "ORDER_DETAIL": [
                        {
                            "FIELD": "BH",
                            "PRODUCT_MODE": vlRadio === 'TOTAL' ?  "TRUC_TIEP" : 'HD_BAO',
                            "PRODUCT_TYPE": "CN.04",
                            "PRODUCT_CODE": props?.sdk_params?.product_code || "ATTD",
                            "REF_ID": paramSdk.contract_code, //Contract_code
                            "ORG_STORE": null,
                            "WEIGHT": null,
                            "COUNT": 1,
                            // "AMOUNT": itemPack.packFess,
                            "AMOUNT": itemPack.totalAmout,
                            "DISCOUNT": itemPack.discount ? itemPack.discount : 0,
                            "DISCOUNT_UNIT": itemPack.selectInsurRange.discount_unit,
                            "VAT": 0,
                            "TOTAL_AMOUNT": itemPack.totalAmout,
                            "DESCRIPTION": "Đơn bảo hiểm ATTD",
                            "EFFECTIVE_DATE": itemPack.effectiveDate,
                            "EXPIRATION_DATE": itemPack.expirationDate,
                        }
                    ],
                    "PAY_INFO": {
                        "PAYMENT_TYPE": paymentMethod ? paymentMethod.value : '',
                        "IS_EMAIL": toggleEmail ? "1" : "0",
                        "IS_SMS": toggleSMS ? "1" : "0",
                    }
                },
                "BUSINESS": {
                    "LOAN": null,
                    "FLIGHT": null,
                    "HEALTH_CARE": null,
                    "LO": {
                        "INSURED": [
                            {
                                "BANK_CODE": "HDBANK_VN",
                                "BRANCH_CODE": "HDBANK_VN",
                                "LO_CONTRACT": loanInsured ? loanInsured.lo_contract : '',
                                "LO_CODE": loCode ? loCode : '',
                                "LO_TYPE": vlRadio,
                                "LO_MODE":vlRadio === 'TOTAL' ?  "TRUC_TIEP" : 'HD_BAO', //voi Type = TOTAL thi la TRUC_TIEP, con lai la HD_BAO
                                "PAYER": "C",
                                "CURRENCY": "VND",
                                "LO_TOTAL_AMOUNT": loanInsured.lo_total ? loanInsured.lo_total.split(",").join(""): 0,
                                "LO_EFFECTIVE_DATE": "",
                                "LO_EXPIRATION_DATE": "",
                                "LO_DATE": loanInsured.lo_date,
                                "INTEREST_RATE": "0",
                                "DURATION": parseInt(loanInsured.duration),
                                "DURATION_UNIT":loanInsured.duration_unnit ?  loanInsured.duration_unnit.value : '',
                                "INSUR_TOTAL_AMOUNT": itemPack.insurTotalAmout,
                                "DISBUR": loanInsured.disbursement,
                                "ID_COMMON": "8b824db4-d7c7-4527-9ea3-73d26b3e14b2",
                                "RELATIONSHIP": "BAN_THAN",
                                "DETAIL_CODE": paramSdk.detail_code, // detail_code truyen vao
                                "CERTIFICATE_NO": null,
                                "PRODUCT_CODE": props?.sdk_params?.product_code || "ATTD",
                                "PACK_CODE":itemPack.selectInsurRange ?  itemPack.selectInsurRange.value : '',
                                "PACK_FEES": itemPack.packFess,
                                "REGION": itemPack.selectRegion ? itemPack.selectRegion.value : '',
                                "EFFECTIVE_DATE": itemPack.effectiveDate,
                                "EXPIRATION_DATE": itemPack.expirationDate,
                                "ADDIITIONAL": null,
                                "ADDITIONAL_FEES": 0,
                                // "AMOUNT": itemPack.packFess,
                                "AMOUNT": itemPack.totalAmout,
                                "DISCOUNT": itemPack.discount ? itemPack.discount : 0,
                                "DISCOUNT_UNIT": itemPack.selectInsurRange.discount_unit,
                                "VAT": 0,
                                "TOTAL_AMOUNT": itemPack.totalAmout,
                                "CUS_ID": "",
                                "TYPE": "CN",
                                "NAME": loanInsured.name,
                                "DOB": loanInsured.dob,
                                "GENDER": loanInsured.gender ? loanInsured.gender.value : '',
                                "PROV": loanInsured.provide.prov,
                                "DIST": loanInsured.provide.dist,
                                "WARDS": loanInsured.provide.ward,
                                "ADDRESS": loanInsured.address,
                                "IDCARD": loanInsured.idcard,
                                "IDCARD_D": loanInsured.idcard_d,
                                "IDCARD_P": loanInsured.idcard_p,
                                "EMAIL": loanInsured.email,
                                "PHONE": loanInsured.phone,
                                "FAX": null,
                                "TAXCODE": null,
                                "DURATION_PAYMENT": itemPack.selectDurationPay ? itemPack.selectDurationPay.value : '',
                                "PERIOD": itemPack.period,
                                "BENEFICIARY": [
                                    {
                                        "ORG_CODE":loanInsured.beneficiary_unit ?  loanInsured.beneficiary_unit.value :'',
                                        "TYPE": "CQ"
                                    }
                                ],
                                "FILES": list_file,
                            }
                        ]
                    }
                }
            },
        };
        try {
            await api.post(url_api, params).then(data =>{
                if(data.Success){
                    if(type==='SAVE'){
                        setLoadingSave(false);
                        if(paramSdk.action === 'ADD'){
                            showDialogDis('Tạo đơn thành công');
                        }else{
                            showDialogDis('Cập nhật đơn thành công');
                        }
                    }else if(type=== 'PAY'){
                        setLoadingSavePay(false);
                        if(data.Data.url_redirect && data.Data.url_redirect?.toString()?.trim() !== ''){
                            if(paymentMethod.value === 'CK' && data?.Data?.[0]?.[0]){
                                setShowModalCK(true);
                                setValueSMS(data.Data[0][0].CONTENT_PAY)
                            }
                            window.open(data.Data.url_redirect,"_self");
                        }else if(paymentMethod.value === 'TM'){
                            if(data.Data){
                                if(data.Data[0]){
                                    if(data.Data[0][0]){
                                        setShowModalCK(true);
                                        setValueSMS(data.Data[0][0].CONTENT_PAY)
                                    }
                                }
                            }
                        }
                    }

                    props.sdk_params.onClose(data =>{
                        console.log(data);
                        return data;
                    });

                }else{
                    showDialogDis(data.ErrorMessage);
                    setLoadingSave(false);
                    setLoadingSavePay(false);
                }
            });
        } catch (e) {
            console.log(e);
        }
    };

    const onNextStep = async (e)=>{
        switch(step){
            case "0":
                setStep("1");
                setEditStep("1");
                if(paramSdk.action === 'EDIT'){
                    setCheckEditSt1(true);
                    setCheckEditSt2(true);
                }else{
                    setCheckEditSt1(false);
                }
                break;
            case "1":
                setStep("2");
                setEditStep("2");
                setCheckEditSt2(false);
                if(paramSdk.action === 'EDIT'){
                    setCheckEditSt2(true);
                }else{
                    setCheckEditSt2(false);
                }
                break;
            case "2":
                // setStep("3");
                // setEditStep("3");
                break;
            default:
                break;
        }
    };

    const handleEditStep = (vl) => {
        const listGYC =  listImgGYC ? listImgGYC.length: '';
        if(listGYC > 0){
            showDialogDis('Chỉnh sửa sẽ ảnh hưởng đến thông tin giấy yêu cầu bảo hiểm đã tải lên!')
        }
        setStep(vl);
        setEditStep(vl);
        if(paramSdk.action === 'EDIT'){
            setCheckEditSt1(true);
            setCheckEditSt2(true);
        }else{
            setCheckEditSt1(false);
            setCheckEditSt2(false);
        }
    };

    const setValueContract  = () => {
        let value = '';
        if(vlRadio === 'TOTAL'){
            value = 'Cấp đơn theo hạn mức';
        }else if(vlRadio === 'DISBUR'){
            value= 'Cấp đơn theo khế ước nhận nợ';
        }else if(vlRadio === 'DECREASE'){
            value='Ngân hàng mua tặng KH';
        }
        return value;
    };

    const downloadGYC = async () => {
        console.log('loanInsured?.lo_total', loanInsured?.lo_total );
        setLoadingDefine(true);
        let packageInsur = itemPack ? itemPack.packageInsur : {} ;
        let provideVl= loanInsured.provide ?  loanInsured.provide.label.split("-") : '';
        let provideFormat = provideVl[2] + ',' + provideVl[1] + ',' + provideVl[0];
        let thv = parseInt(loanInsured.duration) + ' ' + (loanInsured.duration_unnit ?  loanInsured.duration_unnit.label : '');
        let url_api = '/hdi/service/getPDFJson';
        let params = {
            "Action": {
                "ParentCode": "HDI_B2B2C",
                "UserName": "USR_0006",
                "Secret": "B72088C7067CBF9FE0551F6E21B40329",
                "ActionCode": "GET_TEMPLATE"
            },
            "Data": {
                "USER_NAME": "USR_0006",
                "TEMP_CODE": "ATTD_YCBH",
                "TYPE_RES": "BASE64",
                "TRANSACTION_ID": "fff5c44223112020",
                "DATA_TYPE": "JSON",
                "PRODUCT_CODE": props?.sdk_params?.product_code || "ATTD",
                "PACK_CODE": "null",
                "MASTER": {
                    "CREATE_DATE": moment(new Date()).format("DD/MM/YYYY"),
                    "NAME": loanInsured?.name,
                    "DOB": loanInsured?.dob,
                    "GENDER": loanInsured?.gender ?  loanInsured?.gender?.value === 'M' ? 'Nam' : 'Nữ' : '',
                    "IDCARD": loanInsured?.idcard,
                    "IDCARD_DATE": loanInsured?.idcard_d,
                    "CONTRACT_LOAN": loanInsured ? loanInsured?.lo_contract : '',
                    "ADDRESS": loanInsured?.address + ' ' + provideFormat,
                    "CONTRACT_LOAN_DATE": loanInsured?.lo_date,
                    "TOTAL_BOR": loanInsured?.lo_total ? loanInsured?.lo_total?.split(",").join(".") : 0,
                    "EXPIRATION_DATE_LOAN": "",
                    "RELATIONSHIP": "Là người được bảo hiểm",
                    "IDCARD_PLACE": loanInsured?.idcard_p,
                    "DURATION": thv,
                    "UNIT": loanInsured?.duration_unnit ?  loanInsured.duration_unnit.label : '',
                    "TOTAL_AMOUNT":  numberFormatDot(itemPack?.totalAmout),
                    "DECISION": "", //Quyết định số
                    "DECISION_DATE": "", //ngày quyết định
                    "REGION": itemPack?.selectRegion ? itemPack.selectRegion.value : '',
                    "CURRENCY": "VNĐ",
                    "EFFECTIVE_DATE": itemPack?.effectiveDate,
                    "TOTAL_INS": numberFormatDot(itemPack?.insurTotalAmout),
                    "NAME_TH": loanInsured?.beneficiary_unit?.label,
                    "A": packageInsur.A.toString(),
                    "B": packageInsur.B.toString(),
                    "C": packageInsur.C.toString()
                }
            },
            "Signature": "450F4EAC05ED87F03C04011B1978148DB9CE0103FA90F01C7707149068A147E2"
        }
        try {
            await api.post(url_api, params).then(data =>{
                if(data.Success){
                    let link = document.createElement('a');
                    link.setAttribute('href', 'data:application/pdf;base64,' + data.Data.FILE_BASE64);
                    link.setAttribute('download', data.Data.FILE_NAME);
                    link.click();
                    setLoadingDefine(false);
                }else{
                    showDialogDis(data.error_message);
                    setLoadingDefine(false);
                }
            });
        } catch (e) {
            console.log(e);
        }
    };

    const onChangeTogglePayment = (e) =>{
        setTogglePayment(!togglePayment);
    };

    const onChangeToggleSMS = (e) =>{
        setToggleSMS(!toggleSMS);
    };

    const onChangeToggleEmail = (e) =>{
        setToggleEmail(!toggleEmail);
    };

    const handleChangePayentMethod = (value) =>{
        setPaymentMethod(value)
    };

    const onChangeLodate = (date) => {
        if (!date) return;
        if(checkFormatDate(date)){
            let rs = compareDate(moment(new Date()).format("DD/MM/YYYY"), date);
            if(rs){
                setErrLodate('Ngày kí hợp đồng vay không được lớn hơn ngày hiện tại');
            }else{
                setErrLodate('');
            }
            setLoDate(date);
        }
    };

    const onChangeLoContract = (value) =>{
        setLoContract(value);
    }

    useEffect(() =>{
        if(loContract){
            setErrLoConTract('');
        }
    },[loContract])

    const UpdateLo = () =>{
        let rs = compareDate(moment(new Date()).format("DD/MM/YYYY"), loDate);
         if(rs){
            setErrLodate('Ngày kí hợp đồng vay không được lớn hơn ngày hiện tại');
         }
         if(!loContract){
             setErrLoConTract('Vui lòng nhập số hợp đồng vay');
         }
         if(!rs && loContract){
             setErrLoConTract('');
             setErrLodate('');
             setLoanInSured({...loanInsured,lo_contract : loContract, lo_date: loDate});
             setShowModalBSHD(false);
         }
    }

    useEffect(() =>{
        if(togglePayment){
            if(!loanInsured.lo_contract || !checkFormatDate(loanInsured.lo_date)){
                setShowModalBSHD(true);
            }
        }
    },[togglePayment])

    const closeModal = () =>{
        setTogglePayment(false);
        setShowModalBSHD(false);
    }

    return (
        <div className="form-create-ord">
            {loadingDefine && <LoadingForm />}
            <div className="step-vertical-timeline">
                <div className="header-step-timeline">
                    <label className="title-step-timeline">Thông tin gói</label>
                    {editStep !== "0" && isCheckEdit && (
                        <label className="edit-step-1" onClick={()=>handleEditStep("0")}>Sửa</label>
                    )}
                </div>
                <div className="content-step-timeline">
                    {step === "0" && editStep === "0" && (
                        <div className="active-step-1">
                            <div className="form-choose-type">
                                <p>Chọn loại cấp đơn *</p>
                                <div className="rd-group">
                                    <label className="form-check">
                                        <input className="form-check-input" type="radio" value={'TOTAL'}
                                               onChange={onChangeTypeRadio} checked={vlRadio ==='TOTAL'}/>
                                        Cấp đơn theo hạn mức
                                    </label>
                                    <label className="form-check">
                                        <input className="form-check-input" type="radio" value={'DISBUR'}
                                               onChange={onChangeTypeRadio} checked={vlRadio ==='DISBUR'}/>
                                        Cấp đơn theo khế ước nhận nợ
                                    </label>
                                    <label className="form-check">
                                        <input className="form-check-input" type="radio" value={'DECREASE'}
                                               onChange={onChangeTypeRadio} checked={vlRadio ==='DECREASE'}/>
                                       Ngân hàng mua tặng KH
                                    </label>
                                </div>
                            </div>
                                <ContentStep1
                                    onNextStep={onNextStep}
                                    dataDefine={listDefine}
                                    setItemPack={setItemPack}
                                    itemPack={itemPack}
                                    typeRadio={vlRadio}
                                    paramSdk = {paramSdk}
                                    checkEditSt1={checkEditSt1}
                                />
                        </div>
                    )}
                    {editStep !== "0" && (
                        <div className="unactive-step-1 typing-value-form">
                            <div className="row">
                                <div className="col-4">
                                    <label className="col-form-label">Sản phẩm:</label>
                                    <label className="col-form-label">An tâm tín dụng</label>
                                </div>
                                <div className="col-4">
                                    <label className="col-form-label">Loại HĐ:</label>
                                    <label className="col-form-label">
                                        {setValueContract()}
                                    </label>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-4">
                                    <label className="col-form-label">Ngày hiệu lực:</label>
                                    <label className="col-form-label">{itemPack.effectiveDate}</label>
                                </div>
                                <div className="col-4">
                                    <label className="col-form-label">Ngày kết thúc:</label>
                                    <label className="col-form-label">{itemPack.expirationDate}</label>
                                </div>
                                <div className="col-4">
                                    <label className="col-form-label">Số tiền BH:</label>
                                    <label className="col-form-label">{ numberFormatCommas(itemPack.insurTotalAmout)} VNĐ</label>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-4">
                                    <label className="col-form-label">Phí bảo hiểm:</label>
                                    <label className="col-form-label">
                                        {/*{ itemPack.packFess ? numberFormatCommas(itemPack.packFess) : ''} VNĐ*/}
                                        {itemPack.totalAmout ? numberFormatCommas(itemPack.totalAmout) : '' } VNĐ
                                    </label>
                                </div>

                                {/*<div className="col-4">*/}
                                {/*    <label className="col-form-label">Giảm phí:</label>*/}
                                {/*    <label className="col-form-label">*/}
                                {/*        {itemPack.discount ? itemPack.discount : '0'} %*/}
                                {/*    </label>*/}
                                {/*</div>*/}

                                {/*<div className="col-4">*/}
                                {/*    <label className="col-form-label">Tổng tiền giảm:</label>*/}
                                {/*    <label className="col-form-label">*/}
                                {/*        {numberFormatCommas(itemPack.totalDiscount)} VNĐ*/}
                                {/*    </label>*/}
                                {/*</div>*/}
                                <div className="col-4">
                                    <label className="col-form-label">Tổng thanh toán:</label>
                                    <label className="col-form-label">
                                        {itemPack.totalAmout ? numberFormatCommas(itemPack.totalAmout) : '' } VNĐ
                                    </label>
                                </div>
                            </div>
                        </div>
                    )}
                </div>
                <div className="number-step-timeline">1</div>
            </div>
            <div className="step-vertical-timeline">
                <div className="header-step-timeline">
                    <label className="title-step-timeline">
                        Thông tin vay, NĐBH, cán bộ ngân hàng, thụ hưởng
                    </label>
                    {step === "2" && editStep !== "1" && isCheckEdit && (
                        <label className="edit-step-2" onClick={()=>handleEditStep("1")}>Sửa</label>
                    )}
                </div>
                <div className="content-step-timeline">
                    {step ==="1" && editStep === "1" && (
                        <ContentStep2
                            onNextStep={onNextStep}
                            dataDefine={listDefine}
                            setLoanInSured={setLoanInSured}
                            loanInsured={loanInsured}
                            itemPack={itemPack}
                            typeRadio={vlRadio}
                            paramSdk = {paramSdk}
                            checkEditSt2={checkEditSt2}
                            // typeAction = {'ADD'}
                        />
                    )}
                    {step === "2" && editStep !== "1" && (
                        <div className="unactive-step-2 typing-value-form">
                            <p className="txt-sumary-st2">Thông tin vay</p>
                            <div className="row">
                                <div className="col-4 ">
                                    <label className="col-form-label">Số HĐ vay:</label>
                                    <label className="col-form-label">{loanInsured.lo_contract}</label>
                                </div>
                                <div className="col-4 ">
                                    <label className="col-form-label">Ngày ký HĐ vay:</label>
                                    <label className="col-form-label">{loanInsured.lo_date}</label>
                                </div>
                                <div className="col-4">
                                    <label className="col-form-label">Số tiền vay:</label>
                                    <label className="col-form-label">{numberFormatCommas(loanInsured.lo_total)} VNĐ</label>
                                </div>
                                <div className="col-4">
                                    <label className="col-form-label">Thời hạn vay:</label>
                                    <label className="col-form-label">
                                        {loanInsured.duration + ' '}{loanInsured.duration_unnit ? loanInsured.duration_unnit.label : ''}
                                    </label>
                                </div>
                            </div>
                            {paramSdk.action === 'EDIT' && (
                                <div className="row">
                                    <div className="col-4 ">
                                        <label className="col-form-label">Số lần giải ngân:</label>
                                        <label className="col-form-label">{loanInsured.disbursement ? loanInsured.disbursement.DISBUR_NUM : ''}</label>
                                    </div>
                                    <div className="col-4 ">
                                        <label className="col-form-label">Mã KUNN:</label>
                                        <label className="col-form-label">{loanInsured.disbursement ? loanInsured.disbursement.DISBUR_CODE : ''}</label>
                                    </div>
                                    <div className="col-4">
                                        <label className="col-form-label">Số tiền giải ngân:</label>
                                        <label className="col-form-label">{loanInsured.disbursement ? numberFormatCommas(loanInsured.disbursement.DISBUR_AMOUNT) : ''} VNĐ</label>
                                    </div>
                                </div>
                            )}

                            <p className="txt-sumary-st2">Thông tin NĐBH</p>

                            <div className="row">
                                <div className="col-4">
                                    <label className="col-form-label">Tên NĐBH:</label>
                                    <label className="col-form-label">{loanInsured.name}</label>
                                </div>
                                <div className="col-8">
                                    <label className="col-form-label">Địa chỉ:</label>
                                    <label className="col-form-label">{loanInsured.address + ' -'}</label>
                                    <label className="col-form-label">{loanInsured.provide ? loanInsured.provide.label : ''}</label>
                                </div>
                                <div className="col-4">
                                    <label className="col-form-label">Số CMND/CCCD:</label>
                                    <label className="col-form-label">{loanInsured.idcard}</label>
                                </div>
                                <div className="col-4">
                                    <label className="col-form-label">Ngày cấp:</label>
                                    <label className="col-form-label">{loanInsured.idcard_d}</label>
                                </div>
                                <div className="col-4">
                                    <label className="col-form-label">Nơi cấp:</label>
                                    <label className="col-form-label">{loanInsured.idcard_p}</label>
                                </div>
                                <div className="col-4">
                                    <label className="col-form-label">SĐT:</label>
                                    <label className="col-form-label">{loanInsured.phone}</label>
                                </div>
                                <div className="col-4">
                                    <label className="col-form-label">Email:</label>
                                    <label className="col-form-label">{loanInsured.email}</label>
                                </div>
                            </div>

                            <p className="txt-sumary-st2">Đơn vị thụ hưởng</p>

                            <div className="row">
                                <div className="col-4">
                                    <label className="col-form-label">ĐV thụ hưởng:</label>
                                    <label className="col-form-label">{loanInsured.beneficiary_unit ? loanInsured.beneficiary_unit.label : ''}</label>
                                </div>
                                <div className="col-4">
                                    <label className="col-form-label">CBNH cấp đơn:</label>
                                    <label className="col-form-label">{loanInsured.seller_code + ' - ' + loanInsured.seller_name}</label>
                                </div>
                                <div className="col-4">
                                    <label className="col-form-label">Email CBNH:</label>
                                    <label className="col-form-label">{loanInsured.seller_email}</label>
                                </div>
                            </div>
                        </div>
                    )}
                </div>
                <div className="number-step-timeline">2</div>
            </div>
            <div className="step-vertical-timeline">
                <div className="header-step-timeline">
                    <label className="title-step-timeline">Upload File</label>
                </div>
                <div className="content-step-timeline">
                    {step ==="2" && editStep === "2" && (
                        <>
                            {/*<div className="header-content-step-download">*/}
                            {/*    <label>Tải giấy yêu cầu bảo hiểm:</label>*/}
                            {/*    {loadingDown ? (*/}
                            {/*        <div className="btn-download-document">*/}
                            {/*            <Spinner animation="border" role="status" style={{width: 18, height: 18, marginRight: 8}} />*/}
                            {/*            Vui lòng đợi...*/}
                            {/*        </div>*/}
                            {/*    ) : (*/}
                            {/*        <div className="btn-download-document" onClick={downloadGYC}>*/}
                            {/*            Tải về*/}
                            {/*        </div>*/}
                            {/*    )}*/}

                            {/*</div>*/}
                            <div className="active-step-3">
                                <UploadImg
                                    ref={F3Ref}
                                    dataDefine={listDefine}
                                    list_file={listFile}
                                    sdk_params={props.sdk_params}
                                    // dataStep3={dataStep3}
                                    getCertificate={() => downloadGYC()}
                                />
                            </div>
                            {
                                paramSdk.action === 'EDIT' && isCheckEdit && (
                                    <div>

                                        <div className="row form-group">
                                            <FormCheck custom type="switch">
                                                <FormCheck.Input checked={togglePayment} />
                                                <FormCheck.Label onClick={onChangeTogglePayment}>
                                                    Thanh toán luôn
                                                </FormCheck.Label>
                                            </FormCheck>
                                            {/*MODAL NHAP SO HOP DONG VAY VA NGAY KI HOP DONG VAY*/}
                                            <Modal
                                                show={showModalBSHD}
                                                onHide={closeModal}
                                                aria-labelledby="contained-modal-title-vcenter"
                                                centered
                                            >
                                                <Modal.Header closeButton>
                                                    <Modal.Title className="header-cbbshs">
                                                        Cảnh báo
                                                    </Modal.Title>
                                                </Modal.Header>
                                                <Modal.Body>
                                                    <label>Bạn cần bổ sung thông tin số hợp đồng vay để có thể thanh toán luôn</label>
                                                    <div className="row form-group">
                                                        <div className="col-6">
                                                            <InputBase
                                                                disable={false}
                                                                value={loContract}
                                                                changeEvent={onChangeLoContract}
                                                                label={"Số hợp đồng vay"}
                                                                required={true}
                                                                error={errLoConTract}
                                                            />
                                                        </div>
                                                        <div className="col-6">
                                                            <InputDate
                                                                disable={false}
                                                                value={loDate}
                                                                changeEvent={onChangeLodate}
                                                                selectedDay={selectDate(loDate)}
                                                                maximumDate={maximumDate()}
                                                                label={"Ngày ký hợp đồng vay"}
                                                                required={true}
                                                                error={err}
                                                            />
                                                        </div>
                                                    </div>

                                                    <div className="btn-save-dkbs" onClick={UpdateLo}>
                                                        Lưu
                                                    </div>
                                                </Modal.Body>
                                            </Modal>
                                        </div>
                                        {togglePayment && (
                                            <div>
                                                <div className="row form-group">
                                                    <div className="col-3">
                                                        <SelectBase
                                                            required={true}
                                                            menuPlacement="top"
                                                            label={"Chọn phương thức thanh toán "}
                                                            placeholder={"Chọn phương thức thanh toán"}
                                                            value={paymentMethod}
                                                            options={listPmMethod}
                                                            onChange={value => handleChangePayentMethod(value)}
                                                        />
                                                    </div>
                                                    {/*{paymentMethod.value !== 'CTT' && (*/}
                                                    {/*    <div className="col-3" style={{margin: 'auto 0'}}>*/}
                                                    {/*        <FormCheck custom type="switch">*/}
                                                    {/*            <FormCheck.Input  checked={toggleSMS} />*/}
                                                    {/*            <FormCheck.Label onClick={onChangeToggleSMS}>*/}
                                                    {/*                Gửi nội dung chuyển khoản qua SMS*/}
                                                    {/*            </FormCheck.Label>*/}
                                                    {/*        </FormCheck>*/}
                                                    {/*    </div>*/}
                                                    {/*)}*/}
                                                    {paymentMethod.value !== 'CTT' && (
                                                        <div className="col-3" style={{margin: 'auto 0'}}>
                                                            <FormCheck custom type="switch">
                                                                <FormCheck.Input  checked={toggleEmail} />
                                                                <FormCheck.Label onClick={onChangeToggleEmail}>
                                                                    Gửi nội dung chuyển khoản qua Email
                                                                </FormCheck.Label>
                                                            </FormCheck>
                                                        </div>
                                                    )}
                                                </div>
                                            </div>
                                         )}

                                    </div>
                                )
                            }
                            {isCheckEdit && (
                                <div className="form-btn-confirm">
                                    {paramSdk.action === 'EDIT' && togglePayment &&  (
                                        <div>
                                            {loadingSavePay ? (
                                                <div className="btn-next-step">
                                                    <Spinner animation="border" role="status" style={{width: 18, height: 18, marginRight: 8}} />
                                                    Vui lòng đợi...
                                                </div>
                                            ) : (
                                                <div className="btn-next-step" onClick={() => handleSaveAndPayment('PAY')}>
                                                    Lưu và thanh toán
                                                </div>
                                            )}
                                        </div>
                                    )}

                                    {loadingSave ? (
                                        <div className="btn-download-document">
                                            <Spinner animation="border" role="status" style={{width: 18, height: 18, marginRight: 8}} />
                                            Vui lòng đợi...
                                        </div>
                                    ): (
                                        <div className="btn-next-step" onClick={() => handleSaveAndPayment('SAVE')}>
                                            Lưu
                                        </div>
                                    )}

                                </div>
                            )}
                        </>
                    )}
                </div>
                <div className="number-step-timeline">3</div>
            </div>
            {/*MODAL THONG BAO CHUYEN KHOAN*/}
            <Modal
                show={showModalCK}
                onHide={() => setShowModalCK(false)}
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title className="header-sms">
                        Thông báo
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <label>Quý khách vui lòng Chuyển khoản với nội dung "<label className="sms-ck">{valueSMS}</label>" cho tài khoản:</label>
                    <p>
                        - Chủ tài khoản: CÔNG TY TNHH BẢO HIỂM HD
                    </p>
                    <p>
                        - STK: 168704079888888
                    </p>
                    <p>
                        - Ngân hàng HD Bank chi nhánh Nguyễn Đình Chiểu
                    </p>
                    <div className="btn-save-dkbs" onClick={() => setShowModalCK(false)}>
                        Đóng
                    </div>
                </Modal.Body>
            </Modal>
        </div>
    )
}
export default Main;
