import React, {useEffect, useState} from "react";
import moment from 'moment';
import InputDate from "../../component/InputDate";
import {Form} from "react-bootstrap";
import InputBase from "../../component/InputBase";
import SelectBase from "../../component/SelectBase";
import {
    setOptionSelect, calculateDate, numberFormatCommas,
    showDialogDis, convertDate, checkFormatDate, selectDate, compareDate, numberWithCommas, numberFormatDot
} from "../../service/Utils";
import { utils } from "react-modern-calendar-datepicker";
import InputDateTime from "../../component/InputDateTime";

import Network from "../../service/Network";
import FLInputDate from "../../component/FLInputDate";
import LoadingForm from "../../component/Loading";
const api = new Network();

const PackageInfo = (props) => {

    let errors ={};
    let formIsValid = true;
    const [itemPack, setItemPack] =  useState(props.itemPack);

    const [checkVLSt1, setCheckVlSt1] = useState(props.checkEditSt1);
    // const itemPack =  props.itemPack ? props.itemPack : {};

    // console.log('props.valueDetail', vlDetail);


    // const vlDetail =  ;
    const currentDate = moment(new Date()).format("DD/MM/YYYY");
    const addDate = moment(new Date()).add(1, 'years').format("DD/MM/YYYY");

    const [listDefine, setListDefine] = useState(props.dataDefine);
    const [effectiveDate, setEffectiveDate] = useState(itemPack.effectiveDate || currentDate);
    const [expirationDate, setExpirationDate] = useState(itemPack.expirationDate || addDate);
    const [insurTotalAmout, setInsurTotalAmout] = useState(itemPack.insurTotalAmout);
    const [selectDurationPay, setSelectDurationPay] = useState(itemPack.selectDurationPay);
    const [selectRegion, setSelectRegion] = useState(itemPack.selectRegion);
    const [selectInsurRange, setSelectInsurRange] = useState(itemPack.selectInsurRange);
    const [packFess, setPackFess] = useState(itemPack.packFess);
    const [totalDiscount, setTotalDiscount] = useState(itemPack.totalDiscount);
    const [totalAmout, setTotalAmout] = useState(itemPack.totalAmout);
    // const [selectDiscount, setSelectDiscount] = useState(itemPack.selectDiscount);
    const [durationInsur, setDurationInsur] = useState({month_diff: 0, day_diff: 0});
    const [discount, setDiscount] = useState(itemPack.discount ? itemPack.discount : 0 );
    const [totalAYear, setTotalAYear] = useState(null);
    // const [effectiveDatePeriod, setEffectiveDatePeriod] = useState(null);
    // const [editDate, setEditDate] = useState(false);
    const [disableSelect, setDisableSelect] = useState(false);
    const [listPeriod, setListPeriod] = useState(itemPack.period ? itemPack.period : []);
    const [error, setError] = useState({});

    const [packageInsur, setPackageInsur] = useState(itemPack.packageInsur);

    const [loadingPack, setLoadingPack] = useState(false);

    // const selectedEffectiveDate = () => {
    //     const rs1 = moment(effectiveDate, 'DD/MM/YYYY').format('YYYY-MM-DD');
    //    return { year: moment(rs1).year(),
    //        month: moment(rs1).month() + 1,
    //        day: moment(rs1).date()}
    // };

    // const selectedExpirationDate = () => {
    //         const rs2 = moment(expirationDate, 'DD/MM/YYYY').format('YYYY-MM-DD');
    //         return { year: moment(rs2).year(),
    //             month: moment(rs2).month() + 1,
    //             day: moment(rs2).date()}
    // };

    useEffect(() =>{
        setCheckVlSt1(props.checkEditSt1)
    },[checkVLSt1])


    useEffect(() =>{
        if(props.paramSdk && props.paramSdk.action === 'EDIT'){
            setItemPack(props.itemPack)
            if(props.itemPack){
                setEffectiveDate(props.itemPack.effectiveDate || currentDate);
                setExpirationDate(props.itemPack.expirationDate || addDate);
                setInsurTotalAmout(numberFormatCommas(props.itemPack.insurTotalAmout));
                setTotalAmout(numberFormatCommas(props.itemPack.totalAmout));
                setSelectRegion(props.itemPack.selectRegion);
                setSelectInsurRange(props.itemPack.selectInsurRange);
                setSelectDurationPay(props.itemPack.selectDurationPay);
                setItemPack(props.itemPack);
                if(props.itemPack.selectInsurRange){
                    let rs1 =  props.itemPack.selectInsurRange.value;
                    if(rs1 === 'A'){
                        setPackageInsur({A: true, B: false, C: false});
                    }else if(rs1 === 'ABC'){
                        setPackageInsur({A: true, B: true, C:true})
                    }else if(rs1 === 'AB'){
                        setPackageInsur({A: true, B: true, C: false});
                    }else if(rs1 === 'AC'){
                        setPackageInsur({A: true, B: false, C: true});
                    }
                }
            }
        }else if(props.paramSdk && props.paramSdk.action === 'ADD' && checkVLSt1){
            setEffectiveDate(currentDate);
            setExpirationDate(addDate);
            setInsurTotalAmout('0');
            if(listDefine){
                const rs = listDefine[7] ?  [listDefine[7][0]] : [];
                if(rs.length > 0){
                    setSelectInsurRange({value: rs[0].TYPE_CODE, label: rs[0].TYPE_NAME})
                }
                const rs1 = listDefine[8] ?  [listDefine[8][0]] : [];
                if(rs1.length > 0){
                    setSelectDurationPay({value: rs1[0].TYPE_CODE, label: rs1[0].TYPE_NAME})
                }
            }
        }
    },[props.paramSdk,props.itemPack, props.checkEditSt1])

    useEffect(() =>{
        if(selectInsurRange){
            if(selectInsurRange.value === 'A'){
                setPackageInsur({A: true,B: false, C: false})
            }else if(selectInsurRange.value === 'ABC'){
                setPackageInsur({A: true, B: true, C:true})
            }else if(selectInsurRange.value === 'AB'){
                setPackageInsur({A: true, B: true, C: false});
            }else if(selectInsurRange.value === 'AC'){
                setPackageInsur({A: true, B: false, C: true});
            }
        }
    },[selectInsurRange])

    useEffect(() => {
        if(props.typeRadio === 'DISBUR'){
            if(listDefine[8]){
                if(listDefine[8][0]){
                    const rs = [listDefine[8][0]]
                    if(rs.length > 0){
                        setSelectDurationPay({value: rs[0].TYPE_CODE, label: rs[0].TYPE_NAME})
                    }
                }
            }
        }
    },[props.typeRadio])

    useEffect(() =>{
        if( effectiveDate && expirationDate){
            if(compareDate(expirationDate, effectiveDate)){
                errors['effectiveDate'] = 'Ngày hiệu lực không được lớn hơn ngày kết thúc';
                setError(errors);
                formIsValid = false;
            }else if(compareDate(effectiveDate, currentDate)){
                errors['effectiveDate'] = 'Ngày hiệu lực không được nhỏ hơn ngày hiện tại';
                setError(errors);
                formIsValid = false;
            }else{
                errors['effectiveDate'] = null;
                setError(errors);
                setDurationInsur(calculateDate(effectiveDate, expirationDate))
            }
        }
    },[effectiveDate, expirationDate])

    useEffect(() =>{
        setListDefine(props.dataDefine);
        if(props.dataDefine){
            // setListDefine(props.dataDefine);
            if(props.dataDefine[8] && !itemPack.selectDurationPay){
               if(props.dataDefine[8][0]){
                   const rs = [props.dataDefine[8][0]]
                   if(rs.length > 0){
                       setSelectDurationPay({value: rs[0].TYPE_CODE, label: rs[0].TYPE_NAME, discount_max: null, discount_unit: null})
                   }
                   // setSelectDurationPay(setOptionSelect(rs))
               }
            }
            if(props.dataDefine[1] && !itemPack.selectRegion){
                if(props.dataDefine[1][0]){
                    const rs = [props.dataDefine[1][0]]
                    if(rs.length > 0){
                        setSelectRegion({value: rs[0].TYPE_CODE, label: rs[0].TYPE_NAME})
                    }
                }
            }
            if(props.dataDefine[7] && !itemPack.selectInsurRange){
                if(props.dataDefine[7][0]){
                    const rs = [props.dataDefine[7][0]]
                    if(rs.length > 0){
                        if(rs[0].DISCOUNT_MAX === "0"){
                            setDisableSelect(true);
                        }
                        setSelectInsurRange({value: rs[0].TYPE_CODE, label: rs[0].TYPE_NAME, discount_max: rs[0].DISCOUNT_MAX, discount_unit: rs[0].DISCOUNT_UNIT})
                    }
                }
            }
            setListDefine(props.dataDefine);
        }
    },[props.dataDefine]);

    useEffect(() => {
        if(selectInsurRange){
            let typeCheck = selectInsurRange.discount_max;
            if(typeCheck !== "0"){
                setDisableSelect(false)
            }else{
                setDisableSelect(true)
            }
        }
    },[selectInsurRange]);

    useEffect(() => {
        if(effectiveDate && expirationDate && insurTotalAmout && selectRegion && selectInsurRange){
            if(validateForm()){
                getTotalAmount();
            }
        }
    },[effectiveDate, expirationDate, selectRegion, selectInsurRange])

    useEffect(() =>{
        if(selectDurationPay && expirationDate){
            const typeSelect = selectDurationPay.value;
            const monthChange = durationInsur.month_diff;
            if(typeSelect === 'Y' && monthChange && monthChange < 12){
                showDialogDis('Chỉ được chọn thanh toán theo năm khi thời hạn bảo hiểm lớn hơn 1 năm.');
                if(listDefine[8] && !itemPack.selectDurationPay){
                    if(listDefine[8][0]){
                        const rs = [listDefine[8][0]];
                        if(rs.length > 0){
                            setSelectDurationPay({value: rs[0].TYPE_CODE, label: rs[0].TYPE_NAME, discount_max: null, discount_unit: null})
                        }
                    }
                }
            }else if (typeSelect === 'Y' && monthChange > 12 && props.typeRadio !== 'DISBUR'){
                let lenthArr = Math.ceil(monthChange/12);
                let arr = [];
                for ( let i = 0; i < lenthArr; i++){
                    let obj = {};
                    obj['NAME'] = `${i}`;
                    obj['EFFECTIVE_DATE'] = moment(effectiveDate,'DD/MM/YYYY').add(i, 'years').format('DD/MM/YYYY');
                    obj['CURRENCY'] = 'VND';
                    if(i === lenthArr -1){
                        let re = lenthArr -1;
                        obj['AMOUNT'] = totalAmout - (totalAYear * re);
                    }else {
                        obj['AMOUNT'] = totalAYear;
                    }
                    arr.push(obj);
                }
                setListPeriod(arr);
            }else if(typeSelect === 'ALL' || props.typeRadio === 'DISBUR'){
                let itemList ={
                    "CODE": "",
                    "NAME": "0",
                    "EFFECTIVE_DATE": effectiveDate,
                    "EXPIRATION_DATE": "",
                    "CURRENCY": "VND",
                    "AMOUNT": totalAmout ? totalAmout : 0
                }
                let listDataPeriod = [];
                listDataPeriod.push(itemList);
                setListPeriod(listDataPeriod);
            }
        }
    },[durationInsur.month_diff, effectiveDate, expirationDate, props.dataDefine, selectDurationPay, totalAYear, totalAmout, props.typeRadio])


    const  getTotalAmount = async () => {
        setLoadingPack(true);
        // let numberDis = '';
        // let discount_max = selectInsurRange ? selectInsurRange.discount_max : '';
        //
        // if(parseInt(discount) <= parseInt(discount_max)){
        //     numberDis = discount;
        // }else if(parseInt(discount) > parseInt(discount_max)){
        //     showDialogDis('Giảm phí cao hơn giảm phí cho phép!');
        //     numberDis = 0;
        //     setDiscount(0);
        // }

        const  paramTotalAmout = {
            "Action": {
                "ParentCode": "HDI_WEB",
                "UserName": "HDI_WEB",
                "Secret": "B72088C7067CJF9FE0551F6E21B40329",
                "ActionCode": "API0OVG3B1"
            },
            "Data": {
                "org_code": "HDBANK_VN",
                "product_code": "ATTD",
                "pack_code": selectInsurRange.value ? selectInsurRange.value : '',
                "dob": "",
                "gender": "",
                "region":selectRegion.value ? selectRegion.value : '' ,
                "insur_total_amount":insurTotalAmout ? insurTotalAmout.split(',').join("") : '0',
                "eff_date":effectiveDate ? effectiveDate : '',
                "exp_date": expirationDate ? expirationDate : '',
                "currency": "",
                // "discount": numberDis,
                "discount": 0,
                // "discount_unit":  selectInsurRange.discount_unit ? selectInsurRange.discount_unit : '',
                "discount_unit":  'M',
                "gift_code" : '',
            },
        };
        try {
            let rs =  await api.post(`/OpenApi/Post`, paramTotalAmout);
            if(rs.Success){
                try{
                    if(rs.Data.length > 0 && rs.Data[0].length > 0){
                        setTotalAmout(rs.Data[0][0].TOTAL_AMOUNT);
                        setPackFess(rs.Data[0][0].PACK_FESS);
                        setTotalDiscount(rs.Data[0][0].TOTAL_DISCOUNT);
                        setTotalAYear(rs.Data[0][0].TOTAL_1_YEAR);
                        setLoadingPack(false);
                    }
                }catch (e){
                    console.log(e);
                    setLoadingPack(false);
                }
                return rs;
            }else{
                showDialogDis(rs.ErrorMessage);
                setLoadingPack(false);
            }
            // await api.post(`/OpenApi/Post`, paramTotalAmout).then(data =>{
            //
            // });
        } catch (e) {
            setLoadingPack(false);
            console.log(e);
        }
    };

    const onChangeExpirationDate = (value) => {
        if(checkFormatDate(value)){
            setExpirationDate(value);
            setDurationInsur(calculateDate(effectiveDate, value));
        }
    }

    const onChangeEffectiveDate = (value) => {
        if(checkFormatDate(value)){
            setEffectiveDate(value);
        }
    }

    const onChangeInsurTotalAmout = (value) =>{
        setInsurTotalAmout(numberFormatCommas(value));
    }

    const onBlurINsurTotalAmout = () =>{
        if(effectiveDate && expirationDate && insurTotalAmout && selectRegion && selectInsurRange){
            if(validateForm()){
                getTotalAmount();
            }
        }
    }

    const handleChangeSelect = (data, type) =>{
        switch (type){
            case "PVDL":
                setSelectRegion(data);
                break;
            case "PVBH":
                setSelectInsurRange(data);
                // const rs = listDefine.length > 0 ? listDefine[2].filter(vl => vl.TYPE_CODE === data.discount_unit) : null;
                // if(rs.length > 0){
                //     setSelectDiscount(setOptionSelect(rs));
                // }else{
                //     setSelectDiscount(null);
                // }

                break;
            // case "DIS":
            //     setSelectDiscount(data);
            //     break;
            case "HTTT":
                if(data.value === 'Y' && props.typeRadio === 'DISBUR'){
                    showDialogDis('Cấp đơn theo khế ước nhận nợ chỉ được thanh toán 1 lần !');
                }else{
                    setSelectDurationPay(data);
                }
                break;
            default:
                break;
        }
    }

    const renderTableData = () => {
        return listPeriod.map((vl,  index) => {
            const { EFFECTIVE_DATE, AMOUNT } = vl;
            return (
                <tr key={index.toString()}>
                    <td>{index + 1}</td>
                    <td>
                        {EFFECTIVE_DATE}
                        {/*{editDate ? (*/}
                        {/*    <div className="input-table-edit">*/}
                        {/*        <InputDate*/}
                        {/*            disable={false}*/}
                        {/*            value={effectiveDate}*/}
                        {/*            changeEvent={onChangeEffectiveDate}*/}
                        {/*            onBlur={() =>  setEditDate(!editDate)}*/}
                        {/*            selectedDay={selectedEffectiveDate()}*/}
                        {/*            minimumDate={utils().getToday()}*/}
                        {/*        />*/}
                        {/*    </div>*/}
                        {/*) : (*/}
                        {/*    <div onClick={() => setEditDate(!editDate)}>*/}
                        {/*        {EFFECTIVE_DATE}*/}
                        {/*    </div>*/}
                        {/*    )}*/}
                    </td>
                    <td>{AMOUNT ? numberFormatCommas(AMOUNT) : 0 } VNĐ</td>
                </tr>
            )
        })
    }

    // const onChangeDiscount = (value) => {
    //     if(value){
    //         let rs = value.replace('-','');
    //         setDiscount(rs);
    //     }
    // }
    //
    // const onBlurDiscount = () => {
    //     if(expirationDate && insurTotalAmout && selectRegion && selectInsurRange){
    //         getTotalAmount();
    //     }
    // }

    const handleNextStep = () =>{
        if(validateForm()){
                getTotalAmount().then(rs =>{
                    if(rs.Success){
                        try{
                            if(rs.Data.length > 0 && rs.Data[0].length > 0){
                                let listTablePeriod = [];
                                if(totalAmout === "0"){
                                    let itemList ={
                                        "CODE": "",
                                        "NAME": "0",
                                        "EFFECTIVE_DATE": effectiveDate,
                                        "EXPIRATION_DATE": "",
                                        "CURRENCY": "VND",
                                        "AMOUNT": rs.Data[0][0].TOTAL_AMOUNT
                                    }
                                    listTablePeriod.push(itemList);
                                }
                                props.setItemPack({
                                    effectiveDate: effectiveDate,
                                    expirationDate: expirationDate,
                                    insurTotalAmout: insurTotalAmout ? insurTotalAmout.split(',').join("") : '0',
                                    selectDurationPay: selectDurationPay,
                                    selectRegion: selectRegion,
                                    selectInsurRange: selectInsurRange,
                                    // selectDiscount: selectDiscount,
                                    packFess: packFess,
                                    discount:discount,
                                    totalDiscount: totalDiscount,
                                    totalAmout:rs.Data[0][0].TOTAL_AMOUNT,
                                    period: totalAmout === "0"  ? listTablePeriod :  listPeriod,
                                    packageInsur: packageInsur,
                                });
                            }
                        }catch (e){
                            console.log(e);
                        }
                        setLoadingPack(false);
                    }else{
                        showDialogDis(rs.ErrorMessage);
                        setLoadingPack(false);
                    }
                });
            props.onNextStep();
        }
    };

    const validateForm = () =>{
        if(insurTotalAmout){
            const reg = new RegExp('^[0-9]+$');
            const numInsur = insurTotalAmout.split(',').join("");
            if(!reg.test(numInsur)){
                formIsValid = false;
                errors['insurTotalAmout'] = 'Số tiền bảo hiểm không đúng định dạng số';
            }
            if(insurTotalAmout === '0'){
                formIsValid = false;
                errors['insurTotalAmout'] = 'Số tiền bảo hiểm phải lớn hơn 0';
            }
        }
        if(!insurTotalAmout){
            formIsValid = false;
            errors['insurTotalAmout'] = 'Vui lòng nhập số tiền bảo hiểm';
        } else if(compareDate(expirationDate, effectiveDate)){
            errors['effectiveDate'] = 'Ngày hiệu lực không được lớn hơn ngày kết thúc';
            setError(errors);
            formIsValid = false;
        }else if(compareDate(effectiveDate, currentDate)){
            errors['effectiveDate'] = 'Ngày hiệu lực không được nhỏ hơn ngày hiện tại';
            setError(errors);
            formIsValid = false;
        }else if(compareDate(expirationDate, currentDate)){
            errors['expirationDate'] = 'Ngày kết thúc không được nhỏ hơn ngày hiện tại';
            setError(errors);
            formIsValid = false;
        }
        setError(errors);
        return formIsValid;
    }

    return(
        <form className="" id={"id-step-1"}>
            {loadingPack && <LoadingForm />}
            {/*{props.children}*/}
            <div className="row">
                <div className="col-3">
                    <InputDate
                        disable={false}
                        value={effectiveDate}
                        changeEvent={onChangeEffectiveDate}
                        selectedDay={selectDate(effectiveDate)}
                        minimumDate={utils().getToday()}
                        label={"Ngày hiệu lực"}
                        required={true}
                        error={error.effectiveDate}
                    />
                </div>
                <div className="col-3">
                    <InputDate
                        disable={false}
                        value={expirationDate}
                        changeEvent={onChangeExpirationDate}
                        selectedDay={selectDate(expirationDate)}
                        minimumDate={utils().getToday()}
                        label={"Ngày kết thúc"}
                        required={true}
                        error={error.expirationDate}
                    />
                </div>

                <div className="col-3">
                    <Form.Group>
                        <Form.Label>Thời hạn bảo hiểm :</Form.Label>
                        <Form.Text className="txt-duration-insur">
                            {durationInsur.month_diff + ' tháng ' + durationInsur.day_diff + ' ngày'}
                        </Form.Text>
                    </Form.Group>
                </div>
                <div className="col-3">
                    <InputBase
                        disable={false}
                        value={numberFormatCommas(insurTotalAmout)}
                        changeEvent={onChangeInsurTotalAmout}
                        onBlur={onBlurINsurTotalAmout}
                        label={"Số tiền BH (VNĐ)"}
                        required={true}
                        error={error.insurTotalAmout}
                    />
                </div>
            </div>

            <div className="row">
                <div className="col-3">
                    <SelectBase
                        required={true}
                        label={"Phạm vi địa lý"}
                        placeholder="Chọn phạm vi địa lý"
                        value={selectRegion}
                        options={setOptionSelect(listDefine.length > 0 ? listDefine[1] : [])}
                        onChange={value => handleChangeSelect(value, "PVDL")}
                    />
                </div>
                <div className="col-3">
                    <SelectBase
                        required={true}
                        label={"Phạm vi bảo hiểm"}
                        placeholder="Chọn phạm vi bảo hiểm"
                        value={selectInsurRange}
                        options={setOptionSelect(listDefine.length > 0 ? listDefine[7] : [])}
                        onChange={value => handleChangeSelect(value, "PVBH")}
                        tooltip
                    />
                </div>

                {/*<div className="col-3">*/}
                {/*    <SelectBase*/}
                {/*        label={"Giảm phí theo"}*/}
                {/*        disable={disableSelect}*/}
                {/*        placeholder="Chọn giảm phí theo"*/}
                {/*        value={selectDiscount}*/}
                {/*        options={setOptionSelect(listDefine.length > 0 ? listDefine[2] : [])}*/}
                {/*        onChange={value => handleChangeSelect(value, "DIS")}*/}
                {/*    />*/}
                {/*</div>*/}
                {/*<div className="col-3">*/}
                {/*    <InputBase*/}
                {/*        // type="text"*/}
                {/*        disable={disableSelect}*/}
                {/*        value={discount}*/}
                {/*        changeEvent={onChangeDiscount}*/}
                {/*        onBlur={onBlurDiscount}*/}
                {/*        label={"Giảm phí"}*/}
                {/*        type={'number'}*/}
                {/*        pattern="/^-?[0-9]*$/"*/}
                {/*        required={false}*/}
                {/*    />*/}
                {/*</div>*/}
                <div className="col-3">
                    <Form.Group>
                        <Form.Label>Tổng tiền thanh toán :</Form.Label>
                        <Form.Text className="txt-total-payment">{totalAmout ? numberFormatCommas(totalAmout) : 0} VNĐ</Form.Text>
                    </Form.Group>
                </div>
                <div className="col-3">
                    <SelectBase
                        required={true}
                        label={"Hình thức thanh toán"}
                        placeholder="Chọn hình thức thanh toán"
                        value={selectDurationPay}
                        options={setOptionSelect(listDefine.length > 0 ? listDefine[8] : [])}
                        onChange={value => handleChangeSelect(value, "HTTT")}
                    />
                </div>
            </div>
            <div className="row">
                <div className="col-6" style={{margin: '0 auto'}} >
                    <div className="form-table">
                        <table id='payment-expected'>
                            <tbody>
                            <tr>
                                <th>STT</th>
                                <th>Ngày thanh toán dự kiến</th>
                                <th>Số tiền</th>
                            </tr>
                            {renderTableData()}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div className="btn-next-step" onClick={handleNextStep}>
                Tiếp tục
            </div>
        </form>
    )
}
export default PackageInfo;
