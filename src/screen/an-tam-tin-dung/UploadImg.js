import React, {
    createRef,
    forwardRef,
    useEffect,
    useImperativeHandle,
    useState,
} from "react";
import { Col, Form, Row } from "react-bootstrap";
import Upload from "../../component/UploadFile/index";
import {showDialogDis} from "../../service/Utils";

const Main = forwardRef((props, ref) => {
    const formRef = createRef();

    const [validateForm, setValidateForm] = useState(false);

    const [listGyc, setListGyc] = useState(props?.list_file?.list_gyc || []);
    const [listCmnd, setListCmnd] = useState(props?.list_file?.list_cmnd || []);
    const [listKhac, setListKhac] = useState(props?.list_file?.list_khac || []);

    const listUpload = [
        {
            title: "Giấy yêu cầu BH",
            required: true,
            type: "GYC",
            uploadRef: createRef(),
        },
        {
            title: "CMND/ CCCD/ Hộ chiếu",
            required: true,
            type: "CMND",
            uploadRef: createRef(),
        },
        {
            title: "Các file khác",
            required: false,
            type: "KHAC",
            uploadRef: createRef(),
        },
    ];
    useImperativeHandle(ref, () => ({
        handleSubmit() {
            const form = formRef.current;
            if (listGyc.length === 0) {
                showDialogDis("Vui lòng thêm file giấy yêu cầu bảo hiểm.");
                return false;
            }else if(listCmnd.length === 0){
                showDialogDis("Vui lòng thêm file CMND/ CCCD/ Hộ chiếu.");
                return false;
            }
            if (form.checkValidity() === false) {
                setValidateForm(true);
                return false;
            }
            return {
                list_gyc: listGyc,
                list_cmnd: listCmnd,
                list_khac: listKhac,
            };
        },
    }));

    useEffect(() => {
        setListGyc(props?.list_file?.list_gyc || []);
        setListCmnd(props?.list_file?.list_cmnd || []);
        setListKhac(props?.list_file?.list_khac || []);
    }, [props?.list_file]);

    // useEffect(() => {
    //     console.log("GYC", listGyc);
    // }, [listGyc]);
    //
    // useEffect(() => {
    //     console.log("CMND", listCmnd);
    // }, [listCmnd]);
    //
    // useEffect(() => {
    //     console.log("KHAC", listKhac);
    // }, [listKhac]);

    const setFileUpload = (idx, files) => {
        switch (idx) {
            case "GYC":
                setListGyc(files);
                break;
            case "CMND":
                setListCmnd(files);
                break;
            case "KHAC":
                setListKhac(files);
                break;
            default:
                break;
        }
    };
    const openDropZone = (uploadRef) => {
        // console.log("uploadRef", uploadRef);
        uploadRef?.current?.openDropZone();
    };
    return (
        <Form ref={formRef} noValidate validated={validateForm}>
            <div className="header-content-step-download">
                <label>Tải giấy yêu cầu bảo hiểm:</label>
                <div
                    className="btn-download-document"
                    onClick={() => props.getCertificate()}
                >
                    Tải về
                </div>
            </div>
            <Row className="form-group">
                {listUpload &&
                listUpload.map((vl, i) => {
                    let dataFile = [];
                    switch (vl?.type) {
                        case "GYC":
                            dataFile = listGyc;
                            break;
                        case "CMND":
                            dataFile = listCmnd;
                            break;
                        case "KHAC":
                            dataFile = listKhac;
                            break;
                    }
                    return (
                        <Col md={4} key={i}>
                            <Form.Group className="form-group-download">
                                <Form.Label>
                                    {vl.title}
                                    {vl.required ? (
                                        <span style={{ color: "#DA2128" }}>*</span>
                                    ) : null}
                                </Form.Label>
                                {dataFile.length > 0 && (
                                    <Form.Label
                                        className="add-img"
                                        onClick={() => openDropZone(vl?.uploadRef)}
                                    >
                                        Thêm File
                                    </Form.Label>
                                )}
                                <Upload
                                    idx={vl?.type}
                                    setFileUpload={setFileUpload}
                                    dataFile={dataFile}
                                    ref={vl?.uploadRef}
                                    typeUpload={"image/jpeg, image/png, application/pdf"}
                                />
                            </Form.Group>
                        </Col>
                    );
                })}
            </Row>
            {/* <button type="button" onClick={() => uploadfile()}>
        up file
      </button> */}
        </Form>
    );
});

export default Main;
