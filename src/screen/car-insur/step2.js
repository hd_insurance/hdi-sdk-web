import React, {useEffect, useState} from "react";
import {Form, FormCheck, Modal, Spinner} from "react-bootstrap";
import {config, caculate_fees, caculate_fees_tndstn, caculate_fees_vcx, ad} from './concec';
import moment from "moment";

import FLInput from "../../component/FlInput";
import FLSelect from "../../component/FLSelect/index";
import FLInputDate from "../../component/FLInputDate/index";
import FlInputTime from "../../component/FLInputTime/index";
import {
    getListYear,
    selectDate,
    numberWithCommas,
    numberFormatCommas,
    showModalScanQR,
    showDialogDis,
    caculDateLeapYear,
    compareDate,
    getScanQR,
    getFileSocket
} from "../../service/Utils";
import FLAddress from "../../component/FlAddress";
import FLInputTextArea from "../../component/FlInput/textarea";
import SlideImage from "./slide-image-vcx";
import Network from "../../service/Network";
import {io} from "socket.io-client";
import CarouselImage from "../../component/carousel-image";
import {utils} from "react-modern-calendar-datepicker";

const api = new Network();

const listRadioCheck = [
    {
        value: '1',
        label: 'Cấp đơn lẻ',
    },
    {
        value: '0',
        label: 'Cấp đơn gộp',
    },
];

const listBenificary = [
    {
        value: 'BAN_THAN',
        label: 'Chủ xe',
    },
    {
        value: 'KHAC',
        label: 'Khác',
    }
];

const Main = (props) =>{

    const currentDate = moment(new Date()).format("DD/MM/YYYY");
    const addDate = moment(new Date()).add(1, 'years').format("DD/MM/YYYY");
    const curTime = moment(new Date()).format("HH:mm");

    const [infoCar, setInfoCar] = useState(props.vehicleInsur);

    const [objFee, setObjFee] = useState(infoCar.objFeeTNDS ? infoCar.objFeeTNDS :caculate_fees); //obj cau truc tinh phi TNDS Bat buoc

    const [objFeeTN, setObjFeeTN] = useState(infoCar.objFeeTN ? infoCar.objFeeTN : caculate_fees_tndstn); //obj cau truc tinh phi TNDS tu nguyen

    const [dataDefine, setDataDefine] = useState(props.listDefineCar);

    const [listVerhicleGroup, setListVerhicleGroup] = useState([]);
    const [listVerhicleType, setListVerhicleType] = useState([]);
    const [listUsePurpose, setListUsePurpose] = useState([]);
    const [listInvestigation, setListInvestigation] = useState([]);
    const [listManufacturer, setListManufacturer] = useState([]);
    const [listVehicleBrand, setListVehicleBrand] = useState([]);
    const [listDkbs, setListDkbs] = useState([]);

    const listThietHai1 = props.listDefineCar ? props.listDefineCar[7] : [];

    const listThiethai2 = props.listDefineCar ? props.listDefineCar[8] : [];
    const listThiethai3 = props.listDefineCar ? props.listDefineCar[9] : [];
    const listThiethai4 = props.listDefineCar ? props.listDefineCar[10] : [];
    const list_bh_ngoitrenxe = props.listDefineCar ? props.listDefineCar[11] : [];
    const list_bh_hh = props.listDefineCar ? props.listDefineCar[12] : [];
    const list_ctbh = props.listDefineCar ? props.listDefineCar[13] : [];

    const list_muc_mienthuong = props.listDefineCar ? props.listDefineCar[14] : [];

    // let list_dkbs = props.listDefineCar ? props.listDefineCar[15] : [];

    const img_qr_scan = props.listDefineCar ? props.listDefineCar[17] : [];

    const dataCustomerType = props.listDefineCar.length > 0 ? props.listDefineCar[1] : [];
    const dataGender = props.listDefineCar.length > 0 ? props.listDefineCar[0] : [];

    // const list_muc_mienthuong = props.listDefineCar ? props.listDefineCar[14] : [];

    const [vehicleGroup, setVehicleGroup] = useState(infoCar.VEHICLE_GROUP ? infoCar.VEHICLE_GROUP : null); //Nhom xe
    const [typeVehicle, setTypeVehicle] = useState(infoCar.VEHICLE_TYPE ? infoCar.VEHICLE_TYPE : null); //Loai xe

    const [usesPurpose, setUsesPurpose] = useState(infoCar.VEHICLE_USE ? infoCar.VEHICLE_USE : null); // Muc dich su dung
    const [manufacturer , setManufacturer] = useState(infoCar.BRAND ? infoCar.BRAND : null); // Hang xe
    const [vehicleBrand, setVehicleBrand] = useState(infoCar.MODEL ? infoCar.MODEL :null); // Hieu xe
    const [numberSeats, setNumberSeats] = useState(infoCar.SEAT_NO ? infoCar.SEAT_NO :null); // So cho ngoi
    const [weigh, setWeigh] = useState(infoCar.WEIGH ? infoCar.WEIGH : null); // Trọng tải (tấn)
    const [numberPlate, setNumberPlate] = useState(infoCar.NUMBER_PLATE ? infoCar.NUMBER_PLATE :null); // Bien so xe
    const [chassisNo, setChassisNo] = useState(infoCar.CHASSIS_NO ? infoCar.CHASSIS_NO : null); // So khung
    const [engineNo, setEngineNo] = useState(infoCar.ENGINE_NO ? infoCar.ENGINE_NO :null); // So may

    const [mfg, setMfg] = useState(infoCar.MFG ? infoCar.MFG : null); // nam san suat
    const [vehicleRegis, setVehicleRegis] = useState( infoCar.VEHICLE_REGIS || currentDate); //Ngay dang ky xe

    const [isSerialNum, setIsSerialNum] = useState(infoCar.COMPULSORY_CIVIL ? (infoCar.COMPULSORY_CIVIL?.IS_SERIAL_NUM === "1" ? true : false) : false); //Toggle Da cap an chi hay chua
    const [serialNum, setSerialNum] = useState(infoCar.COMPULSORY_CIVIL ? infoCar.COMPULSORY_CIVIL.SERIAL_NUM : ""); //So GCN tren phoi
    const [eff, setEff] = useState(infoCar.COMPULSORY_CIVIL?.EFF || currentDate); //Ngay hieu luc
    const [effTime, setEffTime] = useState(infoCar.COMPULSORY_CIVIL?.TIME_EFF || curTime); //Gio hieu luc
    const [exp, setExp] = useState(infoCar.COMPULSORY_CIVIL?.EXP || addDate); //Ngay ket thuc
    const [expTime, setExpTime] = useState(infoCar.COMPULSORY_CIVIL?.TIME_EXP || curTime); //Gio ket thuc

    const [toggleBKS, setToggleBKS] = useState(infoCar.NONE_NUMBER_PLATE === "1" ? true : false);

    // Trach nhiem dan su tu nguyen
    const [insur3D, setInsur3D] = useState(infoCar.VOLUNTARY_CIVIL?.INSUR_3RD); //Thiệt hại thân thể người thứ 3
    const [insur3DAssets, setInsur3DAssets] = useState(infoCar.VOLUNTARY_CIVIL?.INSUR_3RD_ASSETS); // Thiệt hại tài sản người thứ 3
    const [insurPassenger, setInsurPassenger] = useState(infoCar.VOLUNTARY_CIVIL?.INSUR_PASSENGER);// Thiệt hại thân thể hành khách
    // const [totalLiability, setTotalLiability] = useState(""); //Tổng hạn mức trách nhiệm
    const [driverInsur, setDriverInsur] = useState(infoCar.VOLUNTARY_CIVIL?.DRIVER_INSUR); //Số tiền BH (Bảo hiểm lái, phụ xe và người ngồi trên xe)
    const[weightCargo, setWeightCargo] = useState(infoCar.VOLUNTARY_CIVIL?.WEIGHT_CARGO || null); //Trọng lượng hàng hóa (Bảo hiểm TNDS của chủ xe đối với hàng hóa)

    const [cargoInsur, setCargoInsur] = useState(infoCar.VOLUNTARY_CIVIL?.CARGO_INSUR );// Số tiền bảo hiểm hàng hóa - mức trách nhiệm (Bảo hiểm TNDS của chủ xe đối với hàng hóa)

    const [checkOrder, setCheckOrder] = useState(config.don_le); // Check don le hoac check don gop

    const [checkTNDS, setCheckTNDS] = useState(true);

    const [checkRequireTNDS, setCheckRequireTNDS] =  useState(true);

    // "IS_TNDSTN": checkTNDSTN,
    //     "IS_LPX": checkLPX,
    //     "IS_HHX": checkHHX,


    const [checkTNDSTN, setCheckTNDSTN] = useState(infoCar.IS_VOLUNTARY ? (infoCar.IS_VOLUNTARY === "1" ? true : false) : false);

    const [checkLPX, setCheckLPX] = useState(infoCar.IS_DRIVER ? (infoCar.IS_DRIVER === "1" ? true : false) : false);

    const [checkHHX, setCheckHHX] = useState(infoCar.IS_CARGO ? (infoCar.IS_CARGO === "1" ? true : false) : false);

    const [disableTN, setDisableTN] = useState({
        is_tn_1: parseInt(infoCar?.IS_VOLUNTARY),
        is_tn_2: parseInt(infoCar?.IS_DRIVER),
        is_tn_3: parseInt(infoCar?.IS_CARGO),
    })

    //Vat chat xe

    const [checkVCX, setCheckVCX] = useState(true);

    // const [checkedItem, setCheckedItem] = useState([]);

    const [packCode, setPackCode] = useState(infoCar.PHYSICAL_DAMAGE?.PACK_CODE  || null); //Chuong trinh bao hiem
    const [insurDeductible, setInsurDeductible] = useState(infoCar.PHYSICAL_DAMAGE?.INSUR_DEDUCTIBLE || null); // Muc Mien thuong
    const [vehicleValue, setvehicleValue] = useState(infoCar?.VEHICLE_VALUE ? numberFormatCommas(infoCar?.VEHICLE_VALUE) : null); // Gia tri xe
    const [valueReference, setValueReference] = useState(infoCar?.REF_VEHICLE_VALUE ? numberFormatCommas(infoCar?.REF_VEHICLE_VALUE) : null); // gia tri xe tham khao
    const [insurTotalAmount, setInsurTotalAmount] = useState(infoCar?.PHYSICAL_DAMAGE?.INSUR_TOTAL_AMOUNT ? numberFormatCommas(infoCar?.PHYSICAL_DAMAGE?.INSUR_TOTAL_AMOUNT.toString()): null); // So tien bao hiem
    const [additional, setAdditional] = useState(infoCar.PHYSICAL_DAMAGE?.ADDITIONAL || []); //Danh sach dieu khoan bo sung
    const [checkDKBS, setCheckDKBS] = useState(infoCar.PHYSICAL_DAMAGE?.ADDITIONAL || []);

    const [effVCX, setEffVCX]  = useState(infoCar.PHYSICAL_DAMAGE?.EFF || currentDate); //Ngay hieu luc VCX
    const [effTimeVCX, setEffTimeVCX]  = useState(infoCar.PHYSICAL_DAMAGE?.TIME_EFF || curTime); //Gio hieu luc vcx
    const [expVCX, setExpVCX]  = useState(infoCar.PHYSICAL_DAMAGE?.EXP || addDate); // Ngay ket thuc vcx
    const [expTimeVCX, setExpTimeVCX]  = useState(infoCar.PHYSICAL_DAMAGE?.TIME_EXP || curTime); //Gio ket thuc vcx
    const [vehicleLoss, setVehicleLoss] = useState(infoCar.PHYSICAL_DAMAGE?.IS_VEHICLE_LOSS); // xe co bi ton that ko?

    const [amountOfLoss, setAmountOfLoss] = useState(infoCar.PHYSICAL_DAMAGE?.VEHICLE_LOSS_AMOUNT ? numberFormatCommas(infoCar.PHYSICAL_DAMAGE?.VEHICLE_LOSS_AMOUNT) : "0") // So tien bi ton that

    const [vehicleLossNum, setVehicleLossNum] = useState(infoCar.PHYSICAL_DAMAGE?.VEHICLE_LOSS_NUM || null ); // so nam ko ton that
    const [isDiscount, setIsDiscount] = useState(infoCar.PHYSICAL_DAMAGE?.IS_DISCOUNT === "1" ? true : false); // Có giảm phi không
    const [discount, setDiscount] = useState(infoCar.PHYSICAL_DAMAGE?.DISCOUNT || null); /// Số giảm phi
    const [discountUnit, setDiscountUnit] = useState(infoCar.PHYSICAL_DAMAGE?.DISCOUNT_UNIT || null);  // Đơn vị giảm
    const [reasonDiscount, setReasonDiscount] = useState(infoCar.PHYSICAL_DAMAGE?.REASON_DISCOUNT || null); // Ly do giam phi
    const [isSendHD, setIsSendHD] = useState(infoCar.PHYSICAL_DAMAGE?.IS_SEND_HD === "1" ? true: false); // Gửi đến bảo hiểm giám định ---- 1: gửi giảm định => chờ duyệt  -- 0: đơn nháp
    /// Hình thức giám định:   /// 1. Cá nhân chụp ảnh (hiện chưa có) -- 2. DL chụp ảnh -- 3. Đến giám định theo lịch KH --4. Gọi điện thoại thống nhất thời gian với KH
    const[surveyorType, setSurveyorType]= useState(infoCar.PHYSICAL_DAMAGE?.SURVEYOR_TYPE || config.dl_chup);
    const [calCode, setCalCode] = useState(infoCar.PHYSICAL_DAMAGE?.CAL_CODE);

    //Đến giám định xe theo lịch khách hàng chọn

    const[calendarEff, setCalendarEff] = useState(infoCar.PHYSICAL_DAMAGE?.CALENDAR_EFF || currentDate); // thoi gian hen start
    const [calendarExp, setCalendarExp] = useState( currentDate); // thoi gian ket thuc
    const [calendarTimeF,setCalendarTimeF] = useState(infoCar.PHYSICAL_DAMAGE?.CALENDAR_TIME_F || curTime); // thoi gian hen. tu
    const [calendarTimeT,setCalendarTimeT] = useState(infoCar.PHYSICAL_DAMAGE?.CALENDAR_TIME_T || curTime); // thoi gian hen den'
    const[calendarContact, setCalendarContact] = useState(infoCar.PHYSICAL_DAMAGE?.CALENDAR_CONTACT || null); // thong tin ng lien he
    const [calendarPhone, setCalendarPhone] = useState(infoCar.PHYSICAL_DAMAGE?.CALENDAR_PHONE || null); // sdt lien he
    const [calendarAddress, setCalendarAddress] = useState(infoCar.PHYSICAL_DAMAGE?.CALENDAR_ADDRESS || null); //dia chi
    const [calendarAddressCode, setCalendarAddressCode] = useState({label: infoCar.PHYSICAL_DAMAGE?.CALENDAR_ADDRESS_FORM,prov: infoCar.PHYSICAL_DAMAGE?.CALENDAR_PROV, dist:infoCar.PHYSICAL_DAMAGE?.CALENDAR_DIST, ward: infoCar.PHYSICAL_DAMAGE?.CALENDAR_WARDS} || {});

    //Thông tin người thụ hưởng

    const objBNF = infoCar?.PHYSICAL_DAMAGE?.BENEFICIARY[0];

    // const [objBNF, setObjBNF] = useState({});
    const [typeBenificary, setTypeBenificary] =  useState(objBNF?.RELATIONSHIP ? objBNF?.RELATIONSHIP : config.relationship );
    const [customerType, setCustomerType] = useState(objBNF?.TYPE ? objBNF?.TYPE : config.customer_type);

    const[genderBNF, setGenderBNF] = useState( objBNF?.GENDER);
    const[nameBNF, setNameBNF] = useState(objBNF?.NAME );
    const[phoneBNF, setPhoneBNF] = useState(objBNF?.PHONE );
    const[emailBNF, setEmailBNF] = useState(objBNF?.EMAIL);
    const[idCardBNF, setIdCardBNF] = useState(objBNF?.IDCARD);
    const[addressBNF, setAddressBNF] = useState(objBNF?.ADDRESS);
    const [addressCodeBNF, setAddressCodeBNF] = useState({label: objBNF?.ADDRESS_FORM,prov: objBNF?.PROV, dist:objBNF?.DIST, ward: objBNF?.WARDS} || {});
    // const [addressCodeBNF, setAddressCodeBNF] = useState({label: "Thành phố Hà Nội - Quận Ba Đình - Phường Phúc Xá", prov: "01", dist: "01001", ward: "0100100001"});

    const [taxCodeBNF, setTaxCodeBNF] = useState(objBNF?.TAXCODE);

    const [isLoadGenQr, setIsLoadgenQr] = useState(false);

    // const [toggleAnChi, setToggleAnChi] = useState(false);

    const [openModalDKBS, setOpenDKBS] = useState(false);

    const [error, setError] = useState({});

    const [totalFees, setTotalFees] = useState([]);

    const [transId, setTransId] = useState(infoCar.PHYSICAL_DAMAGE?.TRANS_ID);

    const [flagImgVcx, setFlagImgVcx] = useState("0");
    const [disableInput, setDisableInput] = useState(false);

    const checkValCod = (arr, name, vlChange) => {
        return arr.map((vl, i) => {
            return {...vl, VAL_CODE: vl.VAL_CODE.replace(name, vlChange)}
        });
    };

    useEffect(() =>{
        setDataDefine(props.listDefineCar);
        setListVerhicleGroup(props.listDefineCar ? props.listDefineCar[2] : []);
        setListVerhicleType(props.listDefineCar ? props.listDefineCar[3] : []);
        setListUsePurpose(props.listDefineCar ? props.listDefineCar[4] : []);
        setListInvestigation(props.listDefineCar ? props.listDefineCar[16] : []);
        setListManufacturer(props.listDefineCar ? props.listDefineCar[5] : []);
        setListVehicleBrand(props.listDefineCar ? props.listDefineCar[6] : []);
        setListDkbs(props.listDefineCar ? props.listDefineCar[15] : []);
    },[props.listDefineCar]);

    const onChangeTypeBeneficary = (e) =>{
        setTypeBenificary(e.target.value);
    };

    const onChangeCustomerType = (e) =>{
        setCustomerType(e.target.value);
    };

    const [listArrImg, setListArrImg] = useState(infoCar.PHYSICAL_DAMAGE?.SURVEYOR_FILES || []);

    useEffect(() =>{
        // const transid = props.sdk_params ? props.sdk_params.trans_id : "";
        if(transId){
            // console.log('transId', transId);
            const socket = io(config.urlPath+`?id=`+transId+'&channel=transshipment');

            socket.on('connect', function(data){
                console.log("socket.id", socket.id);
            });

            socket.on('mediadata', function(data){
                // console.log('data', data);
                let rs = getFileSocket(data,transId);
                if(rs && rs.length > 0){
                    const rss = rs.filter(vl => vl.FILE_TYPE !== "id");
                    setListArrImg(rss);
                }
            });
        }
    },[transId]);

    useEffect(() =>{
        if(listArrImg.length > 0){
            if(document.getElementById("btn-scan-qr-vcx")) {
                document.getElementById("btn-scan-qr-vcx").click();
            }
            setIsLoadgenQr(false);
        }
    },[listArrImg])

    useEffect(() =>{
        if(props.vehicleInsur){
            setInfoCar(props.vehicleInsur);
            if(props.vehicleInsur?.PHYSICAL_DAMAGE?.STATUS === config.ord_status_wait){
                setDisableInput(true);
            }else{
                setDisableInput(false);
            }
        }
        // console.log('props.vehicleInsur',props.vehicleInsur );
    },[props.vehicleInsur]);

    useEffect(() =>{
        if(props.flagFee){
            let vehicle_type =  props.listDefineCar[3]?.filter(vl => vl.VH_GROUP_DT === vehicleGroup);
            setListVerhicleType(vehicle_type);
            let list_manufacturer =  props.listDefineCar[5]?.filter(vl => vl.VH_GROUP_DT === vehicleGroup);
            setListManufacturer(list_manufacturer);
            let vehicle_brand =  props.listDefineCar[6]?.filter(vl => vl.VH_CODE === manufacturer);
            setListVehicleBrand(vehicle_brand);
        }
    },[props.flagFee]);

    useEffect(() =>{
       if(typeVehicle){
           let rs1 = props.listDefineCar[3]?.find(vl => vl.value === typeVehicle);
           if(rs1){
               setUsesPurpose(rs1.CAR_PURPOSE ? rs1.CAR_PURPOSE : null);
               setDisableTN({is_tn_1:rs1?.IS_VOLUNTARY, is_tn_2: rs1?.DRIVER_INSUR, is_tn_3: rs1?.CARGO_INSUR})
               // setCheckTNDSTN(rs1?.IS_VOLUNTARY === 1 ? true : false);
               // setCheckLPX(rs1?.DRIVER_INSUR === 1 ? true : false);
               // setCheckHHX(rs1?.CARGO_INSUR === 1 ? true : false);
           }
       }
    },[typeVehicle]);

    useEffect(() =>{
        // if(checkTNDSTN && !config.thiet_hai_than_the_hk.includes(vehicleGroup) && usesPurpose !== config.muc_dich_use){
        //     setInsurPassenger("");
        // }

        if(!checkTNDSTN || disableTN.is_tn_1 === 0){
            setInsur3D("");
            setInsur3DAssets("");
            setInsurPassenger("");
        }
        if(!checkLPX || disableTN.is_tn_2 === 0){
            setDriverInsur("");
        }
        if(!checkHHX || disableTN.is_tn_3 === 0){
            setCargoInsur(null);
            setWeightCargo("");
        }
    },[checkTNDSTN, checkLPX, checkHHX, disableTN])

    // useEffect(() =>{
    //     // let totalYear = moment().year() - parseInt(mfg);
    //     let formatY = moment(effVCX, "DD/MM/YYYY").format("YYYY");
    //     let totalYear = formatY - parseInt(mfg);
    //     if(totalYear < 3){
    //         setTimeout(() =>{
    //             setCheckDKBS(checkDKBS?.filter((e) =>{
    //                 return e.KEY !== 'BS01/HDI-XCG' && e.KEY !== 'BS02/HDI-XCG'
    //             }));
    //             setAdditional(additional?.filter((e) =>{
    //                 return e.KEY !== 'BS01/HDI-XCG' && e.KEY !== 'BS02/HDI-XCG'
    //             }));
    //         },200);
    //     }
    //     if(totalYear < 6){
    //         setCheckDKBS(checkDKBS?.filter(e =>  e.KEY !== 'BS02/HDI-XCG'));
    //         setAdditional(additional.filter(e => e.KEY !== 'BS02/HDI-XCG'))
    //         // setAdditional(additional.filter(element => element.key !== 'BS02/HDI-XCG'))
    //     }
    // },[mfg])


    const onChangeToggleBKS = () =>{
        setToggleBKS(!toggleBKS);
    };

    const onChangeIsSerialNum = () =>{
        setIsSerialNum(!isSerialNum);
    };

    const onChangeIsDiscout = () =>{
        setIsDiscount(!isDiscount);
    };

    const handleChangeSelect = (data, type) =>{
        switch (type){
            case config.vehicle_group:
                setVehicleGroup(data);
                if(props.listDefineCar){
                    if(config.xe_may.includes(data)){
                        setUsesPurpose(null);
                        setNumberSeats(null);
                        setWeigh(null);
                    }
                    if(config.check_tt === data){
                        setWeigh(null);
                    }
                    let vehicle_type =  props.listDefineCar[3].filter(vl => vl.VH_GROUP_DT === data);
                    setListVerhicleType(vehicle_type);
                    if(typeVehicle){
                        setTypeVehicle(vehicle_type.length > 0 ? vehicle_type[0].value : null);
                        let rs1 = props.listDefineCar[3].find(vl => vl.value === vehicle_type[0].value);
                        setUsesPurpose(rs1 ? rs1.CAR_PURPOSE : null);
                    }

                    let list_manufacturer =  props.listDefineCar[5].filter(vl => vl.VH_GROUP_DT === data);
                    // console.log('list_manufacturer', list_manufacturer);
                    setListManufacturer(list_manufacturer);
                    let manu_face = list_manufacturer.length > 0  ? list_manufacturer[0].value : null;
                    if(manufacturer){
                        setManufacturer(manu_face)
                    }

                    let rs =  props.listDefineCar[6].filter(vl => vl.VH_CODE === manu_face);
                    setListVehicleBrand(rs)
                    if(vehicleBrand){
                        setVehicleBrand(rs.length > 0 ? rs[0].value : null);
                        setValueReference(rs.length> 0 ? rs[0].PRICE: null);
                    }
                }
                break;
            case config.vehicle_type:
                setTypeVehicle(data);
                break;
            case config.muc_dich_kd:
                setUsesPurpose(data);
                break;
            case config.hang_xe:
                setManufacturer(data);
                let rs =  props.listDefineCar[6].filter(vl => vl.VH_CODE === data);
                // console.log('rs11', rs.length > 0 ? rs[0] : null);
                setListVehicleBrand(rs);
                if(vehicleBrand){
                    setVehicleBrand(rs.length> 0 ? rs[0].value : null);
                    setValueReference(rs.length> 0 ? rs[0].PRICE: null)
                }
                break;
            case config.hieu_xe:
                setVehicleBrand(data);
                let rs1 =  props.listDefineCar[6].filter(vl => vl.value === data);
                setValueReference(rs1.length> 0 ? rs1[0].PRICE: null)
                break;
            default:
                break;
        }
    };

    const onChangeCheckboxTNDS = (e) =>{
        setCheckTNDS(!checkTNDS);
    }
    const onChangeCheckboxVCX = (e) =>{
        setCheckVCX(!checkVCX);
    }
    const onChangeCheckRequireTNDS = (e) =>{
        setCheckRequireTNDS(!checkRequireTNDS);
    }
    const onChangeCheckTNDSTN = (e) =>{
        setCheckTNDSTN(!checkTNDSTN);
    }
    const onChangeCheckLPX = (e) =>{
        setCheckLPX(!checkLPX);
    }
    const onChangeCheckHHX = (e) =>{
        setCheckHHX(!checkHHX);
    }
    const onChangeIsSendHD = () =>{
        setIsSendHD(!isSendHD);
    }

    const onChangeCheckDKBS = (e, i, vl) =>{
        const target = e.target;
        let value = target.value;
         let a = checkDKBS.find((e) => {
            return e.KEY === vl.value;
        });
        if(a){
            setCheckDKBS(checkDKBS.filter(e => e.KEY !== vl.value));
        }else{
            setCheckDKBS([...checkDKBS, {KEY: value, LABEL: vl.label}]);
        }
    }


    const saveAdditional = () =>{
        setAdditional(checkDKBS);
        setOpenDKBS(false);
    }

    const onChangeInves = (e) =>{
        let value = e.target.value;
        if(value !== config.dl_chup){
            setListArrImg([]);
        }
        setSurveyorType(value);
    }

    const showModalQr = async () =>{
        setFlagImgVcx("1");
        setIsLoadgenQr(true);
        let rs = await getScanQR(toggleBKS);
        if(rs.Success){
            setTransId(rs.Data?.TRANS_ID);
            showModalScanQR(rs.Data?.BASE64_QR);
            setIsLoadgenQr(false);
        }else{
            setIsLoadgenQr(false);
            showDialogDis(rs.ErrorMessage);
        }
    }

    const validateForm = () =>{
        let errors ={};
        let formIsValid = true;
        if(!vehicleGroup){
            formIsValid = false;
            errors['vehicleGroup'] = 'Vui lòng chọn nhóm xe';
        }
        if(!typeVehicle){
            formIsValid = false;
            errors['typeVehicle'] = 'Vui lòng chọn loại xe';
        }
        if(config.check_type_car.includes(vehicleGroup)){
            if(!usesPurpose){
                formIsValid = false;
                errors['usesPurpose'] = 'Vui lòng chọn mục đích sử dụng';
            }
            if(numberSeats && parseInt(numberSeats) > 60){
                formIsValid = false;
                errors['numberSeats'] = 'Không hỗ trợ với các loại xe lớn hơn 60 chỗ';
            }
        }
        if(!numberSeats){
            formIsValid = false;
            errors['numberSeats'] = 'Vui lòng nhập số chỗ ngồi';
        }
        if(!manufacturer){
            formIsValid = false;
            errors['manufacturer'] = 'Vui lòng chọn hãng xe';
        }
        if(!vehicleBrand){
            formIsValid = false;
            errors['vehicleBrand'] = 'Vui lòng chọn hiệu xe';
        }

        if(config.check_weigh.includes(vehicleGroup)){
            if(!weigh){
                formIsValid = false;
                errors['weigh'] = 'Vui lòng nhập trọng tải';
            }
        }

        if(toggleBKS){
            if(!engineNo){
                formIsValid = false;
                errors['engineNo'] = 'Vui lòng nhập số máy';
            }
            if(!chassisNo){
                formIsValid = false;
                errors['chassisNo'] = 'Vui lòng nhập số khung';
            }
        }else{
            if(!numberPlate){
                formIsValid = false;
                errors['numberPlate'] = 'Vui lòng nhập biển kiểm soát';
            }
        }
        if(isSerialNum){
            if(!serialNum){
                formIsValid = false;
                errors['serialNum'] = 'Vui lòng nhập số GCN trên phôi';
            }
        }
        if(checkTNDSTN){
            if(!insur3D){
                formIsValid = false;
                errors['insur3D'] = 'Vui lòng chọn thiệt hại thân thể người thứ 3';
            }
            if(config.thiet_hai_than_the_hk.includes(vehicleGroup) && usesPurpose === config.muc_dich_use){
                if(!insurPassenger){
                    formIsValid = false;
                    errors['insurPassenger'] = 'Vui lòng chọn thiệt hại thân thể hành khách';
                }
            }
            if(!insur3DAssets){
                formIsValid = false;
                errors['insur3DAssets'] = 'Vui lòng chọn thiệt hại tài sản người thứ 3';
            }

        }
        if(checkLPX){
            if(!driverInsur){
                formIsValid = false;
                errors['driverInsur'] = 'Vui lòng chọn số tiền bảo hiểm';
            }
        }
        if(checkHHX){
            if(!weightCargo){
                formIsValid = false;
                errors['weightCargo'] = 'Vui lòng nhập hàng hóa được BH (Tấn)';
            }
            if(!cargoInsur){
                formIsValid = false;
                errors['cargoInsur'] = 'Vui lòng chọn mức trách nhiệm';
            }
        }

        if(props.sdk_params.product_code !== config.product_code){
            if(!packCode){
                formIsValid = false;
                errors['packCode'] = 'Vui lòng chọn chương trình bảo hiểm';
            }
            if(!mfg){
                formIsValid = false;
                errors['mfg'] = 'Vui lòng chọn năm sản xuất';
            }
            if(!vehicleRegis){
                formIsValid = false;
                errors['vehicleRegis'] = 'Vui lòng nhập ngày đăng ký xe';
            }
            if(!vehicleValue){
                formIsValid = false;
                errors['vehicleValue'] = 'Vui lòng nhập giá trị xe';
            }
            if(!insurTotalAmount){
                formIsValid = false;
                errors['insurTotalAmount'] = 'Vui lòng nhập số tiền bảo hiểm';
            }
            if(!insurDeductible){
                formIsValid = false;
                errors['insurDeductible'] = 'Vui lòng chọn mức miễn thuờng';
            }
            if(!vehicleLoss){
                formIsValid = false;
                errors['vehicleLoss'] = 'Vui lòng chọn xe có bị tổn thất không? ';
            }
            if(vehicleLoss === "0" && !vehicleLossNum){
                formIsValid = false;
                errors['vehicleLossNum'] = 'Vui lòng chọn số năm không bị tổn thất';
            }
            if(vehicleLoss === "1" && !amountOfLoss){
                formIsValid = false;
                errors['amountOfLoss'] = 'Vui lòng nhập số tiền bị tổn thất';
            }
            if(isDiscount && !discountUnit){
                formIsValid = false;
                errors['discountUnit'] = 'Vui lòng chọn đơn vị giảm phí';
            }
            if(isDiscount && !discount){
                formIsValid = false;
                errors['discount'] = 'Vui lòng nhập giá trị giảm';
            }
            if(isDiscount && !reasonDiscount){
                formIsValid = false;
                errors['reasonDiscount'] = 'Vui lòng nhập lý do giảm phí';
            }
            if(effVCX && expVCX){
                if(compareDate(expVCX, effVCX)){
                    errors['effVCX'] = 'Ngày hiệu lực không được lớn hơn ngày kết thúc';
                    setError(errors);
                    formIsValid = false;
                }
            }

            if(surveyorType === config.gd_theo_kh){
                if(!calendarEff){
                    formIsValid = false;
                    errors['calendarEff'] = 'Vui lòng nhập ngày hẹn';
                }
                if(!calendarTimeF){
                    formIsValid = false;
                    errors['calendarTimeF'] = 'Vui lòng nhập thời gian hẹn từ';
                }
                if(!calendarTimeT){
                    formIsValid = false;
                    errors['calendarTimeT'] = 'Vui lòng nhập thời gian hẹn đến';
                }
                if(!calendarContact){
                    formIsValid = false;
                    errors['calendarContact'] = 'Vui lòng nhập người liên hệ';
                }
                if(!calendarPhone){
                    formIsValid = false;
                    errors['calendarPhone'] = 'Vui lòng nhập số điện thoại';
                }
                if(!calendarAddress){
                    formIsValid = false;
                    errors['calendarAddress'] = 'Vui lòng nhập số nhà, ngõ/ ngách, đường';
                }
                if(calendarAddressCode ? (calendarAddressCode.label === "" || calendarAddressCode.label === undefined) : null){
                    formIsValid = false;
                    errors['calendarAddressCode'] = 'Vui lòng chọn Tỉnh/TP - Quận/huyện - Phường/ xã';
                }
            }
            if(surveyorType === config.dl_chup && listArrImg.length <= 0){
                formIsValid = false;
                showDialogDis("Vui lòng chụp ảnh để giám định")
            }

            //Validate nguoithu huong
            if(typeBenificary === "KHAC" && customerType){

                if(customerType === config.customer_type){
                    if(!genderBNF){
                        formIsValid = false;
                        errors['genderBNF'] = 'Vui lòng chon danh xưng';
                    }
                    if(!nameBNF){
                        formIsValid = false;
                        errors['nameBNF'] = 'Vui lòng nhập tên chủ xe';
                    }
                    if(!idCardBNF){
                        formIsValid = false;
                        errors['idCardBNF'] = 'Vui lòng nhập CMND hoặc CCCD ';
                    }
                }else{
                    if(!nameBNF){
                        formIsValid = false;
                        errors['nameBNF'] = 'Vui lòng nhập tên cơ quan';
                    }
                    if(!taxCodeBNF){
                        formIsValid = false;
                        errors['taxCodeBNF'] = 'Vui lòng nhập mã số thuế';
                    }
                }
                if(!phoneBNF){
                    formIsValid = false;
                    errors['phoneBNF'] = 'Vui lòng nhập số điện thoại';
                }
                if(!addressBNF){
                    formIsValid = false;
                    errors['addressBNF'] = 'Vui lòng nhập số nhà, ngõ/ ngách, đường';
                }
                if(addressCodeBNF ? (addressCodeBNF.label === "" || addressCodeBNF.label === undefined) : null){
                    formIsValid = false;
                    errors['addressCodeBNF'] = 'Vui lòng chọn Tỉnh/TP - Quận/huyện - Phường/ xã';
                }
            }

        }

        setError(errors);
        return formIsValid;
    }

    function calculateTotalDay(params) {
        
    }

    const handleNextStep = () =>{
        if(validateForm()){
            let choose_Lxe ={}, feeTndsTn = {}, feeVcx = {};
            let totalDay='', totalDayVcx = '', totalYear = '';
            let weightVL = null, numSeat = null, carPrice = null, yearOfUse = null;
            let eff_c = moment(eff, 'DD/MM/YYYY'), exp_c = moment(exp, 'DD/MM/YYYY');
            let effVcx = moment(effVCX, 'DD/MM/YYYY'), expVcx = moment(expVCX, 'DD/MM/YYYY');
            // let vehicleRes =  moment(vehicleRegis, 'DD/MM/YYYY'), curDate = moment(new Date(), 'DD/MM/YYYY');
            
            // totalDay = exp_c.diff(eff_c, 'days');
            totalDay = caculDateLeapYear(effVcx, expVcx);

            totalDayVcx = caculDateLeapYear(effVcx, expVcx);
            console.log(totalDay, totalDayVcx);

            let strAdditional="";
            if(additional.length > 0){
                additional.map((vl, i) =>{
                    return  strAdditional += vl.KEY + '@';
                });
            }

            if(dataDefine){
                if(weigh){
                    let vl1 = dataDefine[22].filter((o) => {
                        return parseFloat(o.MIN_VALUE) <= parseFloat(weigh) && parseFloat(weigh) <= parseFloat(o.MAX_VALUE);
                    });
                    if(vl1.length > 0){
                        weightVL = vl1[0].value;
                    }
                }
                if(numberSeats){
                    let vl2 = dataDefine[23].filter(vl => parseFloat(vl.NUM_OF_SEAT) === parseFloat(numberSeats));
                    if(vl2.length > 0){
                        numSeat = vl2[0].value;
                    }
                }
                if(insurTotalAmount){
                    let formatVl = insurTotalAmount.split(',').join("");
                    let vl3 = dataDefine[20].filter((o) => {
                        return parseFloat(o.MIN_VALUE) <= parseFloat(formatVl) && parseFloat(formatVl) <= parseFloat(o.MAX_VALUE);
                    });
                    if(vl3.length > 0){
                        carPrice = vl3[0].value;
                    }
                }

                if(mfg){
                    let formatY = moment(effVcx, "DD/MM/YYYY").format("YYYY");
                    totalYear = formatY - parseInt(mfg);
                    let vl4 = dataDefine[21].filter((o) => {
                        return parseFloat(o.MIN_VALUE) <= parseFloat(totalYear) && parseFloat(totalYear) <= parseFloat(o.MAX_VALUE);
                    });
                    if(vl4.length > 0){
                        yearOfUse = vl4[0].value;
                    }
                }
            }
                choose_Lxe = checkValCod(caculate_fees,'VH_TYPE', typeVehicle ? typeVehicle : 'VH_TYPE');
                choose_Lxe= checkValCod(choose_Lxe,'CAR_WEIGHT', weightVL ? weightVL : 'CAR_WEIGHT');
                choose_Lxe= checkValCod(choose_Lxe,'CAR_SEAT', numSeat ? numSeat : 'CAR_SEAT');
                choose_Lxe = checkValCod(choose_Lxe,'CAR_PURPOSE', usesPurpose ? usesPurpose : 'CAR_PURPOSE');

                    //Tu nnguyen
                    feeTndsTn = checkValCod(caculate_fees_tndstn,'VH_TYPE', typeVehicle ? typeVehicle : 'VH_TYPE');
                    feeTndsTn = checkValCod(feeTndsTn,'BH_NTX', driverInsur ? driverInsur : 'BH_NTX');
                    feeTndsTn = checkValCod(feeTndsTn,'CAR_PURPOSE', usesPurpose ? usesPurpose : 'CAR_PURPOSE');
                    feeTndsTn = checkValCod(feeTndsTn,'TH_HK', insurPassenger ? insurPassenger : 'TH_HK');
                    feeTndsTn = checkValCod(feeTndsTn,'CAR_WEIGHT', weightVL ? weightVL : 'CAR_WEIGHT');
                    feeTndsTn = checkValCod(feeTndsTn,'TH_TSN3', insur3DAssets ? insur3DAssets : 'TH_TSN3');
                    feeTndsTn = checkValCod(feeTndsTn,'TH_N3', insur3D ? insur3D : 'TH_N3');
                    feeTndsTn = checkValCod(feeTndsTn,'CAR_SEAT', numSeat ? numSeat : 'CAR_SEAT');

                    feeTndsTn = checkValCod(feeTndsTn,'BH_HH', cargoInsur ? cargoInsur : 'BH_HH');
                    feeTndsTn = checkValCod(feeTndsTn,'TTAI_TAN', weightCargo ? weightCargo : 'TTAI_TAN');
                    feeTndsTn = checkValCod(feeTndsTn,'NUM_OF_DAY', totalDay ? totalDay : 'NUM_OF_DAY');

            let rsT3d = choose_Lxe.findIndex((obj => obj.KEY_CODE === 'HS_T30D'));
            let rsD3d = choose_Lxe.findIndex((obj => obj.KEY_CODE === 'HS_D30D'));

            if(totalDay > 30){
                choose_Lxe[rsT3d].VAL_CODE = totalDay.toString();
                choose_Lxe[rsD3d].VAL_CODE = "0";
            }else{
                choose_Lxe[rsT3d].VAL_CODE = "0";
                choose_Lxe[rsD3d].VAL_CODE = "1";
            }


            feeVcx =  checkValCod(caculate_fees_vcx,'STBH_VCX', insurTotalAmount ? insurTotalAmount.split(',').join("") : 'STBH_VCX');
            feeVcx =  checkValCod(feeVcx,'VH_TYPE', typeVehicle ? typeVehicle : 'VH_TYPE');
            feeVcx =  checkValCod(feeVcx,'CAR_WEIGHT', weightVL ? weightVL : 'CAR_WEIGHT');
            feeVcx =  checkValCod(feeVcx,'CAR_PRICE', carPrice ? carPrice : 'CAR_PRICE');
            feeVcx =  checkValCod(feeVcx,'YEAR_OF_USE', yearOfUse ? yearOfUse : 'YEAR_OF_USE');
            feeVcx =  checkValCod(feeVcx,'DAY_OF_VCX', totalDayVcx ? totalDayVcx : 'DAY_OF_VCX');
            feeVcx =  checkValCod(feeVcx,'BOSUNG', strAdditional ? strAdditional : 'BOSUNG');
            feeVcx =  checkValCod(feeVcx,'BH_HH', cargoInsur ? cargoInsur : 'BH_HH');
            feeVcx =  checkValCod(feeVcx,'CAR_SEAT', numSeat ? numSeat : 'CAR_SEAT');
            feeVcx =  checkValCod(feeVcx,'TH_N3', insur3D ? insur3D : 'TH_N3');
            feeVcx =  checkValCod(feeVcx,'BH_NTX', driverInsur ? driverInsur : 'BH_NTX');
            feeVcx =  checkValCod(feeVcx,'CAR_PURPOSE', usesPurpose ? usesPurpose : 'CAR_PURPOSE');
            feeVcx =  checkValCod(feeVcx,'TH_HK', insurPassenger ? insurPassenger : 'TH_HK');
            feeVcx =  checkValCod(feeVcx,'TH_TSN3', insur3DAssets ? insur3DAssets : 'TH_TSN3');
            feeVcx =  checkValCod(feeVcx,'TTAI_TAN', weightCargo ? weightCargo : 'TTAI_TAN');

            setObjFee(choose_Lxe);
            setObjFeeTN(feeTndsTn);

            props.setVehicleInsur({
                ...props.vehicleInsur,
                CONTRACT_CODE: infoCar ? infoCar.CONTRACT_CODE : "",
                VEHICLE_GROUP: vehicleGroup,
                VEHICLE_TYPE: typeVehicle,
                VEHICLE_USE: usesPurpose,
                NONE_NUMBER_PLATE: toggleBKS ? "1" : "0",  // 1: chưa có biển số --- 0: có biển số
                IS_SPLIT_ORDER: checkOrder,// 1: don le? --- 0: don gop
                NUMBER_PLATE: numberPlate,
                CHASSIS_NO: chassisNo, //Số khung
                ENGINE_NO: engineNo, // Số máy
                SEAT_NO: numberSeats, //Số chỗ ngồi
                SEAT_CODE: numSeat, // key value khi Filter
                WEIGH: weigh, //Trọng tải
                WEIGH_CODE: weightVL, //KEY VALUE WEIGH khi filter
                BRAND: manufacturer, //Hãng xe
                MODEL: vehicleBrand, //Hiệu xe
                MFG: mfg, //Năm sản xuất
                CAPACITY: "", //Dung tích
                REF_VEHICLE_VALUE: valueReference ? valueReference.toString().split(',').join("") : 0, //Giá trị xe tham khao
                VEHICLE_REGIS: vehicleRegis, //Ngày đăng ký xe
                VEHICLE_REGIS_CODE: yearOfUse,
                objFeeTNDS: choose_Lxe,
                objFeeTN: feeTndsTn,
                objFeeVcx: feeVcx,
                IS_TNDS: checkTNDS,

                IS_COMPULSORY: props.sdk_params.product_code === config.product_code ? "1" : "0", // Nguoi dung co mua BH TNDS Bat Buoc
                IS_VOLUNTARY_ALL: (checkTNDSTN || checkLPX || checkHHX) ? "1" : "0", // Nguoi dung co mua tu nguyen
                IS_PHYSICAL: props.sdk_params.product_code !== config.product_code ? "1" : "0",    // Nguoi dung co mua Vat chat xe

                IS_VOLUNTARY: checkTNDSTN ? "1" : "0", //Có mua TNDS TN (người và hàng hóa người t3, hành khách)
                IS_DRIVER:checkLPX ? "1" : "0", //Có mua TNDS TN cho LPX, NNTX
                IS_CARGO: checkHHX ? "1" : "0", //Có mua TNDS TN cho hàng hóa
                VEHICLE_VALUE:  vehicleValue ? vehicleValue.split(',').join("") : 0,         /// Giá trị xe

                VEHICLE_VALUE_CODE : carPrice, //Ma so tien bao hiem
                // "IS_TNDSTN": checkTNDSTN,
                // "IS_LPX": checkLPX,
                // "IS_HHX": checkHHX,
                //Trách nhiệm dân sự bắt buộc
                COMPULSORY_CIVIL: props.sdk_params.product_code === config.product_code ? {
                    DETAIL_CODE: infoCar.COMPULSORY_CIVIL ? infoCar.COMPULSORY_CIVIL.DETAIL_CODE : "",
                    PRODUCT_CODE: "XCG_TNDSBB",
                    PACK_CODE: "TNDSBB",
                    IS_SERIAL_NUM: isSerialNum ? "1" : "0",
                    SERIAL_NUM: serialNum,
                    EFF: eff,
                    TIME_EFF: effTime,
                    EXP: exp,
                    TIME_EXP: expTime,
                } : null,
                //TRach nhiem dan su tu nguyen
                VOLUNTARY_CIVIL: {
                    PRODUCT_CODE: "XCG_TNDSTN",
                    PACK_CODE: "TNDSTN",
                    INSUR_3RD: insur3D, //Thiệt hại thân thể người thứ 3
                    INSUR_3RD_ASSETS: insur3DAssets, // Thiệt hại tài sản người thứ 3
                    INSUR_PASSENGER: (config.thiet_hai_than_the_hk.includes(vehicleGroup) && usesPurpose === config.muc_dich_use) ? insurPassenger : "", // Thiệt hại thân thể hành khách
                    TOTAL_LIABILITY: "0", //Tổng hạn mức trách nhiệm
                    DRIVER_NUM: "0", //Số người TG(Bảo hiểm lái, phụ xe và người ngồi trên xe)
                    DRIVER_INSUR: driverInsur, //Số tiền BH (Bảo hiểm lái, phụ xe và người ngồi trên xe)
                    WEIGHT_CARGO: weightCargo, //Trọng lượng hàng hóa (Bảo hiểm TNDS của chủ xe đối với hàng hóa)
                    CARGO_INSUR: cargoInsur, // Số tiền bảo hiểm hàng hóa - mức trách nhiệm (Bảo hiểm TNDS của chủ xe đối với hàng hóa)

                    TOTAL_VOLUNTARY_3RD : "0", //Tổng tiền TNDS TN vượt quá mức (thân thể tài sản t3, hành khách)
                    TOTAL_DRIVER : "0", //Tổng tiền LPX,NNTX
                    TOTAL_CARGO: "0", //Tổng tiền hàng hóa
                    TOTAL_3RD_INSUR: "0", // Tong tien Thiệt hại thân thể người thứ 3
                    TOTAL_3RD_ASSETS:"0", // Tong tien Thiệt hại tài sản người thứ 3
                    TOTAL_PASSENGER: "0", // Tong tien Thiệt hại thân thể hành khách
                },
                //Vat chat xe
                    PHYSICAL_DAMAGE: props.sdk_params.product_code !== config.product_code ? {
                    DETAIL_CODE: infoCar.PHYSICAL_DAMAGE ? infoCar.PHYSICAL_DAMAGE.DETAIL_CODE : "",
                    PRODUCT_CODE: "XCG_VCX_NEW", //mã sản phẩm (VCX)
                    PACK_CODE: packCode,
                    INSUR_TOTAL_AMOUNT: insurTotalAmount ? insurTotalAmount.split(',').join("") : 0 , // so tien bao hiem
                    INSUR_DEDUCTIBLE: insurDeductible,         /// Mức miễn thường
                    EFF: effVCX,         // Ngày hiệu lực
                    TIME_EFF: effTimeVCX,    // Gio Hieu luc
                    EXP: expVCX,         // Ngay ket thuc
                    TIME_EXP: expTimeVCX,    //Gio ket thuc
                    IS_VEHICLE_LOSS: vehicleLoss,   //Xe có tổn thất không ?
                    VEHICLE_LOSS_NUM: vehicleLossNum,        /// Số năm không tổn thất
                    VEHICLE_LOSS_AMOUNT:  amountOfLoss ? amountOfLoss.split(',').join("") : 0, // So tien bi ton that
                    IS_DISCOUNT: isDiscount ? "1" : "0",   /// Có giảm phi không
                    DISCOUNT: isDiscount ? discount : "0",        /// Số giảm phi
                    REASON_DISCOUNT: isDiscount ? reasonDiscount : "",  ///Ly do giam phi
                    DISCOUNT_UNIT: isDiscount ? discountUnit : "",         /// Đơn vị giảm
                    IS_SEND_HD: isSendHD ? "1" : "0",                /// Gửi đến bảo hiểm giám định ---- 1: gửi giảm định => chờ duyệt  -- 0: đơn nháp
                    SURVEYOR_TYPE: surveyorType,        /// Hình thức giám định:   /// 1. Cá nhân chụp ảnh (hiện chưa có) -- 2. DL chụp ảnh -- 3. Đến giám định theo lịch KH --4. Gọi điện thoại thống nhất thời gian với KH
                    CALENDAR_EFF: surveyorType === config.gd_theo_kh ?  calendarEff : null,  /// Thời gian hẹn bắt đầu
                    CALENDAR_EXP: "",   /// Thời gian kết thúc hẹn
                    CALENDAR_TIME_F: surveyorType === config.gd_theo_kh ? calendarTimeF : null, /// Thời gian hẹn từ
                    CALENDAR_TIME_T: surveyorType === config.gd_theo_kh ? calendarTimeT : null, /// Thời gian hẹn đến
                    CALENDAR_CONTACT:surveyorType === config.gd_theo_kh ? calendarContact : null,    /// Thông tin người liên hệ
                    CALENDAR_PHONE: surveyorType === config.gd_theo_kh ? calendarPhone : null,          /// SĐT liên hệ
                    CALENDAR_PROV: surveyorType === config.gd_theo_kh ? calendarAddressCode?.prov : '',  /// Mã tỉnh địa chỉ hẹn
                    CALENDAR_DIST: surveyorType === config.gd_theo_kh ? calendarAddressCode?.dist : '', /// Mã huyện địa chỉ hẹn
                    CALENDAR_WARDS: surveyorType === config.gd_theo_kh ? calendarAddressCode?.ward : '', /// Mã xã địa chỉ hẹn
                    CALENDAR_ADDRESS:surveyorType === config.gd_theo_kh ? calendarAddress : null,        /// Địa chỉ hẹn
                    CALENDAR_ADDRESS_FORM: surveyorType === config.gd_theo_kh ? calendarAddressCode?.label : '',
                    CAL_CODE: calCode ? calCode : "",
                    ADDITIONAL: additional,    //danh sách điều khoản bổ sung chọn thêm -- object {key: ""},
                    BENEFICIARY:[
                        {
                            ORG_CODE: "",
                            RELATIONSHIP: typeBenificary,
                            TYPE: typeBenificary === config.relationship ? infoCar?.TYPE : customerType,
                            NAME: typeBenificary === config.relationship ? infoCar?.NAME : nameBNF,
                            DOB: "",
                            GENDER:  typeBenificary === config.relationship ? infoCar?.GENDER : customerType === config.customer_type ? genderBNF : "",
                            ADDRESS:  typeBenificary === config.relationship ? infoCar?.ADDRESS : addressBNF,
                            IDCARD: typeBenificary === config.relationship ? infoCar?.IDCARD : customerType === config.customer_type ?  idCardBNF : "",
                            EMAIL: typeBenificary === config.relationship ? infoCar?.EMAIL : emailBNF,
                            PHONE: typeBenificary === config.relationship? infoCar?.PHONE : phoneBNF,
                            PROV: typeBenificary === config.relationship ? infoCar?.PROV : addressCodeBNF?.prov,
                            DIST: typeBenificary === config.relationship ? infoCar?.DIST : addressCodeBNF?.dist,
                            WARDS: typeBenificary === config.relationship ? infoCar?.WARDS : addressCodeBNF?.ward,
                            ADDRESS_FORM: typeBenificary === config.relationship ? infoCar?.ADDRESS_LABEL : addressCodeBNF?.label,
                            IDCARD_D: "",
                            IDCARD_P: "",
                            FAX: "",
                            TAXCODE: typeBenificary === config.relationship ? infoCar.TAXCODE : customerType !== config.customer_type ?  taxCodeBNF : "",
                        }
                    ],
                    SURVEYOR_FILES: listArrImg,
                    TRANS_ID: transId
                } : null,

            })
            props.onNextStep();
        }
    }

    const onChangeEffTimeVcx = (value) =>{
        setEffTimeVCX(value);
        setExpTimeVCX(value);
    };

    return(
        <React.Fragment>
            <div className="form-content">
                <label className="col-form-label">
                    Thông tin xe
                    {/*<label style={{color: '#329945', marginLeft: 4, textDecoration: 'underline', cursor: 'pointer'}}>*/}
                    {/*    ( Điền thông tin bằng cách upload ảnh )*/}
                    {/*</label>*/}
                </label>
            </div>

            {/*FORM NHẬP THÔNG TIN XE CHUNG*/}
            <div className="row form-group">

                <div className="col-3">
                    <FLSelect
                        label='Nhóm xe'
                        disable={false}
                        required={true}
                        value={vehicleGroup}
                        changeEvent={vl => handleChangeSelect(vl, config.vehicle_group)}
                        dropdown
                        data={listVerhicleGroup ? listVerhicleGroup : []}
                        error={error.vehicleGroup}
                    />
                </div>

                <div className="col-3">
                    <FLSelect
                        disable={false}
                        label="Loại xe"
                        value={typeVehicle}
                        changeEvent={vl =>handleChangeSelect(vl, config.vehicle_type)}
                        dropdown={true}
                        required={true}
                        search={true}
                        data={listVerhicleType ? listVerhicleType : []}
                        error={error.typeVehicle}
                    />
                </div>

                {config.check_type_car.includes(vehicleGroup) && (
                    <div className="col-3">
                        <FLSelect
                            disable={false}
                            label="Mục đích sử dụng"
                            value={usesPurpose}
                            changeEvent={vl =>handleChangeSelect(vl, config.muc_dich_kd)}
                            dropdown={true}
                            required={true}
                            data={listUsePurpose ? listUsePurpose : []}
                            error={error.usesPurpose}
                        />
                    </div>
                )}

                <div className="col-3 form-group">
                    <FLSelect
                        label='Hãng xe'
                        disable={false}
                        required={true}
                        search={true}
                        value={manufacturer}
                        changeEvent={vl =>handleChangeSelect(vl, config.hang_xe)}
                        dropdown
                        data={listManufacturer ? listManufacturer : []}
                        error={error.manufacturer}
                    />
                </div>
                <div className="col-3 form-group">
                    <FLSelect
                        label='Hiệu xe'
                        disable={false}
                        required={true}
                        search={true}
                        value={vehicleBrand}
                        changeEvent={vl =>handleChangeSelect(vl, config.hieu_xe)}
                        dropdown
                        data={listVehicleBrand ? listVehicleBrand : []}
                        error={error.vehicleBrand}
                    />
                </div>
                {(config.check_type_car.includes(vehicleGroup) || config.xe_may.includes(vehicleGroup)) && (
                    <div className="col-3">
                        <FLInput
                            label='Số chỗ ngồi'
                            type="number"
                            value={numberSeats}
                            changeEvent={setNumberSeats}
                            required={true}
                            error={error.numberSeats}
                        />
                    </div>
                )}
                {config.check_weigh.includes(vehicleGroup) && (
                    <div className="col-3">
                        <FLInput
                            label='Trọng tải (tấn)'
                            type="number"
                            value={weigh}
                            changeEvent={setWeigh}
                            required={true}
                            error={error.weigh}
                        />
                    </div>
                )}
            </div>

            <div className="row form-group">
                {/*<Form.Check*/}
                {/*    type="switch"*/}
                {/*    id="custom-switch-bks"*/}
                {/*    label="Xe mới chưa có biển số xe"*/}
                {/*    value={toggleBKS}*/}
                {/*    onChange={onChangeToggleBKS}*/}
                {/*/>*/}
                <FormCheck custom type="switch">
                    <FormCheck.Input checked={toggleBKS} />
                    <FormCheck.Label onClick={onChangeToggleBKS}>
                        Xe mới chưa có biển số xe
                    </FormCheck.Label>
                </FormCheck>
            </div>
            <div className="row form-group">
                <div className="col-3">
                    <FLInput
                        label='Biển kiểm soát'
                        value={numberPlate}
                        changeEvent={setNumberPlate}
                        required={toggleBKS ? false :true}
                        error={error.numberPlate}
                    />
                </div>
                <div className="col-3">
                    <FLInput
                        label='Số máy'
                        value={engineNo}
                        changeEvent={setEngineNo}
                        required={toggleBKS ? true :false}
                        error={error.engineNo}
                    />
                </div>
                <div className="col-3">
                    <FLInput
                        label='Số khung'
                        value={chassisNo}
                        changeEvent={setChassisNo}
                        required={toggleBKS ? true :false}
                        error={error.chassisNo}
                    />
                </div>
            </div>

            {/*<div className="form-content">*/}
            {/*    <label className="col-form-label">*/}
            {/*        Thông tin gói bảo hiểm*/}
            {/*    </label>*/}
            {/*</div>*/}

            {/*FORM CHON CAP DON LE HAY DON GOP*/}
            {/*<div className="row form-choose-type">*/}
            {/*    <p className="col-12 col-md-3 col-xl-3 col-sm-12">Chọn loại cấp đơn bảo hiểm</p>*/}
            {/*    <div className="col-12 col-md-9 col-xl-9 col-sm-12 rd-group">*/}
            {/*        {listRadioCheck ? (*/}
            {/*            listRadioCheck.map((vl, i) =>{*/}
            {/*                return(*/}
            {/*                    <label key={i} className="form-check">*/}
            {/*                        <input className="form-check-input" type="radio" value={vl.value}*/}
            {/*                               onChange={(e) =>  setCheckOrder(e.target.value)}*/}
            {/*                               checked={checkOrder === vl.value}/>*/}
            {/*                        {vl.label}*/}
            {/*                    </label>*/}
            {/*                )*/}
            {/*            })*/}
            {/*        ) : null}*/}
            {/*    </div>*/}
            {/*</div>*/}

            {/*FORM NHẬP TRÁCH NHIỆM DÂN SỰ VÀ TỰ NGUYỆN*/}
            {!Object.values(disableTN).every(x => x === 0) && (
                <div className="form-group form-group-cus-check">
                    <Form.Check type={'checkbox'} id={`cus-checkbox-tnds`} >
                        <Form.Check.Input
                            type={'checkbox'}
                            value={checkTNDS}
                            checked={checkTNDS}
                            // onChange={onChangeCheckboxTNDS}
                        />
                        <Form.Label>{props.sdk_params.product_code === config.product_code ?  'Bảo hiểm TNDS bắt buộc & tự nguyện' : 'Bảo hiểm tự nguyện'}</Form.Label>
                    </Form.Check>
                </div>
            ) }

            {!Object.values(disableTN).every(x => x === 0) && checkTNDS && (
                <div className="form-content-type">
                    {props.sdk_params.product_code === config.product_code && (
                        <>
                            <div className="row form-group">
                                <FormCheck custom type="switch">
                                    <FormCheck.Input checked={isSerialNum} />
                                    <FormCheck.Label onClick={onChangeIsSerialNum}>
                                        Đã cấp ấn chỉ bản cứng
                                    </FormCheck.Label>
                                </FormCheck>
                            </div>
                            <div className="row form-group">
                                <div className="col-3">
                                    <FLInput
                                        label='Số GCN trên phôi'
                                        value={serialNum}
                                        changeEvent={setSerialNum}
                                        required={true}
                                        disable={isSerialNum ? false : true}
                                        error={error.serialNum}
                                    />
                                </div>
                                <div className="col-9">
                                    <div className="group-ipu-cus">
                                        <div className="col-3">
                                            <FLInputDate
                                                disable={false}
                                                value={eff}
                                                changeEvent={setEff}
                                                selectedDay={selectDate(eff)}
                                                label={"Ngày hiệu lực"}
                                                required={true}
                                                error={''}
                                            />
                                        </div>
                                        <div className="col-3">
                                            <FlInputTime
                                                label='Giờ hiệu lực'
                                                value={effTime}
                                                eff={eff}
                                                changeEvent={setEffTime}
                                                required={true}
                                            />
                                        </div>
                                        <div className="col-3">
                                            <FLInputDate
                                                disable={false}
                                                value={exp}
                                                changeEvent={setExp}
                                                selectedDay={selectDate(exp)}
                                                label={"Ngày kết thúc"}
                                                required={true}
                                                error={''}
                                            />
                                        </div>
                                        <div className="col-3">
                                            <FlInputTime
                                                label='Giờ kết thúc'
                                                value={expTime}
                                                eff={exp}
                                                changeEvent={setExpTime}
                                                required={true}
                                            />
                                        </div>

                                    </div>
                                </div>

                            </div>

                            {/*{(disableTN.is_tn_1 === 1 || disableTN.is_tn_2 === 1 || disableTN.is_tn_1 === 3) && (*/}
                                <div className="form-group-cus-check">
                                    <Form.Check type={'checkbox'} id={`cus-checkbox-rq-tnds`}>
                                        <Form.Check.Input
                                            type={'checkbox'}
                                            value={checkRequireTNDS}
                                            checked={checkRequireTNDS}
                                            // onChange={onChangeCheckRequireTNDS}
                                        />
                                        <Form.Label>Bảo hiểm TNDS bắt buộc</Form.Label>
                                    </Form.Check>
                                </div>
                        </>
                    )}

                    {/*BẢO HIỂM TNDS TỰ NGUYỆN (VƯỢT QUÁ MỨC TNDS BẮT BUỘC)*/}

                    {disableTN.is_tn_1 === 1 && (
                        <div className="form-group-cus-check">
                            <Form.Check type={'checkbox'} id={`cus-checkbox-tndstn`}>
                                <Form.Check.Input
                                    type={'checkbox'}
                                    value={checkTNDSTN}
                                    checked={checkTNDSTN}
                                    onChange={onChangeCheckTNDSTN}
                                    disabled={checkRequireTNDS ? false : true}
                                />
                                <Form.Label>Bảo hiểm TNDS tự nguyện (vượt quá mức TNDS bắt buộc)</Form.Label>
                            </Form.Check>
                        </div>
                    )}

                    {disableTN.is_tn_1 === 1 && checkTNDSTN && (
                        <div className="row form-group">

                            <div className="col-3">
                                <FLSelect
                                    disable={false}
                                    label="Thiệt hại thân thể người thứ 3"
                                    value={insur3D}
                                    changeEvent={setInsur3D}
                                    positionSelect={"column"}
                                    // hideborder={true}
                                    dropdown={true}
                                    required={true}
                                    data={listThietHai1 ? listThietHai1 : []}
                                    error={error.insur3D}
                                />
                            </div>
                            {config.thiet_hai_than_the_hk.includes(vehicleGroup) && usesPurpose === config.muc_dich_use && (
                                <div className="col-3">
                                    <FLSelect
                                        disable={false}
                                        label="Thiệt hại thân thể hành khách"
                                        value={insurPassenger}
                                        changeEvent={setInsurPassenger}
                                        positionSelect={"column"}
                                        // hideborder={true}
                                        dropdown={true}
                                        required={true}
                                        data={listThiethai2 ? listThiethai2 : []}
                                        error={error.insurPassenger}
                                    />
                                </div>
                            )}
                            <div className="col-3">
                                <FLSelect
                                    disable={false}
                                    label="Thiệt hại tài sản người thứ 3"
                                    value={insur3DAssets}
                                    changeEvent={setInsur3DAssets}
                                    positionSelect={"column"}
                                    // hideborder={true}
                                    dropdown={true}
                                    required={true}
                                    data={listThiethai3 ? listThiethai3 : []}
                                    error={error.insur3DAssets}
                                />
                            </div>

                            {/*<div className="col-3">*/}
                            {/*    <FLSelect*/}
                            {/*        disable={false}*/}
                            {/*        label="Tổng hạn mức trách nhiệm"*/}
                            {/*        value={typeVehicle}*/}
                            {/*        changeEvent={setTypeVehicle}*/}
                            {/*        // hideborder={true}*/}
                            {/*        dropdown={true}*/}
                            {/*        required={true}*/}
                            {/*        data={listThiethai4 ? listThiethai4 : []}*/}
                            {/*    />*/}
                            {/*</div>*/}
                        </div>
                    )}

                    {/*BẢO HIỂM LÁI, PHỤ XE VÀ NGƯỜI NGỒI TRÊN XE*/}
                    {disableTN.is_tn_2 === 1 && (
                        <div className="form-group-cus-check">
                            <Form.Check type={'checkbox'} id={`cus-checkbox-lpx`}>
                                <Form.Check.Input
                                    type={'checkbox'}
                                    value={checkLPX}
                                    checked={checkLPX}
                                    onChange={onChangeCheckLPX}
                                    disabled={checkRequireTNDS ? false : true}
                                />
                                <Form.Label>Bảo hiểm lái, phụ xe và người ngồi trên xe</Form.Label>
                            </Form.Check>
                        </div>
                    )}

                    {disableTN.is_tn_2 === 1 && checkLPX && (
                        <div className="row form-group">
                            {/*<div className="col-3">*/}
                            {/*    <FLInput*/}
                            {/*        label='Số người tham gia'*/}
                            {/*        value={name}*/}
                            {/*        changeEvent={setName}*/}
                            {/*        required={true}*/}
                            {/*    />*/}
                            {/*</div>*/}
                            <div className="col-3">
                                <FLSelect
                                    disable={false}
                                    label="Số tiền bảo hiểm"
                                    value={driverInsur}
                                    changeEvent={setDriverInsur}
                                    positionSelect={"column"}
                                    // hideborder={true}
                                    dropdown={true}
                                    required={true}
                                    data={list_bh_ngoitrenxe ? list_bh_ngoitrenxe : []}
                                    error={error.driverInsur}
                                />
                            </div>
                        </div>
                    )}

                    {/*BẢO HIỂM TNDS CỦA CHỦ XE ĐỐI VỚI HÀNG HOÁ*/}
                    {disableTN.is_tn_3 === 1 && (
                        <div className="form-group-cus-check">
                            <Form.Check type={'checkbox'} id={`cus-checkbox-hhx`}>
                                <Form.Check.Input
                                    type={'checkbox'}
                                    value={checkHHX}
                                    checked={checkHHX}
                                    onChange={onChangeCheckHHX}
                                    disabled={checkRequireTNDS ? false : true}
                                />
                                <Form.Label>Bảo hiểm TNDS của chủ xe đối với hàng hóa</Form.Label>
                            </Form.Check>
                        </div>
                    )}
                    {disableTN.is_tn_3 === 1 && checkHHX && (
                        <div className="row form-group">
                            <div className="col-3">
                                <FLInput
                                    label='Hàng hóa được BH (Tấn)'
                                    value={weightCargo}
                                    changeEvent={setWeightCargo}
                                    required={true}
                                    error={error.weightCargo}
                                />
                            </div>
                            <div className="col-3">
                                <FLSelect
                                    disable={false}
                                    label="Mức trách nhiệm"
                                    value={cargoInsur}
                                    changeEvent={setCargoInsur}
                                    // hideborder={true}
                                    dropdown={true}
                                    required={true}
                                    data={list_bh_hh ? list_bh_hh : []}
                                    positionSelect={"column"}
                                    error={error.cargoInsur}
                                />
                            </div>
                        </div>
                    )}

                </div>
            )}

            {/*FORM NHẬP THÔNG TIN VẬT CHẤT XE*/}
            {props.sdk_params.product_code !== config.product_code && (
                <>
                    <div className="form-group form-group-cus-check">
                        <Form.Check type={'checkbox'} id={`cus-checkbox-vcx`}>
                            <Form.Check.Input
                                type={'checkbox'}
                                value={checkVCX}
                                checked={checkVCX}
                                // onChange={onChangeCheckboxVCX}
                                // disabled={true}
                            />
                            <Form.Label>Bảo hiểm vật chất xe</Form.Label>
                        </Form.Check>
                    </div>

                    {checkVCX && (
                        <div className="form-content-type">
                            <div className="row form-group">
                                <div className="col-3">
                                    <FLSelect
                                        disable={false}
                                        label="Chương trình bảo hiểm"
                                        value={packCode}
                                        changeEvent={setPackCode}
                                        // hideborder={true}
                                        dropdown={true}
                                        required={true}
                                        error={error.packCode}
                                        data={list_ctbh ? list_ctbh : []}
                                    />
                                </div>
                                <div className="col-3">
                                    <FLSelect
                                        disable={false}
                                        label="Năm sản xuất"
                                        value={mfg}
                                        changeEvent={setMfg}
                                        // hideborder={true}
                                        dropdown={true}
                                        required={true}
                                        error={error.mfg}
                                        data={getListYear()}
                                    />
                                </div>
                                <div className="col-3">
                                    <FLInputDate
                                        disable={false}
                                        value={vehicleRegis}
                                        changeEvent={setVehicleRegis}
                                        selectedDay={selectDate(vehicleRegis)}
                                        label={"Ngày đăng ký xe"}
                                        required={true}
                                        error={error.vehicleRegis}
                                    />
                                </div>
                                <div className="col-3">
                                    <FLSelect
                                        disable={false}
                                        label="Mức miễn thường"
                                        value={insurDeductible}
                                        changeEvent={setInsurDeductible}
                                        // hideborder={true}
                                        dropdown={true}
                                        required={true}
                                        error={error.insurDeductible}
                                        data={list_muc_mienthuong ? list_muc_mienthuong : []}
                                    />
                                </div>
                            </div>
                            <div className="row form-group">
                                <div className="col-3">
                                    <FLInput
                                        label='Giá trị xe tham khảo'
                                        value={valueReference ? numberWithCommas(valueReference) + " VND" : null}
                                        // changeEvent={setValueReference}
                                        disable={true}
                                    />
                                </div>

                                <div className="col-3">
                                    <FLInput
                                        label='Giá trị xe thực tế'
                                        value={vehicleValue}
                                        changeEvent={(value) => setvehicleValue(numberFormatCommas(value))}
                                        required={true}
                                        error={error.vehicleValue}
                                    />
                                </div>
                                <div className="col-3">
                                    <FLInput
                                        label='Số tiền bảo hiểm'
                                        value={insurTotalAmount}
                                        changeEvent={(value) => setInsurTotalAmount(numberFormatCommas(value))}
                                        required={true}
                                        error={error.insurTotalAmount}
                                    />
                                </div>
                                <div className="col-3">
                                    <div className="btn-dkbs-vcx" onClick={() => setOpenDKBS(true)}>
                                        Điều khoản bổ sung {additional.length > 0 ? "( Đã chọn " + additional.length + " )" : ""}
                                    </div>
                                    {/*MODAL HIEN THI DIEU KHOAN BO SUNG*/}
                                    <Modal
                                        show={openModalDKBS}
                                        onHide={() => setOpenDKBS(false)}
                                        aria-labelledby="contained-modal-title-vcenter"
                                        centered
                                    >
                                        <Modal.Header closeButton>
                                            <Modal.Title className="header-dkbs">
                                                Chọn điều khoản bổ sung
                                            </Modal.Title>
                                        </Modal.Header>
                                        <Modal.Body>
                                            {listDkbs ? (
                                                listDkbs.map((vl, i) =>{
                                                    // let formatY = moment(effVCX, "DD/MM/YYYY").format("YYYY");
                                                    // let totalYear = formatY - parseInt(mfg);
                                                    // // let totalYear = moment().year() - parseInt(mfg);
                                                    // if(totalYear < 3 && vl.value === 'BS01/HDI-XCG'){
                                                    //     return false;
                                                    // }
                                                    // if(totalYear < 6 && vl.value === 'BS02/HDI-XCG'){
                                                    //     return false;
                                                    // }
                                                    return(
                                                        <div className="form-group-cus-check">
                                                            <div className="form-check form-check-inline">
                                                                <input className="form-check-input" type="checkbox" name="checkDKBS"
                                                                       id={`checkbox-dkbs-`+ i} value={vl.value}
                                                                       checked={checkDKBS.some(({ KEY}) => KEY === vl.value)}
                                                                       onChange={(e) => onChangeCheckDKBS(e,i, vl)}/>
                                                                <label className="form-check-label"
                                                                       htmlFor={`checkbox-dkbs-`+ i} >{vl.label}</label>
                                                            </div>
                                                        </div>
                                                    )
                                                })
                                            ) : null}
                                            <div className="btn-save-dkbs" onClick={saveAdditional}>
                                                Lưu
                                            </div>
                                        </Modal.Body>
                                    </Modal>
                                </div>
                            </div>
                            <div className="row form-group">
                                <div className="col-3">
                                    <FLInputDate
                                        disable={false}
                                        value={effVCX}
                                        changeEvent={setEffVCX}
                                        selectedDay={selectDate(effVCX)}
                                        minimumDate={utils().getToday()}
                                        label={"Ngày bắt đầu"}
                                        required={true}
                                        error={error.effVCX}
                                    />
                                </div>
                                <div className="col-3">
                                    <FlInputTime
                                        label='Giờ bắt đầu'
                                        value={effTimeVCX}
                                        eff={effVCX}
                                        changeEvent={(value) =>onChangeEffTimeVcx(value)}
                                        required={true}
                                    />
                                </div>
                                <div className="col-3">
                                    <FLInputDate
                                        disable={false}
                                        value={expVCX}
                                        changeEvent={setExpVCX}
                                        selectedDay={selectDate(expVCX)}
                                        minimumDate={utils().getToday()}
                                        label={"Ngày kết thúc"}
                                        required={true}
                                        error={''}
                                    />
                                </div>
                                <div className="col-3">
                                    <FlInputTime
                                        label='Giờ kết thúc'
                                        value={expTimeVCX}
                                        eff={expVCX}
                                        // changeEvent={setExpTimeVCX}
                                        required={true}
                                        disable={true}
                                    />
                                </div>
                            </div>
                            {/*LICH SU TON THAT CUA XE CO GIOI*/}
                            <div className="row form-group">
                                <div className="col-3">
                                    <div className="vcx-lstt">Lịch sử tổn thất của XCG</div>
                                </div>
                                <div className="col-3">
                                    <FLSelect
                                        disable={false}
                                        label="Xe có từng bị tổn thất không?"
                                        value={vehicleLoss}
                                        changeEvent={setVehicleLoss}
                                        dropdown={true}
                                        required={true}
                                        error={error.vehicleLoss}
                                        data={[{value:'0', label: 'Xe không bị tổn thất'}, {value:'1', label: 'Xe bị tổn thất'}]}
                                    />
                                </div>
                                {vehicleLoss === "0" && (
                                    <div className="col-3">
                                        <FLSelect
                                            disable={false}
                                            label="Số năm liên tục không bị tổn thất"
                                            value={vehicleLossNum}
                                            changeEvent={setVehicleLossNum}
                                            dropdown={true}
                                            required={true}
                                            error={error.vehicleLossNum}
                                            data={[{value:'1', label: '1 năm'}, {value:'2', label: '2 năm'}, {value:'3', label: '3 năm'}]}
                                        />
                                    </div>
                                )}

                                {vehicleLoss === "1" && (
                                    <div className="col-3">
                                        <FLInput
                                            label='Số tiền bị tổn thất'
                                            value={amountOfLoss}
                                            changeEvent={(value) => setAmountOfLoss(numberFormatCommas(value))}
                                            required={true}
                                            error={error.amountOfLoss}
                                        />
                                    </div>
                                )}
                            </div>

                            <div className="row form-group">
                                <FormCheck custom type="switch">
                                    <FormCheck.Input checked={isDiscount} />
                                    <FormCheck.Label onClick={onChangeIsDiscout}>
                                        Áp dụng giảm phí (Yêu cầu này cần HDI Xác nhận)
                                    </FormCheck.Label>
                                </FormCheck>
                            </div>

                            {isDiscount && (
                                <div className="row form-group">
                                    <div className="col-3">
                                        <div style={{marginBottom: 8}}>
                                            <FLSelect
                                                disable={false}
                                                label="Giảm phí theo"
                                                value={discountUnit}
                                                changeEvent={setDiscountUnit}
                                                dropdown={true}
                                                required={true}
                                                error={error.discountUnit}
                                                data={[{value:'P', label: '% (phần trăm)'}, {value:'VND', label: 'VND'}]}
                                            />
                                        </div>
                                        <FLInput
                                            label='Giá trị giảm'
                                            value={discount}
                                            changeEvent={setDiscount}
                                            required={true}
                                            error={error.discount}
                                        />
                                    </div>
                                    <div className="col-9">
                                        <FLInputTextArea
                                            label='Lý do giảm phí'
                                            value={reasonDiscount}
                                            changeEvent={setReasonDiscount}
                                            required={true}
                                            error={error.reasonDiscount}
                                        />
                                    </div>
                                </div>
                            )}

                            <div className="form-group form-group-cus-check">
                                <Form.Check type={'checkbox'} id={`cus-check-is-hd`}>
                                    <Form.Check.Input
                                        type={'checkbox'}
                                        value={isSendHD}
                                        checked={isSendHD}
                                        onChange={onChangeIsSendHD}
                                        // disabled={true}
                                    />
                                    <Form.Label>Gửi Bảo hiểm HD duyệt giám định cấp đơn</Form.Label>
                                </Form.Check>
                            </div>

                            {/*FORM CHỌN KIỂU GĐ */}
                            <div className="rd-group">
                                {listInvestigation ? (
                                    listInvestigation.map((vl, i) =>{
                                        return(
                                            <label className="form-check">
                                                <input className="form-check-input" disabled={disableInput}
                                                       type="radio" value={vl.value}
                                                       onChange={onChangeInves}
                                                       checked={surveyorType === vl.value}/>
                                                {vl.label}
                                            </label>
                                        )
                                    })
                                ) : null}
                            </div>
                            {/*FORM ỨNG VỚI TỪNG TYPE GĐ*/}
                            {surveyorType === config.dl_chup && (
                                <>
                                    {listArrImg.length > 0 ? null : (
                                        <div className="form-scan-qr" onClick={showModalQr}>
                                            {/*<img className="img-fluid img-scan-qr" src={img_qr_scan ? img_qr_scan[0].URL : ''} alt="img upload" />*/}
                                            {/*<label className="label-scan-qr">*/}
                                            {/*    Vui lòng quét mã QR trên điện thoại để bổ sung thông tin giám định bằng cách chụp và quay video xe*/}
                                            {/*</label>*/}
                                            {isLoadGenQr ? (
                                                <div className="label-scan-qr">
                                                    <Spinner animation="border" role="status" style={{width: 18, height: 18, marginRight: 8}} />
                                                    Vui lòng đợi...
                                                </div>
                                            ): (
                                                <label className="label-scan-qr">Ấn vào đây để chụp ảnh</label>
                                            )}
                                        </div>
                                    )}
                                </>
                            )}

                            {surveyorType === config.gd_theo_kh ? (
                                <>
                                    <div className="row form-group">
                                        <div className="col-3">
                                            <FLInputDate
                                                disable={disableInput}
                                                value={calendarEff}
                                                changeEvent={setCalendarEff}
                                                selectedDay={selectDate(calendarEff)}
                                                minimumDate={utils().getToday()}
                                                label={"Ngày hẹn"}
                                                required={true}
                                                error={error.calendarEff}
                                            />
                                        </div>
                                        <div className="col-3">
                                            <FlInputTime
                                                disable={disableInput}
                                                label='Từ giờ'
                                                value={calendarTimeF}
                                                eff={calendarEff}
                                                changeEvent={setCalendarTimeF}
                                                required={true}
                                                error={error.calendarTimeF}
                                            />
                                        </div>
                                        <div className="col-3">
                                            <FlInputTime
                                                disable={disableInput}
                                                label='Đến giờ'
                                                value={calendarTimeT}
                                                eff={calendarEff}
                                                changeEvent={setCalendarTimeT}
                                                required={true}
                                                error={error.calendarTimeT}

                                            />
                                        </div>
                                        <div className="col-3">
                                            <FLInput
                                                disable={disableInput}
                                                label='Người liên hệ'
                                                value={calendarContact}
                                                changeEvent={setCalendarContact}
                                                required={true}
                                                error={error.calendarContact}
                                            />
                                        </div>
                                    </div>
                                    <div className="row form-group">
                                        <div className="col-3">
                                            <FLInput
                                                disable={disableInput}
                                                label='Số điện thoại'
                                                value={calendarPhone}
                                                changeEvent={setCalendarPhone}
                                                required={true}
                                                error={error.calendarPhone}
                                            />
                                        </div>
                                        <div className="col-9">
                                            <div className="group-ipu-cus">
                                                <div className="col-6">
                                                    <FLInput
                                                        disable={disableInput}
                                                        label='Số nhà, ngõ / ngách, đường'
                                                        value={calendarAddress}
                                                        changeEvent={setCalendarAddress}
                                                        required={true}
                                                        error={error.calendarAddress}
                                                    />
                                                </div>
                                                <div className="col-6">
                                                    <FLAddress
                                                        disable={disableInput}
                                                        // disable={false}
                                                        changeEvent={setCalendarAddressCode}
                                                        value={calendarAddressCode}
                                                        positionSelect={"column"}
                                                        label={'Tỉnh/TP - Quận/huyện - Phường/ xã'}
                                                        required={true}
                                                        error={error.calendarAddressCode}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </>
                            ) :null}

                            {listArrImg.length > 0 ? (
                                <SlideImage
                                    dataValue={listArrImg}
                                    sdk_params={props.sdk_params}
                                    flag={flagImgVcx}
                                    checkCommit={props.checkCommit}
                                />
                            ): null}

                            {/*FORM NGUOI THU HUONG*/}

                            <div className="form-content" style={{marginTop: 16}}>
                                <label className="col-form-label">
                                    Thông tin thụ hưởng cho khách hàng mua bảo hiểm vật chất xe
                                    {/*<label style={{color: '#329945', marginLeft: 4, textDecoration: 'underline', cursor: 'pointer'}}>*/}
                                    {/*    ( Điền thông tin bằng cách upload ảnh )*/}
                                    {/*</label>*/}
                                </label>
                            </div>

                            <div className="row form-choose-type">
                                <p className="col-12 col-md-2 col-xl-2 col-sm-12">Chọn người thụ hưởng <label style={{color: 'red'}}>*</label></p>
                                <div className="col-12 col-md-10 col-xl-10 col-sm-12 rd-group">
                                    {listBenificary ? (
                                        listBenificary.map((vl, i) =>{
                                            return(
                                                <label key={i} className="form-check">
                                                    <input className="form-check-input" type="radio" value={vl.value}
                                                           onChange={onChangeTypeBeneficary}
                                                           checked={typeBenificary === vl.value}/>
                                                    {vl.label}
                                                </label>
                                            )
                                        })
                                    ) : null}
                                </div>
                            </div>
                            {typeBenificary === 'KHAC' && (
                                <div className="row form-choose-type">
                                    <p className="col-12 col-md-2 col-xl-2 col-sm-12">Người thụ hưởng khác là? *</p>
                                    <div className="col-12 col-md-10 col-xl-10 col-sm-12 rd-group">
                                        {dataCustomerType ? (
                                            dataCustomerType.map((vl, i) =>{
                                                return(
                                                    <label key={i} className="form-check">
                                                        <input className="form-check-input" type="radio" value={vl.value}
                                                               onChange={onChangeCustomerType}
                                                               checked={customerType === vl.value}/>
                                                        {vl.label}
                                                    </label>
                                                )
                                            })
                                        ) : null}
                                    </div>
                                </div>
                            )}
                            {typeBenificary === 'KHAC' && customerType && (
                                <>
                                    <div className="row">
                                        {customerType === config.customer_type && (
                                            <div className="col-12 col-md-3 form-group">
                                                <FLSelect
                                                    disable={false}
                                                    label="Danh xưng"
                                                    value={genderBNF}
                                                    changeEvent={setGenderBNF}
                                                    // hideborder={true}
                                                    dropdown={true}
                                                    required={true}
                                                    data={dataGender.length > 0 ? dataGender : []}
                                                    error={error.genderBNF}
                                                />
                                            </div>
                                        )}
                                        {customerType === config.customer_type && (
                                            <div className="col-12 col-md-3 form-group">
                                                <FLInput
                                                    label='Họ tên'
                                                    value={nameBNF}
                                                    changeEvent={setNameBNF}
                                                    required={true}
                                                    error={error.nameBNF}
                                                />
                                            </div>
                                            // <div className="col-12 col-md-3 form-group">
                                            //     <Skeleton width={'100%'} />
                                            // </div>
                                        )}
                                        { customerType !== config.customer_type && (
                                            <div className="col-12 col-md-3 form-group">
                                                <FLInput
                                                    label='Tên tổ chức'
                                                    value={nameBNF}
                                                    changeEvent={setNameBNF}
                                                    required={true}
                                                    error={error.nameBNF}
                                                />
                                            </div>
                                        )}
                                        {customerType !== config.customer_type && (
                                            <div className="col-12 col-md-3 form-group">
                                                <FLInput
                                                    label="Mã số thuế"
                                                    value={taxCodeBNF}
                                                    changeEvent={setTaxCodeBNF}
                                                    required={true}
                                                    error={error.taxCodeBNF}
                                                />
                                            </div>
                                        )}

                                        <div className="col-12 col-md-3 form-group">
                                            <FLInput
                                                label='Số điện thoại'
                                                value={phoneBNF}
                                                changeEvent={setPhoneBNF}
                                                required={true}
                                                error={error.phoneBNF}
                                            />
                                        </div>
                                        <div className="col-12 col-md-3 form-group">
                                            <FLInput
                                                label='Email'
                                                value={emailBNF}
                                                changeEvent={setEmailBNF}
                                                error={error.emailBNF}
                                            />
                                        </div>
                                    </div>
                                    <div className="row">
                                        { customerType === config.customer_type && (
                                            <div className="col-12 col-md-3 form-group">
                                                <FLInput
                                                    label='CMND/ CCCD/ Hộ chiếu'
                                                    value={idCardBNF}
                                                    changeEvent={setIdCardBNF}
                                                    required={true}
                                                    error={error.idCardBNF}
                                                />
                                            </div>
                                        )}
                                        <div className="col-12 col-md-9 form-group">
                                            <div className="group-ipu-cus">
                                                <div className="col-12 col-md-6">
                                                    <FLInput
                                                        label='Số nhà, ngõ / ngách, đường'
                                                        value={addressBNF}
                                                        changeEvent={setAddressBNF}
                                                        required={true}
                                                        error={error.addressBNF}
                                                    />
                                                </div>
                                                <div className="col-12 col-md-6 form-group">
                                                    <FLAddress
                                                        disable={false}
                                                        changeEvent={setAddressCodeBNF}
                                                        value={addressCodeBNF}
                                                        positionSelect={"column"}
                                                        label={'Tỉnh/TP - Quận/huyện - Phường/ xã'}
                                                        required={true}
                                                        error={error.addressCodeBNF}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </>
                            )}

                        </div>
                    )}
                </>
            )}

            <div className="btn-next-step" onClick={handleNextStep} style={{margin: 0}}>
                Tiếp tục
            </div>

        </React.Fragment>
    )
}
export default Main;
