
import React, { useEffect, useState } from "react";
import {Form, FormCheck} from "react-bootstrap";
import moment from "moment";
import {config} from './concec';
import FLInput from "../../component/FlInput";
import FLSelect from "../../component/FLSelect/index";
import FLAddress from "../../component/FlAddress/index";
import {checkFormatDate, compareDate, isEmpty, showDialogDis} from "../../service/Utils";

import InputDate from "../../component/InputDateTest";
import Skeleton from "react-loading-skeleton";


const Main = (props) =>{
    // const infoUser = props.vehicleInsur ? props.vehicleInsur : {};
    const [infoUser, setInfoUser] = useState(props.vehicleInsur);
    const dataCustomerType = props.listDefineCar.length > 0 ? props.listDefineCar[1] : [];
    const dataGender = props.listDefineCar.length > 0 ? props.listDefineCar[0] : [];
    const [customerType, setCustomerType] = useState(infoUser.TYPE ? infoUser.TYPE  : config.customer_type);
    const [name, setName]= useState(infoUser.NAME ? infoUser.NAME : null);
    const [gender, setGender] = useState(infoUser.GENDER ? infoUser.GENDER : null);
    const [toggleEmail, setToggleEmail] = useState(infoUser.IS_SEND_MAIL === "1" ? true : false);

    //Các trường với thuộc type Cơ Quan
    const [taxCode, setTaxCode] = useState(infoUser.TAXCODE ? infoUser.TAXCODE : null);

    const [phone, setPhone] = useState(infoUser.PHONE ? infoUser.PHONE : null);
    const [email, setEmail] = useState(infoUser.EMAIL ? infoUser.EMAIL : null);
    const [IdCard, setIdCard] = useState(infoUser.IDCARD ? infoUser.IDCARD : null);
    const [address, setAddress] = useState(infoUser.ADDRESS ? infoUser.ADDRESS : null);
    const [addressCodeInfo, setAddressCodeInfo] = useState({label: infoUser.ADDRESS_LABEL,prov: infoUser.PROV, dist:infoUser.DIST, ward: infoUser.WARDS} || {});
    // const [addressCodeInfo, setAddressCodeInfo] = useState({label: "Thành phố Hà Nội - Quận Ba Đình - Phường Phúc Xá", prov: "01", dist: "01001", ward: "0100100001"});

    const [error, setError] = useState({});

   useEffect(() =>{
       setInfoUser(props.vehicleInsur);
   },[props.vehicleInsur]);


    const onChangeCustommerType = (e) =>{
        setCustomerType(e.target.value);
    };

    const onChangeToggleEmail = (e) =>{
        setToggleEmail(!toggleEmail);
    };

    const validateForm = () =>{
        let errors ={};
        let formIsValid = true;
        if(customerType === config.customer_type){
            if(!gender){
                formIsValid = false;
                errors['gender'] = 'Vui lòng chon danh xưng';
            }
            if(!name){
                formIsValid = false;
                errors['name'] = 'Vui lòng nhập tên chủ xe';
            }
            if(!IdCard){
                formIsValid = false;
                errors['IdCard'] = 'Vui lòng nhập CMND hoặc CCCD ';
            }
        }else{
            if(!name){
                formIsValid = false;
                errors['name'] = 'Vui lòng nhập tên cơ quan';
            }
            if(!taxCode){
                formIsValid = false;
                errors['taxCode'] = 'Vui lòng nhập mã số thuế';
            }
        }
        if(!phone){
            formIsValid = false;
            errors['phone'] = 'Vui lòng nhập số điện thoại';
        }
        if(toggleEmail){
            if(!email){
                formIsValid = false;
                errors['email'] = 'Vui lòng nhập Email';
            }
            if(email !== '' || email !== undefined){
                let pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
                if (!pattern.test(email)) {
                    formIsValid = false;
                    errors["email"] = "Vui lòng nhập đúng định dạng Email";
                }
            }
        }
        if(!address){
            formIsValid = false;
            errors['address'] = 'Vui lòng nhập số nhà, ngõ/ ngách, đường';
        }
        if(addressCodeInfo ? addressCodeInfo.label === undefined : null){
            formIsValid = false;
            errors['addressCodeInfo'] = 'Vui lòng chọn Tỉnh/TP - Quận/huyện - Phường/ xã';
        }
        setError(errors);
        return formIsValid;
    }

    const handleNextStep = () =>{
        if(validateForm()){
            props.setVehicleInsur({
                ...props.vehicleInsur,
                TYPE: customerType,
                IS_SEND_MAIL: toggleEmail ? "1": "0",
                GENDER: gender,
                NAME: name,
                PHONE: phone,
                TAXCODE: taxCode,
                EMAIL: email,
                IDCARD:IdCard,
                ADDRESS: address,
                PROV: addressCodeInfo ? addressCodeInfo.prov : '',
                DIST: addressCodeInfo ? addressCodeInfo.dist : '',
                WARDS: addressCodeInfo ? addressCodeInfo.ward : '',
                ADDRESS_LABEL: addressCodeInfo ? addressCodeInfo.label : '',
            })
            props.setBuyer({
                TYPE: customerType,
                NAME: name,
                DOB: "",
                GENDER: gender,
                ADDRESS: address,
                IDCARD:IdCard,
                EMAIL: email,
                PHONE: phone,
                PROV: addressCodeInfo ? addressCodeInfo.prov : '',
                DIST: addressCodeInfo ? addressCodeInfo.dist : '',
                WARDS: addressCodeInfo ? addressCodeInfo.ward : '',
                IDCARD_D: "",
                IDCARD_P: "",
                FAX: "",
                TAXCODE: taxCode,
            })
            props.onNextStep();
        }
    }

    return(
        <React.Fragment>
            <div className="row form-choose-type">
                <p className="col-12 col-md-3 col-xl-3 col-sm-12">Chủ xe là ai? *</p>
                <div className="col-12 col-md-9 col-xl-9 col-sm-12 rd-group">
                    {dataCustomerType ? (
                        dataCustomerType.map((vl, i) =>{
                        return(
                            <label key={i} className="form-check">
                                <input className="form-check-input" type="radio" value={vl.value}
                                       onChange={onChangeCustommerType}
                                       checked={customerType === vl.value}/>
                                {vl.label}
                            </label>
                        )
                    })
                ) : null}
                </div>
            </div>
            <div className="row form-group">
                {/*<Form.Check*/}
                {/*    type="switch"*/}
                {/*    id="custom-switch"*/}
                {/*    label="Gửi giấy chứng nhận qua email"*/}
                {/*    value={toggleEmail}*/}
                {/*    onChange={onChangeToggleEmail}*/}
                {/*/>*/}
                <FormCheck custom type="switch">
                    <FormCheck.Input checked={toggleEmail} />
                    <FormCheck.Label onClick={onChangeToggleEmail}>
                        Gửi giấy chứng nhận qua email
                    </FormCheck.Label>
                </FormCheck>

            </div>
            <div className="row">
                {customerType === config.customer_type && (
                        <div className="col-12 col-md-3 form-group">
                            <FLSelect
                                disable={false}
                                label="Danh xưng"
                                value={gender}
                                changeEvent={setGender}
                                // hideborder={true}
                                dropdown={true}
                                required={true}
                                data={dataGender.length > 0 ? dataGender : []}
                                error={error.gender}
                            />
                        </div>
                )}
                {customerType === config.customer_type && (
                    <div className="col-12 col-md-3 form-group">
                        <FLInput
                            label='Tên chủ xe '
                            value={name}
                            changeEvent={setName}
                            required={true}
                            error={error.name}
                        />
                    </div>
                    // <div className="col-12 col-md-3 form-group">
                    //     <Skeleton width={'100%'} />
                    // </div>
                )}
                { customerType !== config.customer_type && (
                        <div className="col-12 col-md-3 form-group">
                            <FLInput
                                label='Tên cơ quan'
                                value={name}
                                changeEvent={setName}
                                required={true}
                                error={error.name}
                            />
                        </div>
                )}
                {customerType !== config.customer_type && (
                        <div className="col-12 col-md-3 form-group">
                            <FLInput
                                label="Mã số thuế"
                                value={taxCode}
                                changeEvent={setTaxCode}
                                required={true}
                                error={error.taxCode}
                            />
                        </div>
                )}

                <div className="col-12 col-md-3 form-group">
                    <FLInput
                        label='Số điện thoại'
                        type="number"
                        value={phone}
                        changeEvent={setPhone}
                        required={true}
                        error={error.phone}
                    />
                </div>
                <div className="col-12 col-md-3 form-group">
                    <FLInput
                        label='Email'
                        value={email}
                        changeEvent={setEmail}
                        required={toggleEmail ? true : false}
                        error={error.email}
                    />
                </div>
            </div>
            <div className="row">
                { customerType === config.customer_type && (
                    <div className="col-12 col-md-3 form-group">
                        <FLInput
                            label='CMND/ CCCD/ Hộ chiếu'
                            value={IdCard}
                            changeEvent={setIdCard}
                            required={true}
                            error={error.IdCard}
                        />
                    </div>
                )}
                <div className="col-12 col-md-9 form-group">
                    <div className="group-ipu-cus">
                        <div className="col-12 col-md-6">
                            <FLInput
                                label='Số nhà, ngõ / ngách, đường'
                                value={address}
                                changeEvent={setAddress}
                                required={true}
                                error={error.address}

                            />
                        </div>
                        <div className="col-12 col-md-6 form-group">
                            <FLAddress
                                disable={false}
                                changeEvent={setAddressCodeInfo}
                                value={addressCodeInfo}
                                label={'Tỉnh/TP - Quận/huyện - Phường/ xã'}
                                required={true}
                                error={error.addressCodeInfo}

                            />
                        </div>
                    </div>
                </div>
            </div>
            <div className="btn-next-step" onClick={handleNextStep} style={{margin: 0}}>
                Tiếp tục
            </div>
        </React.Fragment>
    )
}
export default Main;
