import React, {useEffect, useState, useRef, useCallback} from "react";

import Step1 from "./step1";
import Step2 from "./step2";
import './style.css';
import Network from "../../service/Network";
import NextWorkNew from "../../service/Network1";
import {
    numberFormatCommas,
    showDialogDis,
    numberFormatDot,
    numberWithCommas,
    getScanQR, showModalScanQR, getLabelAddress, getFileSocket, toTitleCase
} from "../../service/Utils";
import {Form, FormCheck, Modal, Spinner, OverlayTrigger, Popover, Overlay} from "react-bootstrap";
import FLSelect from "../../component/FLSelect";
import {config, objTruc, paramsCar, buyerInfo} from "./concec";
import FLInput from "../../component/FlInput";
import LoadingForm from "../../component/Loading";
import moment from "moment";
import { io } from "socket.io-client";
import SlideImage from "./slide-image-vcx";
import FLInputTextArea from "../../component/FlInput/textarea";
const api = new Network();

const apiNew = new NextWorkNew();

const Main = (props) =>{
    const scrollToRef = useRef(null);

    const [step, setStep] = useState(0);
    const [editStep, setEditStep] = useState(0);

    const[checkEdit, setCheckEdit] = useState(false);

    const [flagObjFee, setFlagObjFee] = useState(false);

    const [listDefineCar, setListDefineCar] = useState([]);

    const [toggleGCN, setToggleGCN]= useState(false);
    const [paymentType, setPaymentType] = useState('');

    const [typeNTH, setTypeNTH] = useState('CX');

    const [buyer, setBuyer] = useState(buyerInfo);

    const [vehicleInsur, setVehicleInsur]= useState(paramsCar);

    const[voucherCode, setVoucherCode] = useState(null);
    const [isSMS, setIsSMS] = useState(false);
    const [isEmail, setIsEmail] = useState(false);

    const [loading, setLoading]= useState(false);
    const[loadingFees, setLoadingFees] = useState(false);

    const [totalAmoutFees, setTotalAmountFees] = useState('');
    const [feesTNDSBB, setFeeTNDSBB]= useState(null);
    const [feesVQM, setFeeVQM] = useState(null);
    const [feeLPX, setFeeLPX] = useState(null);
    const [feeHH, setFeeHH] = useState(null);

    const [feesVCX, setFeeVCX]= useState('');
    const [feesTNDSTN, setFeeTNDSTN]= useState('');
    const [discountTotal, setDiscountTotal] = useState('');
    const [typingMoney, setTypingMoney] = useState('');
    const [totalLas, setTotallas]= useState(0);

    const [loadingSave, setLoadingSave] =  useState(false);
    const [loadingSavePay, setLoadingSavePay] = useState(false);

    const [isPayment, setIsPayment] = useState(true);
    const [showModalCK, setShowModalCK] = useState(false);

    const [valueSMS, setValueSMS] = useState('');

    const [userPermission, setUserPermission] = useState('');

    const [transIdQr, setTransIdQr]= useState('');

    const [arrImgVcx, setArrImgVcx] = useState(vehicleInsur?.PHYSICAL_DAMAGE?.SURVEYOR_FILES || []);

    const [flagUrl,setFlagUrl] = useState("0");

    const [checkCommit,setCheckCommit] = useState(false);

    const[showModalReason,setShowModalReason] = useState(false);

    const[reason, setReason] = useState("");

    const [isOrder, setIsOrder] = useState("");
    const [labelOrder, setLabelOrder] = useState("");

    const [showQL, setShowQL] = useState(false);
    const target = useRef(null);

    useEffect(() =>{
        // getdefine();
       // const listdefine =  localStorage.getItem(config.list_define);
       // const listdefine =  sessionStorage.getItem(config.list_define);
       //  if(listdefine){
       //      let rs = listdefine ? JSON.parse(listdefine) : [];
       //      setListDefineCar(rs)
       //  }else{
            getDefineCar();
        // }
    },[]);

    useEffect(() =>{
        if(props.sdk_params){
            // console.log('props.sdk_params', props.sdk_params);
            if(props.sdk_params.action === 'ADD'){
                setCheckEdit(true);
                setStep(0);
                setEditStep(0);
                setArrImgVcx([]);
                setIsPayment(true);
                setVehicleInsur(paramsCar);
                setCheckCommit(false);
                setDiscountTotal('');
            }else if(props.sdk_params.action === 'EDIT'){
                setFlagUrl("0")
                setStep(3);
                setEditStep(3);
                if(props?.sdk_params?.user_permission){
                    let parseJs = JSON.parse(props?.sdk_params?.user_permission);
                    // console.log('parseJs', parseJs);
                    setUserPermission(parseJs);
                }
                getDataDetail(props.sdk_params.contract_code, props.sdk_params.detail_code);
            }
        }
    },[props.sdk_params]);

    useEffect(() =>{
        // const transid = props.sdk_params ? props.sdk_params.trans_id : "";
        if(transIdQr){
            const socket = io(config.urlPath+`?id=`+transIdQr+'&channel=transshipment');

            socket.on('connect', function(data){
                console.log("socket.id", socket.id);
            });

            socket.on('mediadata', function(data){
                console.log('data', data);
                let rs = getFileSocket(data,transIdQr);
                if(rs && rs.length > 0){
                    const rss = rs.filter(vl => vl.FILE_TYPE !== "id");
                    setArrImgVcx(rss);
                }
            });
        }
    },[transIdQr]);

    useEffect(() =>{
        // console.log('arrImgVcx', arrImgVcx);
        if(arrImgVcx && arrImgVcx.length > 0){
            if(document.getElementById("btn-scan-qr-vcx")) {
                document.getElementById("btn-scan-qr-vcx").click();
            }
            if(flagUrl === "1"){
                handleInspection();
            }
            setLoading(false);
        }
    },[arrImgVcx]);

    useEffect(() =>{
        if(editStep === 2){
            if(props.sdk_params.product_code === config.product_code){
                caculateFees(vehicleInsur.objFeeTNDS, vehicleInsur.objFeeTN);
            }else{
                caculateFeesV2(vehicleInsur.objFeeVcx, vehicleInsur?.PHYSICAL_DAMAGE?.DISCOUNT, vehicleInsur?.PHYSICAL_DAMAGE?.DISCOUNT_UNIT);
            }
        }
        setArrImgVcx(vehicleInsur?.PHYSICAL_DAMAGE?.SURVEYOR_FILES);
    },[vehicleInsur]);

    useEffect(() =>{
        setTotallas(totalAmoutFees - typingMoney);
    },[typingMoney]);

    useEffect(() =>{
        // {isOrder === 'CALENDAR_RESET' ? " Giám định" : "Từ chối đơn bảo hiểm"}
        if(isOrder === 'CALENDAR_RESET'){
            setLabelOrder("giám định");
        }else if(isOrder === "REJECT_CONFIRM"){
            setLabelOrder("yêu cầu sửa đơn bảo hiểm");
        }else{
            setLabelOrder("từ chối đơn bảo hiểm");
        }
    },[isOrder])

    const onChangeTypeTH = (e) =>{
        setTypeNTH(e.target.value);
    }

    const onNextStep = async (e)=>{
        switch(step){
            case 0:
                setStep(1);
                setEditStep(1);
                break;
            case 1:
                setStep(2);
                setEditStep(2);
                // setFlagObjFee(false);
                break;
            case 2:
                break;
            default:
                break;
        }
    }

    const handleEditStep = (vl) => {
        if(vl ===1){
            setFlagObjFee(true);
        }
        setStep(vl);
        setEditStep(vl);
    };

    const getDefineCar = async () => {
        setLoading(true);
        const paramDefine = {
            "Action": {
                "ParentCode": "HDI_WEB",
                "UserName": "HDI_WEB",
                "Secret": "B72088C7067CJF9FE0551F6E21B40329",
                "ActionCode": "APISZ0XSJ1"
            },
            "Data": {
                "channel": 'SDK_CAR', // props.sdk_params?.channel,
                "username": props.sdk_params?.user_name,
                "org_code": props.sdk_params?.org_code,
                "product_code": "",
                "language": ""
            },
        }
        try {
            let rs = await api.post(`/OpenApi/Post`, paramDefine);
            if(rs.Success){
                // localStorage.setItem(config.list_define,JSON.stringify(rs.Data));
                sessionStorage.setItem(config.list_define, JSON.stringify(rs.Data))
                setPaymentType(rs.Data ? rs.Data[19][0].value : null);
                setListDefineCar(rs.Data);
                setLoading(false);

                // console.log('rs.Data', rs.Data);
            }else{
                showDialogDis(rs.ErrorMessage);
                setLoading(false);
            }
        } catch (e) {
            console.log(e);
        }
    }

    const CreateOrderCar = async (type) =>{
        let verApi = '';
        // if(props.sdk_params.product_code === config.product_code){
        //     verApi= '';
        // }else {
        //     verApi='/ver2'
        // }
        if(isPayment && !paymentType){
            showDialogDis('Vui lòng chọn hình thức thanh toán');
            return;
        }
        let url_api='' ;
        if(type === 'SAVE'){
            setLoadingSave(true);
            url_api = '/OpenApi/ver2/insur/vehicle/create';
        }else if(type === 'PAY'){
            setLoadingSavePay(true);
            url_api='/OpenApi/ver2/insur/vehicle/create_pay';
        }

        let listImg = vehicleInsur?.PHYSICAL_DAMAGE?.SURVEYOR_FILES;
        let Trans_id = vehicleInsur?.PHYSICAL_DAMAGE?.TRANS_ID;
        // console.log('Trans_id', Trans_id);
        if(Trans_id && listImg.length > 0 ){
            commitImgVcx(vehicleInsur?.PHYSICAL_DAMAGE?.TRANS_ID);
        }
        const paramCar = {
            "Action": {
                "ParentCode": "HDI_WEB",
                "UserName": "HDI_WEB",
                "Secret": "B72088C7067CJF9FE0551F6E21B40329",
                "ActionCode": "APIZTPI8X1"
            },
            "Data": {
                "ORG_CODE": props.sdk_params?.org_code,
                "CHANNEL": props.sdk_params?.channel,
                "USERNAME": props.sdk_params?.user_name,
                "ACTION": props.sdk_params.action === 'ADD' ?  "BH_M" : "BH_S",
                "BUYER": buyer,
                "VEHICLE_INSUR": [
                    vehicleInsur
                ],
                "PAY_INFO": {
                    "PAYMENT_TYPE": paymentType ? paymentType : "",
                    "IS_SMS": isSMS ? "1" : "0",
                    "IS_EMAIL": isEmail ? "1" : "0",
                    "PAYMENT_MIX": null
                    // "PAYMENT_MIX": [
                    //     {
                    //         "AMOUNT": 0,
                    //         "PAYMENT_TYPE": paymentType ? paymentType : "",
                    //     }]
                },
            },
        }
        try {
            let rs = await api.post(url_api, paramCar);
            if(rs.Success){
                if(type==='SAVE'){
                    setLoadingSave(false);
                    if(props.sdk_params.action === 'ADD'){
                        showDialogDis('Tạo đơn thành công');
                    }else{
                        showDialogDis('Cập nhật đơn thành công');
                    }
                }else if(type=== 'PAY'){
                    setLoadingSavePay(false);
                    if(rs.Data.url_redirect && rs.Data.url_redirect?.toString()?.trim() !== ''){
                        // window.open(rs.Data.url_redirect,"_self");
                        if(paymentType === 'CK' && rs?.Data?.[0]?.[0]){
                            setShowModalCK(true);
                            setValueSMS(rs.Data[0][0].CONTENT_PAY)
                        }
                        window.open(
                            `${rs.Data.url_redirect}&callback=${
                                window.location.origin
                            }`,
                            "_self"
                        );
                    } else if(paymentType === 'TM' || paymentType === 'TH'){
                        showDialogDis('Thanh toán thành công');
                    }
                }

                props.sdk_params.onClose(data =>{
                    // console.log(data);
                    return data;
                });

            }else{
                showDialogDis(rs.ErrorMessage);
                setLoadingSave(false);
                setLoadingSavePay(false);
            }
        } catch (e) {
            console.log(e);
            showDialogDis(e);
            setLoadingSave(false);
            setLoadingSavePay(false);
        }
    }

    const getDataDetail = async  (contract_code, detail_code) =>{
        setLoading(true);
        const params = {
            "Action": {
                "ParentCode": "HDI_WEB",
                "UserName": "HDI_WEB",
                "Secret": "B72088C7067CJF9FE0551F6E21B40329",
                "ActionCode": "API4CQ0Z1U"
            },
            "Data": {
                "channel": "WEB_B2B",
                "username": props.sdk_params?.user_name,
                "language": "VN",
                "category": "XE",
                "product_code": props.sdk_params?.product_code,
                "contract_code": contract_code,
                "detail_code": detail_code,
                "org_code": props.sdk_params?.org_code
            },
        }
        try {
            let rs = await api.post(`/OpenApi/hdi/v1/product/detail`, params);
            if(rs.Success){
                if(rs.Data){
                    let st1 = rs.Data.BUYER;
                    let st2;
                    if(rs.Data.VEHICLE_INSUR.length > 0){
                        if(rs.Data.VEHICLE_INSUR.length > 0){
                            st2 = rs.Data.VEHICLE_INSUR[0];
                            let order_status = st2.PHYSICAL_DAMAGE?.STATUS;
                            let calendar_status = st2.PHYSICAL_DAMAGE?.CALENDAR_STATUS;
                            let use_per = JSON.parse(props?.sdk_params?.user_permission);
                            if(props.sdk_params?.product_code === config.product_code){
                                if(config.ord_status_tt.includes(st2.COMPULSORY_CIVIL?.STATUS)){
                                    setCheckEdit(true);
                                }else{
                                    setCheckEdit(false)
                                }
                            }else if (props.sdk_params?.product_code !== config.product_code){
                                if(use_per.CREATE_ORDER === "1" && order_status === config.ord_status_draft){
                                    setCheckEdit(true);
                                }else if(use_per.INSPECTION === "1"
                                    && order_status === config.ord_status_wait &&
                                    (calendar_status === config.da_xac_nhan_lh || calendar_status === config.chua_dat_lh
                                    || calendar_status === config.cho_xac_nhan_lh || calendar_status === config.giam_dinh_lai)){
                                    setCheckEdit(true);
                                }else if(use_per.APPROVE === "1" && order_status === config.ord_status_wait){
                                    setCheckEdit(false);
                                }else{
                                    setCheckEdit(false);
                                }
                            }

                            if(st2.PHYSICAL_DAMAGE?.SURVEYOR_FILES.length > 0){
                                setCheckCommit(true);
                            }else{
                                setCheckCommit(false);
                            }
                        }
                    }
                    setFeeTNDSBB(st2.COMPULSORY_CIVIL?.TOTAL_AMOUNT);

                    if(st2.VOLUNTARY_CIVIL){
                        setFeeVQM(st2?.VOLUNTARY_CIVIL?.FEES_DATA_VOLUNTARY_3RD);
                        setFeeLPX(st2?.VOLUNTARY_CIVIL?.FEES_DATA_DRIVER);
                        setFeeHH(st2?.VOLUNTARY_CIVIL?.FEES_DATA_CARGO);
                    }

                    setFeeVCX(st2.PHYSICAL_DAMAGE?.FEES_DATA);
                    setFeeTNDSTN(st2.VOLUNTARY_CIVIL?.TOTAL_AMOUNT);
                    setDiscountTotal(st2?.TOTAL_DISCOUNT);
                    setTotalAmountFees(st2.TOTAL_AMOUNT);
                    setArrImgVcx(st2.PHYSICAL_DAMAGE?.SURVEYOR_FILES);



                    setVehicleInsur({
                        ...vehicleInsur,
                        CONTRACT_CODE: st2.CONTRACT_CODE,
                        TYPE: st1.TYPE,
                        IS_SEND_MAIL: st2.IS_SEND_MAIL,
                        GENDER: st1.GENDER,
                        NAME: st1.NAME,
                        PHONE: st1.PHONE,
                        TAXCODE: st1.TAXCODE,
                        EMAIL: st1.EMAIL,
                        IDCARD:st1.IDCARD,
                        ADDRESS: st1.ADDRESS,
                        ADDRESS_LABEL: st2.ADDRESS_FORM,
                        PROV: st1.PROV,
                        DIST: st1.DIST,
                        WARDS: st1.WARDS,
                        VEHICLE_GROUP: st2.VEHICLE_GROUP,
                        VEHICLE_TYPE: st2.VEHICLE_TYPE,
                        VEHICLE_USE: st2.VEHICLE_USE,
                        NONE_NUMBER_PLATE: st2.NONE_NUMBER_PLATE,
                        IS_SPLIT_ORDER: st2.IS_SPLIT_ORDER,// 1: don le? --- 0: don gop
                        NUMBER_PLATE: st2.NUMBER_PLATE,
                        CHASSIS_NO: st2.CHASSIS_NO, //Số khung
                        ENGINE_NO: st2.ENGINE_NO, // Số máy
                        SEAT_NO: st2.SEAT_NO, //Số chỗ ngồi
                        SEAT_CODE: st2.SEAT_CODE, // key value khi Filter
                        WEIGH: st2.WEIGH, //Trọng tải
                        WEIGH_CODE: st2.WEIGH_CODE, //KEY VALUE WEIGH khi filter
                        BRAND: st2.BRAND, //Hãng xe
                        MODEL: st2.MODEL, //Hiệu xe
                        MFG: st2.MFG, //Năm sản xuất
                        CAPACITY: st2.CAPACITY, //Dung tích
                        REF_VEHICLE_VALUE: st2.REF_VEHICLE_VALUE, //Giá trị xe tham khao
                        VEHICLE_REGIS: st2.VEHICLE_REGIS, //Ngày đăng ký xe
                        VEHICLE_REGIS_CODE: st2.VEHICLE_REGIS_CODE,
                        VEHICLE_VALUE: st2.VEHICLE_VALUE,
                        VEHICLE_VALUE_CODE: st2.VEHICLE_VALUE_CODE,
                        // "objFeeTNDS": st2.objFeeTNDS,
                        // "objFeeTN": st2.objFeeTN,
                        // "objTruc": objTruc,
                        // "IS_TNDS": checkTNDS,
                        // IS_TNDS: checkTNDS,

                        IS_COMPULSORY: st2.IS_COMPULSORY, // Nguoi dung co mua BH TNDS Bat Buoc
                        IS_VOLUNTARY_ALL: st2.IS_VOLUNTARY_ALL, // Nguoi dung co mua tu nguyen
                        IS_PHYSICAL: st2.IS_PHYSICAL,    // Nguoi dung co mua Vat chat xe

                        IS_VOLUNTARY: st2.IS_VOLUNTARY, //Có mua TNDS TN (người và hàng hóa người t3, hành khách)
                        IS_DRIVER:st2.IS_DRIVER, //Có mua TNDS TN cho LPX, NNTX
                        IS_CARGO: st2.IS_CARGO, //Có mua TNDS TN cho hàng hóa
                        TOTAL_AMOUNT : st2.TOTAL_AMOUNT,
                        // //Trách nhiệm dân sự bắt buộc
                        COMPULSORY_CIVIL: st2.COMPULSORY_CIVIL,
                        // //TRach nhiem dan su tu nguyen
                        VOLUNTARY_CIVIL: st2.VOLUNTARY_CIVIL,
                        // ADDRESS_LABEL: addressCodeInfo ? addressCodeInfo.label : '',
                        //vat chat xe
                        PHYSICAL_DAMAGE:  st2.PHYSICAL_DAMAGE,
                    })
                    setBuyer({
                        TYPE:  st1.TYPE,
                        NAME: st1.NAME,
                        DOB: "",
                        GENDER: st1.GENDER,
                        ADDRESS: st1.ADDRESS,
                        IDCARD:st1.IDCARD,
                        EMAIL: st1.EMAIL,
                        PHONE: st1.PHONE,
                        PROV: st1.PROV,
                        DIST: st1.DIST,
                        WARDS: st1.WARDS,
                        IDCARD_D: "",
                        IDCARD_P: "",
                        FAX: "",
                        TAXCODE: st1.TAXCODE,
                    });
                }
                setLoading(false);
            }else{
                showDialogDis(rs.ErrorMessage);
                setLoading(false);
            }
        } catch (e) {
            setLoading(false);
            console.log(e);
        }
    };

    const showModalQr = async () => {
        setFlagUrl("1");
        setLoading(true);
        let isNumberPlate = vehicleInsur?.NONE_NUMBER_PLATE === "1" ? true : false;
        let rs = await getScanQR(isNumberPlate);
        if(rs.Success){
            setTransIdQr(rs.Data?.TRANS_ID);
            showModalScanQR(rs.Data?.BASE64_QR);
            setLoading(false);
        }else{
            setLoading(false);
            showDialogDis(rs.ErrorMessage);
        }
    }

    const handleInspection = async() =>{
        setLoading(true);
        if(transIdQr){
            commitImgVcx(transIdQr);
        }
        const params = {
            "Action": {
                "ParentCode": "HDI_WEB",
                "UserName": "HDI_WEB",
                "Secret": "B72088C7067CJF9FE0551F6E21B40329",
                "ActionCode": "API2EMUFFT"
            },
            "Data": {
                "CHANNEL": props.sdk_params?.channel,
                "USERNAME": props.sdk_params?.user_name,
                "contract_code": props.sdk_params?.contract_code,
                "detail_code": props.sdk_params?.detail_code,
                "o_file": {
                    "SURVEYOR_FILES": arrImgVcx
                }
            },
            "Signature": "034F0DA18BCBDDDB216211594505FB0D8877BB41BE73F6285AECFF7939FA051A",
        }
        try{
            let rs =  await api.post(`/OpenApi/Post`, params);
            if(rs.Success){
                setLoading(false);
                // console.log('rs', rs);
                showDialogDis("Giám định thành công");
                props.sdk_params.onClose(data =>{
                    // console.log(data);
                    return data;
                });
            }else{
                setLoading(false);
                showDialogDis(rs.ErrorMessage);
            }
        }catch (e){
            setLoading(false);
            showDialogDis(e);
            console.log(e);
        }
    }

    const summaryStep1 = () => {
        // console.log('vehicleInsur.ADDRESS_LABEL', vehicleInsur.ADDRESS_LABEL);
        // let provideVl= vehicleInsur.ADDRESS_LABEL ?  vehicleInsur.ADDRESS_LABEL.split("-") : '';
        let provideFormat = getLabelAddress(vehicleInsur?.ADDRESS_LABEL);
        return(
            <div className="unactive-step-1 typing-value-form">
                <div className="row">
                    <div className="col-4">
                        <label className="col-form-label">Chủ xe:</label>
                        <label className="col-form-label">{vehicleInsur.TYPE === config.customer_type ? 'Cá nhân' : 'Cơ quan'}</label>
                    </div>
                    <div className="col-4">
                        <label className="col-form-label">Gửi GCN qua email:</label>
                        <label className="col-form-label">{vehicleInsur.IS_SEND_MAIL === '1' ? 'Có' : 'Không'}</label>
                    </div>
                </div>
                <div className="row">
                    <div className="col-4">
                        {vehicleInsur.TYPE === config.customer_type ? (
                            <label className="col-form-label">{!vehicleInsur.GENDER ? 'Ông/Bà' : (vehicleInsur.GENDER === "M" ? "Ông" : "Bà")}:</label>
                        ): (
                            <label className="col-form-label">Tên cơ quan:</label>
                        )}
                        <label className="col-form-label">{vehicleInsur.NAME}</label>
                    </div>
                    <div className="col-4">
                        <label className="col-form-label">Số điện thoại:</label>
                        <label className="col-form-label">{vehicleInsur.PHONE}</label>
                    </div>
                    {vehicleInsur.TYPE === config.customer_type && (
                        <div className="col-4">
                            <label className="col-form-label">CMND/CCCD/Hộ chiếu:</label>
                            <label className="col-form-label">{vehicleInsur.IDCARD}</label>
                        </div>
                    )}
                    {vehicleInsur.TYPE !== config.customer_type && (
                        <div className="col-4">
                            <label className="col-form-label">Mã số thuế:</label>
                            <label className="col-form-label">{vehicleInsur.TAXCODE}</label>
                        </div>
                    )}
                </div>
                <div className="row">
                    <div className="col-4">
                        <label className="col-form-label">Email:</label>
                        <label className="col-form-label">{vehicleInsur.EMAIL}</label>
                    </div>
                    <div className="col-8">
                        <label className="col-form-label">Địa chỉ:</label>
                        <label className="col-form-label">{vehicleInsur.ADDRESS + ' , ' +  provideFormat}</label>
                    </div>
                </div>
            </div>
        )
    }

    const summaryStep2 = () => {
        const product_code = props.sdk_params.product_code;
        const {PHYSICAL_DAMAGE, COMPULSORY_CIVIL, VOLUNTARY_CIVIL, VEHICLE_GROUP, VEHICLE_TYPE, BRAND,MODEL,NUMBER_PLATE,
            ENGINE_NO,CHASSIS_NO, IS_VOLUNTARY, IS_DRIVER,IS_CARGO, WEIGH, SEAT_NO, VEHICLE_USE, MFG, REF_VEHICLE_VALUE,
            VEHICLE_REGIS, VEHICLE_VALUE, TYPE} = vehicleInsur;

        const labelQL = PHYSICAL_DAMAGE?.ADDITIONAL ? PHYSICAL_DAMAGE?.ADDITIONAL : [];

        let rangeTNDS = COMPULSORY_CIVIL?.TIME_EFF+' '+COMPULSORY_CIVIL?.EFF + ' - '+ COMPULSORY_CIVIL?.TIME_EXP+' '+COMPULSORY_CIVIL?.EXP;
        let labelVehicleGroup = listDefineCar.length > 0 ? listDefineCar[2].filter((vl) => {
            return vl.value === VEHICLE_GROUP;
        }) : "";
        let labelVehicleType = listDefineCar.length > 0 ? listDefineCar[3].filter((vl) => {
            return vl.value === VEHICLE_TYPE;
        }) : "";
        let labelBrand = listDefineCar.length > 0 ?  listDefineCar[5].filter((vl) => {
            return vl.value === BRAND;
        }) : "";
        let labelModel = listDefineCar.length > 0 ?  listDefineCar[6].filter((vl) => {
            return vl.value === MODEL;
        }) : "";
        let labelVehicleUse = listDefineCar.length > 0 ?  listDefineCar[4].filter((vl) => {
            return vl.value === VEHICLE_USE;
        }) : "";
        let labelInsur3D = listDefineCar.length > 0 ?  listDefineCar[7].filter((vl) => {
            return vl.value === VOLUNTARY_CIVIL?.INSUR_3RD;
        }) : "";
        let labelInsurPass = listDefineCar.length > 0 ?  listDefineCar[8].filter((vl) => {
            return vl.value === VOLUNTARY_CIVIL?.INSUR_PASSENGER;
        }) : "";

        let labelInsur3DAsset = listDefineCar.length > 0 ?  listDefineCar[9].filter((vl) => {
            return vl.value === VOLUNTARY_CIVIL?.INSUR_3RD_ASSETS;
        }) : "";
        let labelInsurInsur = listDefineCar.length > 0 ?  listDefineCar[11].filter((vl) => {
            return vl.value === VOLUNTARY_CIVIL?.DRIVER_INSUR;
        }) : "";
        let labelCargoInsur =  listDefineCar.length > 0 ? listDefineCar[12].filter((vl) => {
            return vl.value === VOLUNTARY_CIVIL?.CARGO_INSUR;
        }) : "";

        let rangeVCX = '', labelPackCode= '', labelInsurDeduc= '';
        if(props.sdk_params !== config.product_code){
             rangeVCX = PHYSICAL_DAMAGE?.TIME_EFF+' '+PHYSICAL_DAMAGE?.EFF + ' - '+ PHYSICAL_DAMAGE?.TIME_EXP+' '+PHYSICAL_DAMAGE?.EXP;
             labelPackCode = listDefineCar.length > 0 ? listDefineCar[13].filter((vl) => {
                return vl.value === PHYSICAL_DAMAGE?.PACK_CODE;
            }) : "";
            // INSUR_DEDUCTIBLE
             labelInsurDeduc = listDefineCar.length > 0 ? listDefineCar[14].filter((vl) => {
                return vl.value === PHYSICAL_DAMAGE?.INSUR_DEDUCTIBLE;
            }) : "";
        }

        let provideFormat = getLabelAddress(PHYSICAL_DAMAGE?.CALENDAR_ADDRESS_FORM);
        let objBNF = PHYSICAL_DAMAGE?.BENEFICIARY ? PHYSICAL_DAMAGE?.BENEFICIARY : [];
        let labelNameTH = ''
        if(objBNF[0]?.TYPE === config.customer_type){
            labelNameTH = objBNF[0]?.GENDER === "M" ? "Ông" : "Bà";
        }else{
            labelNameTH = 'Tên cơ quan'
        }
        let formatAddresBNF = null;
        formatAddresBNF = getLabelAddress(objBNF[0]?.ADDRESS_FORM);
        return(
            <div className="unactive-step-2 typing-value-form">
                <div className="row ">
                    {/*<div className="col-4">*/}
                    {/*    <label className="col-form-label">Số GCN:</label>*/}
                    {/*    <label className="col-form-label">{COMPULSORY_CIVIL?.SERIAL_NUM}</label>*/}
                    {/*</div>*/}
                    <div className="col-4">
                        <label className="col-form-label">Nhóm xe:</label>
                        <label className="col-form-label">{labelVehicleGroup.length > 0 ? labelVehicleGroup[0].label : '' }</label>
                    </div>
                    <div className="col-4">
                        <label className="col-form-label">Loại xe:</label>
                        <label className="col-form-label">{labelVehicleType.length > 0 ? labelVehicleType[0].label : '' }</label>
                    </div>
                    <div className="col-4">
                        <label className="col-form-label">Hãng xe:</label>
                        <label className="col-form-label">{labelBrand.length > 0 ? labelBrand[0].label : ''}</label>
                    </div>
                </div>
                <div className="row ">
                    <div className="col-4">
                        <label className="col-form-label">Hiệu xe:</label>
                        <label className="col-form-label">{labelModel.length > 0 ? labelModel[0].label : ''}</label>
                    </div>
                    <div className="col-4">
                        <label className="col-form-label">Số chỗ:</label>
                        <label className="col-form-label">{SEAT_NO}</label>
                    </div>
                    <div className="col-4">
                        <label className="col-form-label">Trọng tải:</label>
                        <label className="col-form-label">{WEIGH ? WEIGH + ' tấn' : null}</label>
                    </div>
                    <div className="col-4">
                        <label className="col-form-label">Mục đích sử dụng:</label>
                        <label className="col-form-label">{labelVehicleUse.length > 0 ? labelVehicleUse[0].label : ''}</label>
                    </div>
                </div>
                <div className="row ">
                    <div className="col-4">
                        <label className="col-form-label">Biển KS:</label>
                        <label className="col-form-label">{NUMBER_PLATE}</label>
                    </div>
                    <div className="col-4">
                        <label className="col-form-label">Số máy:</label>
                        <label className="col-form-label">{ENGINE_NO}</label>
                    </div>
                    <div className="col-4">
                        <label className="col-form-label">Số khung: </label>
                        <label className="col-form-label">{CHASSIS_NO}</label>
                    </div>
                </div>
                {product_code === config.product_code && (
                    <>
                        <div className="row">
                            <div className="col-12">
                                <label className="col-form-label">Thời hạn bảo hiểm TNDS:</label>
                                <label className="col-form-label">{rangeTNDS}</label>
                            </div>
                        </div>
                        <p className="txt-sumary-st2">1. Bảo hiểm TNDS bắt buộc</p>
                    </>
                )}

                {product_code !== config.product_code && (
                    <>
                        <p className="txt-sumary-st2">1. Bảo hiểm vật chất xe</p>
                        <div className="row">
                            <div className="col-4">
                                <label className="col-form-label">Chương trình:</label>
                                <label className="col-form-label">{labelPackCode.length > 0 ? labelPackCode[0].label : '' }</label>
                            </div>
                            <div className="col-4">
                                <label className="col-form-label">Năm sản xuất :</label>
                                <label className="col-form-label">{MFG}</label>
                            </div>
                            <div className="col-4">
                                <label className="col-form-label">Ngày đăng ký xe:</label>
                                <label className="col-form-label">{VEHICLE_REGIS}</label>
                            </div>
                            <div className="col-4">
                                <label className="col-form-label">Mức miễn thường:</label>
                                <label className="col-form-label">{labelInsurDeduc.length > 0 ? labelInsurDeduc[0].label : '' }</label>
                            </div>
                            <div className="col-4">
                                <label className="col-form-label">Giá trị xe tham khảo:</label>
                                <label className="col-form-label">{REF_VEHICLE_VALUE ? numberFormatCommas(REF_VEHICLE_VALUE) : 0} VNĐ</label>
                            </div>
                            <div className="col-4">
                                <label className="col-form-label">Giá trị xe thực tế:</label>
                                <label className="col-form-label">{VEHICLE_VALUE ? numberFormatCommas(VEHICLE_VALUE) : 0} VNĐ</label>
                            </div>

                            <div className="col-4">
                                <label className="col-form-label">Số tiền BH:</label>
                                <label className="col-form-label">{PHYSICAL_DAMAGE ? numberFormatCommas(PHYSICAL_DAMAGE?.INSUR_TOTAL_AMOUNT.toString()) : 0} VNĐ</label>
                            </div>
                            <div className="col-4">
                                <label className="col-form-label">Điều khoản bổ sung:</label>
                                <label className="col-form-label" ref={target} onClick={() => setShowQL(!showQL)}
                                    style={{color: '#329945', textDecorationLine: 'underline', fontWeight: 'bold'}}>
                                    {PHYSICAL_DAMAGE?.ADDITIONAL ? PHYSICAL_DAMAGE?.ADDITIONAL.length + ' quyền lợi' : null}
                                </label>
                                {/*<OverlayTrigger trigger="click" placement="bottom" overlay={popover}>*/}
                                {/*</OverlayTrigger>*/}
                                <Overlay onHide={() => setShowQL(false)}
                                         target={target.current}
                                         show={showQL} rootClose={true}
                                         placement="bottom">
                                    <Popover id="popover-basic">
                                        <Popover.Title >Điều khoản bổ sung</Popover.Title>
                                        <Popover.Content>
                                            {labelQL && labelQL.map((vl, i) =>{
                                                return  <p>{(i+1)+ '. ' +vl.LABEL}</p>
                                            })}
                                        </Popover.Content>
                                    </Popover>
                                </Overlay>
                            </div>
                        </div>

                        <div className="row">

                            <div className="col-4">
                                <label className="col-form-label">TGBH:</label>
                                <label className="col-form-label">{rangeVCX}</label>
                            </div>
                            <div className="col-4">
                                <label className="col-form-label">Tổn thất:</label>
                                <label className="col-form-label">{PHYSICAL_DAMAGE?.IS_VEHICLE_LOSS === "0" ? "Xe không bị tổn thất" : "Xe bị tổn thất"}</label>
                            </div>
                            {PHYSICAL_DAMAGE?.IS_VEHICLE_LOSS === "0" ? (
                                <div className="col-4">
                                    <label className="col-form-label">Số năm không bị tổn thất:</label>
                                    <label className="col-form-label">{PHYSICAL_DAMAGE?.VEHICLE_LOSS_NUM}</label>
                                </div>
                            ) : (
                                <div className="col-4">
                                    <label className="col-form-label">Số tiền bị tổn thất:</label>
                                    <label className="col-form-label">{PHYSICAL_DAMAGE?.VEHICLE_LOSS_AMOUNT ? numberFormatCommas(PHYSICAL_DAMAGE?.VEHICLE_LOSS_AMOUNT) : 0} VNĐ</label>
                                </div>
                            )}

                            <div className="col-4">
                                <label className="col-form-label">Giảm phí:</label>
                                <label className="col-form-label">{PHYSICAL_DAMAGE?.DISCOUNT ? PHYSICAL_DAMAGE?.DISCOUNT : "0"}{ PHYSICAL_DAMAGE?.DISCOUNT_UNIT === "P" ? '%'  : "VNĐ"}</label>
                            </div>
                            <div className="col-4">
                                <label className="col-form-label">Lí do giảm:</label>
                                <label className="col-form-label">{PHYSICAL_DAMAGE?.REASON_DISCOUNT || ""}</label>
                            </div>
                        </div>
                    </>
                )}

                {IS_VOLUNTARY ==="1" &&(
                    <div>
                        <p className="txt-sumary-st2">2. Bảo hiểm TNDS tự nguyện (vượt quá mức TNDS bắt buộc)</p>

                        <div className="row">
                            <div className="col-4">
                                <label className="col-form-label">Thiệt hạn thân thể người thứ 3:  </label>
                                <label className="col-form-label">{labelInsur3D.length > 0 ? labelInsur3D[0].label : '' }</label>
                            </div>
                            <div className="col-4">
                                <label className="col-form-label">Thiệt hại thân thể hành khách:  </label>
                                <label className="col-form-label">{labelInsurPass.length > 0 ? labelInsurPass[0].label : '' }</label>
                            </div>
                            <div className="col-4">
                                <label className="col-form-label">Thiệt hại tài sản người thứ 3:</label>
                                <label className="col-form-label">{labelInsur3DAsset.length > 0 ? labelInsur3DAsset[0].label : ''}</label>
                            </div>
                            {/*<div className="col-6">*/}
                            {/*    <label className="col-form-label">Tổng hạn mức trách nhiệm:</label>*/}
                            {/*    <label className="col-form-label">20 triệu VNĐ/ người/ vụ</label>*/}
                            {/*</div>*/}
                        </div>
                    </div>
                )}
                {IS_DRIVER === "1" && (
                    <div>
                        <p className="txt-sumary-st2">3. Bảo hiểm lái xe và người ngồi trên xe</p>
                        <div className="row ">
                            {/*<div className="col-6">*/}
                            {/*    <label className="col-form-label">Số người tham gia:</label>*/}
                            {/*    <label className="col-form-label">2 người</label>*/}
                            {/*</div>*/}
                            <div className="col-6">
                                <label className="col-form-label">Số tiền bảo hiểm:</label>
                                <label className="col-form-label">{labelInsurInsur.length > 0 ? labelInsurInsur[0].label : ''}</label>
                            </div>
                        </div>
                    </div>
                )}
                {IS_CARGO === "1" && (
                    // ảohiểm TNDS củachủ xe đốivới hàng hóa
                    <div>
                        <p className="txt-sumary-st2">4. Bảo hiểm TNDS của chủ xe đối với hàng hóa</p>
                        <div className="row ">
                            <div className="col-6">
                                <label className="col-form-label">Hàng hóa được BH:</label>
                                <label className="col-form-label">{VOLUNTARY_CIVIL?.WEIGHT_CARGO} tấn</label>
                            </div>
                            <div className="col-6">
                                <label className="col-form-label">Mức trách nhiệm:</label>
                                <label className="col-form-label">{labelCargoInsur.length > 0 ? labelCargoInsur[0].label : ''}</label>
                            </div>
                        </div>
                    </div>
                )}

                {PHYSICAL_DAMAGE?.SURVEYOR_TYPE === config.gd_theo_kh && (
                    <div>
                        <p className="txt-sumary-st2">Đến giám định xe theo lịch do khách hàng chọn</p>
                       <div className="row">
                           <div className="col-4">
                               <label className="col-form-label">Thời gian:</label>
                               <label className="col-form-label">{PHYSICAL_DAMAGE?.CALENDAR_EFF+ '('+PHYSICAL_DAMAGE?.CALENDAR_TIME_F +'-' + PHYSICAL_DAMAGE?.CALENDAR_TIME_T +')'}</label>
                           </div>
                           <div className="col-4">
                               <label className="col-form-label">Người liên hệ:</label>
                               <label className="col-form-label">{PHYSICAL_DAMAGE?.CALENDAR_CONTACT}</label>
                           </div>
                           <div className="col-4">
                               <label className="col-form-label">Số điện thoại:</label>
                               <label className="col-form-label">{PHYSICAL_DAMAGE?.CALENDAR_PHONE}</label>
                           </div>
                           <div className="col-4">
                               <label className="col-form-label">Địa chỉ:</label>
                               <label className="col-form-label">{PHYSICAL_DAMAGE?.CALENDAR_ADDRESS + ' , ' +  provideFormat}</label>
                           </div>
                       </div>
                    </div>
                )}

                {objBNF.length > 0 && (
                    <div>
                        <p className="txt-sumary-st2">Thông tin thụ hưởng</p>
                        <div className="row">
                            <div className="col-4">
                                <label className="col-form-label">{labelNameTH}:</label>
                                <label className="col-form-label">{objBNF[0]?.NAME}</label>
                            </div>
                            <div className="col-4">
                                <label className="col-form-label">Số điện thoại:</label>
                                <label className="col-form-label">{objBNF[0]?.PHONE}</label>
                            </div>
                            <div className="col-4">
                                <label className="col-form-label">Email:</label>
                                <label className="col-form-label">{objBNF[0]?.EMAIL}</label>
                            </div>
                            {objBNF[0]?.TYPE === config.customer_type && (<div className="col-4">
                                <label className="col-form-label">CMND/ CCCD/ HC:</label>
                                <label className="col-form-label">{objBNF[0]?.IDCARD}</label>
                            </div>)}
                            {objBNF[0]?.TYPE !== config.customer_type && (<div className="col-4">
                                <label className="col-form-label">Mã số thuế:</label>
                                <label className="col-form-label">{objBNF[0]?.TAXCODE}</label>
                            </div>)}
                            <div className="col-4">
                                <label className="col-form-label">Địa chỉ:</label>
                                <label className="col-form-label">{objBNF[0]?.ADDRESS + ' , ' +  formatAddresBNF}</label>
                            </div>
                        </div>
                    </div>
                )
                }
                {arrImgVcx && arrImgVcx.length > 0 && (
                    <div className="col-12">
                        <SlideImage
                            dataValue={arrImgVcx}
                            sdk_params={props.sdk_params}
                            flag={flagUrl}
                            checkCommit={checkCommit}
                        />
                    </div>
                )}


                {/*{PHYSICAL_DAMAGE?.SURVEYOR_FILES.length > 0 && (*/}

                {/*)}*/}

            </div>
        )
    };

    const summaryStep3 = () =>{
        const {COMPULSORY_CIVIL, VOLUNTARY_CIVIL, PHYSICAL_DAMAGE, TOTAL_AMOUNT} = vehicleInsur;
        return(
            <div className="unactive-step-1 typing-value-form">
                <div className="row">
                    {props.sdk_params.product_code === config.product_code ? (
                        <div className="col-5">
                            <label className="col-form-label">Phí bảo hiểm TNDS bắt buộc (Có VAT): </label>
                            <label className="col-form-label">{COMPULSORY_CIVIL?.FEES_DATA ? numberWithCommas(COMPULSORY_CIVIL?.FEES_DATA) : 0 }  VNĐ</label>
                        </div>
                    ) : (
                        <div className="col-5">
                            <label className="col-form-label">Phí bảo hiểm vật chất xe (Có VAT): </label>
                            <label className="col-form-label">{PHYSICAL_DAMAGE?.FEES_DATA ? numberWithCommas(PHYSICAL_DAMAGE?.FEES_DATA) : 0 }  VNĐ</label>
                        </div>
                    )}
                    <div className="col-5">
                        <label className="col-form-label">Phí bảo hiểm TNDS tự nguyện vượt quá mức (Có VAT): </label>
                        <label className="col-form-label">{VOLUNTARY_CIVIL?.FEES_DATA_VOLUNTARY_3RD ? numberWithCommas(VOLUNTARY_CIVIL?.FEES_DATA_VOLUNTARY_3RD) : 0} VNĐ</label>
                    </div>
                </div>
                <div className="row">
                    <div className="col-5">
                        <label className="col-form-label">Bảo hiểm lái xe và người ngồi trên xe (Có VAT): </label>
                        <label className="col-form-label">{VOLUNTARY_CIVIL?.FEES_DATA_DRIVER ? numberWithCommas(VOLUNTARY_CIVIL?.FEES_DATA_DRIVER) : 0 } VNĐ</label>
                    </div>
                    <div className="col-5">
                        <label className="col-form-label">Bảo hiểm TNDS của chủ xe đối với hàng hóa (Có VAT): </label>
                        <label className="col-form-label">{VOLUNTARY_CIVIL?.FEES_DATA_CARGO ? numberWithCommas(VOLUNTARY_CIVIL?.FEES_DATA_CARGO) : 0} VNĐ</label>
                    </div>
                </div>
                <div className="row">
                    <div className="col-5">
                        <label className="col-form-label">Mã voucher: </label>
                        <label className="col-form-label">{}</label>
                    </div>
                    <div className="col-5">
                        <label className="col-form-label">Giảm phí: </label>
                        <label className="col-form-label">- {discountTotal ? numberWithCommas(discountTotal) : 0} VNĐ</label>
                    </div>
                </div>
                <div className="row">
                    <div className="col-4">
                        <label className="col-form-label">Tổng phí: </label>
                        <label className="col-form-label">{TOTAL_AMOUNT ? numberWithCommas(TOTAL_AMOUNT) : 0} VNĐ</label>
                    </div>
                </div>
            </div>
        )
    }

    const onChangeSMS = (e) =>{
        setIsSMS(!isSMS);
    };

    const onChangeEmail = (e) =>{
        setIsEmail(!isEmail);
    };

    const caculateFees = async (objFee) =>{
        setLoadingFees(true);
        const _effDate = vehicleInsur.COMPULSORY_CIVIL?.EFF ? `${vehicleInsur.COMPULSORY_CIVIL?.EFF} ${vehicleInsur.COMPULSORY_CIVIL?.TIME_EFF}` :
         `${vehicleInsur.PHYSICAL_DAMAGE?.EFF} ${vehicleInsur.PHYSICAL_DAMAGE?.TIME_EFF}`;
        const _expDate = vehicleInsur.COMPULSORY_CIVIL?.EXP ? `${vehicleInsur.COMPULSORY_CIVIL?.EXP} ${vehicleInsur.COMPULSORY_CIVIL?.TIME_EXP}` :
         `${vehicleInsur.PHYSICAL_DAMAGE?.EXP} ${vehicleInsur.PHYSICAL_DAMAGE?.TIME_EXP}`;
        const params = {
            "Action": {
                "ParentCode": "HDI_WEB",
                "UserName": "HDI_WEB",
                "Secret": "B72088C7067CJF9FE0551F6E21B40329",
                "ActionCode": "APIZTPI8X1"
            },
            "Data": {
                "channel": props.sdk_params?.channel,
                "userName": props.sdk_params?.user_name,
                "ORG_CODE": props.sdk_params?.org_code,
                "PRODUCT_INFO": [
                    {
                        "INX": "0",
                        "CATEGORY": "XE",
                        "PRODUCT_CODE": props.sdk_params?.product_code,
                        "PACK_CODE": "TNDSBB",
                        "DISCOUNT": 0,
                        "DISCOUNT_UNIT": "",
                        "DYNAMIC_FEES": objFee,
                        EFF: _effDate,
                        EXP: _expDate
                    }
                ],
                "GIFTS": null
            },
            "Signature": "9496EA5EBD92B58D3D217E2F3C2DE1E73993A1CFD871685FDE12EAE03EF6BCD4",
        };
        try {
            let rs =  await api.post(`/OpenApi/insur/calculate/v3/fees`, params);
            if(rs.Success){
                if(rs.Data){
                    // console.log(' -===========> data Fee', rs.Data);
                    let rs1 = rs?.Data;
                    if(rs1?.PRODUCT_DETAIL.length > 0){
                        let jsonFee = rs1?.PRODUCT_DETAIL[0];
                        let rs2 = jsonFee?.TOTAL_AMOUNT;
                        setFeeTNDSBB(rs2);
                        // let rs3 = rs1.PRODUCT_DETAIL[1].TOTAL_AMOUNT;
                        // setFeeTNDSTN(rs3);
                        if(jsonFee?.OUT_DETAIL.length >0){
                            let jsDetail = jsonFee?.OUT_DETAIL;
                            const feeTNDSBB = jsDetail.find(x => x.CODE === 'TNDSBB_F');
                            const feeVQM = jsDetail.find(x => x.CODE === 'XCGTN_VQM');
                            const feeLPX = jsDetail.find(x => x.CODE === 'XCGTN_FNTX');
                            const feeHH = jsDetail.find(x => x.CODE === 'XCGTN_FBHHH');
                            setFeeTNDSBB(feeTNDSBB?.FEES_DATA); // phí TNDSBB
                            setFeeVQM(feeVQM?.FEES_DATA); // Phí Vượt quá mức
                            setFeeLPX(feeLPX?.FEES_DATA); // Phí Lái Phụ Xe
                            setFeeHH(feeHH?.FEES_DATA); // Phí hàng hoá
                        }
                    }
                    // const [feesVQM, setFeeVQM] = useState(null);
                    // const [feeLPX, setFeeLPX] = useState(null);
                    // const [feeHH, setFeeHH] = useState(null);
                    setTotalAmountFees(rs1.TOTAL_AMOUNT);
                    setLoadingFees(false);
                }
            }else{
                setLoadingFees(false);
                showDialogDis(rs.ErrorMessage);
            }
            // await api.post(`/OpenApi/Post`, paramTotalAmout).then(data =>{
            //
            // });
        } catch (e) {
            console.log(e);
        }
    }

    const caculateFeesV2 = async (objFee, discount, discount_unit ) =>{
        setLoadingFees(true);
        const _effDate = vehicleInsur.COMPULSORY_CIVIL?.EFF ? `${vehicleInsur.COMPULSORY_CIVIL?.EFF} ${vehicleInsur.COMPULSORY_CIVIL?.TIME_EFF}` :
         `${vehicleInsur.PHYSICAL_DAMAGE?.EFF} ${vehicleInsur.PHYSICAL_DAMAGE?.TIME_EFF}`;
        const _expDate = vehicleInsur.COMPULSORY_CIVIL?.EXP ? `${vehicleInsur.COMPULSORY_CIVIL?.EXP} ${vehicleInsur.COMPULSORY_CIVIL?.TIME_EXP}` :
         `${vehicleInsur.PHYSICAL_DAMAGE?.EXP} ${vehicleInsur.PHYSICAL_DAMAGE?.TIME_EXP}`;
        const paramV2 = {
            "Action": {
                "ParentCode": "HDI_WEB",
                "UserName": "HDI_WEB",
                "Secret": "B72088C7067CJF9FE0551F6E21B40329",
                "ActionCode": "APIZTPI8X1"
            },
            "Data": {
                "channel": props.sdk_params?.channel,
                "userName": props.sdk_params?.user_name,
                "ORG_CODE": props.sdk_params?.org_code,
                "PRODUCT_INFO": [
                    {
                        "INX": "0",
                        "CATEGORY": "XE",
                        "PRODUCT_CODE": "XCG_VCX_NEW",
                        "PACK_CODE": "",
                        "DISCOUNT": discount ? discount : "",
                        "DISCOUNT_UNIT": discount_unit ? discount_unit : "",
                        "TYPE_DISCOUNT": "VC_XE",
                        "DYNAMIC_FEES": objFee,
                        EFF: _effDate,
                        EXP: _expDate
                    }
                ],
                "GIFTS": null
            },
            "Signature": "9496EA5EBD92B58D3D217E2F3C2DE1E73993A1CFD871685FDE12EAE03EF6BCD4",
        };
        try {
            let rsV2 =  await api.post(`/OpenApi/insur/calculate/v3/fees`, paramV2);
            if(rsV2.Success){
                if(rsV2.Data){
                    // console.log('Dataaa tinh phi', rsV2.Data);
                    let rs1 = rsV2.Data;
                    if(rs1.PRODUCT_DETAIL.length > 0){
                        let objDataFee = rs1.PRODUCT_DETAIL[0];
                        setTotalAmountFees(objDataFee?.TOTAL_AMOUNT);
                        setDiscountTotal(objDataFee?.TOTAL_DISCOUNT);
                        if(objDataFee.OUT_DETAIL.length > 0){
                            let feesVcx = objDataFee.OUT_DETAIL.filter(vl => vl.CODE === "XCG_VCX_FEES");
                            let feesVcxTn = objDataFee.OUT_DETAIL.filter(vl => vl.CODE === "XCGTN_BS");
                            setFeeVCX(feesVcx[0]?.FEES_DATA);
                            setFeeTNDSTN(feesVcxTn[0]?.TOTAL_AMOUNT);
                        }
                        // let rs2 = rs1.PRODUCT_DETAIL[0].TOTAL_AMOUNT;
                        // setFeeTNDSBB(rs2);
                        // let rs3 = rs1.PRODUCT_DETAIL[1].TOTAL_AMOUNT;
                        // setFeeTNDSTN(rs3);
                    }
                    // setTotalAmountFees(rs1.TOTAL_AMOUNT);
                    setLoadingFees(false);
                }
            }else{
                setLoadingFees(false);
                showDialogDis(rsV2.ErrorMessage);
            }
        } catch (e) {
            console.log(e);
        }
    }

    const showModalCheckOrder = (stt_order) =>{
        setReason("");
        setShowModalReason(true);
        setIsOrder(stt_order);
    }

    const saveReason = (contract_code,action, reason) =>{
        setShowModalReason(false);
        updateStatusOrder(contract_code,action, reason);
    }

    const updateStatusOrder = async (contract_code,action, reason) =>{
        const params = {
            "Action": {
                "ParentCode": "HDI_WEB",
                "UserName": "HDI_WEB",
                "Secret": "B72088C7067CJF9FE0551F6E21B40329",
                "ActionCode": "APIL920XFE"
            },
            "Data": {
                "CHANNEL": "SELLING",
                "USERNAME": props?.sdk_params?.user_name,
                "CATEGORY": "XE",
                "PRODUCT_CODE": "XCG_VCX_NEW",
                "contract_code": contract_code,
                "action": action,
                "reason": reason
            },
            "Signature": "9496EA5EBD92B58D3D217E2F3C2DE1E73993A1CFD871685FDE12EAE03EF6BCD4",
        };
        try {
            let rs =  await api.post(`/OpenApi/v1/mask/insur/status`, params);
            if(rs.Success){
                let labelStt = "";
                if(action === "APPROVE"){
                    labelStt = "Duyệt đơn thành công"
                }else if(action === "CALENDAR_RESET"){
                    labelStt = "Gửi yêu cầu giám định lại thành công"
                }else if(action === "REJECT"){
                    labelStt = "Từ chối đơn thành công"
                }else if(action === "WAIT_CONFIRM"){
                    labelStt = "Gửi duyệt thành công"
                }else if(action === "REJECT_CONFIRM"){
                    labelStt = "Yêu cầu sửa thành công"
                }
                showDialogDis(labelStt);
                props.sdk_params.onClose(data =>{
                    // console.log(data);
                    return data;
                });
            }else{
                showDialogDis(rs.ErrorMessage);
            }
        } catch (e) {
            console.log(e);
        }
    }

    const handlePayment = () =>{
        setStep(2);
        setEditStep(2);
            if (scrollToRef && scrollToRef.current) {
                scrollToRef.current.scrollIntoView({ behavior: "smooth", block: "end", inline: "nearest"});
            }
        setIsPayment(!isPayment);
    }

    const commitImgVcx = async (trans_id) =>{
        // setIsLoadgenQr(true);
        const params = {
            "Action": {
                "ParentCode": "HDI_WEB",
                "UserName": "HDI_WEB",
                "Secret": "B72088C7067CJF9FE0551F6E21B40329",
                "ActionCode": "API73BHAIB"
            },
            "Data": {
                "transhipmentId": trans_id
            },
            "Signature": "034F0DA18BCBDDDB216211594505FB0D8877BB41BE73F6285AECFF7939FA051A",
        }
        try{
            let rs =  await api.post(`/OpenApi/hdi/v1/record/commit`, params);
            if(rs.Success){
                setCheckCommit(true);
                console.log('rs', rs);
            }else{
                showDialogDis(rs.ErrorMessage);
            }
        }catch (e){
            console.log(e);
        }
    }

    const previewGCN = (data) =>{
        if(props.sdk_params.product_code === config.product_code){
            let rs1 = data?.COMPULSORY_CIVIL?.FILE[0]?.FILE_ID;
            window.open(config.urlPath+"/f/"+rs1,"_blank");
        }else{
            let rs2 = data?.PHYSICAL_DAMAGE?.FILE[0]?.FILE_ID;
            window.open(config.urlPath+"/f/"+rs2,"_blank");
        }
    }

    return(
        <div className="form-car">
            {loading && <LoadingForm />}
            {props.sdk_params && props.sdk_params.action === 'EDIT' &&(
                <div className="form-btn-confirm">

                    {/*<div className="btn-next-step" >*/}
                    {/*    <i className="fa fa-check" aria-hidden="true"></i>*/}
                    {/*    Gửi duyệt*/}
                    {/*</div>*/}

                    {vehicleInsur?.PHYSICAL_DAMAGE?.STATUS === config.ord_status_wait && (
                        <>
                            {vehicleInsur?.PHYSICAL_DAMAGE?.CALENDAR_STATUS === config.da_xac_nhan_lh && userPermission?.INSPECTION==="1" && (
                                <div className="btn-next-step btn-reject" onClick={() => showModalQr()}>
                                    {/*<i className="fa fa-check" aria-hidden="true"></i>*/}
                                    Giám định
                                </div>
                            )}
                            {vehicleInsur?.PHYSICAL_DAMAGE.CALENDAR_STATUS === config.da_giam_dinh && userPermission?.APPROVE==="1" && (
                                <>
                                    <div className="btn-next-step btn-confirm" onClick={() => updateStatusOrder(vehicleInsur?.CONTRACT_CODE,"APPROVE","")}>
                                        <i className="fa fa-check" aria-hidden="true"></i>
                                        Duyệt đơn
                                    </div>

                                    <div className="btn-next-step btn-gdl" onClick={() =>showModalCheckOrder("CALENDAR_RESET")}>
                                        <i className="fa fa-redo-alt"></i>
                                        Yêu cầu giám định lại
                                    </div>
                                    <div className="btn-next-step btn-reject" onClick={() =>showModalCheckOrder("REJECT")}>
                                        <i className="fa fa-check" aria-hidden="true"></i>
                                        Từ chối
                                    </div>
                                    <div className="btn-next-step btn-reject" onClick={() =>showModalCheckOrder("REJECT_CONFIRM")}>
                                        <i className="fa fa-check" aria-hidden="true"></i>
                                        Yêu cầu sửa đơn
                                    </div>
                                </>
                            )}
                        </>
                    )}

                    {(vehicleInsur?.PHYSICAL_DAMAGE?.STATUS === config.ord_status_draft || vehicleInsur?.PHYSICAL_DAMAGE?.STATUS === 'REJECT_CONFIRM')
                    && userPermission?.CREATE_ORDER ==="1" && (
                        <div className="btn-next-step btn-confirm" onClick={() =>updateStatusOrder(vehicleInsur?.CONTRACT_CODE,"WAIT_CONFIRM","")}>
                            <i className="fa fa-check" aria-hidden="true"></i>
                            Gửi duyệt
                        </div>
                    )}

                    {(vehicleInsur?.COMPULSORY_CIVIL?.STATUS || vehicleInsur?.PHYSICAL_DAMAGE?.STATUS)  === config.ord_status_paid
                        && userPermission?.CREATE_ORDER ==="1" &&(
                        <div className="btn-next-step btn-preview-gcn" onClick={() => previewGCN(vehicleInsur)}>
                            <i className="fa fa-print" aria-hidden="true"></i>
                            Xem và in GCN
                        </div>
                    )}
                    {/* eslint-disable-next-line no-mixed-operators */}
                    {config.ord_status_tt.includes(vehicleInsur?.COMPULSORY_CIVIL?.STATUS ||  vehicleInsur?.PHYSICAL_DAMAGE?.STATUS) &&
                    vehicleInsur?.COMPULSORY_CIVIL?.IS_SERIAL_NUM !== "1" && userPermission?.CREATE_ORDER ==="1" ? (
                        <div className="btn-next-step btn-payment-tt" onClick={handlePayment}>
                            <i className="fas fa-cash-register"></i>
                            Thanh toán
                        </div>

                    ) : null}

                    {/*{config.ord_status_ed.includes(vehicleInsur?.COMPULSORY_CIVIL?.STATUS ||  vehicleInsur?.PHYSICAL_DAMAGE?.STATUS) && (*/}
                    {/*    <div className="btn-download-document btn-delete-order" onClick={() => alert("Sắp ra mắt")}>*/}
                    {/*        <i className="fa fa-trash" aria-hidden="true"></i>*/}
                    {/*        Xóa đơn*/}
                    {/*    </div>*/}
                    {/*)}*/}

                </div>
            )}

            <div className="form-create-ord">
                <div className="step-vertical-timeline">
                    <div className="header-step-timeline">
                        <label className="title-step-timeline">Thông tin chủ xe</label>
                        {editStep !== 0 && checkEdit && (
                            <label className="edit-step-1" onClick={()=>handleEditStep(0)}>Sửa</label>
                        )}
                    </div>
                    <div className="content-step-timeline">
                        <div className="active-step-1">
                            {step === 0 && editStep === 0 && (
                                <Step1
                                    onNextStep={onNextStep}
                                    listDefineCar={listDefineCar}
                                    vehicleInsur={vehicleInsur}
                                    setBuyer={setBuyer}
                                    setVehicleInsur={setVehicleInsur}/>
                            )}
                            {editStep !== 0 && (
                                summaryStep1()
                            )}
                        </div>
                    </div>
                    <div className="number-step-timeline">1</div>
                </div>
                <div className="step-vertical-timeline">
                    <div className="header-step-timeline">
                        <label className="title-step-timeline">
                            Thông tin xe & gói bảo hiểm
                        </label>
                        {(step === 2 || step === 3) && editStep !== 1 && checkEdit && (
                            <label className="edit-step-1" onClick={()=>handleEditStep(1)}>Sửa</label>
                        )}
                    </div>
                    <div className="content-step-timeline">
                        {step === 1 && editStep === 1  && (
                            <Step2
                                onNextStep={onNextStep}
                                listDefineCar={listDefineCar}
                                vehicleInsur={vehicleInsur}
                                setVehicleInsur={setVehicleInsur}
                                flagFee={flagObjFee}
                                sdk_params={props.sdk_params}
                                checkCommit={checkCommit}
                            />
                        )}
                        {(step === 2 || step === 3) && editStep !==1 && (
                            summaryStep2()
                        )}

                    </div>
                    <div className="number-step-timeline">2</div>
                </div>
                <div className="step-vertical-timeline">
                    <div className="header-step-timeline" ref={scrollToRef}>
                        <label className="title-step-timeline">Thông tin thanh toán</label>
                        { step === 3 && editStep !== 2 && checkEdit && (
                            <label className="edit-step-2" onClick={()=>handleEditStep(2)}>Sửa</label>
                        )}
                    </div>
                    {step === 2 && editStep === 2 && (
                        <div className="content-step-timeline" >
                            {loadingFees && <LoadingForm />}
                            {/*FORM TỔNG PHÍ BẢO HIỂM*/}
                            <div className="form-content">
                                <label className="col-form-label">Tổng phí bảo hiểm (Có VAT)</label>
                            </div>
                            <div className="row form-total-premium">
                                <div className="col-6">
                                    {props.sdk_params.product_code === config.product_code ? (
                                        <>
                                            <p>
                                                Phí bảo hiểm TNDS bắt buộc
                                                <label>{feesTNDSBB ? numberWithCommas(feesTNDSBB) : 0} VNĐ</label>
                                            </p>
                                            <p>
                                                Phí bảo hiểm TNDS tự nguyện vượt quá mức
                                                <label>{feesVQM ? numberWithCommas(feesVQM) : 0} VNĐ</label>
                                            </p>
                                            <p>
                                                Phí bảo hiểm lái, phụ xe, người ngồi trên xe
                                                <label>{feeLPX ? numberWithCommas(feeLPX) : 0} VNĐ</label>
                                            </p>
                                            <p>
                                                Bảo hiểm TNDS của chủ xe đối với hàng hóa
                                                <label>{feeHH ? numberWithCommas(feeHH) : 0} VNĐ</label>
                                            </p>
                                        </>
                                    ) : (
                                        <p>
                                            Phí bảo hiểm vật chất xe
                                            <label>{feesVCX ? numberWithCommas(feesVCX) : 0} VNĐ</label>
                                        </p>
                                    )}
                                    {/*<p>*/}
                                    {/*    Tổng phí bảo hiểm tự nguyện*/}
                                    {/*    <label>{feesTNDSTN ? numberWithCommas(feesTNDSTN) : 0} VNĐ</label>*/}
                                    {/*</p>*/}
                                    {/*<p>*/}

                                    <p>
                                        Giảm phí
                                        <label style={{color: '#DA2128'}}>- {discountTotal ? numberWithCommas(discountTotal) : 0} VNĐ</label>
                                    </p>
                                    <p>
                                        Tổng tiền (đã bao gồm VAT)
                                        <label>{totalAmoutFees ? numberWithCommas(totalAmoutFees) : 0} VNĐ</label>
                                    </p>
                                </div>
                                <div className="col-6">
                                    <div className="form-voucher">
                                        <p>
                                            Mã voucher chỉ áp dụng cho Bảo hiểm TNDS tự nguyện, lái phụ xe, người ngồi trên xe,
                                            hàng hóa và BH vật chất. Không áp dụng cho TNDS bắt buộc
                                        </p>
                                        <div className="row form-group">
                                            <div className="col-9">
                                                <FLInput
                                                    label='Mã voucher'
                                                    value={voucherCode}
                                                    changeEvent={setVoucherCode}
                                                />
                                            </div>
                                            <div className="col-3">
                                                <div className="btn-apply-voucher">
                                                    Áp dụng
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {props.sdk_params.product_code === config.product_code && vehicleInsur?.COMPULSORY_CIVIL?.IS_SERIAL_NUM !== "1" && (
                                <div className="row form-group">
                                    <FormCheck custom type="switch">
                                        <FormCheck.Input checked={isPayment} />
                                        <FormCheck.Label onClick={() => setIsPayment(!isPayment)}>
                                            Thanh toán luôn
                                        </FormCheck.Label>
                                    </FormCheck>
                                </div>
                            )}

                            {isPayment && (
                                <div className="row form-group" >
                                    {/*<div className="col-3">*/}
                                    {/*    <Form.Check*/}
                                    {/*        type="switch"*/}
                                    {/*        id="custom-switch-gcn"*/}
                                    {/*        label="Cấp GCN bản cứng"*/}
                                    {/*        value={toggleGCN}*/}
                                    {/*        onChange={setToggleGCN}*/}
                                    {/*        style={{marginTop: 15}}*/}
                                    {/*    />*/}
                                    {/*</div>*/}
                                    {/*<div className="col-3">*/}
                                    {/*    <FLSelect*/}
                                    {/*        disable={false}*/}
                                    {/*        label="Hình thức nhận GCN "*/}
                                    {/*        value={formalityGCN}*/}
                                    {/*        changeEvent={setFormalityGCN}*/}
                                    {/*        // hideborder={true}*/}
                                    {/*        dropdown={true}*/}
                                    {/*        required={true}*/}
                                    {/*        data={[{value: 'M', label: 'Trực tiếp'}]}*/}
                                    {/*    />*/}
                                    {/*</div>*/}


                                    <div className="col-3">
                                        <FLSelect
                                            disable={false}
                                            label="Hình thức thanh toán"
                                            value={paymentType}
                                            changeEvent={setPaymentType}
                                            // hideborder={true}
                                            dropdown={true}
                                            required={true}
                                            positionSelect={"column"}
                                            data={listDefineCar ? listDefineCar[19] : []}
                                        />
                                    </div>
                                    {config.payment_mix.includes(paymentType) && (
                                        <div className="col-3">
                                            <FLInput
                                                label='Nhập số tiền mặt thanh toán'
                                                value={typingMoney}
                                                changeEvent={setTypingMoney}
                                                required={false}
                                                // error={error.numberPlate}
                                            />
                                        </div>
                                    )}
                                    {config.payment_mix.includes(paymentType) && (
                                        <div className="col-3">
                                            <FLInput
                                                label='Số tiền thanh toán trực tuyến'
                                                value={totalLas}
                                                required={false}
                                                disable={true}
                                                // error={error.numberPlate}
                                            />
                                        </div>
                                    )}
                                </div>
                            )}

                            {/*<div className="row form-group">*/}
                            {/*    <div className="col-4" style={{margin: 'auto 0'}}>*/}
                            {/*        <FormCheck custom type="switch">*/}
                            {/*            <FormCheck.Input  checked={isSMS} />*/}
                            {/*            <FormCheck.Label onClick={onChangeSMS}>*/}
                            {/*                Gửi nội dung chuyển khoản qua SMS*/}
                            {/*            </FormCheck.Label>*/}
                            {/*        </FormCheck>*/}
                            {/*    </div>*/}
                            {/*    <div className="col-4" style={{margin: 'auto 0'}}>*/}
                            {/*        <FormCheck custom type="switch">*/}
                            {/*            <FormCheck.Input  checked={isEmail} />*/}
                            {/*            <FormCheck.Label onClick={onChangeEmail}>*/}
                            {/*                Gửi nội dung chuyển khoản qua Email*/}
                            {/*            </FormCheck.Label>*/}
                            {/*        </FormCheck>*/}
                            {/*    </div>*/}
                            {/*</div>*/}


                            {/*THÔNG TIN THỤ HƯỞNG CHO KHÁCH HANFG MUA BẢO HIỂM VẬT CHẤT */}
                            {/*<div className="form-content">*/}
                            {/*    <label className="col-form-label">*/}
                            {/*        Thông tin thụ hưởng cho khách hàng mua bảo hiểm vật chất xe*/}
                            {/*    </label>*/}
                            {/*</div>*/}

                            {/*<div className="row">*/}
                            {/*    <label className="col-3">Chọn người thụ hưởng <label style={{color: 'red'}}>*</label></label>*/}
                            {/*    <div className="col-9 rd-group">*/}
                            {/*        {listThuHuong ? (*/}
                            {/*            listThuHuong.map((vl, i) =>{*/}
                            {/*                return(*/}
                            {/*                    <label className="form-check">*/}
                            {/*                        <input className="form-check-input" type="radio" value={vl.value}*/}
                            {/*                               onChange={onChangeTypeTH}*/}
                            {/*                               checked={typeNTH === vl.value}/>*/}
                            {/*                        {vl.label}*/}
                            {/*                    </label>*/}
                            {/*                )*/}
                            {/*            })*/}
                            {/*        ) : null}*/}
                            {/*    </div>*/}
                            {/*</div>*/}


                            <div className="form-btn-confirm" >
                                {/*<div className="btn-next-step" >*/}
                                {/*    Lưu và thanh toán*/}
                                {/*</div>*/}

                                {/*<div className="btn-download-document" onClick={() => CreateOrderCar()}>*/}
                                {/*    Lưu*/}
                                {/*</div>*/}
                                {isPayment && (
                                    <div>
                                        {loadingSavePay ? (
                                            <div className="btn-next-step">
                                                <Spinner animation="border" role="status" style={{width: 18, height: 18, marginRight: 8}} />
                                                Vui lòng đợi...
                                            </div>
                                        ) : (
                                            <div className="btn-next-step" onClick={() => CreateOrderCar('PAY')}>
                                                Lưu và thanh toán
                                            </div>
                                        )}
                                    </div>
                                )}

                                {loadingSave ? (
                                    <div className="btn-download-document">
                                        <Spinner animation="border" role="status" style={{width: 18, height: 18, marginRight: 8}} />
                                        Vui lòng đợi...
                                    </div>
                                ): (
                                    <div className="btn-download-document" onClick={() => CreateOrderCar('SAVE')}>
                                        Lưu
                                    </div>
                                )}
                            </div>
                        </div>
                    )}
                    {step === 3 && editStep !==2  && (
                        summaryStep3()
                    )}
                    <div className="number-step-timeline">3</div>
                    {/*MODAL THONG BAO CHUYEN KHOAN*/}
                    <Modal
                        show={showModalCK}
                        onHide={() => setShowModalCK(false)}
                        aria-labelledby="contained-modal-title-vcenter"
                        centered
                    >
                        <Modal.Header closeButton>
                            <Modal.Title className="header-sms">
                                Thông báo
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <label>Quý khách vui lòng Chuyển khoản với nội dung "<label className="sms-ck">{valueSMS}</label>" cho tài khoản:</label>
                            <p>
                                - Chủ tài khoản: CÔNG TY TNHH BẢO HIỂM HD
                            </p>
                            <p>
                                - STK: 168704079888888
                            </p>
                            <p>
                                - Ngân hàng HD Bank chi nhánh Nguyễn Đình Chiểu
                            </p>
                            <div className="btn-save-dkbs" onClick={() => setShowModalCK(false)}>
                                Đóng
                            </div>
                        </Modal.Body>
                    </Modal>

                    <Modal
                        show={showModalReason}
                        onHide={() => setShowModalReason(false)}
                        aria-labelledby="contained-modal-title-vcenter"
                        centered
                    >
                        <Modal.Header closeButton>
                            <Modal.Title className="header-sms">
                                {toTitleCase(labelOrder)}
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <label>{"Lý do "+ labelOrder}</label>

                            <FLInputTextArea
                                placeholder={"Nhập lý do" + labelOrder}
                                value={reason}
                                changeEvent={setReason}
                                // error={''}
                            />
                            <div className="btn-save-dkbs" onClick={() => saveReason(vehicleInsur?.CONTRACT_CODE,isOrder,reason) }>
                                Lưu
                            </div>
                        </Modal.Body>
                    </Modal>


                </div>
            </div>
        </div>
    )
}
export default Main;
