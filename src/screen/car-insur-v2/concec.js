

//Các Trạng thái thanh toán của 1 đơn
//
//     WAIT_PAY: Chờ thanh toán,
//     APPLY: Áp dụng(bỏ),
//     MODIFY: Sửa đổi bổ sung
//     DISCARD: Huỷ bỏ,
//     DRAFT: Đơn nháp,
//     WAIT_CONFIRM: Chờ Duyệt,
//     PAID: Đã thanh toán

export const config = {
    list_define: "list_define",
    urlPath: process.env.REACT_APP_DM1,
    // urlPath: "https://hyperservices.hdinsurance.com.vn",
    product_code: 'XCG_TNDSBB_NEW',
    customer_type: 'CN',
    relationship: 'BAN_THAN',
    check_type_car: ['XE_OTO_CHONGUOI', 'XE_OTO_CHOHANG', 'XE_OTO_NGUOIHANG','XE_OTO_CDUNG'],
    check_weigh: ['XE_OTO_CHOHANG', 'XE_OTO_NGUOIHANG','XE_OTO_CDUNG'],
    check_tt: 'XE_OTO_CHONGUOI',
    xe_may: ['XE_MAY', 'XE_MAYCD'],
    vehicle_group: 'NHOM_XE',
    vehicle_type: 'LOAI_XE',
    hang_xe: 'HANG_XE',
    muc_dich_kd: 'MD_KD',
    hieu_xe: 'HIEU_XE',
    dl_chup: 'DLCA',
    gd_theo_kh: 'GDLKH',
    thiet_hai_than_the_hk : ['XE_OTO_CHONGUOI','XE_OTO_NGUOIHANG'],
    muc_dich_use: 'KD', // KD: Kinh Doanh --- KKD: Khong kinh doanh,
    don_le: '0',
    payment_mix: ['CTT_TM','CT_TM'],
    ord_status_draft: "DRAFT",
    ord_status_ed : ["WAIT_CONFIRM","DRAFT"],
    ord_status_paid: "PAID",
    ord_status_tt: ["WAIT_PAY"],
    ord_status_wait: "WAIT_CONFIRM",
    chua_dat_lh: "CDLH", // Chưa đặt lịch hẹn
    cho_xac_nhan_lh: "CXN", // chờ xác nhận lịch hẹn
    da_xac_nhan_lh: "XNLH", // Đã xác nhận lịch hẹn
    da_giam_dinh: "DGD", //Đã giám định
    giam_dinh_lai: "GDL", //Giam dinh lai
}

export const buyerInfo ={
    "TYPE": "",
    "NAME": "",
    "DOB": "",
    "GENDER": "",
    "ADDRESS": "",
    "IDCARD": "",
    "EMAIL": "",
    "PHONE": "",
    "PROV": "",
    "DIST": "",
    "WARDS": "",
    "IDCARD_D": "",
    "IDCARD_P": "",
    "FAX": "",
    "TAXCODE": ""
}

export const paramsCar = {
    "CONTRACT_CODE" : "",
    "TYPE": "",
    "NAME": "",
    "DOB": "",
    "GENDER": "",
    "PROV": "",
    "DIST": "",
    "WARDS": "",
    "ADDRESS": "",
    "IDCARD": "",
    "IDCARD_D": "",
    "IDCARD_P": "",
    "EMAIL": "",
    "PHONE": "",
    "FAX": "",
    "TAXCODE": "",
    "IS_SPLIT_ORDER": "0", //Tách đơn hay gộp đơn
    "IS_SEND_MAIL": "0", // Gửi GCN qua email
    "VEHICLE_GROUP": "", //Nhóm xe
    "VEHICLE_TYPE": "", //Loại xe
    "VEHICLE_USE": "", //Mục dích sử dụng
    "NONE_NUMBER_PLATE": "",      // 1: chưa có biển số --- 0: có biển số
    "NUMBER_PLATE":"", // bien so xe
    "CHASSIS_NO": "", //Số khung
    "ENGINE_NO": "", // Số máy
    "SEAT_NO": "", //Số chỗ ngồi
    "SEAT_CODE": "", //KEY VALUE SEAT khi filter
    "WEIGH": "", //Trọng tải
    "WEIGH_CODE": "", //KEY VALUE WEIGH khi filter
    "BRAND": "", //Hãng xe
    "MODEL": "", //Hiệu xe
    "CAPACITY": "", //Dung tích
    "REF_VEHICLE_VALUE": "", //Giá trị xe tham khao
    "MFG": "", //Năm sản xuất
    "VEHICLE_REGIS": "", //Ngày đăng ký xe
    "VEHICLE_REGIS_CODE" : "", //Mã ngày sử dụng (tính từ đăng ký xe đến hiện tại)

    "IS_COMPULSORY": "", // Nguoi dung co mua BH TNDS Bat Buoc
    "IS_VOLUNTARY_ALL": "", // Nguoi dung co mua tu nguyen
    "IS_PHYSICAL": "",    // Nguoi dung co mua Vat chat xe

    "IS_VOLUNTARY": "0", //Có mua TNDS TN (người và hàng hóa người t3, hành khách)
    "IS_DRIVER":"0", //Có mua TNDS TN cho LPX, NNTX
    "IS_CARGO": "0", //Có mua TNDS TN cho hàng hóa

    "VEHICLE_VALUE": "",         /// Giá trị xe
    "VEHICLE_VALUE_CODE" : "", //Mã Giá trị xe

    //Trách nhiệm dân sự bắt buộc
    "COMPULSORY_CIVIL": {
        "DETAIL_CODE": "",
        "PRODUCT_CODE": "XCG_TNDSBB",
        "PACK_CODE": "TNDSBB",
        "IS_SERIAL_NUM": "",
        "SERIAL_NUM" : "",
        "EFF": "",
        "TIME_EFF" : "",
        "EXP" : "",
        "TIME_EXP" : "",
        "AMOUNT" : 0,
        "VAT" : 0,
        "DISCOUNT" : 0,
        "DISCOUNT_UNIT" : 0,
        "TOTAL_DISCOUNT" : 0,
        "TOTAL_AMOUNT": 0,
    },

    //Trách nhiệm dân sự tự nguyện
    "VOLUNTARY_CIVIL": {
        "PRODUCT_CODE": "XCG_TNDSTN",
        "PACK_CODE": "TNDSTN",
        "INSUR_3RD": "", //Thiệt hại thân thể người thứ 3
        "INSUR_3RD_ASSETS": "", // Thiệt hại tài sản người thứ 3
        "INSUR_PASSENGER": "", // Thiệt hại thân thể hành khách
        "TOTAL_LIABILITY": "", //Tổng hạn mức trách nhiệm
        "DRIVER_NUM": "", //Số người TG(Bảo hiểm lái, phụ xe và người ngồi trên xe)
        "DRIVER_INSUR": "", //Số tiền BH (Bảo hiểm lái, phụ xe và người ngồi trên xe)
        "WEIGHT_CARGO": "", //Trọng lượng hàng hóa (Bảo hiểm TNDS của chủ xe đối với hàng hóa)
        "CARGO_INSUR": "", // Số tiền bảo hiểm hàng hóa - mức trách nhiệm (Bảo hiểm TNDS của chủ xe đối với hàng hóa)

        "TOTAL_VOLUNTARY_3RD" : "", //Tổng tiền TNDS TN vượt quá mức (thân thể tài sản t3, hành khách)
        "TOTAL_DRIVER" : "", //Tổng tiền LPX,NNTX
        "TOTAL_CARGO": "", //Tổng tiền hàng hóa
        "TOTAL_3RD_INSUR": "", // Tong tien Thiệt hại thân thể người thứ 3
        "TOTAL_3RD_ASSETS":"", // Tong tien Thiệt hại tài sản người thứ 3
        "TOTAL_PASSENGER": "", // Tong tien Thiệt hại thân thể hành khách
    },

    // Bảo hiểm thiệt hại vật chất
    "PHYSICAL_DAMAGE": {
        "DETAIL_CODE": "",
        "PRODUCT_CODE": "XCG_VCX_NEW", //mã sản phẩm (VCX)
        "PACK_CODE": "",
        "INSUR_TOTAL_AMOUNT": "", // Số tiền bảo hiểm
        "INSUR_DEDUCTIBLE": "",         /// Mức miễn thường
        "EFF": "",         // Ngày hiệu lực
        "TIME_EFF": "",    // Gio Hieu luc
        "EXP": "",         // Ngay ket thuc
        "TIME_EXP": "",    //Gio ket thuc
        "IS_VEHICLE_LOSS": "",   //Xe có tổn thất không ?
        "VEHICLE_LOSS_NUM": "",        /// Số năm không tổn thất
        "VEHICLE_LOSS_AMOUNT": "0", // So tien bi ton that
        "IS_DISCOUNT": "",   /// Có giảm phi không
        "DISCOUNT": "",        /// Số giảm phi
        "REASON_DISCOUNT": "", // Ly do giam phi
        "DISCOUNT_UNIT": "",         /// Đơn vị giảm
        "IS_SEND_HD": "",                /// Gửi đến bảo hiểm giám định ---- 1: gửi giảm định => chờ duyệt  -- 0: đơn nháp
        "SURVEYOR_TYPE": "",        /// Hình thức giám định:   /// 1. Cá nhân chụp ảnh (hiện chưa có) -- 2. DL chụp ảnh -- 3. Đến giám định theo lịch KH --4. Gọi điện thoại thống nhất thời gian với KH
        "CALENDAR_EFF": "",  /// Thời gian hẹn bắt đầu
        "CALENDAR_EXP": "",   /// Thời gian kết thúc hẹn
        "CALENDAR_TIME_F": "", /// Thời gian hẹn từ
        "CALENDAR_TIME_T": "", /// Thời gian hẹn đến
        "CALENDAR_CONTACT": "",    /// Thông tin người liên hệ
        "CALENDAR_PHONE": "",          /// SĐT liên hệ
        "CALENDAR_PROV": "",             /// Mã tỉnh địa chỉ hẹn
        "CALENDAR_DIST": "",     /// Mã huyện địa chỉ hẹn
        "CALENDAR_WARDS": "",    /// Mã xã địa chỉ hẹn
        "CALENDAR_ADDRESS": "",        /// Địa chỉ hẹn
        "CALENDAR_ADDRESS_FORM": "",
        "CAL_CODE": "",
        "ADDITIONAL": [],    //danh sách điều khoản bổ sung chọn thêm -- object {key: ""}
        "BENEFICIARY": [],
        "FILE": [],
        "SURVEYOR_FILES": [],
        "TRANS_ID":"",
        // public string FILE_NAME { get; set; }
        // public string FILE_ID { get; set; }
        // public string FILE_TYPE { get; set; }
        // public string FILE_FORMAT { get; set; }
    }
}

export const caculate_fees = [
    {
        "KEY_CODE": "BBPLA_LXE",
        "VAL_CODE": "VH_TYPE"
    },
    {
        "KEY_CODE": "BBPLA_TTAI",
        "VAL_CODE": "VH_TYPE@CAR_WEIGHT"
    },
    {
        "KEY_CODE": "BBPLA_CNGOI",
        "VAL_CODE": "VH_TYPE@CAR_SEAT"
    },
    {
        "KEY_CODE": "BBPLA_MDKD",
        "VAL_CODE": "VH_TYPE@CAR_PURPOSE"
    },
    {
        "KEY_CODE": "BBPLA_MDKD_CNGOI",
        "VAL_CODE": "VH_TYPE@CAR_PURPOSE@CAR_SEAT"
    },
    {
        "KEY_CODE": "HS_T30D",
        "VAL_CODE": "0",
        "TYPE_CODE": "CONSTANT"
    },
    {
        "KEY_CODE": "HS_D30D",
        "VAL_CODE": "0",
        "TYPE_CODE": "CONSTANT"
    },
];

export const caculate_fees_tndstn = [
    {
        "KEY_CODE": "TN_NTX",
        "VAL_CODE": "BH_NTX"
    },
    {
        "KEY_CODE": "TN_TH_HK",
        "VAL_CODE": "VH_TYPE@CAR_PURPOSE@TH_HK"
    },
    {
        "KEY_CODE": "TN_TSN3_TTAI",
        "VAL_CODE": "VH_TYPE@CAR_WEIGHT@TH_TSN3"
    },
    {
        "KEY_CODE": "TN_N3_TTAI",
        "VAL_CODE": "VH_TYPE@CAR_WEIGHT@TH_N3"
    },
    {
        "KEY_CODE": "TN_N3_CNGOI",
        "VAL_CODE": "VH_TYPE@CAR_SEAT@TH_N3"
    },
    {
        "KEY_CODE": "TN_TSN3_CNGOI",
        "VAL_CODE": "VH_TYPE@CAR_SEAT@TH_TSN3"
    },
    {
        "KEY_CODE": "TN_TSN3_LXE",
        "VAL_CODE": "VH_TYPE@TH_TSN3"
    },
    {
        "KEY_CODE": "TN_N3_LXE",
        "VAL_CODE": "VH_TYPE@TH_N3"
    },
    {
        "KEY_CODE": "TN_BH_HH",
        "VAL_CODE": "BH_HH"
    },
    {
        "KEY_CODE": "TN_TTAI_HH",
        "VAL_CODE": "TTAI_TAN",
        "TYPE_CODE": "CONSTANT",
    },
    {
        "KEY_CODE": "TN_TGBH",
        "VAL_CODE": "NUM_OF_DAY",
        "TYPE_CODE": "CONSTANT"
    },
    {
        "KEY_CODE": "TN_CNGOI",
        "VAL_CODE": "CAR_SEAT",
    }
]

export const objTruc = {
    VH_TYPE: 'VH_TYPE',
    CAR_WEIGHT: 'CAR_WEIGHT',
    CAR_SEAT : 'CAR_SEAT',
    CAR_PURPOSE: 'CAR_PURPOSE',
    BH_NTX: 'BH_NTX',
    TH_HK: 'TH_HK',
    TH_TSN3: 'TH_TSN3',
    TH_N3: 'TH_N3',
    BH_HH: 'BH_HH',
    TTAI_TAN: 'TTAI_TAN',
    NUM_OF_DAY: 'NUM_OF_DAY',
    // SO_CNGOI: 'SO_CNGOI',
};

export const caculate_fees_vcx = [
    {
        "KEY_CODE": "STBH",
        "VAL_CODE": "STBH_VCX",
        "TYPE_CODE": "CONSTANT"
    },
    {
        "KEY_CODE": "XCG_PL1_1",
        "VAL_CODE": "VH_TYPE@CAR_WEIGHT@CAR_PRICE@YEAR_OF_USE",
        "TYPE_CODE": "CACHE"
    },
    {
        "KEY_CODE": "XCG_PL1_2",
        "VAL_CODE": "VH_TYPE@CAR_PRICE@YEAR_OF_USE",
        "TYPE_CODE": "CACHE"
    },
    {
        "KEY_CODE": "XCG_TGBH",
        "VAL_CODE": "DAY_OF_VCX",
        "TYPE_CODE": "CONSTANT"
    },
    {
        "KEY_CODE": "XCG_VCX_BS",
        "VAL_CODE": "BOSUNG",
        "TYPE_CODE": "FORMULA"
    },
    {
        "KEY_CODE": "XCGTN_BH_HH",
        "VAL_CODE": "BH_HH",
        "TYPE_CODE": "CACHE"
    },
    {
        "KEY_CODE": "XCGTN_CNGOI",
        "VAL_CODE": "CAR_SEAT",
        "TYPE_CODE": "CACHE"
    },
    {
        "KEY_CODE": "XCGTN_N3_CNGOI",
        "VAL_CODE": "VH_TYPE@CAR_SEAT@TH_N3",
        "TYPE_CODE": "CACHE"
    },
    {
        "KEY_CODE": "XCGTN_N3_LXE",
        "VAL_CODE": "VH_TYPE@TH_N3",
        "TYPE_CODE": "CACHE"
    },
    {
        "KEY_CODE": "XCGTN_N3_TTAI",
        "VAL_CODE": "VH_TYPE@CAR_WEIGHT@TH_N3",
        "TYPE_CODE": "CACHE"
    },
    {
        "KEY_CODE": "XCGTN_NTX",
        "VAL_CODE": "BH_NTX",
        "TYPE_CODE": "CACHE"
    },
    {
        "KEY_CODE": "XCGTN_TH_HK",
        "VAL_CODE": "VH_TYPE@CAR_PURPOSE@TH_HK",
        "TYPE_CODE": "CACHE"
    },
    {
        "KEY_CODE": "XCGTN_TSN3_CNGOI",
        "VAL_CODE": "VH_TYPE@CAR_SEAT@TH_TSN3",
        "TYPE_CODE": "CACHE"
    },
    {
        "KEY_CODE": "XCGTN_TSN3_LXE",
        "VAL_CODE": "VH_TYPE@TH_TSN3",
        "TYPE_CODE": "CACHE"
    },
    {
        "KEY_CODE": "XCGTN_TSN3_TTAI",
        "VAL_CODE": "VH_TYPE@CAR_WEIGHT@TH_TSN3",
        "TYPE_CODE": "CACHE"
    },
    {
        "KEY_CODE": "XCGTN_TTAI_HH",
        "VAL_CODE": "TTAI_TAN",
        "TYPE_CODE": "CONSTANT"
    },
    {
        "KEY_CODE": "BS01_YEAR_OF_USE",
        "VAL_CODE": "BS01/HDI-XCG@YEAR_OF_USE",
        "TYPE_CODE": "CACHE"
    },
    {
        "KEY_CODE": "BS02_YEAR_OF_USE",
        "VAL_CODE": "BS02/HDI-XCG@YEAR_OF_USE",
        "TYPE_CODE": "CACHE"
    }
];

export const caculate_fees_v2 = [
        {
            "KEY_CODE": "BBPLA_CNGOI",
            "VAL_CODE": "VH_TYPE@CAR_SEAT",
            "TYPE_CODE": "CACHE"
        },
        {
            "KEY_CODE": "BBPLA_LXE",
            "VAL_CODE": "VH_TYPE",
            "TYPE_CODE": "CACHE"
        },
        {
            "KEY_CODE": "BBPLA_TTAI",
            "VAL_CODE": "VH_TYPE@CAR_WEIGHT",
            "TYPE_CODE": "CACHE"
        },
        {
            "KEY_CODE": "BBPLA_MDKD",
            "VAL_CODE": "VH_TYPE@CAR_PURPOSE",
            "TYPE_CODE": "CACHE"
        },
        {
            "KEY_CODE": "BBPLA_MDKD_CNGOI",
            "VAL_CODE": "VH_TYPE@CAR_PURPOSE@CAR_SEAT",
            "TYPE_CODE": "CACHE"
        },
        {
            "KEY_CODE": "HS_T30D",
            "VAL_CODE": "K_DAY",
            "TYPE_CODE": "CONSTANT"
        },
        {
            "KEY_CODE": "HS_D30D",
            "VAL_CODE": "K_DAY",
            "TYPE_CODE": "CONSTANT"
        },
        {
            "KEY_CODE": "XCGTN_BH_HH",
            "VAL_CODE": "BH_HH",
            "TYPE_CODE": "CACHE"
        },
        {
            "KEY_CODE": "XCGTN_CNGOI",
            "VAL_CODE": "CAR_SEAT",
            "TYPE_CODE": "CACHE"
        },
        {
            "KEY_CODE": "XCGTN_N3_CNGOI",
            "VAL_CODE": "VH_TYPE@CAR_SEAT@TH_N3",
            "TYPE_CODE": "CACHE"
        },
        {
            "KEY_CODE": "XCGTN_N3_LXE",
            "VAL_CODE": "VH_TYPE@TH_N3",
            "TYPE_CODE": "CACHE"
        },
        {
            "KEY_CODE": "XCGTN_N3_TTAI",
            "VAL_CODE": "VH_TYPE@CAR_WEIGHT@TH_N3",
            "TYPE_CODE": "CACHE"
        },
        {
            "KEY_CODE": "XCGTN_NTX",
            "VAL_CODE": "BH_NTX",
            "TYPE_CODE": "CACHE"
        },
        {
            "KEY_CODE": "XCGTN_TH_HK",
            "VAL_CODE": "VH_TYPE@CAR_PURPOSE@TH_HK",
            "TYPE_CODE": "CACHE"
        },
        {
            "KEY_CODE": "XCGTN_TSN3_CNGOI",
            "VAL_CODE": "VH_TYPE@CAR_SEAT@TH_TSN3",
            "TYPE_CODE": "CACHE"
        },
        {
            "KEY_CODE": "XCGTN_TSN3_LXE",
            "VAL_CODE": "VH_TYPE@TH_TSN3",
            "TYPE_CODE": "CACHE"
        },
        {
            "KEY_CODE": "XCGTN_TSN3_TTAI",
            "VAL_CODE": "VH_TYPE@CAR_WEIGHT@TH_TSN3",
            "TYPE_CODE": "CACHE"
        },
        {
            "KEY_CODE": "XCGTN_TTAI_HH",
            "VAL_CODE": "TTAI_TAN",
            "TYPE_CODE": "CONSTANT"
        },
        {
            "KEY_CODE": "XCG_TGBH",
            "VAL_CODE": "NUM_OF_DAY",
            "TYPE_CODE": "CONSTANT"
        }
    ]


