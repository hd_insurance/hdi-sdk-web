import React, {useEffect, useState} from "react";
import Carousel from "react-multi-carousel";
import Slider from "react-slick";
import {config} from "./concec";
import imgPdf from "../../img_pdf.png";
import {openBase64Pdf} from "../../service/Utils";

const SlideImage = (props) =>{
    const [listImgVcx, setListImgVcx] = useState(props.dataValue || []);

    const [showPreview, setShowPreview] = useState(false);
    const [imgActive, setImgActive] = useState(null);

    useEffect(() => {
        setListImgVcx(props.dataValue);
    },[props.dataValue]);

    const responsive = {
        superLargeDesktop: {
            breakpoint: { max: 4000, min: 3000 },
            items: 5,
        },
        desktop: {
            breakpoint: { max: 3000, min: 1024 },
            items: 3,
        },
        tablet: {
            breakpoint: { max: 1024, min: 464 },
            items: 2,
        },
        mobile: {
            breakpoint: { max: 464, min: 0 },
            items: 1,
        },
    };

    // const showImgChoose = (img) => {
    //     setImgActive(img);
    // }
    const showImage = (e, i) => {
        e.preventDefault();
        setImgActive(i);
        setShowPreview(!showPreview);

    }
    const showImgActive = (i) => {
        setImgActive(i);
    }

    const showImgChoose = (vl) => {
        let url="";
        if(props.checkCommit){
            url = config.urlPath+"/f/"+vl.FILE_ID
        }else{
            url = config.urlPath+"/api/media/cache/photothumbnail/"+vl.TRANS_ID+"/"+vl.FILE_ID
        }
        return (
            <img className="img-fluid" src={url} alt="img upload"/>
        )
    }

    return(
        <React.Fragment>
            {listImgVcx && listImgVcx.length > 0 ? (
                <Carousel
                    prefixCls="cus-tom-carousel"
                    // showDots={true}
                    responsive={responsive}
                    deviceType={"desktop"}
                    itemClass="image-item"
                >
                    {listImgVcx.map((vl, i) => {
                        // let action = props.sdk_params?.action;
                        let url="";
                        if(props.checkCommit){
                            url = config.urlPath+"/f/"+vl.FILE_ID
                        }else{
                            url = config.urlPath+"/api/media/cache/photothumbnail/"+vl.TRANS_ID+"/"+vl.FILE_ID
                        }
                        return (
                            <div key={i} onClick={(e) => showImage(e,i)} className=""  style={{width: 225, height:152, borderRadius: 6, cursor: 'pointer'}}>
                                <img style={{width: "100%", height: "100%", borderRadius: 6}} src={url} alt="img" />
                            </div>
                        );
                    })}
                </Carousel>
            ) : null}

            {showPreview && (
                <div className="preview-image">
                    <div className="item-choose">
                        {
                            showImgChoose(listImgVcx ? listImgVcx[imgActive] : null)
                        }
                    </div>
                    <div className="list-preview-img">
                        {listImgVcx && listImgVcx.map((vl, i) => {
                            let url="";
                            if(props.checkCommit){
                                url = config.urlPath+"/f/"+vl.FILE_ID
                            }else{
                                url = config.urlPath+"/api/media/cache/photothumbnail/"+vl.TRANS_ID+"/"+vl.FILE_ID
                            }
                            return (
                                <div key={i} className={imgActive === i ? "item-preview-active" : "item-preview"}>
                                    <div onClick={() => showImgActive(i)}>
                                        <img className="img-fluid" src={url} alt="img upload"/>
                                    </div>
                                </div>
                            );
                        })}
                    </div>
                    <div className="btn-close-preview-img" onClick={showImage}>
                        <i className="fas fa-times"></i>
                        <label>Thoát</label>
                    </div>
                </div>
            )}
        </React.Fragment>

    )
}

export default SlideImage;