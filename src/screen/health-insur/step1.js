import React, {
  createRef,
  forwardRef,
  useEffect,
  useImperativeHandle,
  useState,
} from "react";
import { Form, Row, Col } from "react-bootstrap";
import FLInput from "../../component/FlInput";
import InfoBuyer from "../component/InfoBuyer";
import InfoCompany from "../component/InfoCompany";
import { checkFormatDate } from "../../service/Utils";
import moment from "moment";

const Main = forwardRef((props, ref) => {
  const formRef = createRef();
  const infoBuyerRef = createRef();
  const infoCompanyRef = createRef();
  const [validateForm, setValidateForm] = useState(false);
  const [typeBeneficiary, setTypeBeneficiary] = useState(
    props?.dataStep1?.BUYER?.TYPE_VAT || "N"
  );
  const [companyInfo, setCompanyInfo] = useState({});

  const [codeSeller, setCodeSeller] = useState(
    props?.dataStep1?.SELLER?.SELLER_CODE || null
  );
  const [nameSeller, setNameSeller] = useState(
    props?.dataStep1?.SELLER?.SELLER_NAME || null
  );
  const [mailSeller, setMailSeller] = useState(
    props?.dataStep1?.SELLER?.SELLER_EMAIL || null
  );
  const [errMes, setErrMes] = useState({});


  useEffect(() =>{
    console.log('sdk_param', props?.sdk_params);
  },[props?.sdk_params])

  useEffect(() => {
    setTypeBeneficiary(props?.dataStep1?.BUYER?.TYPE_VAT || "N");
    if (props?.dataStep1?.BUYER?.TYPE_VAT == "E") {
      // console.log("da vao day", props?.dataStep1?.BILL_INFO);
      setCompanyInfo({
        NAME: props?.dataStep1?.BILL_INFO.NAME,
        PROV: props?.dataStep1?.BILL_INFO.PROV,
        DIST: props?.dataStep1?.BILL_INFO.DIST,
        WARDS: props?.dataStep1?.BILL_INFO.WARDS,
        ADDRESS: props?.dataStep1?.BILL_INFO.ADDRESS,
        ADDRESS_FORM: props?.dataStep1?.BILL_INFO.ADDRESS_FORM,
        EMAIL: props?.dataStep1?.BILL_INFO.EMAIL,
        FAX: "",
        TAXCODE: props?.dataStep1?.BILL_INFO.TAXCODE,
      });
    }
    setCodeSeller(props?.dataStep1?.SELLER?.SELLER_CODE || "");
    setNameSeller(props?.dataStep1?.SELLER?.SELLER_NAME || "");
    setMailSeller(props?.dataStep1?.SELLER?.SELLER_EMAIL || "");
  }, [props?.dataStep1?.BUYER]);

  const checkDOB = (dob) => {
    if (!dob) return true;
    const dobDate = moment(dob, "DD/MM/YYYY");
    const maxDate = moment().subtract(18, "years");
    if (!checkFormatDate(dob)) {
      setErrMes((preMess) => {
        return { ...preMess, dob: "Lỗi định dạng ngày DD/MM/YYYY" };
      });
      return false;
    }
    if (dobDate > maxDate) {
      setErrMes((preMess) => {
        return { ...preMess, dob: "Người mua bảo hiểm phải đủ 18 tuổi" };
      });
      return false;
    }
    setErrMes((preMess) => {
      return { ...preMess, dob: "" };
    });
    return true;
  };

  useImperativeHandle(ref, () => ({
    handleSubmit() {
      const form = formRef.current;
      const buyer = infoBuyerRef?.current?.handleInfoBuyer();
      const company = infoCompanyRef?.current?.handleInfoEnterprise();
      if (form.checkValidity() === false) {
        setValidateForm(true);
        return false;
      }
      //validate DOB
      if (!checkDOB(buyer.DOB)) {
        return false;
      }


      let BILL_INFO = null;
      switch (typeBeneficiary) {
        case "E":
          BILL_INFO = {
            TYPE: "CQ",
            NAME: company.NAME,
            DOB: "",
            GENDER: "",
            PROV: company.PROV,
            DIST: company.DIST,
            WARDS: company.WARDS,
            ADDRESS: company.ADDRESS,
            IDCARD: "",
            EMAIL: company.EMAIL,
            PHONE: "",
            FAX: company.FAX,
            TAXCODE: company.TAXCODE,
          };
          break;
        case "P":
          BILL_INFO = {
            TYPE: "CN",
            NAME: buyer.NAME,
            DOB: buyer.DOB,
            GENDER: buyer.GENDER,
            PROV: buyer.PROV,
            DIST: buyer.DIST,
            WARDS: buyer.WARDS,
            ADDRESS: buyer.ADDRESS,
            IDCARD: buyer.IDCARD,
            EMAIL: buyer.EMAIL,
            PHONE: buyer.PHONE,
            FAX: buyer.FAX,
            TAXCODE: buyer.TAXCODE,
          };
          break;
        default:
          break;
      }
      const SELLER = {
        SELLER_CODE: props?.sdk_params?.org_code === 'HDI' ? props?.sdk_params?.user_name :  codeSeller,
        ORG_CODE: props?.sdk_params?.org_code,
        ORG_TRAFFIC: "",
        TRAFFIC_LINK: "",
        ENVIROMENT: "WEB",
        SELLER_NAME: props?.sdk_params?.org_code === 'HDI' ? props?.sdk_params?.user_name : nameSeller,
        SELLER_EMAIL: mailSeller,
        SELLER_PHONE: "",
        SELLER_GENDER: "",
      };
      return {
        BUYER: {
          CUS_ID: buyer.CUS_ID,
          TYPE: buyer.TYPE,
          NATIONALITY: buyer.NATIONALITY,
          NAME: buyer.NAME,
          DOB: buyer.DOB,
          GENDER: buyer.GENDER,
          PROV: buyer.PROV,
          DIST: buyer.DIST,
          WARDS: buyer.WARDS,
          ADDRESS: buyer.ADDRESS,
          ADDRESS_FORM: buyer.ADDRESS_FORM,
          IDCARD: buyer.IDCARD,
          EMAIL: buyer.EMAIL,
          PHONE: buyer.PHONE,
          FAX: buyer.FAX,
          TAXCODE: buyer.TAXCODE,
          TYPE_VAT: typeBeneficiary,
        },
        BILL_INFO: BILL_INFO,
        SELLER: SELLER,
      };
    },
  }));
  const onChangeBeneficiary = (e) => {
    setTypeBeneficiary(e.target.value);
  };
  return (
    <Form ref={formRef} noValidate validated={validateForm}>
      <InfoBuyer
        ref={infoBuyerRef}
        dataGender={props?.dataDefine ? props?.dataDefine[0] : []}
        vat={typeBeneficiary}
        buyer={props?.dataStep1?.BUYER}
        errorsMess={errMes}
      />
      <Row className="form-choose-type">
        <Col md={3}>
          <p>Có lấy hóa đơn VAT không?</p>
        </Col>
        <Col md={9} className="rd-group">
          {props?.dataDefine[6]
            ? props?.dataDefine[6]?.map((vl, i) => {
                return (
                  <label key={i} className="form-check">
                    <input
                      className="form-check-input"
                      type="radio"
                      value={vl.value}
                      onChange={onChangeBeneficiary}
                      checked={typeBeneficiary == vl.value}
                    />
                    {vl.label}
                  </label>
                );
              })
            : null}
        </Col>
      </Row>
      {typeBeneficiary === "E" && (
        <InfoCompany ref={infoCompanyRef} company={companyInfo} />
      )}
      {props?.sdk_params?.org_code !== 'HDI' && (
          <>
            <div className="form-content">
              <label className="col-form-label">Thông tin người cấp đơn</label>
            </div>
            <Row className="form-group">
              <Col md={4}>
                <FLInput
                    label="Mã cán bộ cấp đơn"
                    value={codeSeller}
                    changeEvent={setCodeSeller}
                    required={true}
                />
              </Col>
              <Col md={4}>
                <FLInput
                    label="Tên cán bộ cấp đơn "
                    value={nameSeller}
                    changeEvent={setNameSeller}
                    required={true}
                />
              </Col>
              <Col md={4}>
                <FLInput
                    label="Mail cán bộ cấp đơn"
                    value={mailSeller}
                    changeEvent={setMailSeller}
                    required={false}
                />
              </Col>
            </Row>
          </>
      ) }

    </Form>
  );
});

export default Main;
