import React, { useEffect, useState, useRef, createRef } from "react";

import Step1 from "./step1";
import Step2 from "./step2";
import Step3 from "./step3";
import Step4 from "./Step4";
// import './style.css';
import Network from "../../service/Network";
import NextWorkNew from "../../service/Network1";
import {
  showDialogDis,
  ModalCK,
  isEmptyObj,
} from "../../service/Utils";
import LoadingForm from "../../component/Loading";
import DataTable from "../../component/DataTable";
import SlideImage from "../../component/UploadFile/slide-image";
import { Col, Form, Row, FormCheck } from "react-bootstrap";
import Upload from "../../component/UploadFile";
import axios from "axios";
import FLSelect from "../../component/FLSelect";
import cogoToast from "cogo-toast";

const api = new Network();

const apiNew = new NextWorkNew();

const listColFile = [
  {
    title: "Giấy yêu cầu BH",
    required: true,
  },
  {
    title: "CMND/ CCCD/ Hộ chiếu",
    required: false,
  },
  {
    title: "Các file khác",
    required: false,
  },
];

const payStatusConfig = {
  PAID: "PAID",
  WAIT_PAY: "WAIT_PAY",
};

const Main = (props) => {
  const F1Ref = createRef();
  const F2Ref = createRef();
  const F3Ref = createRef();
  const formRef = createRef();
  const [validateForm, setValidateForm] = useState(false);

  const [step, setStep] = useState(1);
  const [editStep, setEditStep] = useState(1);

  const [checkEdit, setCheckEdit] = useState(false);

  const [loading, setLoading] = useState(false);

  const [flagUrl, setFlagUrl] = useState("0");

  const [buyer, setBuyer] = useState({});
  const [conpany, setCompany] = useState({});
  const [vat, setVat] = useState("N");

  const [listInsur, setListInsur] = useState([]);

  const [listFile, setListFile] = useState({});

  const [dataDefine, setDataDefine] = useState([]);

  const [disableBtn, setDisableBtn] = useState(false);
  const [totalAmount, setTotalAmount] = useState(0);

  const [isPayment, setIsPayment] = useState(false);
  const [paymentType, setPaymentType] = useState("");

  const [showModalCK, setShowModalCK] = useState(false);
  const [valueSMS, setValueSMS] = useState("");

  const [dataStep1, setDataStep1] = useState({});
  const [dataStep2, setDataStep2] = useState({});
  const [dataStep3, setDataStep3] = useState({});
  const [payStatus, setPayStatus] = useState("");

  const config_define = {
    PRODUCT: props?.sdk_params?.product_code,
    CHANNEL: props?.sdk_params?.channel,
    CATEGORY: props?.sdk_params?.category,
    ACTION: props.sdk_params.action === "ADD" ? "BH_M" : "BH_S",
    USERNAME: props?.sdk_params.user_name,
    ORG_CODE: props?.sdk_params?.org_code,
  };
  const getLabelAddress = (label, address = "") => {
    let provideFormat = address || "";
    if (label) {
      let provideVl = label.split("-");
      provideFormat && (provideFormat += ",");
      provideFormat += provideVl[2] + "," + provideVl[1] + "," + provideVl[0];
    }
    return provideFormat;
  };
  const formatBenefit = (pkinfo) => {
    if (!pkinfo) return {};
    return {
      label: pkinfo.label,
      link: `${pkinfo.URL_BEN}`,
      description: JSON.parse(pkinfo.DESCRIPTION),
    };
  };
  const convertBenefitToString = (des) => {
    let result = "";
    (des?.BENEFITS || []).map((e, i) => {
      let str = `${e.CODE}. ${e.NAME}`;
      if (i != des?.BENEFITS.length) str += "\n";
      result += str;
    });
    return result;
  };

  const getCertificate = async () => {
    setLoading(true);
    try {
      const data = {
        PRODUCT_CODE: config_define.PRODUCT,
        TEMP_CODE: `${config_define.PRODUCT}_VIEW`, // PRODUCT_CODE + "_VIEW"
        PACK_CODE: null, // Goi bao hiem de null
        DATA_TYPE: "JSON",
        JSON_DATA: {
          NAME: dataStep1.BUYER.NAME, // nguoi mua buoc 1
          DOB: dataStep1.BUYER.DOB, // nguoi mua buoc 1
          IDCARD: dataStep1.BUYER.IDCARD, // nguoi mua buoc 1
          IDCARD_DATE: "",
          IDCARD_PLACE: "",
          ADDRESS: getLabelAddress(
            dataStep1.BUYER.ADDRESS_FORM,
            dataStep1.BUYER.ADDRESS
          ), // nguoi mua buoc 1
          PHONE: dataStep1.BUYER.PHONE, // nguoi mua buoc 1
          EMAIL: dataStep1.BUYER.EMAIL, // nguoi mua buoc 1
          A: false, // Mặc định để False
          A1: false,
          B: false,
          B1: false,
          C: false,
          C1: false,
          LOCAL: "........", // Để dấu........
          DATE: "........", // Để dấu........
          MONTH: "........", // Để dấu........
          YEAR: "........", // Để dấu........
          DS_DK: (dataStep2.HEALTH_INSUR || []).map((e, i) => {
            const pkgInfo = dataDefine[2]?.filter(
              (p) => p.value === e.PACK_CODE
            );
            const formatedPack = formatBenefit(pkgInfo[0]);
            return {
              STT: i,
              NAME: e.NAME,
              DOB: e.DOB,
              IDCARD: e.IDCARD,
              ADDRESS: getLabelAddress(e.ADDRESS_FORM, e.ADDRESS),
              EFFECTIVE_DATE: `${e.EFFECTIVE_DATE} - ${e.EXPIRATION_DATE}`,
              BENEFITS: formatedPack?.description[0]?.REGION,
              PACK_NAME: formatedPack?.label,
            };
          }),
        },
      };
      const res = await apiNew.post("/api/view-certificate", data);
      if (res.Success) {
        let link = document.createElement("a");
        link.setAttribute(
          "href",
          "data:application/pdf;base64," + res.Data.FILE_BASE64
        );
        link.setAttribute("download", res.Data.ID_FILE);
        link.click();
        setLoading(false);
        // const pdfWindow = window.open("");
        // pdfWindow?.document?.write(
        //   "<iframe frameborder='0' style='border:0; top:0px; left:0px; bottom:0px; right:0px; position: absolute;' width='100%' height='100%' src='data:application/pdf;base64, " +
        //     encodeURI(data) +
        //     "'></iframe>"
        // );
        // console.log(res);
      }
    } catch (error) {
      setLoading(false);
      cogoToast.error("Có lỗi xảy ra");
      console.log(error);
    }
  };

  useEffect(() => {
    getdefine();
  }, [props?.sdk_params]);

  const calcTotalAmount = (listInsur) => {
    let am = 0;
    for (const e of listInsur) {
      am += e.TOTAL_AMOUNT * 1;
    }
    return am;
  };

  useEffect(() => {
    // console.log("listInsur", dataStep2?.HEALTH_INSUR);
    const am = calcTotalAmount(dataStep2?.HEALTH_INSUR || []);

    setTotalAmount(am);
  }, [dataStep2?.HEALTH_INSUR]);

  const getdefine = async () => {
    const data = {
      org_code: props?.sdk_params?.org_code || 'HDI',
      channel: props?.sdk_params?.channel || null,
      product_code: props?.sdk_params?.product_code || null,
    };
    const response = await apiNew.post("/api/get-define", data);
    // console.log(response);
    setDataDefine(response);
  };


  const filterFile = (files = []) => {
    let list_gyc = [];
    let list_cmnd = [];
    let list_khac = [];
    files.forEach((f) => {
      const type =
        f.FILE_NAME.split(".").pop() == "pdf"
          ? "pdf"
          : f.FILE_NAME.split(".").pop();
      const file = {
        ...f,
        preview: apiNew.domainViewFile + f.FILE_ID,
        type: type,
      };
      if (file.FILE_TYPE == "GYC") {
        list_gyc.push(file);
      }
      if (file.FILE_TYPE == "CMND") {
        list_cmnd.push(file);
      }
      if (file.FILE_TYPE == "KHAC") {
        list_khac.push(file);
      }
    });
    setListFile({
      list_gyc: list_gyc,
      list_cmnd: list_cmnd,
      list_khac: list_khac,
    });
  };
  const getDetailData = async () => {
    setLoading(true);
    try {
      const data = {
        channel: props.sdk_params.channel,
        username: props.sdk_params?.user_name,
        language: "VN",
        category: props.sdk_params?.category,
        product_code: props.sdk_params?.product_code,
        contract_code: props.sdk_params?.contract_code,
        detail_code: props.sdk_params?.detail_code,
        org_code: props.sdk_params?.org_code,
      };
      const response = await apiNew.post("/api/get-detail", data);
      if (response.Success) {
        setLoading(false);
        console.log(response.Data);
        setBuyer(response.Data.BUYER);
        setDataStep1({
          BUYER: response.Data.BUYER,
          BILL_INFO: response.Data.BILL_INFO,
          SELLER: response.Data.SELLER,
        });
        setDataStep2({
          HEALTH_INSUR: response.Data.HEALTH_INSUR,
        });
        filterFile(response?.Data?.FILE);
        setPayStatus(response.Data.STATUS);
      }
    } catch (err) {
      console.log("Loi get detail", err);
    }
  };
  const updateDataStep1 = (BUYER_UPDATE) => {
    setDataStep1((prevState) => {
      return {
        ...prevState,
        BUYER: {
          ...prevState.BUYER,
          ...BUYER_UPDATE,
        },
      };
    });
  };
  useEffect(() => {
    if (props.sdk_params) {
      if (props.sdk_params.action === "ADD") {
        setDataStep1({});
        setBuyer({});
        setDataStep2({});
        setDataStep3({});
        setListFile({});
        setPayStatus("");
        setPaymentType("");
        setStep(0);
        setEditStep(0);
      } else if (props.sdk_params.action === "EDIT") {
        setFlagUrl("0");
        setStep(2);
        setEditStep(2);
        setPaymentType("");
        getDetailData();
        // if(props?.sdk_params?.user_permission){
        //     let parseJs = JSON.parse(props?.sdk_params?.user_permission);
        //     console.log('parseJs', parseJs);
        // }
      }
    }
  }, [props.sdk_params]);

  const submitStep1 = (e) => {
    return F1Ref.current.handleSubmit();
  };
  const submitStep2 = () => {
    return F2Ref.current.handleSubmit();
  };

  // const submitStep3 = () => {
  //   return F3Ref.current.handleSubmit();
  // };

  const onNextStep = async (e) => {
    e.preventDefault();
    switch (step) {
      case 0:
        if (submitStep1()) {
          const data = submitStep1();
          setDataStep1(data);
          console.log("data Step1 =======>", submitStep1());
          // update step 2 voi health issue la BAN_THAN
          if (dataStep2.HEALTH_INSUR) {
            const indexIssue = (dataStep2.HEALTH_INSUR || []).findIndex(
              (e) => e.RELATIONSHIP == "BAN_THAN"
            );
            if (indexIssue > -1) {
              let tmp = dataStep2.HEALTH_INSUR[indexIssue];
              tmp.NAME = data.BUYER.NAME;
              tmp.PHONE = data.BUYER.PHONE;
              tmp.EMAIL = data.BUYER.EMAIL;
              tmp.IDCARD = data.BUYER.IDCARD;
              tmp.DOB = data.BUYER.DOB;
              tmp.ADDRESS = data.BUYER.ADDRESS;
              tmp.ADDRESS_FORM = data.BUYER.ADDRESS_FORM;
              tmp.PROV = data.BUYER.PROV;
              tmp.DIST = data.BUYER.DIST;
              tmp.WARDS = data.BUYER.WARDS;
              dataStep2.HEALTH_INSUR[indexIssue] = tmp;
              setDataStep2(dataStep2);
            }
          }
          setStep(1);
          setEditStep(1);
        }
        break;
      case 1:
        if (submitStep2()) {
          // setListInsur(submitStep2());
          setDataStep2(submitStep2());
          console.log("data Step2 =======>", submitStep2());
          setStep(2);
          setEditStep(2);
        }
        break;
      // case 2:
      //   if (submitStep3()) {
      //     setListFile(submitStep3());
      //     console.log("data Step3 =======>", submitStep3());
      //     setStep(3);
      //     setEditStep(3);
      //   }
      //   break;
      case 2:
        const form = formRef.current;
        if (form.checkValidity() === false) {
          setValidateForm(true);
          return false;
        }
        const result = await createOrder();
        break;
      default:
        break;
    }
  };

  const handleEditStep = (vl) => {
    setStep(vl);
    setEditStep(vl);
  };

  const getLabelRelationShip = (code, gender) => {
    let rs = dataDefine[1]?.filter(
      (vl) => vl.DEFINE_CODE === code && vl.TYPE_CODE === gender
    );
    let label = "";
    if (rs?.length > 0) {
      label = rs[0].TYPE_NAME;
    }
    return label;
  };

  const appendFile = (listFile) => {
    return new Promise((resolve, reject) => {
      let formData = new FormData();
      listFile.forEach((file) => {
        formData.append("files", file.rawFile, file.name);
      });
      resolve(formData);
    });
  };

  const uploadfile = async () => {
    const url = `${process.env.REACT_APP_DM1}/upload/`;
    const config = {
      headers: {
        ParentCode: "HDI_UPLOAD",
        UserName: "HDI_UPLOAD",
        Secret: "HDI_UPLOAD_198282911FASE1239212",
        environment: "LIVE",
        DeviceEnvironment: "WEB",
        ActionCode: "UPLOAD_SIGN",
      },
    };
    // const upListFile = JSON.parse(JSON.stringify(listFile));
    const files = [
      ...listFile.list_gyc,
      ...listFile.list_cmnd,
      ...listFile.list_khac,
    ];
    // console.log("files", files);
    const filesUploaded = files
      .filter((f) => !f.rawFile)
      .map((e) => {
        // map instead of delete property
        const file_extension = e.FILE_NAME.split(".").pop();
        return {
          FILE_TYPE: e.FILE_TYPE,
          FILE_NAME: e.FILE_NAME,
          FILE_ID: e.FILE_ID,
          IS_DEL: "0",
          FILE_EXTENSION: file_extension,
        };
      });
    // console.log(filesUploaded);
    const filesUpload = files.filter((f) => f.rawFile);

    const data = await appendFile(filesUpload);
    try {
      const rs = await axios.post(url, data, config);
      //   console.log(rs);
      const result = (rs?.data?.data || []).map((file) => {
        const file_extension = file?.file_org_name?.split(".").pop();
        const type = file?.file_org_name?.split("_")[0];
        return {
          FILE_TYPE: type,
          FILE_NAME: file.file_org_name,
          FILE_ID: file.file_key,
          IS_DEL: "0",
          FILE_EXTENSION: file_extension,
        };
      });
      // console.log(listFile)
      return [...filesUploaded, ...result];
    } catch (error) {
      console.log(error);
      return [];
    }
  };

  const createOrder = async () => {
    setLoading(true);
    const SELLER = {
      ...dataStep1?.SELLER,
      ORG_CODE: config_define.ORG_CODE || "HDI",
    };
    const PAY_INFO = {
      PAYMENT_TYPE: isPayment ? paymentType : "",
    };
    const BUYER = dataStep1?.BUYER;
    const BILL_INFO = dataStep1?.BILL_INFO;

    const HEALTH_INSUR =
      dataStep2.HEALTH_INSUR ||
      [].map((item) => {
        return {
          CUS_ID: item.CUS_ID,
          TYPE: item.TYPE,
          NATIONALITY: item.NATIONALITY,
          NAME: item.NAME,
          DOB: item.DOB,
          GENDER: item.GENDER,
          PROV: item.PROV,
          DIST: item.DIST,
          WARDS: item.WARDS,
          ADDRESS: item.ADDRESS,
          IDCARD: item.IDCARD,
          EMAIL: item.EMAIL,
          PHONE: item.PHONE,
          FAX: item.FAX,
          TAXCODE: item.TAXCODE,
          RELATIONSHIP: item.RELATIONSHIP,
          PRODUCT_CODE: config_define.PRODUCT,
          PACK_CODE: item.PACK_CODE,
          REGION: item.REGION,
          EFFECTIVE_DATE: item.EFFECTIVE_DATE,
          EXPIRATION_DATE: item.EXPIRATION_DATE,
          FEES: item.FEES,
          AMOUNT: item.amount,
          TOTAL_DISCOUNT: item.TOTAL_DISCOUNT,
          TOTAL_ADD: item.TOTAL_ADD,
          VAT: item.VAT,
          TOTAL_AMOUNT: item.TOTAL_AMOUNT,
        };
      });
    // const resultFiles = await uploadfile();
    // console.log(resultFiles);\

    const data = {
      SELLER: SELLER,
      BUYER: BUYER,
      HEALTH_INSUR: HEALTH_INSUR,
      PAY_INFO: PAY_INFO,
      // FILE: resultFiles.length > 0 ? resultFiles : null,
      FILE: null,
      BILL_INFO: BILL_INFO,
      URL: isPayment
        ? "/OpenApi/v1/mask/insur/create_pay"
        : "/OpenApi/v1/mask/insur/create",
    };

    // console.log("=========== data ===============", data);

    try {
      const rs = await apiNew.post(
        `/api/createOrder/${config_define.ACTION}/${config_define.PRODUCT}/${config_define.CHANNEL}/${config_define.CATEGORY}/${config_define.USERNAME}`,
        data
      );
      console.log(rs);
      if (rs.Success) {
        if (!isPayment) {
          if (props.sdk_params.action === "ADD") {
            showDialogDis("Lưu đơn thành công");
          }
          if (props.sdk_params.action === "EDIT") {
            showDialogDis("Sửa đơn thành công");
          }
        }
        if (rs.Data.url_redirect && rs.Data.url_redirect?.toString()?.trim() !== '') {
          if (paymentType === "CK" && rs?.Data?.[0]?.[0]) {
            setShowModalCK(true);
            setValueSMS(rs.Data[0][0].CONTENT_PAY);
          }
          window.open(
            `${rs.Data.url_redirect}&callback=${window.location.origin}`,
            "_self"
          );
        }
        setLoading(false);
        if (paymentType === "TM" || paymentType === "TH") {
          showDialogDis("Thanh toán thành công");
        }
        props?.sdk_params?.onClose((data) => {
          return data;
        });
        return rs?.Data;
      } else {
        // console.log(listFile)
        setLoading(false);
        showDialogDis(rs.ErrorMessage);
        return false;
      }
      // return false;
    } catch (error) {
      console.log("err create order", error);
      return false;
    }
  };
  const onChangeStatusBtn = (vl) => {
    setDisableBtn(vl);
  };

  const summaryStep1 = (buyer, bill, seller) => {
    if (!buyer) return <></>;
    let provideFormat = getLabelAddress(buyer?.ADDRESS_FORM, buyer?.ADDRESS);
    return (
      <div className="unactive-step-1 typing-value-form">
        <div className="row">
          <div className="col-4">
            <label className="col-form-label">
              {buyer?.GENDER === "M" ? "Ông" : "Bà"}:
            </label>
            <label className="col-form-label">{buyer?.NAME}</label>
          </div>
          <div className="col-4">
            <label className="col-form-label">Số điện thoại:</label>
            <label className="col-form-label">{buyer?.PHONE}</label>
          </div>
          <div className="col-4">
            <label className="col-form-label">Email:</label>
            <label className="col-form-label">{buyer?.EMAIL}</label>
          </div>
          <div className="col-4">
            <label className="col-form-label">CMND/CCCD/Hộ chiếu:</label>
            <label className="col-form-label">{buyer?.IDCARD}</label>
          </div>
          <div className="col-4">
            <label className="col-form-label">Ngày sinh:</label>
            <label className="col-form-label">{buyer?.DOB}</label>
          </div>
        </div>
        <div className="row">
          <div className="col-8">
            <label className="col-form-label">Địa chỉ:</label>
            <label className="col-form-label">{provideFormat}</label>
          </div>
        </div>
        <div className="row">
          <div className="col-4">
            <label className="col-form-label">Lấy hoá đơn cho:</label>
            <label className="col-form-label">
              {buyer?.TYPE_VAT == "N"
                ? "Không lấy"
                : buyer?.TYPE_VAT == "P"
                ? "Cá nhân"
                : "Doanh nghiệp"}
            </label>
          </div>

          {buyer?.TYPE_VAT == "E" && (
            <>
              <div className="col-4">
                <label className="col-form-label">Mã số thuế công ty:</label>
                <label className="col-form-label">{bill?.TAXCODE}</label>
              </div>
              <div className="col-4">
                <label className="col-form-label">Tên công ty:</label>
                <label className="col-form-label">{bill?.NAME}</label>
              </div>
              <div className="col-4">
                <label className="col-form-label">Email doanh nghiệp:</label>
                <label className="col-form-label">{bill?.EMAIL}</label>
              </div>
              <div className="col-4">
                <label className="col-form-label">Địa chỉ doanh nghiệp:</label>
                <label className="col-form-label">{bill?.ADDRESS}</label>
              </div>
            </>
          )}
        </div>
        <div className="row">
          <div className="col-4">
            <label className="col-form-label">Mã cán bộ cấp đơn:</label>
            <label className="col-form-label">{seller?.SELLER_CODE}</label>
          </div>
          <div className="col-4">
            <label className="col-form-label">Tên cán bộ cấp đơn:</label>
            <label className="col-form-label">{seller?.SELLER_NAME}</label>
          </div>
          <div className="col-4">
            <label className="col-form-label">Mail cán bộ cấp đơn:</label>
            <label className="col-form-label">{seller?.SELLER_EMAIL}</label>
          </div>
        </div>
      </div>
    );
  };
  const handlePayment = () => {
    if (formRef?.current) {
      window.scrollTo({
        top: formRef?.current.offsetTop,
        left: 0,
        behavior: "smooth",
      });
    }
  };
  const handleViewCer = (idCer = "") => {
    if (idCer) {
      window.open(`${apiNew.domainViewFile}${idCer}`, "_blank");
    } else {
      const mess = "Chưa có Giấy Chứng Nhận!";
      showDialogDis(mess);
      // cogoToast.error("Chưa có Giấy Chứng Nhận!")
    }
  };
  return (
    <div className="form-car">
      {loading && <LoadingForm />}
      <ModalCK
        showModalCK={showModalCK}
        setShowModalCK={setShowModalCK}
        valueSMS={valueSMS}
      />
      <div className="d-flex">
        {payStatus == payStatusConfig.WAIT_PAY && (
          <div className="btn-next-step btn-payment-tt" onClick={handlePayment}>
            <i className="fas fa-cash-register"></i>
            Thanh toán
          </div>
        )}
        {/* {payStatus == payStatusConfig.PAID && (
          <div
            className="btn-next-step btn-preview-gcn"
          >
            <i className="fa fa-print" aria-hidden="true"></i>
            Xem và in GCN
          </div>
        )} */}
      </div>
      <div className="form-create-ord">
        <div className="step-vertical-timeline">
          <div className="header-step-timeline">
            <label className="title-step-timeline">
              Thông tin người mua bảo hiểm
            </label>
            {editStep !== 0 &&
              !isEmptyObj(dataStep1) &&
              payStatus != payStatusConfig.PAID && (
                <label
                  className="edit-step-1"
                  onClick={() => handleEditStep(0)}
                >
                  Sửa
                </label>
              )}
          </div>
          <div className="content-step-timeline">
            <div className="active-step-1">
              {step === 0 && editStep === 0 && (
                <>
                  <Step1
                    ref={F1Ref}
                    dataDefine={dataDefine}
                    dataStep1={dataStep1}
                    sdk_params={props.sdk_params}
                  />
                  <button
                    className="btn-next-step"
                    onClick={(e) => onNextStep(e)}
                    style={{ margin: 0 }}
                    type="button"
                  >
                    Tiếp tục
                  </button>
                </>
              )}
              {editStep !== 0 &&
                !isEmptyObj(dataStep1) &&
                summaryStep1(
                  dataStep1?.BUYER,
                  dataStep1?.BILL_INFO,
                  dataStep1?.SELLER
                )}
            </div>
          </div>
          <div className="number-step-timeline">1</div>
        </div>
        <div className="step-vertical-timeline">
          <div className="header-step-timeline">
            <label className="title-step-timeline">
              Thông tin người được bảo hiểm
            </label>
            {editStep !== 1 &&
              !isEmptyObj(dataStep2) &&
              payStatus != payStatusConfig.PAID && (
                <label
                  className="edit-step-1"
                  onClick={() => handleEditStep(1)}
                >
                  Sửa
                </label>
              )}
          </div>
          <div className="content-step-timeline">
            {step === 1 && editStep === 1 && (
              <>
                <Step2
                  ref={F2Ref}
                  getLabelRelationShip={getLabelRelationShip}
                  dataDefine={dataDefine}
                  buyer_info={dataStep1?.BUYER}
                  onChangeStatusBtn={onChangeStatusBtn}
                  sdk_params={props.sdk_params}
                  dataStep2={dataStep2}
                  updateDataStep1={updateDataStep1}
                  config_define={config_define}
                />
                <button
                  className="btn-next-step"
                  onClick={(e) => onNextStep(e)}
                  style={{ margin: 0 }}
                  disabled={disableBtn}
                  type="button"
                >
                  Tiếp tục
                </button>
              </>
            )}
            {editStep !== 1 && !isEmptyObj(dataStep2) && (
              <DataTable
                list_insur={
                  dataStep2?.HEALTH_INSUR ? dataStep2?.HEALTH_INSUR : []
                }
                getLabelRelationShip={getLabelRelationShip}
                dataDefine={dataDefine}
                preview={false}
                view_cer={payStatus == payStatusConfig.PAID}
                handleViewCer={handleViewCer}
              />
            )}
          </div>
          <div className="number-step-timeline">2</div>
        </div>
        {/*<div className="step-vertical-timeline">*/}
        {/*  <div className="header-step-timeline">*/}
        {/*    <label className="title-step-timeline">Upload File</label>*/}
        {/*    {editStep !== 2 &&*/}
        {/*      !isEmptyObj(listFile) &&*/}
        {/*      payStatus != payStatusConfig.PAID && (*/}
        {/*        <label*/}
        {/*          className="edit-step-2"*/}
        {/*          onClick={() => handleEditStep(2)}*/}
        {/*        >*/}
        {/*          Sửa*/}
        {/*        </label>*/}
        {/*      )}*/}
        {/*  </div>*/}
        {/*  {step === 2 && editStep === 2 && (*/}
        {/*    <div className="content-step-timeline">*/}
        {/*      <Step3*/}
        {/*        ref={F3Ref}*/}
        {/*        dataDefine={dataDefine}*/}
        {/*        list_file={listFile}*/}
        {/*        sdk_params={props.sdk_params}*/}
        {/*        dataStep3={dataStep3}*/}
        {/*        getCertificate={() => getCertificate()}*/}
        {/*      />*/}
        {/*      <button*/}
        {/*        className="btn-next-step"*/}
        {/*        style={{ margin: 0 }}*/}
        {/*        onClick={(e) => onNextStep(e)}*/}
        {/*        type="button"*/}
        {/*      >*/}
        {/*        Tiếp tục*/}
        {/*      </button>*/}
        {/*    </div>*/}
        {/*  )}*/}
        {/*  {editStep !== 2 && !isEmptyObj(listFile) && (*/}
        {/*    <div className="row" style={{ marginTop: 16 }}>*/}
        {/*      {listColFile &&*/}
        {/*        listColFile.map((vl, i) => {*/}
        {/*          let dataFile = [];*/}
        {/*          switch (i) {*/}
        {/*            case 0:*/}
        {/*              dataFile = listFile.list_gyc;*/}
        {/*              break;*/}
        {/*            case 1:*/}
        {/*              dataFile = listFile.list_cmnd;*/}
        {/*              break;*/}
        {/*            case 2:*/}
        {/*              dataFile = listFile.list_khac;*/}
        {/*              break;*/}
        {/*            default:*/}
        {/*              break;*/}
        {/*          }*/}
        {/*          return (*/}
        {/*            <Col md={4} key={i}>*/}
        {/*              <label>*/}
        {/*                {vl.title}*/}
        {/*                {vl.required ? (*/}
        {/*                  <span style={{ color: "#DA2128" }}>*</span>*/}
        {/*                ) : null}*/}
        {/*              </label>*/}
        {/*              <SlideImage listImg={dataFile} type={"preview"} />*/}
        {/*            </Col>*/}
        {/*          );*/}
        {/*        })}*/}
        {/*    </div>*/}
        {/*  )}*/}
        {/*  <div className="number-step-timeline">3</div>*/}
        {/*</div>*/}
        <div className="step-vertical-timeline">
          <div className="header-step-timeline">
            <label className="title-step-timeline">Thanh toán</label>
            {step === 3 && editStep !== 2 && payStatus != payStatusConfig.PAID && (
              <label className="edit-step-2" onClick={() => handleEditStep(2)}>
                Sửa
              </label>
            )}
          </div>
          {step === 2 && editStep === 2 && (
            <div className="content-step-timeline">
              <Step4
                // ref={F3Ref}
                dataDefine={dataDefine}
                totalAmount={totalAmount}
                sdk_params={props.sdk_params}
              />
              {payStatus != payStatusConfig.PAID && (
                <Form ref={formRef} noValidate validated={validateForm}>
                  <div className="row form-group">
                    <FormCheck custom type="switch">
                      <FormCheck.Input
                        checked={isPayment}
                        onChange={() => setIsPayment(!isPayment)}
                      />
                      <FormCheck.Label onClick={() => setIsPayment(!isPayment)}>
                        Thanh toán luôn
                      </FormCheck.Label>
                    </FormCheck>
                  </div>

                  {isPayment && (
                    <div className="row form-group">
                      <div className="col-3">
                        <FLSelect
                          disable={false}
                          label="Hình thức thanh toán"
                          value={paymentType}
                          changeEvent={setPaymentType}
                          dropdown={true}
                          required={true}
                          positionSelect={"column"}
                          data={dataDefine ? dataDefine[7] : []}
                        />
                      </div>
                    </div>
                  )}
                  <button
                    className="btn-next-step"
                    onClick={(e) => onNextStep(e)}
                    type="button"
                    style={{ margin: 0 }}
                  >
                    {isPayment ? "Lưu và thanh toán" : "Lưu"}
                  </button>
                </Form>
              )}
            </div>
          )}

          {/*{step === 4 && editStep !== 3  && (*/}
          {/*    <label>sum 4</label>*/}
          {/*)}*/}
          <div className="number-step-timeline">3</div>
        </div>
      </div>
    </div>
  );
};
export default Main;
