import React, {
  createRef,
  forwardRef,
  useEffect,
  useImperativeHandle,
  useState,
} from "react";
import { Form, Table, Col, Row, Modal, Button } from "react-bootstrap";
import Popover from "react-popover";

import moment from "moment";
import FLInput from "../../component/FlInput";
import FLSelect from "../../component/FLSelect/index";
import FLAddress from "../../component/FlAddress/index";
import RelationSelect from "../../component/FLMutilSelect/index";
import { utils } from "react-modern-calendar-datepicker";
import {
  checkFormatDate,
  selectDate,
  convertRelation,
  numberFormatCommas,
} from "../../service/Utils";

import FLInputDate from "../../component/FLInputDate";
import cogoToast from "cogo-toast";
import NextWorkNew from "../../service/Network1";
import LoadingForm from "../../component/Loading";
import DataTable from "../../component/DataTable";
import {config} from "../car-insur-v2/concec";

const apiNew = new NextWorkNew();

const Main = forwardRef((props, ref) => {
  const formRefModal = createRef();

  const [validateForm, setValidateForm] = useState(false);

  const [show, setShow] = useState(false);

  const [relationShip, setRelationShip] = useState(null);
  const [packageCode, setPackageCode] = useState(null);
  const [packageFormat, setPackageFormat] = useState(null);

  const [openBenefit, setOpenBenefit] = useState(false);
  const [benefit, setBenefit] = useState("");
  // const [detailBenefit, setDetailBenefit] = useState(false);

  // const [gender, setGender] = useState( null);
  const [name, setName] = useState("");
  const [phone, setPhone] = useState("");
  const [email, setEmail] = useState("");
  const [idCard, setIdCard] = useState("");
  const [dob, setDob] = useState("");
  const [address, setAddress] = useState("");
  const [addressCode, setAddressCode] = useState({});
  const [eff, setEff] = useState(moment().add(5, "days").format("DD/MM/YYYY"));
  const [exp, setExp] = useState(
    moment().format("DD/MM/YYYY")
  );
  const [feePack, setFeePack] = useState(0);
  const [loadingFee, setLoadingFee] = useState(false);
  const [listInsur, setListInsur] = useState(
    props?.dataStep2?.HEALTH_INSUR ? props?.dataStep2?.HEALTH_INSUR : []
  );
  const [labelBtn, setLabelBtn] = useState(null);
  const [idxList, setIdxList] = useState(null)

  const [lockOut1, setLockOut1] = useState(false);
  const [lockOut2, setLockOut2] = useState(false);
  const [labelLockOut1, setLabelLockOut1] = useState(false);
  const [labelLockOut2, setLabelLockOut2] = useState(false);

  const [disableInput, setDisableInput] = useState(false);
  const [errorsMess, setErrorsMess] = useState({});

  const maximumDate = () => {
    let tdate = moment(eff, "DD/MM/YYYY").subtract(18, "years");
    if (props.config_define.CATEGORY == "CN.03") {
      // tai nan
      tdate = moment(eff, "DD/MM/YYYY").subtract(18, "years");
    }
    if (props.config_define.CATEGORY == "CN.02") {
      // suc khoe
      tdate = moment(eff, "DD/MM/YYYY").subtract(15, "days");
    }

    var now = moment();
    if (now >= tdate) {
      return {
        year: tdate.year(),
        month: tdate.month() + 1,
        day: tdate.date(),
      };
    } else {
      return {
        year: now.year(),
        month: now.month() + 1,
        day: now.date(),
      };
    }
  };
  const minimumDate = () => {
    const tdate = moment(eff, "DD/MM/YYYY");
    return {
      year: tdate.year() - 65,
      month: tdate.month() + 1,
      day: tdate.date(),
    };
  };
  const defaultDate = () => {
    let tdate = moment(eff, "DD/MM/YYYY").subtract(18, "years");
    if (props.config_define.CATEGORY == "CN.03") {
      // tai nan
      tdate = moment(eff, "DD/MM/YYYY").subtract(18, "years");
    }
    if (props.config_define.CATEGORY == "CN.02") {
      // suc khoe
      tdate = moment(eff, "DD/MM/YYYY").subtract(15, "days");
    }
    return {
      year: tdate.year(),
      month: tdate.month() + 1,
      day: tdate.date(),
    };
  };

  const defaultEff = () => {
    const tdate = moment().add(1, "days");
    return {
      year: tdate.year(),
      month: tdate.month() + 1,
      day: tdate.date(),
    };
  };

  useEffect(() =>{
    props?.onChangeStatusBtn(true);
  },[])

  useEffect(() => {
    if(props.dataDefine[2]){
      setPackageFormat(props.dataDefine[2]);
    }
    if(props.dataDefine[9]){
      let lbl = props.dataDefine[9][0];
      let lbl1 = props.dataDefine[9][1];
      setLabelLockOut1(lbl.label);
      setLabelLockOut2(lbl1.label);
    }
  }, [props.dataDefine]);

  // useEffect(() => {
  //   props?.onChangeStatusBtn(listInsur.length > 0 ? false : true);
  // }, [listInsur]);

  useEffect(() => {
    if(lockOut1 && lockOut2 && listInsur.length > 0){
      props?.onChangeStatusBtn(false);
    }else{
      props?.onChangeStatusBtn(true);
    }
  }, [lockOut1, lockOut2, listInsur]);

  useEffect(() => {
    if (packageCode && dob && relationShip && checkFormatDate(eff)) {
      setLoadingFee(true);
      setTimeout(() => {
        handleCalcFee();
      }, 500);
    }
  }, [packageCode, dob, relationShip, eff]);

  const onChangePackage = (data) => {
    setPackageCode(data);
    let iPack = packageFormat.findIndex((e) => e.value === data);
    let pkg = formatBenefit(props.dataDefine[2][iPack]);
    if (pkg) {
      setBenefit(pkg.description);
    }
  };
  const handleClose = () => {
    setShow(false);
    setErrorsMess({});
  };

  const handleShow = (action, index) => {
    setValidateForm(false);
    if (action === "ADD") {
      setLabelBtn("Thêm");
      setIdxList(null);
      setRelationShip(null);
      setName(null);
      setPhone(null);
      setEmail(null);
      setIdCard(null);
      setDob("");
      setAddress(null);
      setAddressCode({});
      setPackageCode(null);
      setBenefit("");
      setEff(moment().add(5, "days").format("DD/MM/YYYY"));
      setExp(moment().add(5, "days").add(1, "years").format("DD/MM/YYYY"));
      setFeePack(0);
    } else {
      let item = listInsur[index];
      // console.log(item);
      setLabelBtn("Sửa");
      setIdxList(index);
      setRelationShip({
        title:
          item.RELATIONSHIP === "BAN_THAN"
            ? "Bản thân"
            : props?.getLabelRelationShip(item.RELATIONSHIP, item.GENDER),
        value: item.RELATIONSHIP,
        gender: item.GENDER,
      });
      setName(item.NAME);
      setPhone(item.PHONE);
      setEmail(item.EMAIL);
      setIdCard(item.IDCARD);
      setDob(item.DOB);
      setAddress(item.ADDRESS);
      setAddressCode({
        label: item.ADDRESS_FORM,
        prov: item.PROV,
        dist: item.DIST,
        ward: item.WARDS,
      });
      // setPackageCode(item.PACK_CODE);
      onChangePackage(item.PACK_CODE);
      setEff(item.EFFECTIVE_DATE);
      setFeePack(item.TOTAL_AMOUNT);
    }
    setShow(true);
  };

  useImperativeHandle(ref, () => ({
    handleSubmit() {
      return { HEALTH_INSUR: listInsur };
    },
  }));

  const checkDOB = (dobValue, effValue) => {
    // validate khi typing
    const dobDate = moment(dobValue, "DD/MM/YYYY");
    let maxDate = moment(effValue, "DD/MM/YYYY");
    let minDate = moment(effValue, "DD/MM/YYYY").subtract(65, "years");
    let mess = "";
    if (!checkFormatDate(dobValue)) {
      setErrorsMess((preMess) => {
        return { ...preMess, dob: "Lỗi định dạng ngày DD/MM/YYYY" };
      });
      return false;
    }
    if (props.config_define.CATEGORY == "CN.03") {
      // tai nan
      maxDate = maxDate.subtract(18, "years");
      mess = "Người được bảo hiểm phải từ 18 đến 65 tuổi";
    }
    if (props.config_define.CATEGORY == "CN.02") {
      // suc khoe
      maxDate = maxDate.subtract(15, "days");
      mess = "Người được bảo hiểm phải từ 15 ngày đến 65 tuổi";
    }
    if (dobDate > maxDate) {
      setErrorsMess((preMess) => {
        return { ...preMess, dob: mess };
      });
      return false;
    }
    if (dobDate < minDate) {
      setErrorsMess((preMess) => {
        return {
          ...preMess,
          dob: "Người được bảo hiểm không vượt quá 65 tuổi",
        };
      });
      return false;
    }
    setErrorsMess((preMess) => {
      return { ...preMess, dob: "" };
    });
    return true;
  };
  const checkEff = (effValue) => {
    // validate khi typing
    const minDate = moment();
    const effDate = moment(effValue, "DD/MM/YYYY");
    if (!checkFormatDate(effValue)) {
      setErrorsMess((preMess) => {
        return { ...preMess, eff: "Lỗi định dạng ngày DD/MM/YYYY" };
      });
      return false;
    }
    if (effDate < minDate) {
      setErrorsMess((preMess) => {
        return {
          ...preMess,
          eff: "Ngày hiệu lực phải lớn hơn ngày hiện tại",
        };
      });
      return false;
    }
    setErrorsMess((preMess) => {
      return { ...preMess, eff: "" };
    });
    return true;
  };

  const addPersonInsured = () => {
    const form = formRefModal.current;

    if (form.checkValidity() === false) {
      setValidateForm(true);
      return false;
    }

    // validate DOb, EFF
    if (!checkDOB(dob, eff) || !checkEff(eff)) {
      return false;
    }

    if (typeof listInsur[idxList] === "undefined") {
      const item_insur = {
        CUS_ID: "",
        TYPE: "CN",
        NATIONALITY: "",
        NAME: name,
        DOB: dob,
        GENDER:
          relationShip?.type_code === "BAN_THAN"
            ? props?.buyer_info?.GENDER
            : relationShip?.gender,
        PROV: addressCode?.prov,
        DIST: addressCode?.dist,
        WARDS: addressCode?.ward,
        ADDRESS: address,
        ADDRESS_FORM: addressCode?.label,
        IDCARD: idCard,
        EMAIL: email,
        PHONE: phone,
        FAX: "",
        TAXCODE: "",
        RELATIONSHIP: relationShip.value,
        PRODUCT_CODE: props?.sdk_params?.product_code,
        PACK_CODE: packageCode,
        REGION: "VN",
        EFFECTIVE_DATE: eff,
        EXPIRATION_DATE: exp, // mac dinh + 1 nam
        FEES: 0, // dang chua co fees
        AMOUNT: 0,
        TOTAL_DISCOUNT: 0,
        TOTAL_ADD: 0,
        VAT: 0,
        TOTAL_AMOUNT: feePack,
      };
      setListInsur([...listInsur, item_insur]);
    } else {
      const temp = [...listInsur];
      temp[idxList].NAME = name;
      temp[idxList].DOB = dob;
      temp[idxList].GENDER =
        relationShip?.type_code === "BAN_THAN"
          ? props?.buyer_info?.GENDER
          : relationShip?.gender;
      temp[idxList].PROV = addressCode.prov;
      temp[idxList].DIST = addressCode.dist;
      temp[idxList].WARDS = addressCode.ward;
      temp[idxList].ADDRESS = address;
      temp[idxList].ADDRESS_FORM = addressCode.label;
      temp[idxList].IDCARD = idCard;
      temp[idxList].EMAIL = email;
      temp[idxList].PHONE = phone;
      temp[idxList].RELATIONSHIP = relationShip.value;
      temp[idxList].PACK_CODE = packageCode;
      temp[idxList].EFFECTIVE_DATE = eff;
      temp[idxList].EXPIRATION_DATE = exp;
      temp[idxList].TOTAL_AMOUNT = feePack;
      setListInsur(temp);
    }
    // update thong tin step 1
    if (relationShip.value == "BAN_THAN") {
      props.updateDataStep1({
        NAME: name,
        DOB: dob,
        PROV: addressCode.prov,
        DIST: addressCode.dist,
        WARDS: addressCode.ward,
        ADDRESS: address,
        ADDRESS_FORM: addressCode.label,
        IDCARD: idCard,
        EMAIL: email,
        PHONE: phone,
      });
    }
    handleClose();
  };

  const actionopenBenefit = () => {
    if (packageCode) {
      setOpenBenefit(true);
    } else {
      cogoToast.error("Vui lòng chọn gói bảo hiểm.");
    }
  };

  const popoverBenefit = {
    isOpen: openBenefit,
    place: "below",
    preferPlace: "right",
    className: "flselect-cus",
    onOuterAction: () => setOpenBenefit(false),
    body: [
      <div className={"list_benefit_relative"}>
        <div className="h">{benefit ? benefit[0]?.PACK_NAME : null}</div>
        <div className="list_xxx">
          {(benefit ? benefit[0]?.BENEFITS : []).map((item, index) => {
            return (
              <div className="ite" key={index}>
                {item.NAME}
              </div>
            );
          })}
          {/*<Button*/}
          {/*    className='view_detail_benefit'*/}
          {/*    onClick={(e) => console.log(1)}*/}
          {/*>*/}
          {/*    Xem Chi tiet*/}
          {/*</Button>*/}
        </div>
      </div>,
    ],
  };

  useEffect(() => {
    if (checkFormatDate(eff)) {
      let rs = moment().add(5, "days").add(1, "years").format("DD/MM/YYYY");
      setExp(rs);
    }
  }, [eff]);

  const handleCalcFee = async () => {
    let data = {
      a1: props.sdk_params.org_code,
      a2: props.sdk_params.product_code,
      a3: packageCode,
      a4: dob,
      a5: relationShip.gender,
      a6: eff,
    };
    try {
      setLoadingFee(true);
      const response = await apiNew.post("/api/bhsk/calc/fee", data);
      if (response) {
        setLoadingFee(false);
        if (response?.[0]?.[0]?.MESSAGE) {
          setFeePack(response[0][0].MESSAGE);
        }
      }
    } catch (err) {
      console.log(err);
    }
  };

  const removeItemInsur = (idx) => {
    const temp = [...listInsur];
    temp.splice(idx, 1);
    setListInsur(temp);
  };

  const formatBenefit = (pkinfo) => {
    return {
      label: pkinfo.label,
      link: `${pkinfo.URL_BEN}`,
      description: JSON.parse(pkinfo.DESCRIPTION),
    };
  };

  const selectRelation = (item) => {
    setRelationShip(item);
    if (item?.value === "BAN_THAN") {
      setDisableInput(false);
      setName(props?.buyer_info?.NAME);
      setPhone(props?.buyer_info?.PHONE);
      setEmail(props?.buyer_info?.EMAIL);
      setIdCard(props?.buyer_info?.IDCARD);
      setDob(props?.buyer_info?.DOB);
      setAddress(props?.buyer_info?.ADDRESS);
      setAddressCode({
        label: props?.buyer_info?.ADDRESS_FORM,
        prov: props?.buyer_info?.PROV,
        dist: props?.buyer_info?.DIST,
        ward: props?.buyer_info?.WARDS,
      });
    }
    // else {
    //   setDisableInput(false);
    //   setName("");
    //   setPhone("");
    //   setEmail("");
    //   setIdCard("");
    //   setDob("");
    //   setAddress("");
    //   setAddressCode({
    //     label: "",
    //     prov: "",
    //     dist: "",
    //     ward: "",
    //   });
    // }
  };

  return (
    <div>
      <div className="btn-download-document" onClick={() => handleShow("ADD")} style={{marginLeft: 0, marginBottom: 15}}>
        <i className="fas fa-plus" style={{ fontSize: 12, marginRight: 5 }} />
        Thêm người được bảo hiểm
      </div>

        <DataTable
          list_insur={listInsur}
          showModal={handleShow}
          removeItemInsur={removeItemInsur}
          getLabelRelationShip={props?.getLabelRelationShip}
          dataDefine={props?.dataDefine}
          preview={true}
        />

      <div className="form-group form-group-cus-check">
        <Form.Check type={'checkbox'} id={`cus-checkbox-tnds`} >
          <Form.Check.Input
              type={'checkbox'}
              value={lockOut1}
              checked={lockOut1}
              onChange={() =>setLockOut1(!lockOut1)}
          />
          <div dangerouslySetInnerHTML={{__html: labelLockOut1}} className='label-lockout'>
          </div>
        </Form.Check>
      </div>

      <div className="form-group form-group-cus-check">
        <Form.Check type={'checkbox'} id={`cus-checkbox-tnds`} >
          <Form.Check.Input
              type={'checkbox'}
              value={lockOut2}
              checked={lockOut2}
              onChange={() =>setLockOut2(!lockOut2)}
          />
          <div dangerouslySetInnerHTML={{__html: labelLockOut2}} className='label-lockout'>
          </div>

        </Form.Check>
      </div>

      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
        size="lg"
        enforceFocus={false}
      >
        <Modal.Header closeButton>
          <label>Thêm người được bảo hiểm</label>
        </Modal.Header>
        <Modal.Body>
          {loadingFee && <LoadingForm style={{ margin: -16 }} />}
          <Form
            id="create-course-form"
            ref={formRefModal}
            noValidate
            validated={validateForm}
            // onSubmit={(e) => addPersonInsured(e)}
          >
            <Row className={"form-group"}>
              <Col md={4}>
                <RelationSelect
                  // description={'313133'}
                  label="Quan hệ người mua"
                  value={relationShip}
                  changeEvent={selectRelation}
                  data={convertRelation(
                    props.dataDefine[1] ? props.dataDefine[1] : []
                  )}
                  required={true}
                  // component_obj={item}
                  // isDisable={disableInput}
                />
              </Col>
              <Col md={4}>
                <FLInput
                  label="Tên người được bảo hiểm"
                  value={name}
                  changeEvent={setName}
                  required={true}
                  disable={disableInput}
                />
              </Col>
              <Col md={4}>
                <FLInput
                  label="Số điện thoại"
                  value={phone}
                  changeEvent={setPhone}
                  required={true}
                  disable={disableInput}
                  pattern={"[0-9]{10,11}"}
                />
              </Col>
            </Row>

            <Row className={"form-group"}>
              <Col md={4}>
                <FLInput
                  label="Email"
                  value={email}
                  changeEvent={setEmail}
                  required={true}
                  disable={disableInput}
                  pattern={"[a-zA-Z0-9._%+-]{3,}@[a-z0-9.-]+\\.[a-z]{2,}$"}
                />
              </Col>
              <Col md={4}>
                <FLInput
                  label="CMND/ CCCD/ Hộ chiếu"
                  value={idCard}
                  changeEvent={setIdCard}
                  required={true}
                  disable={disableInput}
                  pattern={"[a-zA-Z0-9]{7,13}"}
                />
              </Col>
              <Col md={4}>
                <FLInputDate
                  disable={false}
                  value={dob}
                  changeEvent={setDob}
                  selectedDay={defaultDate()}
                  label={"Ngày sinh"}
                  required={true}
                  error={errorsMess.dob}
                  disable={disableInput}
                  minimumDate={minimumDate()}
                  maximumDate={maximumDate()}
                />
              </Col>
            </Row>

            <Row className={"form-group"}>
              <Col md={6}>
                <FLInput
                  label="Số nhà, ngõ / ngách, đường"
                  value={address}
                  changeEvent={setAddress}
                  required={true}
                  disable={disableInput}
                />
              </Col>
              <Col md={6}>
                <FLAddress
                  disable={disableInput}
                  label={"Tỉnh/TP - Quận/huyện - Phường/ xã"}
                  value={addressCode}
                  changeEvent={setAddressCode}
                  required={true}
                />
              </Col>
            </Row>

            <Row className={"form-group"}>
              <Col md={8}>
                <div className="group_ipnv">
                  <div style={{ width: "50%" }}>
                    <FLSelect
                      // disable={registeredChecked}
                      hideborder={true}
                      label="Gói bảo hiểm"
                      value={packageCode}
                      changeEvent={onChangePackage}
                      dropdown={true}
                      required={true}
                      data={packageFormat ? packageFormat : []}
                    />
                  </div>
                  <div className="ver-line" />
                  <div className="kkjs">
                    <Popover {...popoverBenefit}>
                      <div
                        className="btn-download-document view_ql"
                        style={{ marginLeft: 0 }}
                        onClick={(e) => {
                          actionopenBenefit();
                        }}
                      >
                        <i className="fas fa-shield-alt" /> Xem quyền lợi
                      </div>
                    </Popover>
                  </div>
                </div>
              </Col>
            </Row>
            <Row className={"form-group"}>
              <Col md={4}>
                <FLInputDate
                  disable={true}
                  value={eff}
                  changeEvent={setEff}
                  selectedDay={selectDate(eff)}
                  minimumDate={defaultEff()}
                  label={"Ngày hiệu lực"}
                  required={true}
                  error={errorsMess.eff}
                />
              </Col>
              <Col md={4}>
                <FLInputDate
                  disable={true}
                  value={exp}
                  changeEvent={setExp}
                  selectedDay={selectDate(exp)}
                  label={"Ngày kết thúc"}
                  required={true}
                />
              </Col>
              <Col md={4}>
                <FLInput
                  label="Phí"
                  value={numberFormatCommas(feePack?.toString()) + " VNĐ"}
                  changeEvent={setFeePack}
                  required={false}
                  disable={true}
                />
              </Col>
            </Row>
            <div className="custom-footer-modal">
              <button
                className="btn-custom-sdk btn-add-ndbh"
                type="button"
                onClick={addPersonInsured}
              >
                {labelBtn}
              </button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>
    </div>
  );
});

export default Main;
