// import moment from 'moment';
import React from "react";
import { confirmAlert } from "react-confirm-alert";
import moment from "moment";
import Network from "./Network";
import { Modal } from "react-bootstrap";
import { useState, useEffect } from 'react';

const api = new Network();

const pdf2base64 = require("pdf-to-base64");

export const setOptionSelect = (listSelect) => {
  return (
    listSelect.length > 0 &&
    listSelect.map((vl) => ({
      value: vl.TYPE_CODE,
      label: vl.TYPE_NAME,
      discount_max: vl.DISCOUNT_MAX ? vl.DISCOUNT_MAX : null,
      discount_unit: vl.DISCOUNT_UNIT ? vl.DISCOUNT_UNIT : null,
    }))
  );
};

export const formatTimeByDay = (value, effDay) => {
  //format HH:mm to Date GMT
  let d = "";
  if (value && effDay) {
    let rs = moment(effDay, "DD/MM/YYYY");
    let rs1 = rs.format("MM/DD/YYYY");
    d = new Date(rs1);

    let [hours, minutes] = value.split(":");
    d.setHours(+hours);
    d.setMinutes(minutes);
    // setStartDate(d.toString());
  }
  // console.log('dklmmm', d.toString());
  return d.toString();
};

export const selectDate = (date) => {
  if (!date) return null;
  const rs1 = moment(date, "DD/MM/YYYY").format("YYYY-MM-DD");
  return {
    year: moment(rs1).year(),
    month: moment(rs1).month() + 1,
    day: moment(rs1).date(),
  };
};

export const checkFormatDate = (date) => {
  //check format dd/mm/yyyy
  if (date) {
    return moment(date, "DD/MM/YYYY", true).isValid();
  }
};

export const convertDate = (date) => {
  //Convert dd/mm/yyy to date
  let rs = moment(date, "DD/MM/YYYY").format("YYYY-MM-DD");
  return moment(rs).toDate();
};

export const convertDate1 = (date) => {
  //Convert Date to dd/mm/yyyy
  let rs = moment(date).format("DD/MM/YYYY");
  return rs;
};

export const compareDate = (startDate, endDate) => {
  // So sanh 2 ngay
  let a = moment(startDate, "DD/MM/YYYY");
  let b = moment(endDate, "DD/MM/YYYY");
  return a < b;
};

export const calculateDate = (startDate, endDate) => {
  let rs1 = startDate ? startDate.split("/").reverse().join("-") : null;
  let rs2 = endDate ? endDate.split("/").reverse().join("-") : null;
  // return dateDiff(rs1,rs2);
  return dateDiff(rs1, rs2);
};

export const maximumDate = () => {
  const date = new Date();
  return {
    year: date.getFullYear(),
    month: date.getMonth() + 1,
    day: date.getDate() + 1,
  };
};

export const dateDiff = (startingDate, endingDate) => {
  let startDate = new Date(new Date(startingDate).toISOString().substr(0, 10));
  if (!endingDate) {
    endingDate = new Date().toISOString().substr(0, 10);
  }
  let endDate = new Date(endingDate);
  if (startDate > endDate) {
    let swap = startDate;
    startDate = endDate;
    endDate = swap;
  }
  let startYear = startDate.getFullYear();
  let february =
    (startYear % 4 === 0 && startYear % 100 !== 0) || startYear % 400 === 0
      ? 29
      : 28;
  let daysInMonth = [31, february, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

  let yearDiff = endDate.getFullYear() - startYear;
  let monthDiff = endDate.getMonth() - startDate.getMonth();
  if (monthDiff < 0) {
    yearDiff--;
    monthDiff += 12;
  }
  let dayDiff = endDate.getDate() - startDate.getDate();
  if (dayDiff < 0) {
    if (monthDiff > 0) {
      monthDiff--;
    } else {
      yearDiff--;
      monthDiff = 11;
    }
    dayDiff += daysInMonth[startDate.getMonth()];
  }
  let rsMonthDiff = yearDiff * 12 + monthDiff;
  // return rsMonthDiff + ' tháng ' + dayDiff + ' ngày ';
  return {
    month_diff: rsMonthDiff,
    day_diff: dayDiff,
    year_diff: yearDiff,
  };
};

export const numberWithCommas = (x) => {
  if (x) {
    return x.toLocaleString();
  }
};

// export const numberWithDots = (x) => {
//     if(x){
//         return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
//     }
// }

export const numberFormatCommas = (strNum) => {
  let currentVal = 0, vl = strNum ? strNum.toString() : null;
  let x = Number(vl ? vl.replace(/,/g, "") : null);
  let numFormat = new Intl.NumberFormat("en-US", { minimumFractionDigits: 0 });
  currentVal = numFormat.format(x);
  return currentVal;
};

export const numberFormatDot = (vl) => {
  let currentVal = 0;
  let numFormat = new Intl.NumberFormat("de-DE", { minimumFractionDigits: 0 });
  currentVal = numFormat.format(vl?.toString());
  return currentVal;
};

// export const formatNumber = inputNumber => {
//     let formetedNumber=(Number(inputNumber)).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
//     let splitArray=formetedNumber.split('.');
//     if(splitArray.length>1){
//         formetedNumber=splitArray[0];
//     }
//     return(formetedNumber);
// };

export const base64ImageToBlob = (str) => {
  // extract content type and base64 payload from original string
  let pos = str.indexOf(";base64,");
  let type = str.substring(5, pos);
  let b64 = str.substr(pos + 8);

  // decode base64
  let imageContent = atob(b64);

  // create an ArrayBuffer and a view (as unsigned 8-bit)
  let buffer = new ArrayBuffer(imageContent.length);
  let view = new Uint8Array(buffer);

  // fill the view, using the decoded base64
  for (let n = 0; n < imageContent.length; n++) {
    view[n] = imageContent.charCodeAt(n);
  }
  // convert ArrayBuffer to Blob
  return new Blob([buffer], { type: type });
};

export const openBase64Pdf = (base64Pdf) => {
  var blob = base64toBlob(base64Pdf);
  if (window.navigator && window.navigator.msSaveOrOpenBlob) {
    window.navigator.msSaveOrOpenBlob(blob, "pdfBase64.pdf");
  } else {
    const blobUrl = URL.createObjectURL(blob);
    window.open(blobUrl);
  }
};

const base64toBlob = (base64Data) => {
  const sliceSize = 1024;
  const byteCharacters = atob(base64Data);
  const bytesLength = byteCharacters.length;
  const slicesCount = Math.ceil(bytesLength / sliceSize);
  const byteArrays = new Array(slicesCount);

  for (let sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
    const begin = sliceIndex * sliceSize;
    const end = Math.min(begin + sliceSize, bytesLength);

    const bytes = new Array(end - begin);
    for (let offset = begin, i = 0; offset < end; ++i, ++offset) {
      bytes[i] = byteCharacters[offset].charCodeAt(0);
    }
    byteArrays[sliceIndex] = new Uint8Array(bytes);
  }
  return new Blob(byteArrays, { type: "application/pdf" });
};

export const getBase64pdf = (imgUrl, callback) => {
  let rs = pdf2base64(imgUrl);
  return rs;
  // console.log('rs', rs);
  //  let dataURL = "data:application/pdf;base64," + rs;
  // callback(dataURL);

  // pdf2base64(imgUrl).then((ts) => {
  //   let dataURL = "data:application/pdf;base64," + ts;
  //   callback(dataURL);
  // });
};

export const getBase64Image = (imgUrl, callback) => {
  var img = new Image();
  img.onload = function () {
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);
    var dataURL = canvas.toDataURL("image/png");
    callback(dataURL);
  };

  // set attributes and src
  img.setAttribute("crossOrigin", "anonymous"); //
  img.src = imgUrl;
};

export const ModalCK = (props) => {
  return (
    <Modal
      show={props.showModalCK}
      onHide={() => props.setShowModalCK(false)}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title className="header-sms">Thông báo</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <label>
          Quý khách vui lòng Chuyển khoản với nội dung "
          <label className="sms-ck">{props.valueSMS}</label>" cho tài khoản:
        </label>
        <p>- Chủ tài khoản: CÔNG TY TNHH BẢO HIỂM HD</p>
        <p>- STK: 168704079888888</p>
        <p>- Ngân hàng HD Bank chi nhánh Nguyễn Đình Chiểu</p>
        <div
          className="btn-save-dkbs"
          onClick={() => props.setShowModalCK(false)}
        >
          Đóng
        </div>
      </Modal.Body>
    </Modal>
  );
};

export const showDialogDis = (message) => {
  confirmAlert({
    customUI: ({ onClose }) => {
      return (
        <div className="custom-content-dialog">
          <p>{message}</p>
          <div className="btn-confirm-dialog" onClick={onClose}>
            Đồng ý
          </div>
        </div>
      );
    },
  });
};

export const isEmpty = (obj) => {
  // check empty object
  for (let key in obj) {
    if (obj.hasOwnProperty(key)) return false;
  }
  return true;
};

export const getListYear = () => {
  let currentYear = new Date().getFullYear(),
    years = [];
  for (currentYear; currentYear >= 2000; --currentYear) {
    years.push({ value: currentYear.toString(), label: "Năm " + currentYear });
  }
  return years;
};

export const getListYearHouseDoneYears = (year_diff) => {
  let currentYear = new Date().getFullYear(),
    years = [];
  let i = currentYear - year_diff;
  for (i; i <= currentYear; i++) {
    years.push({ value: i.toString(), label: "Năm " + i });
  }
  return years.reverse();
};

export const getLabelAddress = (label) => {

  if (!label || label == "") return '';

  let provideVl = label ? label.split("-") : "";
  let provideFormat = provideVl[2] + "," + provideVl[1] + "," + provideVl[0];
  return provideFormat;
};

export const getScanQR = async (isNumPlate) => {
  // setIsLoadgenQr(true);
  const params = {
    Action: {
      ParentCode: "HDI_WEB",
      UserName: "HDI_WEB",
      Secret: "B72088C7067CJF9FE0551F6E21B40329",
      ActionCode: "API73BHAIB",
    },
    Data: {
      CHANNEL: "SELLING",
      USERNAME: "ADMIN",
      LANG: "BH_S",
      CATEGORY: "XE",
      PRODUCT_CODE: "XCG_VCX_NEW",
      TYPE_RECORD: isNumPlate ? "NONE_NUMBER_PLATE" : "NUMBER_PLATE",
      OBJ_DATA: {
        a: "tuvm",
      },
    },
    Signature:
      "034F0DA18BCBDDDB216211594505FB0D8877BB41BE73F6285AECFF7939FA051A",
  };
  try {
    let rs = await api.post(`/OpenApi/hdi/v1/record/qr`, params);
    return rs;
  } catch (e) {
    console.log(e);
  }
};

export const showModalScanQR = (image) => {
  confirmAlert({
    customUI: ({ onClose }) => {
      return (
        <div className="custom-content-dialog">
          <img
            src={"data:image/png;base64," + image}
            alt="Scan Qr"
            style={{ width: 200, height: 200 }}
          />
          <label className="genqr-content">
            Vui lòng quét mã QR trên điện thoại để bổ sung thông tin giám định
            bằng cách chụp và quay video xe
          </label>
          <div
            id="btn-scan-qr-vcx"
            className="btn-confirm-dialog"
            onClick={onClose}
          >
            Đóng
          </div>
        </div>
      );
    },
  });
};

export const getFileSocket = (obj_file, trans_id) => {
  let arrImg = [];
  Object.keys(obj_file).map((vl, i) => {
    let rs = null,
      rs1 = null;
    if (vl === "other") {
      let listOther = obj_file["other"];
      Object.keys(listOther).map((vl, i) => {
        rs = {
          FILE_NAME: listOther[vl]?.file,
          FILE_ID: listOther[vl]?.file,
          FILE_TYPE: "KHAC",
          FILE_FORMAT: listOther[vl]?.format,
          TRANS_ID: trans_id,
        };
        arrImg.push(rs);
      });
    } else {
      rs1 = {
        FILE_NAME: obj_file[vl]?.file,
        FILE_ID: obj_file[vl]?.file,
        FILE_TYPE: vl,
        FILE_FORMAT: obj_file[vl]?.format,
        TRANS_ID: trans_id,
      };
      arrImg.push(rs1);
    }
  });
  return arrImg;
};

export const caculDateLeapYear = (startDate, endDate) => {
  try {

    let totalDay = endDate.diff(startDate, "days");
    const diffYear = endDate.diff(startDate, "years");
    // console.log('diff year', diffYear, totalDay);

    const _start = {
      day: moment(startDate, "DD/MM/YYYY").date(),
      month: moment(startDate, "DD/MM/YYYY").month(),
      year: moment(startDate, "DD/MM/YYYY").year(),
    };
    const _end = {
      day: moment(endDate, "DD/MM/YYYY").date(),
      month: moment(endDate, "DD/MM/YYYY").month(),
      year: moment(endDate, "DD/MM/YYYY").year(),
    };

    if (_end.day === _start.day && _end.month === _start.month) {
      return 365 * diffYear;
    }
    /*
    https://hdinsurance.atlassian.net/browse/MR-67
    https://hdinsurance.atlassian.net/browse/MR-247
    /  Trường hợp đặc biệt:
    09 / 10 / 2023 - 
    08 / 10 / 2024: tính phí ngày của 365 ngày

    09 / 10 / 2023 - 
    10 / 10 / 2024: tính phí ngày của 367 ngày
    */
    const _dd = moment(`${_end.year}-${_start.month + 1}-${_start.day}`)
    const dayDiffSameYear = endDate.diff(_dd, 'days');
    // console.log('diffSameYear', diffSameYear, 'date', _dd)
    for (let y = _start.year; y <= _end.year; y++) {
      const _date = moment(`${y}-02-29`);
      if (_date.isLeapYear() && _date.isBetween(startDate, endDate) && Math.abs(dayDiffSameYear) !== 1) {
        totalDay -= 1;
      }
    }

    return totalDay;

  } catch (error) {
    console.log(error);
    return 0;
  }
};

const leap_year_range = (st_year, end_year) => {
  let year_range = [];
  for (let i = st_year; i <= end_year; i++) {
    year_range.push(i);
  }
  let new_array = [];

  year_range.forEach(function (year) {
    if (checkLeapYear(year)) new_array.push(year);
  });
  return new_array;
};

const checkLeapYear = (year) => {
  if (
    (year % 4 === 0 && year % 100 !== 0) ||
    (year % 100 === 0 && year % 400 === 0)
  ) {
    return year;
  } else {
    return false;
  }
};

const getYear = (day) => {
  return moment(day, "DD/MM/YYYY").year();
};

export const toTitleCase = (str) => {
  return str.replace(/(?:^|\s)\S/g, (match) => {
    return match.toUpperCase();
  });
};

export const convertRelation = (define) => {
  return define
    .filter(
      (e) => e.DEFINE_CODE === "MOI_QUAN_HE"
      // && e.TYPE_CODE !== "BAN_THAN"
    )
    .map((e) => {
      const list = define
        .filter((item) => item.DEFINE_CODE === e.TYPE_CODE)
        .map((item) => {
          return {
            title: item.TYPE_NAME,
            value: item.DEFINE_CODE,
            gender: item.TYPE_CODE,
          };
        });
      if (e.TYPE_CODE == "BAN_THAN")
        return {
          title: e.TYPE_NAME,
          type_code: e.TYPE_CODE,
          value: "BAN_THAN",
          list: list.length !== 0 ? list : null,
        };
      return {
        title: e.TYPE_NAME,
        type_code: e.TYPE_CODE,
        list: list.length !== 0 ? list : null,
      };
    });
};

export const isEmptyObj = (obj) => {
  return obj && Object.keys(obj).length === 0 && obj.constructor === Object;
};

function getWindowDimensions() {
  const { innerWidth: width, innerHeight: height } = window;
  return {
    width,
    height
  };
}

export function useWindowDimensions() {
  const [windowDimensions, setWindowDimensions] = useState(getWindowDimensions());

  useEffect(() => {
    function handleResize() {
      setWindowDimensions(getWindowDimensions());
    }

    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, []);

  return windowDimensions;
}