import axios from 'axios';

var self = null
export default class Networking {
    constructor() {
        this.domain = process.env.REACT_APP_DM4;
        this.domainAdd = process.env.REACT_APP_DM3;
        this.post = this.post.bind(this)
        this.patch = this.patch.bind(this)
        this.get = this.get.bind(this)
        this.handleError = this.handleError.bind(this)
        this.getHeader = this.getHeader.bind(this)
        self = this
    }

    getHeader(){
        return {
            headers: {
                'Access-Control-Allow-Headers': 'Content-Type',
                'Access-Control-Allow-Methods': 'POST',
                "Content-Type": "application/json",
                'Access-Control-Allow-Origin': '*',  }
        }
    }
    getParams(action, data, signature ){
        return {
            "Device": {
                "DeviceId": "",
                "DeviceCode": "",
                "DeviceName": null,
                "IpPrivate": "123",
                "IpPublic": "",
                "PortPublic": null,
                "X": "",
                "Y": "",
                "Province": "",
                "District": "",
                "Wards": "",
                "Address": "",
                "Environment": "",
                "Browser": "",
                "DeviceEnvironment": "WEB"
            },
            "Action": action,
            "Data": data,
            "Signature": signature ? signature : "034F0DA18BCBDDDB216211594505FB0D8877BB41BE73F6285AECFF7939FA051A",
            "Process": null
        }
    }
    post(url, params){
        return new Promise((resolve, rejected)=>{
            axios.post(this.domain+url, this.getParams(params.Action, params.Data, params.Signature), this.getHeader()).then(function (response) {
                return resolve(response.data)
            })
                .catch(function (error) {
                    return rejected(error)
                })
        })
    }

    patch(url, params){
        return new Promise((resolve, rejected)=>{
            axios.patch(this.domain+url, params).then(function (response) {
                return resolve(response.data)
            })
                .catch(function (error) {
                    console.log(error)
                    self.handleError(error, rejected)
                })
        })
    }

    get(url){
        return new Promise((resolve, rejected)=>{
            axios.get(this.domainAdd+url).then(function (response) {
                return resolve(response.data)
            })
                .catch(function (error) {
                    self.handleError(error, rejected)
                })
        })
    }
    delete(url){
        return new Promise((resolve, rejected)=>{
            axios.delete(this.domain+url).then(function (response) {
                return resolve(response.data)
            }).catch(function (error) {
                self.handleError(error, rejected)
            })
        })
    }

    handleError(error, callback){
        if (error.response) {
            if(error.response.status==401){

            }else if(error.response.status==403){

            }else{
                console.log("ERROR---->",error)
                callback(error, null)
            }
        }else{
            console.log("ERROR---->",error)
            callback(error, null)
        }
    }



}
