import "./App.css";
import "./page.css";
import React, { useEffect, useState } from "react";
import CreateContract from "./screen/an-tam-tin-dung/main";


// import DetailContract from  "./screen/detail-contracts/main";

import CreateCar from "./screen/car-insur/main";

import CreateCarV2 from "./screen/car-insur-v2/main"

import HealthInsur from "./screen/health-insur/main";

import HouseInsur from "./screen/house-insur/main";

import Stream from "./screen/stream-media/index";


const paramss = {
  channel: "SDK_CAR",//  'SDK_HOUSE_SELL', //SDK_SELLING //SDK_HOUSE_SELL //SDK_CAR
  action: "ADD",
  // category: "CN.04", //Tai nan: CN.03 //Suc Khoe: CN.02 //NHA365 : TS10
  category: "XE",
  contract_code: "CE25974AF2E558F5E0530100007FA687",
  detail_code: "CE25974AF2E658F5E0530100007FA687",
  // org_code: "HDBANK_VN",
  org_code: "HDI",
  partner: "SELLING",
  user_name: "TUVM", //HDB043_CD
  // product_code: "ATTD_HDB", //Tai nan: TAINAN365 //Suc Khoe: SUCKHOE365 //NHA365 : NHA365
  // product_code: "ATTD_HDB",
  product_code: "XCG_TNDSBB_NEW", //XCG_TNDSBB_NEW XCG_VCX_NEW
  trans_id: "sftgyjint",
  user_permission: '{"APPROVE":"1","CREATE_ORDER":"1","INSPECTION":"1"}',
};


function App() {
  const [params, setParms] = useState({});

  window.openForm = (ac) => {
    console.log("Nupakachi", ac);
    setParms(ac);
  };

  const displayView = () => {
    if (params) {
      switch (params.category) {
        case "CN.04":
          return <CreateContract sdk_params={params} />;
        case "XE":
          // return <CreateCar sdk_params={params} />;
          return <CreateCarV2 sdk_params={params} />;
        case "CN.03":
          return <HealthInsur sdk_params={params} />;
        case "CN.02":
          return <HealthInsur sdk_params={params} />;
        case "TS10":
          return <HouseInsur sdk_params={params} />;
        case "MEDIA":
          return <Stream sdk_params={params} />;
        default:
          break;
      }
    }
  };

  return <React.Fragment>
    {displayView()}
  </React.Fragment>;

  //Unit test nha365 HDBANK

  //   const _p = {
  //     action: " ADD", // ADD | EDIT
  //     partner: "LO_HDB", // default
  //     org_code: "HDBANK_VN", // default
  //     category: "TS10", // HD: "CN.04", //Tai nan: CN.03 //Suc Khoe: CN.02 //NHA365 : TS10 // Xe: XE
  //     channel: "SDK_HOUSE_SELL", //  'SDK_HOUSE_SELL', //SDK_SELLING //SDK_HOUSE_SELL //SDK_CAR
  //     product_code: "NHA365", //XCG_TNDSBB_NEW XCG_VCX_NEW  NHA365
  //     user_name: "TEST-01",
  //     user_permission: { "APPROVE": "1", "CREATE_ORDER": "1", "INSPECTION": "1" },
  //     contract_code: "",
  //     detail_code: "",
  //   }

  //   const _p2 = {
  //     "partner": "LO_HDB",
  //     "action": "EDIT",
  //     "category": "TS10",
  //     "contract_code": "E163CD89AD3532DEE0530100007F0143",
  //     "detail_code": "E163CD89AD3632DEE0530100007F0143",
  //     "org_code": "HDBANK_VN",
  //     "user_name": "HDBANK_THU_THU",
  //     "user_permission": "{&quot;APPROVE&quot;:0,&quot;CREATE_ORDER&quot;:1,&quot;INSPECTION&quot;:0}",
  //     "product_code": "NHA365",
  //     "channel": "SDK_HOUSE_SELL"
  // }

  //   const _p3 = {
  //     "partner": "LO_HDB",
  //     "action": "EDIT",
  //     "category": "TS10",
  //     "contract_code": "E163FFA50EC93D9EE0530100007F5090",
  //     "detail_code": "E163FFA50ECA3D9EE0530100007F5090",
  //     "org_code": "HDBANK_VN",
  //     "user_name": "HDBANK_THU_THU",
  //     "user_permission": "{&quot;APPROVE&quot;:0,&quot;CREATE_ORDER&quot;:1,&quot;INSPECTION&quot;:0}",
  //     "product_code": "NHA365",
  //     "channel": "SDK_HOUSE_SELL"
  // }
  //   return <React.Fragment>
  //     <HouseInsur sdk_params={_p3} />
  //   </React.Fragment>;

}
export default App